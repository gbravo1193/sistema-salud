<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="js/jquery-1.12.0.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/Style.css">
        <title>Menu Principal</title>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet"></link>

    </head>
    <body>
          <jsp:include page="barraNavegacion.jsp"></jsp:include>

    <div class="container">
	<table class="table">
		<tr>
			<td>
				<h1>Menú Principal</h1>
			</td>
		
			
		</tr>
	</table>
	
	<button class="accordion"><h3>Gestión de Personas</h3></button>
	<div class="panel">
		<table class="table table-responsive">	
			<tr>
				<td>
		  			<a href="CrearPersona.jsp" class="btn btn-default">Crear Persona</a>
  				</td>
  				
				<td>
					<a href="BuscarPersona.jsp" class="btn btn-default">Buscar Persona</a>
  				</td>
  			
  			</tr>
	  	</table>
	</div>
	
	<button class="accordion"><h3>Gestión de Pacientes</h3></button>
	<div class="panel">
		<table class="table table-responsive">	
			<tr>
				<td>
		  			<a href="CreadorPaciente.jsp" class="btn btn-default">Crear Paciente</a>
  				</td>
  				
				<td>
					<a href="CrearPaciente.jsp" class="btn btn-default">Buscar Paciente</a>
  				</td>
  			
  			</tr>
	  	</table>
	</div>
 	
	</div>

	<script>
	var acc = document.getElementsByClassName("accordion");
	var i;

	for (i = 0; i < acc.length; i++) {
	    acc[i].onclick = function(){
	        this.classList.toggle("active");
	        this.nextElementSibling.classList.toggle("show");
	  }
	}
	</script>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->

<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script  type="text/javascript" src="ValidacionFrontend.js" ></script>

</div>
    </body>
</html>