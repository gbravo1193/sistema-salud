<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" src="js/jquery-1.12.0.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/Style.css">
<title>Editar datos de Persona</title>
</head>
<body>
  <jsp:include page="barraNavegacion.jsp"></jsp:include>

<% 
	int id = Integer.parseInt(request.getParameter("id")); 
  	String rut= request.getParameter("rut");
  	String nombres= request.getParameter("nombres");
  	String apellido_paterno= request.getParameter("apellido_paterno");
  	String apellido_materno= request.getParameter("apellido_materno");
  	String email= request.getParameter("email");
  	String fecha_nacimiento= request.getParameter("fecha_nacimiento");
  	String identificadorfacebook= request.getParameter("idFacebook");
  	int activo= Integer.parseInt(request.getParameter("activo"));
  	String password= request.getParameter("password");
  	int actVal=1;
  	String actText="", actT="";
%>
<div class="container">
		<h1>Editar Persona</h1>
		<div class="col-md-5">
			<form action="EditarPersona" method="post">
				<input type="hidden" value="<%= id %>" name="id"> 
				<div class="form-group">
					<label>RUT: </label> <input type="text" id="rut" name="rut" class="form-control"  value="<%out.print(rut);%>" pattern=".{10}" required>
				</div>

				<div class="form-group">
					<label>Nombres: </label> <input type="text" id="nombres" name="nombres" class="form-control" value="<%out.print(nombres); %>" pattern=".{1,50}" required>
				</div>

				<div class="form-group">
					<label>Apellido Paterno: </label> <input type="text" id="apellidoPaterno" name="apellidoPaterno" class="form-control" value="<%out.print(apellido_paterno); %>" pattern=".{1,50}" required>
				</div>

				<div class="form-group">
					<label>Apellido Materno: </label> <input type="text" id="apellidoMaterno" name="apellidoMaterno" class="form-control" value="<%out.print(apellido_materno); %>" pattern=".{1,50}" required>
				</div>

				<div class="form-group">
					<label>E-mail: </label> <input type="text" id="email" name="email" class="form-control" value="<%out.print(email); %>" pattern=".{1,75}" required>
				</div>

				<div class="form-group">
					<label>Fecha Nacimiento: </label>
					<input type="text" name="fecha_nacimiento" class="form-control" value="<%out.print(fecha_nacimiento); %>" required>
				</div>

				<div class="form-group">
					<label>Identificador Facebook: </label> 
					<input type="text" id="idFacebook" name="idFacebook" class="form-control" value="<%out.print(identificadorfacebook); %>" pattern=".{1,150}" required>
				</div>
				
				<%
				if(activo==1){
					actVal=2;
					actText="Si";
					actT="No";
				}
				else if(activo==2){
					actVal=1;
					actText="No";
					actT="Si";
				}
				%>
				
				<div class="form-group">
					<label>Contraseņa: </label> 
					<input type="password" id="password" name="password" class="form-control" value="<%out.print(password); %>" pattern=".{1,30}" required>
				</div>
				
				<div class="form-group">
					<label>Activo: </label> 
					<select name="activo" class="form-control">
						<option value="<%out.print(activo);%>"><%out.print(actText);%></option>
						<option value="<%out.print(actVal);%>"><%out.print(actT); %></option>
					</select>
				</div>
				
				<div class="form-group">
				<input type="submit" value="Ingresar" class="btn btn-default">
				<a class="btn btn-default" href="Index.jsp">Cancelar</a>
				</div>
			</form>
		</div>
	</div>
</body>
</html>