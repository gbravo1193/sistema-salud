<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Listar Recetas</title>

<link rel="stylesheet" href="css/estiloVistas.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">


</head>
<body>
  <jsp:include page="barraNavegacion.jsp"></jsp:include>

<% 	request.removeAttribute("lista"); %>


<div class="container">
<center><h1><span class="label label-info">Listar Recetas</span></h1></center><br>


<div id="formListReceta">

<form id="formulario" action="ServletListarReceta" method="post" class="form-horizontal" role="form" novalidate autocomplete="off">
	<div class="form-group">
	
	<label for="pacienteid">Paciente ID</label>
	
	
	<input type="text" name="pacienteid" id="pacienteid" class="form-control" onkeypress="return validarNumero(event)" placeholder="Ingrese paciente ID">
	
	</div>
	
	<center><div class="form-group">
	<button type="submit" class="btn btn-default">Listar</button>
    <button type="button" id="buttonClean" class="btn btn-default">Limpiar</button>
	</div></center>
	
</form>

</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->

<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script  type="text/javascript" src="ValidacionFrontend.js" ></script>


</div>

</body>
</html>
