<!--  Jquery  -->
<script type="text/javascript" src="js/lib/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/dist/jquery.validate.js"></script>
<script type="text/javascript" src="js/dist/additional-methods.js"></script>
<script type="text/javascript" src="js/dist/localization/messages_es.js"></script>


 <script>
 $(document).ready(function() {
	 

$('#formulario').validate(
		{
    rules: {
      pacienteid: {
        required: true,
        digits: true
      },
      rceid:{
        required: true,
        digits: true
      },
     detalle: {
        required: true
        
    },
    alergiaid:{
    	required:true,
    	digits:true
    },
    recetaid:{
    	required: true,
    	digits:true
    	
    }
    
    }
    });
});
 
</script>