function encodeImage(){
    
    var imagen = document.getElementById("foto").files;
    var newImage;
    
    if (imagen.length > 0){
        var fileToLoad = imagen[0];
        var fileReader = new FileReader();// para leer el contenido de fileToLoad

        //onloadstart, onloadprogress, onload, onloadend
        fileReader.onload = function(fileLoadedEvent) {
            var srcData = fileLoadedEvent.target.result; // <--- data:image base64

            newImage = document.createElement('img'); //elemento img
            newImage.src = srcData;
            
            document.getElementById("imgContainer").innerHTML = newImage.outerHTML;
            //document.getElementById("imgTest").innerHTML);
            document.getElementById("textImg").innerHTML = newImage.src;
        }
        fileReader.readAsDataURL(fileToLoad);
        return newImage.src;
    }
}

function decodeImage(){
    document.getElementById("decode").innerHTML = window.atob(encodeImage());
}