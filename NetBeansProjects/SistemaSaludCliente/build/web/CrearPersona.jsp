<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript" src="js/jquery-1.12.0.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/Style.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet"></link>
    <link href="//oss.maxcdn.com/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" rel="stylesheet"></link>
    
    <script src="//oss.maxcdn.com/jquery/1.11.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<title>Crear Persona</title>
</head>
<body>
  <jsp:include page="barraNavegacion.jsp"></jsp:include>

	<div class="formulario container">
		<h1 class="tituloForm"><span class="label label-info">Crear Persona</span></h1>
		<div class="col-md-5">
			<form action="AgregarPersona" id="AgregarPersona" method="post">

				<div class="form-group">
					<label>RUT: </label> <input type="text" id="rut" name="rut" class="form-control" placeholder="12345678-9" pattern=".{10}" required>
				</div>

				<div class="form-group">
					<label>Nombres: </label> <input type="text" id="nombres" onkeypress="return validarLetra(event)"  name="nombres" class="form-control" pattern=".{1,50}" required >
				</div>

				<div class="form-group">
					<label>Apellido Paterno: </label> <input type="text" id="apellidoPaterno" onkeypress="return validarLetra(event)" name="apellidoPaterno" class="form-control" pattern=".{1,50}" required>
				</div>

				<div class="form-group">
					<label>Apellido Materno: </label> <input type="text" id="apellidoMaterno" onkeypress="return validarLetra(event)" name="apellidoMaterno" class="form-control" pattern=".{1,50}" required>
				</div>

				<div class="form-group">
					<label>E-mail: </label> <input type="email" id="email" name="email" class="form-control" pattern=".{1,75}" required>
				</div>

				<div class="form-group">
					<label>Fecha Nacimiento: </label>
					<input type="text" name="fecha_nacimiento" placeholder="yyy-mmm-ddd"   class="form-control" pattern=".{1, 10}" required>
				</div>

				<div class="form-group">
					<label>Identificador Facebook: </label> 
					<input type="text" id="idFacebook" name="idFacebook" class="form-control" pattern=".{1,150}" required>
				</div>
				
				<div class="form-group">
					<label>Contraseña: </label> 
					<input type="password" id="password" name="password" class="form-control" pattern=".{1,30}" required>
				</div>

				<div class="form-group">
					<label>Activo: </label> 
					<select name="activo" class="form-control">
						<option value="1">Si</option>
						<option value="0">No</option>
					</select>
				</div>
				
				<div class="btnForm form-group">
				<input type="submit" value="Ingresar" class="btn btn-default">
				<a class="btn btn-default" href="Index.jsp">Cancelar</a>
				</div>
			</form>
			
			<script type="text/javascript" src="js/validador.js"></script>
			<script src="//oss.maxcdn.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>
			<script  type="text/javascript" src="ValidacionFrontend.js" ></script>
		</div>
	</div>
</body>
</html>