<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="i"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript" src="js/jquery-1.12.0.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/Style.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Agregar Profesional</title>
</head>
<body>
      <jsp:include page="barraNavegacion.jsp"></jsp:include>

	<%
	String idPersona= (request.getParameter("idPersona"));
	String nombres= request.getParameter("nombres");
	String apellido_paterno= request.getParameter("apellido_paterno");
	%>
	<div class="container">
		<h1>Crear Persona</h1>
		<div class="col-md-5">
		
			<form action="CrearProfesional" method="post">
				<div class="form-group">
					<label>ID Persona:</label>
					<select name="idPersona" class="form-control">
						<option value="<%out.print(idPersona);%>"><%out.print(idPersona);%></option>
					</select>
				</div>
				
				<div class="form-group">
					<label>Nombre:</label>
					<input type="text" name="nombrePersona" class="form-control" value="<%out.print(nombres+" "+apellido_paterno);%>" disabled>
				</div>
				
				<div class="form-group">
					<label>Activo: </label> 
					<select name="activo" class="form-control">
						<option value="1">Si</option>
						<option value="0">No</option>
					</select>
				</div>
				
				<input type="submit" value="Ingresar" class="btn btn-primary">
				<a class="btn btn-primary" href="Index.jsp">Cancelar</a>
			</form>
		</div>
	</div>
</body>
</html>