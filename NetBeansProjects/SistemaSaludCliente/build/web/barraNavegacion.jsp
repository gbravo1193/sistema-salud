<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.jsp">HIS</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="index.jsp">Inicio <span class="sr-only">(current)</span></a></li>
                <li><a href="index.jsp">Gestion de Usuarios</a></li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reserva de Horas <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="agregarReservaMedica.jsp">Agregar Reserva</a></li>
                        <li><a href="buscarReservasPorProfesional.jsp">Listar Reservas</a></li>
                    </ul>
                </li>   
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Recetas <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="formAgregarReceta.jsp">Agregar Recetas</a></li>
                        <li><a href="formListarRecetas.jsp">Listar Recetas</a></li>
                        <li><a href="formEliminarReceta.jsp">Eliminar Recetas</a></li>
                    </ul>
                </li>                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Alergias <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="agregarAlergia.jsp">Agregar Alergia</a></li>
                        <li><a href="formListarAlergia.jsp">Listar Alergias</a></li>
                        <li><a href="eliminarAlergia.jsp">Eliminar Alergias</a></li>
                    </ul>
                </li>        

            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li><a class="btn btn-default" href="login.jsp">Cerrar Sesi�n</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>