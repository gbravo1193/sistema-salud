<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Agregar Receta</title>
<link rel="stylesheet" href="css/estiloVistas.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">

</head>
<body>

  <jsp:include page="barraNavegacion.jsp"></jsp:include>
<div class="container">
<center><h1><span class="label label-info"> Nueva Receta</span></h1></center><br>

<div id="formAddReceta">

<form id="formulario" action="ServletAgregarReceta" method="post" class="form-horizontal" role="form" autocomplete="off">
	<div class="form-group">
	
	<label for="pacienteid">Paciente ID</label>
	
	
	<input type="text" name="pacienteid" id="pacienteid" class="form-control required" onkeypress="return validarNumero(event)" placeholder="Ingrese ID paciente.">
	
	</div>
	<div class="form-group">
	
	<label for="rceid">RCE ID</label>
	
	
	<input type="text" name="rceid" id="rceid" class="form-control required" onkeypress="return validarNumero(event)" placeholder="Ingrese RCE ID.">
	
	</div>
	<div class="form-group">
	
	<label for="detalle">Detalle Alergia</label>
	
	
	<textarea row="6" col="50"  name="detalle" id="detalle" class="form-control required" placeholder="Ingrese detalle receta." maxlength="150"></textarea>
	
	</div>
	
	<center><div class="form-group">
	<button type="submit" class="btn btn-default">Agregar</button>
    <button type="button" id="buttonClean" class="btn btn-default">Limpiar</button>
	</div></center>
	
</form>

</div>

</div>

</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->

<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script  type="text/javascript" src="ValidacionFrontend.js" ></script>




</body>
</html>
