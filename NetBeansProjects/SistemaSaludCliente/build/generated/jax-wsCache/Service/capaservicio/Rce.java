
package capaservicio;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para rce complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="rce"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="anamnesis" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="box" type="{http://capaServicio/}box" minOccurs="0"/&gt;
 *         &lt;element name="cerrado" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="destino" type="{http://capaServicio/}destino" minOccurs="0"/&gt;
 *         &lt;element name="fechacierre" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="indicaciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="observaciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="persona_id_realiza_examen" type="{http://capaServicio/}persona" minOccurs="0"/&gt;
 *         &lt;element name="procedencia" type="{http://capaServicio/}procedencia" minOccurs="0"/&gt;
 *         &lt;element name="reserva" type="{http://capaServicio/}reserva" minOccurs="0"/&gt;
 *         &lt;element name="tipocierre" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rce", propOrder = {
    "anamnesis",
    "box",
    "cerrado",
    "destino",
    "fechacierre",
    "id",
    "indicaciones",
    "observaciones",
    "personaIdRealizaExamen",
    "procedencia",
    "reserva",
    "tipocierre"
})
public class Rce {

    protected String anamnesis;
    protected Box box;
    protected int cerrado;
    protected Destino destino;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechacierre;
    protected int id;
    protected String indicaciones;
    protected String observaciones;
    @XmlElement(name = "persona_id_realiza_examen")
    protected Persona personaIdRealizaExamen;
    protected Procedencia procedencia;
    protected Reserva reserva;
    protected Integer tipocierre;

    /**
     * Obtiene el valor de la propiedad anamnesis.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnamnesis() {
        return anamnesis;
    }

    /**
     * Define el valor de la propiedad anamnesis.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnamnesis(String value) {
        this.anamnesis = value;
    }

    /**
     * Obtiene el valor de la propiedad box.
     * 
     * @return
     *     possible object is
     *     {@link Box }
     *     
     */
    public Box getBox() {
        return box;
    }

    /**
     * Define el valor de la propiedad box.
     * 
     * @param value
     *     allowed object is
     *     {@link Box }
     *     
     */
    public void setBox(Box value) {
        this.box = value;
    }

    /**
     * Obtiene el valor de la propiedad cerrado.
     * 
     */
    public int getCerrado() {
        return cerrado;
    }

    /**
     * Define el valor de la propiedad cerrado.
     * 
     */
    public void setCerrado(int value) {
        this.cerrado = value;
    }

    /**
     * Obtiene el valor de la propiedad destino.
     * 
     * @return
     *     possible object is
     *     {@link Destino }
     *     
     */
    public Destino getDestino() {
        return destino;
    }

    /**
     * Define el valor de la propiedad destino.
     * 
     * @param value
     *     allowed object is
     *     {@link Destino }
     *     
     */
    public void setDestino(Destino value) {
        this.destino = value;
    }

    /**
     * Obtiene el valor de la propiedad fechacierre.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechacierre() {
        return fechacierre;
    }

    /**
     * Define el valor de la propiedad fechacierre.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechacierre(XMLGregorianCalendar value) {
        this.fechacierre = value;
    }

    /**
     * Obtiene el valor de la propiedad id.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad indicaciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicaciones() {
        return indicaciones;
    }

    /**
     * Define el valor de la propiedad indicaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicaciones(String value) {
        this.indicaciones = value;
    }

    /**
     * Obtiene el valor de la propiedad observaciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservaciones() {
        return observaciones;
    }

    /**
     * Define el valor de la propiedad observaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservaciones(String value) {
        this.observaciones = value;
    }

    /**
     * Obtiene el valor de la propiedad personaIdRealizaExamen.
     * 
     * @return
     *     possible object is
     *     {@link Persona }
     *     
     */
    public Persona getPersonaIdRealizaExamen() {
        return personaIdRealizaExamen;
    }

    /**
     * Define el valor de la propiedad personaIdRealizaExamen.
     * 
     * @param value
     *     allowed object is
     *     {@link Persona }
     *     
     */
    public void setPersonaIdRealizaExamen(Persona value) {
        this.personaIdRealizaExamen = value;
    }

    /**
     * Obtiene el valor de la propiedad procedencia.
     * 
     * @return
     *     possible object is
     *     {@link Procedencia }
     *     
     */
    public Procedencia getProcedencia() {
        return procedencia;
    }

    /**
     * Define el valor de la propiedad procedencia.
     * 
     * @param value
     *     allowed object is
     *     {@link Procedencia }
     *     
     */
    public void setProcedencia(Procedencia value) {
        this.procedencia = value;
    }

    /**
     * Obtiene el valor de la propiedad reserva.
     * 
     * @return
     *     possible object is
     *     {@link Reserva }
     *     
     */
    public Reserva getReserva() {
        return reserva;
    }

    /**
     * Define el valor de la propiedad reserva.
     * 
     * @param value
     *     allowed object is
     *     {@link Reserva }
     *     
     */
    public void setReserva(Reserva value) {
        this.reserva = value;
    }

    /**
     * Obtiene el valor de la propiedad tipocierre.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTipocierre() {
        return tipocierre;
    }

    /**
     * Define el valor de la propiedad tipocierre.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTipocierre(Integer value) {
        this.tipocierre = value;
    }

}
