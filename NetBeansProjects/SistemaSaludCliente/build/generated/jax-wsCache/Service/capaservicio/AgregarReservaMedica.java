
package capaservicio;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para agregarReservaMedica complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="agregarReservaMedica"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="rutProfesional" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="rutAdministrador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="rutPaciente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="fechaReserva" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="motivoConsulta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "agregarReservaMedica", propOrder = {
    "rutProfesional",
    "rutAdministrador",
    "rutPaciente",
    "fechaReserva",
    "motivoConsulta"
})
public class AgregarReservaMedica {

    protected String rutProfesional;
    protected String rutAdministrador;
    protected String rutPaciente;
    protected String fechaReserva;
    protected String motivoConsulta;

    /**
     * Obtiene el valor de la propiedad rutProfesional.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutProfesional() {
        return rutProfesional;
    }

    /**
     * Define el valor de la propiedad rutProfesional.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutProfesional(String value) {
        this.rutProfesional = value;
    }

    /**
     * Obtiene el valor de la propiedad rutAdministrador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutAdministrador() {
        return rutAdministrador;
    }

    /**
     * Define el valor de la propiedad rutAdministrador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutAdministrador(String value) {
        this.rutAdministrador = value;
    }

    /**
     * Obtiene el valor de la propiedad rutPaciente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutPaciente() {
        return rutPaciente;
    }

    /**
     * Define el valor de la propiedad rutPaciente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutPaciente(String value) {
        this.rutPaciente = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaReserva.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaReserva() {
        return fechaReserva;
    }

    /**
     * Define el valor de la propiedad fechaReserva.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaReserva(String value) {
        this.fechaReserva = value;
    }

    /**
     * Obtiene el valor de la propiedad motivoConsulta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMotivoConsulta() {
        return motivoConsulta;
    }

    /**
     * Define el valor de la propiedad motivoConsulta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMotivoConsulta(String value) {
        this.motivoConsulta = value;
    }

}
