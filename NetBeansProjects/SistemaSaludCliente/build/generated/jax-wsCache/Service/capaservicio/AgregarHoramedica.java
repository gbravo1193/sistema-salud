
package capaservicio;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para agregarHoramedica complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="agregarHoramedica"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="rutProfesional" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="rutAdministrador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="horaInicio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "agregarHoramedica", propOrder = {
    "rutProfesional",
    "rutAdministrador",
    "horaInicio"
})
public class AgregarHoramedica {

    protected String rutProfesional;
    protected String rutAdministrador;
    protected String horaInicio;

    /**
     * Obtiene el valor de la propiedad rutProfesional.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutProfesional() {
        return rutProfesional;
    }

    /**
     * Define el valor de la propiedad rutProfesional.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutProfesional(String value) {
        this.rutProfesional = value;
    }

    /**
     * Obtiene el valor de la propiedad rutAdministrador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutAdministrador() {
        return rutAdministrador;
    }

    /**
     * Define el valor de la propiedad rutAdministrador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutAdministrador(String value) {
        this.rutAdministrador = value;
    }

    /**
     * Obtiene el valor de la propiedad horaInicio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHoraInicio() {
        return horaInicio;
    }

    /**
     * Define el valor de la propiedad horaInicio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHoraInicio(String value) {
        this.horaInicio = value;
    }

}
