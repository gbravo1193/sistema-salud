
package capaservicio;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para efectuarPago complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="efectuarPago"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="idMedioPago" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="idRce" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="monto" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="comprobante" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="estadoPago" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="subsidio" type="{http://www.w3.org/2001/XMLSchema}float"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "efectuarPago", propOrder = {
    "idMedioPago",
    "idRce",
    "monto",
    "comprobante",
    "estadoPago",
    "subsidio"
})
public class EfectuarPago {

    protected int idMedioPago;
    protected int idRce;
    protected double monto;
    protected double comprobante;
    protected int estadoPago;
    protected float subsidio;

    /**
     * Obtiene el valor de la propiedad idMedioPago.
     * 
     */
    public int getIdMedioPago() {
        return idMedioPago;
    }

    /**
     * Define el valor de la propiedad idMedioPago.
     * 
     */
    public void setIdMedioPago(int value) {
        this.idMedioPago = value;
    }

    /**
     * Obtiene el valor de la propiedad idRce.
     * 
     */
    public int getIdRce() {
        return idRce;
    }

    /**
     * Define el valor de la propiedad idRce.
     * 
     */
    public void setIdRce(int value) {
        this.idRce = value;
    }

    /**
     * Obtiene el valor de la propiedad monto.
     * 
     */
    public double getMonto() {
        return monto;
    }

    /**
     * Define el valor de la propiedad monto.
     * 
     */
    public void setMonto(double value) {
        this.monto = value;
    }

    /**
     * Obtiene el valor de la propiedad comprobante.
     * 
     */
    public double getComprobante() {
        return comprobante;
    }

    /**
     * Define el valor de la propiedad comprobante.
     * 
     */
    public void setComprobante(double value) {
        this.comprobante = value;
    }

    /**
     * Obtiene el valor de la propiedad estadoPago.
     * 
     */
    public int getEstadoPago() {
        return estadoPago;
    }

    /**
     * Define el valor de la propiedad estadoPago.
     * 
     */
    public void setEstadoPago(int value) {
        this.estadoPago = value;
    }

    /**
     * Obtiene el valor de la propiedad subsidio.
     * 
     */
    public float getSubsidio() {
        return subsidio;
    }

    /**
     * Define el valor de la propiedad subsidio.
     * 
     */
    public void setSubsidio(float value) {
        this.subsidio = value;
    }

}
