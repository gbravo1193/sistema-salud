
package capaservicio;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para agregarTipoPrevision complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="agregarTipoPrevision"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="nombrePrevision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "agregarTipoPrevision", propOrder = {
    "nombrePrevision"
})
public class AgregarTipoPrevision {

    protected String nombrePrevision;

    /**
     * Obtiene el valor de la propiedad nombrePrevision.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombrePrevision() {
        return nombrePrevision;
    }

    /**
     * Define el valor de la propiedad nombrePrevision.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombrePrevision(String value) {
        this.nombrePrevision = value;
    }

}
