
package capaservicio;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para reserva complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="reserva"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="fecha" type="{http://capaServicio/}timestamp" minOccurs="0"/&gt;
 *         &lt;element name="hora_medica" type="{http://capaServicio/}horamedica" minOccurs="0"/&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="motivoconsulta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="paciente" type="{http://capaServicio/}paciente" minOccurs="0"/&gt;
 *         &lt;element name="persona_id_ingresa_reserva" type="{http://capaServicio/}persona" minOccurs="0"/&gt;
 *         &lt;element name="rce" type="{http://capaServicio/}rce" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reserva", propOrder = {
    "fecha",
    "horaMedica",
    "id",
    "motivoconsulta",
    "paciente",
    "personaIdIngresaReserva",
    "rce"
})
public class Reserva {

    protected Timestamp fecha;
    @XmlElement(name = "hora_medica")
    protected Horamedica horaMedica;
    protected int id;
    protected String motivoconsulta;
    protected Paciente paciente;
    @XmlElement(name = "persona_id_ingresa_reserva")
    protected Persona personaIdIngresaReserva;
    protected Rce rce;

    /**
     * Obtiene el valor de la propiedad fecha.
     * 
     * @return
     *     possible object is
     *     {@link Timestamp }
     *     
     */
    public Timestamp getFecha() {
        return fecha;
    }

    /**
     * Define el valor de la propiedad fecha.
     * 
     * @param value
     *     allowed object is
     *     {@link Timestamp }
     *     
     */
    public void setFecha(Timestamp value) {
        this.fecha = value;
    }

    /**
     * Obtiene el valor de la propiedad horaMedica.
     * 
     * @return
     *     possible object is
     *     {@link Horamedica }
     *     
     */
    public Horamedica getHoraMedica() {
        return horaMedica;
    }

    /**
     * Define el valor de la propiedad horaMedica.
     * 
     * @param value
     *     allowed object is
     *     {@link Horamedica }
     *     
     */
    public void setHoraMedica(Horamedica value) {
        this.horaMedica = value;
    }

    /**
     * Obtiene el valor de la propiedad id.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad motivoconsulta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMotivoconsulta() {
        return motivoconsulta;
    }

    /**
     * Define el valor de la propiedad motivoconsulta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMotivoconsulta(String value) {
        this.motivoconsulta = value;
    }

    /**
     * Obtiene el valor de la propiedad paciente.
     * 
     * @return
     *     possible object is
     *     {@link Paciente }
     *     
     */
    public Paciente getPaciente() {
        return paciente;
    }

    /**
     * Define el valor de la propiedad paciente.
     * 
     * @param value
     *     allowed object is
     *     {@link Paciente }
     *     
     */
    public void setPaciente(Paciente value) {
        this.paciente = value;
    }

    /**
     * Obtiene el valor de la propiedad personaIdIngresaReserva.
     * 
     * @return
     *     possible object is
     *     {@link Persona }
     *     
     */
    public Persona getPersonaIdIngresaReserva() {
        return personaIdIngresaReserva;
    }

    /**
     * Define el valor de la propiedad personaIdIngresaReserva.
     * 
     * @param value
     *     allowed object is
     *     {@link Persona }
     *     
     */
    public void setPersonaIdIngresaReserva(Persona value) {
        this.personaIdIngresaReserva = value;
    }

    /**
     * Obtiene el valor de la propiedad rce.
     * 
     * @return
     *     possible object is
     *     {@link Rce }
     *     
     */
    public Rce getRce() {
        return rce;
    }

    /**
     * Define el valor de la propiedad rce.
     * 
     * @param value
     *     allowed object is
     *     {@link Rce }
     *     
     */
    public void setRce(Rce value) {
        this.rce = value;
    }

}
