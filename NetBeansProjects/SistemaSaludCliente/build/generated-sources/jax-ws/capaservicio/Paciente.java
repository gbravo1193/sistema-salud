
package capaservicio;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para paciente complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="paciente"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="activo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="persona" type="{http://capaServicio/}persona" minOccurs="0"/&gt;
 *         &lt;element name="persona_id" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="tipo_prevision" type="{http://capaServicio/}tipoPrevision" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "paciente", propOrder = {
    "activo",
    "descripcion",
    "id",
    "persona",
    "personaId",
    "tipoPrevision"
})
public class Paciente {

    protected Integer activo;
    protected String descripcion;
    protected int id;
    protected Persona persona;
    @XmlElement(name = "persona_id")
    protected int personaId;
    @XmlElement(name = "tipo_prevision")
    protected TipoPrevision tipoPrevision;

    /**
     * Obtiene el valor de la propiedad activo.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getActivo() {
        return activo;
    }

    /**
     * Define el valor de la propiedad activo.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setActivo(Integer value) {
        this.activo = value;
    }

    /**
     * Obtiene el valor de la propiedad descripcion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Define el valor de la propiedad descripcion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcion(String value) {
        this.descripcion = value;
    }

    /**
     * Obtiene el valor de la propiedad id.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad persona.
     * 
     * @return
     *     possible object is
     *     {@link Persona }
     *     
     */
    public Persona getPersona() {
        return persona;
    }

    /**
     * Define el valor de la propiedad persona.
     * 
     * @param value
     *     allowed object is
     *     {@link Persona }
     *     
     */
    public void setPersona(Persona value) {
        this.persona = value;
    }

    /**
     * Obtiene el valor de la propiedad personaId.
     * 
     */
    public int getPersonaId() {
        return personaId;
    }

    /**
     * Define el valor de la propiedad personaId.
     * 
     */
    public void setPersonaId(int value) {
        this.personaId = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoPrevision.
     * 
     * @return
     *     possible object is
     *     {@link TipoPrevision }
     *     
     */
    public TipoPrevision getTipoPrevision() {
        return tipoPrevision;
    }

    /**
     * Define el valor de la propiedad tipoPrevision.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoPrevision }
     *     
     */
    public void setTipoPrevision(TipoPrevision value) {
        this.tipoPrevision = value;
    }

}
