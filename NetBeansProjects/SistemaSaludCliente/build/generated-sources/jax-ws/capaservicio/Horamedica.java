
package capaservicio;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para horamedica complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="horamedica"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="administrador" type="{http://capaServicio/}administrador" minOccurs="0"/&gt;
 *         &lt;element name="hora_inicio" type="{http://capaServicio/}timestamp" minOccurs="0"/&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="profesional" type="{http://capaServicio/}profesional" minOccurs="0"/&gt;
 *         &lt;element name="reserva" type="{http://capaServicio/}reserva" minOccurs="0"/&gt;
 *         &lt;element name="tiempo_periodo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "horamedica", propOrder = {
    "administrador",
    "horaInicio",
    "id",
    "profesional",
    "reserva",
    "tiempoPeriodo"
})
public class Horamedica {

    protected Administrador administrador;
    @XmlElement(name = "hora_inicio")
    protected Timestamp horaInicio;
    protected int id;
    protected Profesional profesional;
    protected Reserva reserva;
    @XmlElement(name = "tiempo_periodo")
    protected Integer tiempoPeriodo;

    /**
     * Obtiene el valor de la propiedad administrador.
     * 
     * @return
     *     possible object is
     *     {@link Administrador }
     *     
     */
    public Administrador getAdministrador() {
        return administrador;
    }

    /**
     * Define el valor de la propiedad administrador.
     * 
     * @param value
     *     allowed object is
     *     {@link Administrador }
     *     
     */
    public void setAdministrador(Administrador value) {
        this.administrador = value;
    }

    /**
     * Obtiene el valor de la propiedad horaInicio.
     * 
     * @return
     *     possible object is
     *     {@link Timestamp }
     *     
     */
    public Timestamp getHoraInicio() {
        return horaInicio;
    }

    /**
     * Define el valor de la propiedad horaInicio.
     * 
     * @param value
     *     allowed object is
     *     {@link Timestamp }
     *     
     */
    public void setHoraInicio(Timestamp value) {
        this.horaInicio = value;
    }

    /**
     * Obtiene el valor de la propiedad id.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad profesional.
     * 
     * @return
     *     possible object is
     *     {@link Profesional }
     *     
     */
    public Profesional getProfesional() {
        return profesional;
    }

    /**
     * Define el valor de la propiedad profesional.
     * 
     * @param value
     *     allowed object is
     *     {@link Profesional }
     *     
     */
    public void setProfesional(Profesional value) {
        this.profesional = value;
    }

    /**
     * Obtiene el valor de la propiedad reserva.
     * 
     * @return
     *     possible object is
     *     {@link Reserva }
     *     
     */
    public Reserva getReserva() {
        return reserva;
    }

    /**
     * Define el valor de la propiedad reserva.
     * 
     * @param value
     *     allowed object is
     *     {@link Reserva }
     *     
     */
    public void setReserva(Reserva value) {
        this.reserva = value;
    }

    /**
     * Obtiene el valor de la propiedad tiempoPeriodo.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTiempoPeriodo() {
        return tiempoPeriodo;
    }

    /**
     * Define el valor de la propiedad tiempoPeriodo.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTiempoPeriodo(Integer value) {
        this.tiempoPeriodo = value;
    }

}
