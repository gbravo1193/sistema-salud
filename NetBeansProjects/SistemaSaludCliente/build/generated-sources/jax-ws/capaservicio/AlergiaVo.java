
package capaservicio;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para alergiaVo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="alergiaVo"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "alergiaVo")
public class AlergiaVo {
    private int id;
    private int rceid;
    private String detalle;    
        
    public AlergiaVo()     
    {
        this.id=0;
        this.rceid=0;
        this.detalle="";
    }
    
    
    public  AlergiaVo(int id , int rceid, String detalle){       
        this.id=id;
        this.rceid=rceid;
        this.detalle=detalle;
    }
    
    public int getId() {
        return id;
    }

    public int getRceid() {
        return rceid;
    }

    public String getDetalle() {
        return detalle;
    }
}

