
package capaservicio;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para obtenerDeudasPorRut complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="obtenerDeudasPorRut"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="rutPaciente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "obtenerDeudasPorRut", propOrder = {
    "rutPaciente"
})
public class ObtenerDeudasPorRut {

    protected String rutPaciente;

    /**
     * Obtiene el valor de la propiedad rutPaciente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutPaciente() {
        return rutPaciente;
    }

    /**
     * Define el valor de la propiedad rutPaciente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutPaciente(String value) {
        this.rutPaciente = value;
    }

}
