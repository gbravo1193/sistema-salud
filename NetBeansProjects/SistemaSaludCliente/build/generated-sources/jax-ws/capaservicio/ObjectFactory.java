
package capaservicio;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the capaservicio package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ParseException_QNAME = new QName("http://capaServicio/", "ParseException");
    private final static QName _PersistentException_QNAME = new QName("http://capaServicio/", "PersistentException");
    private final static QName _AgregarAdministrador_QNAME = new QName("http://capaServicio/", "agregarAdministrador");
    private final static QName _AgregarAdministradorResponse_QNAME = new QName("http://capaServicio/", "agregarAdministradorResponse");
    private final static QName _AgregarAlergia_QNAME = new QName("http://capaServicio/", "agregarAlergia");
    private final static QName _AgregarAlergiaResponse_QNAME = new QName("http://capaServicio/", "agregarAlergiaResponse");
    private final static QName _AgregarEspecialidad_QNAME = new QName("http://capaServicio/", "agregarEspecialidad");
    private final static QName _AgregarEspecialidadResponse_QNAME = new QName("http://capaServicio/", "agregarEspecialidadResponse");
    private final static QName _AgregarHoramedica_QNAME = new QName("http://capaServicio/", "agregarHoramedica");
    private final static QName _AgregarHoramedicaResponse_QNAME = new QName("http://capaServicio/", "agregarHoramedicaResponse");
    private final static QName _AgregarPaciente_QNAME = new QName("http://capaServicio/", "agregarPaciente");
    private final static QName _AgregarPacienteResponse_QNAME = new QName("http://capaServicio/", "agregarPacienteResponse");
    private final static QName _AgregarPersona_QNAME = new QName("http://capaServicio/", "agregarPersona");
    private final static QName _AgregarPersonaResponse_QNAME = new QName("http://capaServicio/", "agregarPersonaResponse");
    private final static QName _AgregarProfesional_QNAME = new QName("http://capaServicio/", "agregarProfesional");
    private final static QName _AgregarProfesionalResponse_QNAME = new QName("http://capaServicio/", "agregarProfesionalResponse");
    private final static QName _AgregarReceta_QNAME = new QName("http://capaServicio/", "agregarReceta");
    private final static QName _AgregarRecetaResponse_QNAME = new QName("http://capaServicio/", "agregarRecetaResponse");
    private final static QName _AgregarReservaMedica_QNAME = new QName("http://capaServicio/", "agregarReservaMedica");
    private final static QName _AgregarReservaMedicaResponse_QNAME = new QName("http://capaServicio/", "agregarReservaMedicaResponse");
    private final static QName _AgregarTipoPrevision_QNAME = new QName("http://capaServicio/", "agregarTipoPrevision");
    private final static QName _AgregarTipoPrevisionResponse_QNAME = new QName("http://capaServicio/", "agregarTipoPrevisionResponse");
    private final static QName _AtencionesPorProfesionalConFecha_QNAME = new QName("http://capaServicio/", "atencionesPorProfesionalConFecha");
    private final static QName _AtencionesPorProfesionalConFechaResponse_QNAME = new QName("http://capaServicio/", "atencionesPorProfesionalConFechaResponse");
    private final static QName _BuscarAdministrador_QNAME = new QName("http://capaServicio/", "buscarAdministrador");
    private final static QName _BuscarAdministradorResponse_QNAME = new QName("http://capaServicio/", "buscarAdministradorResponse");
    private final static QName _BuscarPacientePorRut_QNAME = new QName("http://capaServicio/", "buscarPacientePorRut");
    private final static QName _BuscarPacientePorRutResponse_QNAME = new QName("http://capaServicio/", "buscarPacientePorRutResponse");
    private final static QName _BuscarPersonaPorRut_QNAME = new QName("http://capaServicio/", "buscarPersonaPorRut");
    private final static QName _BuscarPersonaPorRutResponse_QNAME = new QName("http://capaServicio/", "buscarPersonaPorRutResponse");
    private final static QName _BuscarProfesional_QNAME = new QName("http://capaServicio/", "buscarProfesional");
    private final static QName _BuscarProfesionalResponse_QNAME = new QName("http://capaServicio/", "buscarProfesionalResponse");
    private final static QName _BuscarReservasPorEspecialidad_QNAME = new QName("http://capaServicio/", "buscarReservasPorEspecialidad");
    private final static QName _BuscarReservasPorEspecialidadResponse_QNAME = new QName("http://capaServicio/", "buscarReservasPorEspecialidadResponse");
    private final static QName _BuscarReservasPorPaciente_QNAME = new QName("http://capaServicio/", "buscarReservasPorPaciente");
    private final static QName _BuscarReservasPorPacienteResponse_QNAME = new QName("http://capaServicio/", "buscarReservasPorPacienteResponse");
    private final static QName _BuscarReservasPorProfesional_QNAME = new QName("http://capaServicio/", "buscarReservasPorProfesional");
    private final static QName _BuscarReservasPorProfesionalResponse_QNAME = new QName("http://capaServicio/", "buscarReservasPorProfesionalResponse");
    private final static QName _EditarPersona_QNAME = new QName("http://capaServicio/", "editarPersona");
    private final static QName _EditarPersonaResponse_QNAME = new QName("http://capaServicio/", "editarPersonaResponse");
    private final static QName _EfectuarPago_QNAME = new QName("http://capaServicio/", "efectuarPago");
    private final static QName _EfectuarPagoResponse_QNAME = new QName("http://capaServicio/", "efectuarPagoResponse");
    private final static QName _EliminarAdministrador_QNAME = new QName("http://capaServicio/", "eliminarAdministrador");
    private final static QName _EliminarAdministradorResponse_QNAME = new QName("http://capaServicio/", "eliminarAdministradorResponse");
    private final static QName _EliminarAlergiaID_QNAME = new QName("http://capaServicio/", "eliminarAlergiaID");
    private final static QName _EliminarAlergiaIDResponse_QNAME = new QName("http://capaServicio/", "eliminarAlergiaIDResponse");
    private final static QName _EliminarPersona_QNAME = new QName("http://capaServicio/", "eliminarPersona");
    private final static QName _EliminarPersonaResponse_QNAME = new QName("http://capaServicio/", "eliminarPersonaResponse");
    private final static QName _EliminarProfesional_QNAME = new QName("http://capaServicio/", "eliminarProfesional");
    private final static QName _EliminarProfesionalResponse_QNAME = new QName("http://capaServicio/", "eliminarProfesionalResponse");
    private final static QName _EliminarReceta_QNAME = new QName("http://capaServicio/", "eliminarReceta");
    private final static QName _EliminarRecetaResponse_QNAME = new QName("http://capaServicio/", "eliminarRecetaResponse");
    private final static QName _IngresarMedioPago_QNAME = new QName("http://capaServicio/", "ingresarMedioPago");
    private final static QName _IngresarMedioPagoResponse_QNAME = new QName("http://capaServicio/", "ingresarMedioPagoResponse");
    private final static QName _IngresarTipoPrevision_QNAME = new QName("http://capaServicio/", "ingresarTipoPrevision");
    private final static QName _IngresarTipoPrevisionResponse_QNAME = new QName("http://capaServicio/", "ingresarTipoPrevisionResponse");
    private final static QName _ListarAlergia_QNAME = new QName("http://capaServicio/", "listarAlergia");
    private final static QName _ListarAlergiaResponse_QNAME = new QName("http://capaServicio/", "listarAlergiaResponse");
    private final static QName _ListarAtenciones_QNAME = new QName("http://capaServicio/", "listarAtenciones");
    private final static QName _ListarAtencionesResponse_QNAME = new QName("http://capaServicio/", "listarAtencionesResponse");
    private final static QName _ListarEspecialidades_QNAME = new QName("http://capaServicio/", "listarEspecialidades");
    private final static QName _ListarEspecialidadesResponse_QNAME = new QName("http://capaServicio/", "listarEspecialidadesResponse");
    private final static QName _ListarPrevision_QNAME = new QName("http://capaServicio/", "listarPrevision");
    private final static QName _ListarPrevisionResponse_QNAME = new QName("http://capaServicio/", "listarPrevisionResponse");
    private final static QName _ListarReceta_QNAME = new QName("http://capaServicio/", "listarReceta");
    private final static QName _ListarRecetaByRut_QNAME = new QName("http://capaServicio/", "listarRecetaByRut");
    private final static QName _ListarRecetaByRutResponse_QNAME = new QName("http://capaServicio/", "listarRecetaByRutResponse");
    private final static QName _ListarRecetaResponse_QNAME = new QName("http://capaServicio/", "listarRecetaResponse");
    private final static QName _ObtenerComprobantePorIdPago_QNAME = new QName("http://capaServicio/", "obtenerComprobantePorIdPago");
    private final static QName _ObtenerComprobantePorIdPagoResponse_QNAME = new QName("http://capaServicio/", "obtenerComprobantePorIdPagoResponse");
    private final static QName _ObtenerDetalleAtencionPorRutPaciente_QNAME = new QName("http://capaServicio/", "obtenerDetalleAtencionPorRutPaciente");
    private final static QName _ObtenerDetalleAtencionPorRutPacienteResponse_QNAME = new QName("http://capaServicio/", "obtenerDetalleAtencionPorRutPacienteResponse");
    private final static QName _ObtenerDeudasPorRut_QNAME = new QName("http://capaServicio/", "obtenerDeudasPorRut");
    private final static QName _ObtenerDeudasPorRutResponse_QNAME = new QName("http://capaServicio/", "obtenerDeudasPorRutResponse");
    private final static QName _ObtenerReserva_QNAME = new QName("http://capaServicio/", "obtenerReserva");
    private final static QName _ObtenerReservaResponse_QNAME = new QName("http://capaServicio/", "obtenerReservaResponse");
    private final static QName _ProfesionalesPorEspecialidad_QNAME = new QName("http://capaServicio/", "profesionalesPorEspecialidad");
    private final static QName _ProfesionalesPorEspecialidadResponse_QNAME = new QName("http://capaServicio/", "profesionalesPorEspecialidadResponse");
    private final static QName _ReservasPorPaciente_QNAME = new QName("http://capaServicio/", "reservasPorPaciente");
    private final static QName _ReservasPorPacienteResponse_QNAME = new QName("http://capaServicio/", "reservasPorPacienteResponse");
    private final static QName _SolicitarHce_QNAME = new QName("http://capaServicio/", "solicitarHce");
    private final static QName _SolicitarHceResponse_QNAME = new QName("http://capaServicio/", "solicitarHceResponse");
    private final static QName _ValidarUsuario_QNAME = new QName("http://capaServicio/", "validarUsuario");
    private final static QName _ValidarUsuarioResponse_QNAME = new QName("http://capaServicio/", "validarUsuarioResponse");
    private final static QName _VerBusquedaHcePorPaciente_QNAME = new QName("http://capaServicio/", "verBusquedaHcePorPaciente");
    private final static QName _VerBusquedaHcePorPacienteResponse_QNAME = new QName("http://capaServicio/", "verBusquedaHcePorPacienteResponse");
    private final static QName _VerBusquedaHcePorProfesional_QNAME = new QName("http://capaServicio/", "verBusquedaHcePorProfesional");
    private final static QName _VerBusquedaHcePorProfesionalResponse_QNAME = new QName("http://capaServicio/", "verBusquedaHcePorProfesionalResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: capaservicio
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ParseException }
     * 
     */
    public ParseException createParseException() {
        return new ParseException();
    }

    /**
     * Create an instance of {@link PersistentException }
     * 
     */
    public PersistentException createPersistentException() {
        return new PersistentException();
    }

    /**
     * Create an instance of {@link AgregarAdministrador }
     * 
     */
    public AgregarAdministrador createAgregarAdministrador() {
        return new AgregarAdministrador();
    }

    /**
     * Create an instance of {@link AgregarAdministradorResponse }
     * 
     */
    public AgregarAdministradorResponse createAgregarAdministradorResponse() {
        return new AgregarAdministradorResponse();
    }

    /**
     * Create an instance of {@link AgregarAlergia }
     * 
     */
    public AgregarAlergia createAgregarAlergia() {
        return new AgregarAlergia();
    }

    /**
     * Create an instance of {@link AgregarAlergiaResponse }
     * 
     */
    public AgregarAlergiaResponse createAgregarAlergiaResponse() {
        return new AgregarAlergiaResponse();
    }

    /**
     * Create an instance of {@link AgregarEspecialidad }
     * 
     */
    public AgregarEspecialidad createAgregarEspecialidad() {
        return new AgregarEspecialidad();
    }

    /**
     * Create an instance of {@link AgregarEspecialidadResponse }
     * 
     */
    public AgregarEspecialidadResponse createAgregarEspecialidadResponse() {
        return new AgregarEspecialidadResponse();
    }

    /**
     * Create an instance of {@link AgregarHoramedica }
     * 
     */
    public AgregarHoramedica createAgregarHoramedica() {
        return new AgregarHoramedica();
    }

    /**
     * Create an instance of {@link AgregarHoramedicaResponse }
     * 
     */
    public AgregarHoramedicaResponse createAgregarHoramedicaResponse() {
        return new AgregarHoramedicaResponse();
    }

    /**
     * Create an instance of {@link AgregarPaciente }
     * 
     */
    public AgregarPaciente createAgregarPaciente() {
        return new AgregarPaciente();
    }

    /**
     * Create an instance of {@link AgregarPacienteResponse }
     * 
     */
    public AgregarPacienteResponse createAgregarPacienteResponse() {
        return new AgregarPacienteResponse();
    }

    /**
     * Create an instance of {@link AgregarPersona }
     * 
     */
    public AgregarPersona createAgregarPersona() {
        return new AgregarPersona();
    }

    /**
     * Create an instance of {@link AgregarPersonaResponse }
     * 
     */
    public AgregarPersonaResponse createAgregarPersonaResponse() {
        return new AgregarPersonaResponse();
    }

    /**
     * Create an instance of {@link AgregarProfesional }
     * 
     */
    public AgregarProfesional createAgregarProfesional() {
        return new AgregarProfesional();
    }

    /**
     * Create an instance of {@link AgregarProfesionalResponse }
     * 
     */
    public AgregarProfesionalResponse createAgregarProfesionalResponse() {
        return new AgregarProfesionalResponse();
    }

    /**
     * Create an instance of {@link AgregarReceta }
     * 
     */
    public AgregarReceta createAgregarReceta() {
        return new AgregarReceta();
    }

    /**
     * Create an instance of {@link AgregarRecetaResponse }
     * 
     */
    public AgregarRecetaResponse createAgregarRecetaResponse() {
        return new AgregarRecetaResponse();
    }

    /**
     * Create an instance of {@link AgregarReservaMedica }
     * 
     */
    public AgregarReservaMedica createAgregarReservaMedica() {
        return new AgregarReservaMedica();
    }

    /**
     * Create an instance of {@link AgregarReservaMedicaResponse }
     * 
     */
    public AgregarReservaMedicaResponse createAgregarReservaMedicaResponse() {
        return new AgregarReservaMedicaResponse();
    }

    /**
     * Create an instance of {@link AgregarTipoPrevision }
     * 
     */
    public AgregarTipoPrevision createAgregarTipoPrevision() {
        return new AgregarTipoPrevision();
    }

    /**
     * Create an instance of {@link AgregarTipoPrevisionResponse }
     * 
     */
    public AgregarTipoPrevisionResponse createAgregarTipoPrevisionResponse() {
        return new AgregarTipoPrevisionResponse();
    }

    /**
     * Create an instance of {@link AtencionesPorProfesionalConFecha }
     * 
     */
    public AtencionesPorProfesionalConFecha createAtencionesPorProfesionalConFecha() {
        return new AtencionesPorProfesionalConFecha();
    }

    /**
     * Create an instance of {@link AtencionesPorProfesionalConFechaResponse }
     * 
     */
    public AtencionesPorProfesionalConFechaResponse createAtencionesPorProfesionalConFechaResponse() {
        return new AtencionesPorProfesionalConFechaResponse();
    }

    /**
     * Create an instance of {@link BuscarAdministrador }
     * 
     */
    public BuscarAdministrador createBuscarAdministrador() {
        return new BuscarAdministrador();
    }

    /**
     * Create an instance of {@link BuscarAdministradorResponse }
     * 
     */
    public BuscarAdministradorResponse createBuscarAdministradorResponse() {
        return new BuscarAdministradorResponse();
    }

    /**
     * Create an instance of {@link BuscarPacientePorRut }
     * 
     */
    public BuscarPacientePorRut createBuscarPacientePorRut() {
        return new BuscarPacientePorRut();
    }

    /**
     * Create an instance of {@link BuscarPacientePorRutResponse }
     * 
     */
    public BuscarPacientePorRutResponse createBuscarPacientePorRutResponse() {
        return new BuscarPacientePorRutResponse();
    }

    /**
     * Create an instance of {@link BuscarPersonaPorRut }
     * 
     */
    public BuscarPersonaPorRut createBuscarPersonaPorRut() {
        return new BuscarPersonaPorRut();
    }

    /**
     * Create an instance of {@link BuscarPersonaPorRutResponse }
     * 
     */
    public BuscarPersonaPorRutResponse createBuscarPersonaPorRutResponse() {
        return new BuscarPersonaPorRutResponse();
    }

    /**
     * Create an instance of {@link BuscarProfesional }
     * 
     */
    public BuscarProfesional createBuscarProfesional() {
        return new BuscarProfesional();
    }

    /**
     * Create an instance of {@link BuscarProfesionalResponse }
     * 
     */
    public BuscarProfesionalResponse createBuscarProfesionalResponse() {
        return new BuscarProfesionalResponse();
    }

    /**
     * Create an instance of {@link BuscarReservasPorEspecialidad }
     * 
     */
    public BuscarReservasPorEspecialidad createBuscarReservasPorEspecialidad() {
        return new BuscarReservasPorEspecialidad();
    }

    /**
     * Create an instance of {@link BuscarReservasPorEspecialidadResponse }
     * 
     */
    public BuscarReservasPorEspecialidadResponse createBuscarReservasPorEspecialidadResponse() {
        return new BuscarReservasPorEspecialidadResponse();
    }

    /**
     * Create an instance of {@link BuscarReservasPorPaciente }
     * 
     */
    public BuscarReservasPorPaciente createBuscarReservasPorPaciente() {
        return new BuscarReservasPorPaciente();
    }

    /**
     * Create an instance of {@link BuscarReservasPorPacienteResponse }
     * 
     */
    public BuscarReservasPorPacienteResponse createBuscarReservasPorPacienteResponse() {
        return new BuscarReservasPorPacienteResponse();
    }

    /**
     * Create an instance of {@link BuscarReservasPorProfesional }
     * 
     */
    public BuscarReservasPorProfesional createBuscarReservasPorProfesional() {
        return new BuscarReservasPorProfesional();
    }

    /**
     * Create an instance of {@link BuscarReservasPorProfesionalResponse }
     * 
     */
    public BuscarReservasPorProfesionalResponse createBuscarReservasPorProfesionalResponse() {
        return new BuscarReservasPorProfesionalResponse();
    }

    /**
     * Create an instance of {@link EditarPersona }
     * 
     */
    public EditarPersona createEditarPersona() {
        return new EditarPersona();
    }

    /**
     * Create an instance of {@link EditarPersonaResponse }
     * 
     */
    public EditarPersonaResponse createEditarPersonaResponse() {
        return new EditarPersonaResponse();
    }

    /**
     * Create an instance of {@link EfectuarPago }
     * 
     */
    public EfectuarPago createEfectuarPago() {
        return new EfectuarPago();
    }

    /**
     * Create an instance of {@link EfectuarPagoResponse }
     * 
     */
    public EfectuarPagoResponse createEfectuarPagoResponse() {
        return new EfectuarPagoResponse();
    }

    /**
     * Create an instance of {@link EliminarAdministrador }
     * 
     */
    public EliminarAdministrador createEliminarAdministrador() {
        return new EliminarAdministrador();
    }

    /**
     * Create an instance of {@link EliminarAdministradorResponse }
     * 
     */
    public EliminarAdministradorResponse createEliminarAdministradorResponse() {
        return new EliminarAdministradorResponse();
    }

    /**
     * Create an instance of {@link EliminarAlergiaID }
     * 
     */
    public EliminarAlergiaID createEliminarAlergiaID() {
        return new EliminarAlergiaID();
    }

    /**
     * Create an instance of {@link EliminarAlergiaIDResponse }
     * 
     */
    public EliminarAlergiaIDResponse createEliminarAlergiaIDResponse() {
        return new EliminarAlergiaIDResponse();
    }

    /**
     * Create an instance of {@link EliminarPersona }
     * 
     */
    public EliminarPersona createEliminarPersona() {
        return new EliminarPersona();
    }

    /**
     * Create an instance of {@link EliminarPersonaResponse }
     * 
     */
    public EliminarPersonaResponse createEliminarPersonaResponse() {
        return new EliminarPersonaResponse();
    }

    /**
     * Create an instance of {@link EliminarProfesional }
     * 
     */
    public EliminarProfesional createEliminarProfesional() {
        return new EliminarProfesional();
    }

    /**
     * Create an instance of {@link EliminarProfesionalResponse }
     * 
     */
    public EliminarProfesionalResponse createEliminarProfesionalResponse() {
        return new EliminarProfesionalResponse();
    }

    /**
     * Create an instance of {@link EliminarReceta }
     * 
     */
    public EliminarReceta createEliminarReceta() {
        return new EliminarReceta();
    }

    /**
     * Create an instance of {@link EliminarRecetaResponse }
     * 
     */
    public EliminarRecetaResponse createEliminarRecetaResponse() {
        return new EliminarRecetaResponse();
    }

    /**
     * Create an instance of {@link IngresarMedioPago }
     * 
     */
    public IngresarMedioPago createIngresarMedioPago() {
        return new IngresarMedioPago();
    }

    /**
     * Create an instance of {@link IngresarMedioPagoResponse }
     * 
     */
    public IngresarMedioPagoResponse createIngresarMedioPagoResponse() {
        return new IngresarMedioPagoResponse();
    }

    /**
     * Create an instance of {@link IngresarTipoPrevision }
     * 
     */
    public IngresarTipoPrevision createIngresarTipoPrevision() {
        return new IngresarTipoPrevision();
    }

    /**
     * Create an instance of {@link IngresarTipoPrevisionResponse }
     * 
     */
    public IngresarTipoPrevisionResponse createIngresarTipoPrevisionResponse() {
        return new IngresarTipoPrevisionResponse();
    }

    /**
     * Create an instance of {@link ListarAlergia }
     * 
     */
    public ListarAlergia createListarAlergia() {
        return new ListarAlergia();
    }

    /**
     * Create an instance of {@link ListarAlergiaResponse }
     * 
     */
    public ListarAlergiaResponse createListarAlergiaResponse() {
        return new ListarAlergiaResponse();
    }

    /**
     * Create an instance of {@link ListarAtenciones }
     * 
     */
    public ListarAtenciones createListarAtenciones() {
        return new ListarAtenciones();
    }

    /**
     * Create an instance of {@link ListarAtencionesResponse }
     * 
     */
    public ListarAtencionesResponse createListarAtencionesResponse() {
        return new ListarAtencionesResponse();
    }

    /**
     * Create an instance of {@link ListarEspecialidades }
     * 
     */
    public ListarEspecialidades createListarEspecialidades() {
        return new ListarEspecialidades();
    }

    /**
     * Create an instance of {@link ListarEspecialidadesResponse }
     * 
     */
    public ListarEspecialidadesResponse createListarEspecialidadesResponse() {
        return new ListarEspecialidadesResponse();
    }

    /**
     * Create an instance of {@link ListarPrevision }
     * 
     */
    public ListarPrevision createListarPrevision() {
        return new ListarPrevision();
    }

    /**
     * Create an instance of {@link ListarPrevisionResponse }
     * 
     */
    public ListarPrevisionResponse createListarPrevisionResponse() {
        return new ListarPrevisionResponse();
    }

    /**
     * Create an instance of {@link ListarReceta }
     * 
     */
    public ListarReceta createListarReceta() {
        return new ListarReceta();
    }

    /**
     * Create an instance of {@link ListarRecetaByRut }
     * 
     */
    public ListarRecetaByRut createListarRecetaByRut() {
        return new ListarRecetaByRut();
    }

    /**
     * Create an instance of {@link ListarRecetaByRutResponse }
     * 
     */
    public ListarRecetaByRutResponse createListarRecetaByRutResponse() {
        return new ListarRecetaByRutResponse();
    }

    /**
     * Create an instance of {@link ListarRecetaResponse }
     * 
     */
    public ListarRecetaResponse createListarRecetaResponse() {
        return new ListarRecetaResponse();
    }

    /**
     * Create an instance of {@link ObtenerComprobantePorIdPago }
     * 
     */
    public ObtenerComprobantePorIdPago createObtenerComprobantePorIdPago() {
        return new ObtenerComprobantePorIdPago();
    }

    /**
     * Create an instance of {@link ObtenerComprobantePorIdPagoResponse }
     * 
     */
    public ObtenerComprobantePorIdPagoResponse createObtenerComprobantePorIdPagoResponse() {
        return new ObtenerComprobantePorIdPagoResponse();
    }

    /**
     * Create an instance of {@link ObtenerDetalleAtencionPorRutPaciente }
     * 
     */
    public ObtenerDetalleAtencionPorRutPaciente createObtenerDetalleAtencionPorRutPaciente() {
        return new ObtenerDetalleAtencionPorRutPaciente();
    }

    /**
     * Create an instance of {@link ObtenerDetalleAtencionPorRutPacienteResponse }
     * 
     */
    public ObtenerDetalleAtencionPorRutPacienteResponse createObtenerDetalleAtencionPorRutPacienteResponse() {
        return new ObtenerDetalleAtencionPorRutPacienteResponse();
    }

    /**
     * Create an instance of {@link ObtenerDeudasPorRut }
     * 
     */
    public ObtenerDeudasPorRut createObtenerDeudasPorRut() {
        return new ObtenerDeudasPorRut();
    }

    /**
     * Create an instance of {@link ObtenerDeudasPorRutResponse }
     * 
     */
    public ObtenerDeudasPorRutResponse createObtenerDeudasPorRutResponse() {
        return new ObtenerDeudasPorRutResponse();
    }

    /**
     * Create an instance of {@link ObtenerReserva }
     * 
     */
    public ObtenerReserva createObtenerReserva() {
        return new ObtenerReserva();
    }

    /**
     * Create an instance of {@link ObtenerReservaResponse }
     * 
     */
    public ObtenerReservaResponse createObtenerReservaResponse() {
        return new ObtenerReservaResponse();
    }

    /**
     * Create an instance of {@link ProfesionalesPorEspecialidad }
     * 
     */
    public ProfesionalesPorEspecialidad createProfesionalesPorEspecialidad() {
        return new ProfesionalesPorEspecialidad();
    }

    /**
     * Create an instance of {@link ProfesionalesPorEspecialidadResponse }
     * 
     */
    public ProfesionalesPorEspecialidadResponse createProfesionalesPorEspecialidadResponse() {
        return new ProfesionalesPorEspecialidadResponse();
    }

    /**
     * Create an instance of {@link ReservasPorPaciente }
     * 
     */
    public ReservasPorPaciente createReservasPorPaciente() {
        return new ReservasPorPaciente();
    }

    /**
     * Create an instance of {@link ReservasPorPacienteResponse }
     * 
     */
    public ReservasPorPacienteResponse createReservasPorPacienteResponse() {
        return new ReservasPorPacienteResponse();
    }

    /**
     * Create an instance of {@link SolicitarHce }
     * 
     */
    public SolicitarHce createSolicitarHce() {
        return new SolicitarHce();
    }

    /**
     * Create an instance of {@link SolicitarHceResponse }
     * 
     */
    public SolicitarHceResponse createSolicitarHceResponse() {
        return new SolicitarHceResponse();
    }

    /**
     * Create an instance of {@link ValidarUsuario }
     * 
     */
    public ValidarUsuario createValidarUsuario() {
        return new ValidarUsuario();
    }

    /**
     * Create an instance of {@link ValidarUsuarioResponse }
     * 
     */
    public ValidarUsuarioResponse createValidarUsuarioResponse() {
        return new ValidarUsuarioResponse();
    }

    /**
     * Create an instance of {@link VerBusquedaHcePorPaciente }
     * 
     */
    public VerBusquedaHcePorPaciente createVerBusquedaHcePorPaciente() {
        return new VerBusquedaHcePorPaciente();
    }

    /**
     * Create an instance of {@link VerBusquedaHcePorPacienteResponse }
     * 
     */
    public VerBusquedaHcePorPacienteResponse createVerBusquedaHcePorPacienteResponse() {
        return new VerBusquedaHcePorPacienteResponse();
    }

    /**
     * Create an instance of {@link VerBusquedaHcePorProfesional }
     * 
     */
    public VerBusquedaHcePorProfesional createVerBusquedaHcePorProfesional() {
        return new VerBusquedaHcePorProfesional();
    }

    /**
     * Create an instance of {@link VerBusquedaHcePorProfesionalResponse }
     * 
     */
    public VerBusquedaHcePorProfesionalResponse createVerBusquedaHcePorProfesionalResponse() {
        return new VerBusquedaHcePorProfesionalResponse();
    }

    /**
     * Create an instance of {@link AlergiaVo }
     * 
     */
    public AlergiaVo createAlergiaVo() {
        return new AlergiaVo();
    }

    /**
     * Create an instance of {@link Receta }
     * 
     */
    public Receta createReceta() {
        return new Receta();
    }

    /**
     * Create an instance of {@link Paciente }
     * 
     */
    public Paciente createPaciente() {
        return new Paciente();
    }

    /**
     * Create an instance of {@link Persona }
     * 
     */
    public Persona createPersona() {
        return new Persona();
    }

    /**
     * Create an instance of {@link Administrador }
     * 
     */
    public Administrador createAdministrador() {
        return new Administrador();
    }

    /**
     * Create an instance of {@link Profesional }
     * 
     */
    public Profesional createProfesional() {
        return new Profesional();
    }

    /**
     * Create an instance of {@link TipoPrevision }
     * 
     */
    public TipoPrevision createTipoPrevision() {
        return new TipoPrevision();
    }

    /**
     * Create an instance of {@link Recetarce }
     * 
     */
    public Recetarce createRecetarce() {
        return new Recetarce();
    }

    /**
     * Create an instance of {@link Rce }
     * 
     */
    public Rce createRce() {
        return new Rce();
    }

    /**
     * Create an instance of {@link Box }
     * 
     */
    public Box createBox() {
        return new Box();
    }

    /**
     * Create an instance of {@link Destino }
     * 
     */
    public Destino createDestino() {
        return new Destino();
    }

    /**
     * Create an instance of {@link Procedencia }
     * 
     */
    public Procedencia createProcedencia() {
        return new Procedencia();
    }

    /**
     * Create an instance of {@link Reserva }
     * 
     */
    public Reserva createReserva() {
        return new Reserva();
    }

    /**
     * Create an instance of {@link Timestamp }
     * 
     */
    public Timestamp createTimestamp() {
        return new Timestamp();
    }

    /**
     * Create an instance of {@link Horamedica }
     * 
     */
    public Horamedica createHoramedica() {
        return new Horamedica();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ParseException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "ParseException")
    public JAXBElement<ParseException> createParseException(ParseException value) {
        return new JAXBElement<ParseException>(_ParseException_QNAME, ParseException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersistentException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "PersistentException")
    public JAXBElement<PersistentException> createPersistentException(PersistentException value) {
        return new JAXBElement<PersistentException>(_PersistentException_QNAME, PersistentException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarAdministrador }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "agregarAdministrador")
    public JAXBElement<AgregarAdministrador> createAgregarAdministrador(AgregarAdministrador value) {
        return new JAXBElement<AgregarAdministrador>(_AgregarAdministrador_QNAME, AgregarAdministrador.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarAdministradorResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "agregarAdministradorResponse")
    public JAXBElement<AgregarAdministradorResponse> createAgregarAdministradorResponse(AgregarAdministradorResponse value) {
        return new JAXBElement<AgregarAdministradorResponse>(_AgregarAdministradorResponse_QNAME, AgregarAdministradorResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarAlergia }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "agregarAlergia")
    public JAXBElement<AgregarAlergia> createAgregarAlergia(AgregarAlergia value) {
        return new JAXBElement<AgregarAlergia>(_AgregarAlergia_QNAME, AgregarAlergia.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarAlergiaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "agregarAlergiaResponse")
    public JAXBElement<AgregarAlergiaResponse> createAgregarAlergiaResponse(AgregarAlergiaResponse value) {
        return new JAXBElement<AgregarAlergiaResponse>(_AgregarAlergiaResponse_QNAME, AgregarAlergiaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarEspecialidad }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "agregarEspecialidad")
    public JAXBElement<AgregarEspecialidad> createAgregarEspecialidad(AgregarEspecialidad value) {
        return new JAXBElement<AgregarEspecialidad>(_AgregarEspecialidad_QNAME, AgregarEspecialidad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarEspecialidadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "agregarEspecialidadResponse")
    public JAXBElement<AgregarEspecialidadResponse> createAgregarEspecialidadResponse(AgregarEspecialidadResponse value) {
        return new JAXBElement<AgregarEspecialidadResponse>(_AgregarEspecialidadResponse_QNAME, AgregarEspecialidadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarHoramedica }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "agregarHoramedica")
    public JAXBElement<AgregarHoramedica> createAgregarHoramedica(AgregarHoramedica value) {
        return new JAXBElement<AgregarHoramedica>(_AgregarHoramedica_QNAME, AgregarHoramedica.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarHoramedicaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "agregarHoramedicaResponse")
    public JAXBElement<AgregarHoramedicaResponse> createAgregarHoramedicaResponse(AgregarHoramedicaResponse value) {
        return new JAXBElement<AgregarHoramedicaResponse>(_AgregarHoramedicaResponse_QNAME, AgregarHoramedicaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarPaciente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "agregarPaciente")
    public JAXBElement<AgregarPaciente> createAgregarPaciente(AgregarPaciente value) {
        return new JAXBElement<AgregarPaciente>(_AgregarPaciente_QNAME, AgregarPaciente.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarPacienteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "agregarPacienteResponse")
    public JAXBElement<AgregarPacienteResponse> createAgregarPacienteResponse(AgregarPacienteResponse value) {
        return new JAXBElement<AgregarPacienteResponse>(_AgregarPacienteResponse_QNAME, AgregarPacienteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarPersona }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "agregarPersona")
    public JAXBElement<AgregarPersona> createAgregarPersona(AgregarPersona value) {
        return new JAXBElement<AgregarPersona>(_AgregarPersona_QNAME, AgregarPersona.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarPersonaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "agregarPersonaResponse")
    public JAXBElement<AgregarPersonaResponse> createAgregarPersonaResponse(AgregarPersonaResponse value) {
        return new JAXBElement<AgregarPersonaResponse>(_AgregarPersonaResponse_QNAME, AgregarPersonaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarProfesional }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "agregarProfesional")
    public JAXBElement<AgregarProfesional> createAgregarProfesional(AgregarProfesional value) {
        return new JAXBElement<AgregarProfesional>(_AgregarProfesional_QNAME, AgregarProfesional.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarProfesionalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "agregarProfesionalResponse")
    public JAXBElement<AgregarProfesionalResponse> createAgregarProfesionalResponse(AgregarProfesionalResponse value) {
        return new JAXBElement<AgregarProfesionalResponse>(_AgregarProfesionalResponse_QNAME, AgregarProfesionalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarReceta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "agregarReceta")
    public JAXBElement<AgregarReceta> createAgregarReceta(AgregarReceta value) {
        return new JAXBElement<AgregarReceta>(_AgregarReceta_QNAME, AgregarReceta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarRecetaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "agregarRecetaResponse")
    public JAXBElement<AgregarRecetaResponse> createAgregarRecetaResponse(AgregarRecetaResponse value) {
        return new JAXBElement<AgregarRecetaResponse>(_AgregarRecetaResponse_QNAME, AgregarRecetaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarReservaMedica }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "agregarReservaMedica")
    public JAXBElement<AgregarReservaMedica> createAgregarReservaMedica(AgregarReservaMedica value) {
        return new JAXBElement<AgregarReservaMedica>(_AgregarReservaMedica_QNAME, AgregarReservaMedica.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarReservaMedicaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "agregarReservaMedicaResponse")
    public JAXBElement<AgregarReservaMedicaResponse> createAgregarReservaMedicaResponse(AgregarReservaMedicaResponse value) {
        return new JAXBElement<AgregarReservaMedicaResponse>(_AgregarReservaMedicaResponse_QNAME, AgregarReservaMedicaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarTipoPrevision }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "agregarTipoPrevision")
    public JAXBElement<AgregarTipoPrevision> createAgregarTipoPrevision(AgregarTipoPrevision value) {
        return new JAXBElement<AgregarTipoPrevision>(_AgregarTipoPrevision_QNAME, AgregarTipoPrevision.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarTipoPrevisionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "agregarTipoPrevisionResponse")
    public JAXBElement<AgregarTipoPrevisionResponse> createAgregarTipoPrevisionResponse(AgregarTipoPrevisionResponse value) {
        return new JAXBElement<AgregarTipoPrevisionResponse>(_AgregarTipoPrevisionResponse_QNAME, AgregarTipoPrevisionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AtencionesPorProfesionalConFecha }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "atencionesPorProfesionalConFecha")
    public JAXBElement<AtencionesPorProfesionalConFecha> createAtencionesPorProfesionalConFecha(AtencionesPorProfesionalConFecha value) {
        return new JAXBElement<AtencionesPorProfesionalConFecha>(_AtencionesPorProfesionalConFecha_QNAME, AtencionesPorProfesionalConFecha.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AtencionesPorProfesionalConFechaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "atencionesPorProfesionalConFechaResponse")
    public JAXBElement<AtencionesPorProfesionalConFechaResponse> createAtencionesPorProfesionalConFechaResponse(AtencionesPorProfesionalConFechaResponse value) {
        return new JAXBElement<AtencionesPorProfesionalConFechaResponse>(_AtencionesPorProfesionalConFechaResponse_QNAME, AtencionesPorProfesionalConFechaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarAdministrador }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "buscarAdministrador")
    public JAXBElement<BuscarAdministrador> createBuscarAdministrador(BuscarAdministrador value) {
        return new JAXBElement<BuscarAdministrador>(_BuscarAdministrador_QNAME, BuscarAdministrador.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarAdministradorResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "buscarAdministradorResponse")
    public JAXBElement<BuscarAdministradorResponse> createBuscarAdministradorResponse(BuscarAdministradorResponse value) {
        return new JAXBElement<BuscarAdministradorResponse>(_BuscarAdministradorResponse_QNAME, BuscarAdministradorResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarPacientePorRut }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "buscarPacientePorRut")
    public JAXBElement<BuscarPacientePorRut> createBuscarPacientePorRut(BuscarPacientePorRut value) {
        return new JAXBElement<BuscarPacientePorRut>(_BuscarPacientePorRut_QNAME, BuscarPacientePorRut.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarPacientePorRutResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "buscarPacientePorRutResponse")
    public JAXBElement<BuscarPacientePorRutResponse> createBuscarPacientePorRutResponse(BuscarPacientePorRutResponse value) {
        return new JAXBElement<BuscarPacientePorRutResponse>(_BuscarPacientePorRutResponse_QNAME, BuscarPacientePorRutResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarPersonaPorRut }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "buscarPersonaPorRut")
    public JAXBElement<BuscarPersonaPorRut> createBuscarPersonaPorRut(BuscarPersonaPorRut value) {
        return new JAXBElement<BuscarPersonaPorRut>(_BuscarPersonaPorRut_QNAME, BuscarPersonaPorRut.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarPersonaPorRutResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "buscarPersonaPorRutResponse")
    public JAXBElement<BuscarPersonaPorRutResponse> createBuscarPersonaPorRutResponse(BuscarPersonaPorRutResponse value) {
        return new JAXBElement<BuscarPersonaPorRutResponse>(_BuscarPersonaPorRutResponse_QNAME, BuscarPersonaPorRutResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarProfesional }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "buscarProfesional")
    public JAXBElement<BuscarProfesional> createBuscarProfesional(BuscarProfesional value) {
        return new JAXBElement<BuscarProfesional>(_BuscarProfesional_QNAME, BuscarProfesional.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarProfesionalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "buscarProfesionalResponse")
    public JAXBElement<BuscarProfesionalResponse> createBuscarProfesionalResponse(BuscarProfesionalResponse value) {
        return new JAXBElement<BuscarProfesionalResponse>(_BuscarProfesionalResponse_QNAME, BuscarProfesionalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarReservasPorEspecialidad }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "buscarReservasPorEspecialidad")
    public JAXBElement<BuscarReservasPorEspecialidad> createBuscarReservasPorEspecialidad(BuscarReservasPorEspecialidad value) {
        return new JAXBElement<BuscarReservasPorEspecialidad>(_BuscarReservasPorEspecialidad_QNAME, BuscarReservasPorEspecialidad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarReservasPorEspecialidadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "buscarReservasPorEspecialidadResponse")
    public JAXBElement<BuscarReservasPorEspecialidadResponse> createBuscarReservasPorEspecialidadResponse(BuscarReservasPorEspecialidadResponse value) {
        return new JAXBElement<BuscarReservasPorEspecialidadResponse>(_BuscarReservasPorEspecialidadResponse_QNAME, BuscarReservasPorEspecialidadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarReservasPorPaciente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "buscarReservasPorPaciente")
    public JAXBElement<BuscarReservasPorPaciente> createBuscarReservasPorPaciente(BuscarReservasPorPaciente value) {
        return new JAXBElement<BuscarReservasPorPaciente>(_BuscarReservasPorPaciente_QNAME, BuscarReservasPorPaciente.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarReservasPorPacienteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "buscarReservasPorPacienteResponse")
    public JAXBElement<BuscarReservasPorPacienteResponse> createBuscarReservasPorPacienteResponse(BuscarReservasPorPacienteResponse value) {
        return new JAXBElement<BuscarReservasPorPacienteResponse>(_BuscarReservasPorPacienteResponse_QNAME, BuscarReservasPorPacienteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarReservasPorProfesional }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "buscarReservasPorProfesional")
    public JAXBElement<BuscarReservasPorProfesional> createBuscarReservasPorProfesional(BuscarReservasPorProfesional value) {
        return new JAXBElement<BuscarReservasPorProfesional>(_BuscarReservasPorProfesional_QNAME, BuscarReservasPorProfesional.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarReservasPorProfesionalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "buscarReservasPorProfesionalResponse")
    public JAXBElement<BuscarReservasPorProfesionalResponse> createBuscarReservasPorProfesionalResponse(BuscarReservasPorProfesionalResponse value) {
        return new JAXBElement<BuscarReservasPorProfesionalResponse>(_BuscarReservasPorProfesionalResponse_QNAME, BuscarReservasPorProfesionalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EditarPersona }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "editarPersona")
    public JAXBElement<EditarPersona> createEditarPersona(EditarPersona value) {
        return new JAXBElement<EditarPersona>(_EditarPersona_QNAME, EditarPersona.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EditarPersonaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "editarPersonaResponse")
    public JAXBElement<EditarPersonaResponse> createEditarPersonaResponse(EditarPersonaResponse value) {
        return new JAXBElement<EditarPersonaResponse>(_EditarPersonaResponse_QNAME, EditarPersonaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EfectuarPago }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "efectuarPago")
    public JAXBElement<EfectuarPago> createEfectuarPago(EfectuarPago value) {
        return new JAXBElement<EfectuarPago>(_EfectuarPago_QNAME, EfectuarPago.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EfectuarPagoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "efectuarPagoResponse")
    public JAXBElement<EfectuarPagoResponse> createEfectuarPagoResponse(EfectuarPagoResponse value) {
        return new JAXBElement<EfectuarPagoResponse>(_EfectuarPagoResponse_QNAME, EfectuarPagoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EliminarAdministrador }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "eliminarAdministrador")
    public JAXBElement<EliminarAdministrador> createEliminarAdministrador(EliminarAdministrador value) {
        return new JAXBElement<EliminarAdministrador>(_EliminarAdministrador_QNAME, EliminarAdministrador.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EliminarAdministradorResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "eliminarAdministradorResponse")
    public JAXBElement<EliminarAdministradorResponse> createEliminarAdministradorResponse(EliminarAdministradorResponse value) {
        return new JAXBElement<EliminarAdministradorResponse>(_EliminarAdministradorResponse_QNAME, EliminarAdministradorResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EliminarAlergiaID }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "eliminarAlergiaID")
    public JAXBElement<EliminarAlergiaID> createEliminarAlergiaID(EliminarAlergiaID value) {
        return new JAXBElement<EliminarAlergiaID>(_EliminarAlergiaID_QNAME, EliminarAlergiaID.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EliminarAlergiaIDResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "eliminarAlergiaIDResponse")
    public JAXBElement<EliminarAlergiaIDResponse> createEliminarAlergiaIDResponse(EliminarAlergiaIDResponse value) {
        return new JAXBElement<EliminarAlergiaIDResponse>(_EliminarAlergiaIDResponse_QNAME, EliminarAlergiaIDResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EliminarPersona }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "eliminarPersona")
    public JAXBElement<EliminarPersona> createEliminarPersona(EliminarPersona value) {
        return new JAXBElement<EliminarPersona>(_EliminarPersona_QNAME, EliminarPersona.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EliminarPersonaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "eliminarPersonaResponse")
    public JAXBElement<EliminarPersonaResponse> createEliminarPersonaResponse(EliminarPersonaResponse value) {
        return new JAXBElement<EliminarPersonaResponse>(_EliminarPersonaResponse_QNAME, EliminarPersonaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EliminarProfesional }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "eliminarProfesional")
    public JAXBElement<EliminarProfesional> createEliminarProfesional(EliminarProfesional value) {
        return new JAXBElement<EliminarProfesional>(_EliminarProfesional_QNAME, EliminarProfesional.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EliminarProfesionalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "eliminarProfesionalResponse")
    public JAXBElement<EliminarProfesionalResponse> createEliminarProfesionalResponse(EliminarProfesionalResponse value) {
        return new JAXBElement<EliminarProfesionalResponse>(_EliminarProfesionalResponse_QNAME, EliminarProfesionalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EliminarReceta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "eliminarReceta")
    public JAXBElement<EliminarReceta> createEliminarReceta(EliminarReceta value) {
        return new JAXBElement<EliminarReceta>(_EliminarReceta_QNAME, EliminarReceta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EliminarRecetaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "eliminarRecetaResponse")
    public JAXBElement<EliminarRecetaResponse> createEliminarRecetaResponse(EliminarRecetaResponse value) {
        return new JAXBElement<EliminarRecetaResponse>(_EliminarRecetaResponse_QNAME, EliminarRecetaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IngresarMedioPago }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "ingresarMedioPago")
    public JAXBElement<IngresarMedioPago> createIngresarMedioPago(IngresarMedioPago value) {
        return new JAXBElement<IngresarMedioPago>(_IngresarMedioPago_QNAME, IngresarMedioPago.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IngresarMedioPagoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "ingresarMedioPagoResponse")
    public JAXBElement<IngresarMedioPagoResponse> createIngresarMedioPagoResponse(IngresarMedioPagoResponse value) {
        return new JAXBElement<IngresarMedioPagoResponse>(_IngresarMedioPagoResponse_QNAME, IngresarMedioPagoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IngresarTipoPrevision }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "ingresarTipoPrevision")
    public JAXBElement<IngresarTipoPrevision> createIngresarTipoPrevision(IngresarTipoPrevision value) {
        return new JAXBElement<IngresarTipoPrevision>(_IngresarTipoPrevision_QNAME, IngresarTipoPrevision.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IngresarTipoPrevisionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "ingresarTipoPrevisionResponse")
    public JAXBElement<IngresarTipoPrevisionResponse> createIngresarTipoPrevisionResponse(IngresarTipoPrevisionResponse value) {
        return new JAXBElement<IngresarTipoPrevisionResponse>(_IngresarTipoPrevisionResponse_QNAME, IngresarTipoPrevisionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarAlergia }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "listarAlergia")
    public JAXBElement<ListarAlergia> createListarAlergia(ListarAlergia value) {
        return new JAXBElement<ListarAlergia>(_ListarAlergia_QNAME, ListarAlergia.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarAlergiaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "listarAlergiaResponse")
    public JAXBElement<ListarAlergiaResponse> createListarAlergiaResponse(ListarAlergiaResponse value) {
        return new JAXBElement<ListarAlergiaResponse>(_ListarAlergiaResponse_QNAME, ListarAlergiaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarAtenciones }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "listarAtenciones")
    public JAXBElement<ListarAtenciones> createListarAtenciones(ListarAtenciones value) {
        return new JAXBElement<ListarAtenciones>(_ListarAtenciones_QNAME, ListarAtenciones.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarAtencionesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "listarAtencionesResponse")
    public JAXBElement<ListarAtencionesResponse> createListarAtencionesResponse(ListarAtencionesResponse value) {
        return new JAXBElement<ListarAtencionesResponse>(_ListarAtencionesResponse_QNAME, ListarAtencionesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarEspecialidades }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "listarEspecialidades")
    public JAXBElement<ListarEspecialidades> createListarEspecialidades(ListarEspecialidades value) {
        return new JAXBElement<ListarEspecialidades>(_ListarEspecialidades_QNAME, ListarEspecialidades.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarEspecialidadesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "listarEspecialidadesResponse")
    public JAXBElement<ListarEspecialidadesResponse> createListarEspecialidadesResponse(ListarEspecialidadesResponse value) {
        return new JAXBElement<ListarEspecialidadesResponse>(_ListarEspecialidadesResponse_QNAME, ListarEspecialidadesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarPrevision }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "listarPrevision")
    public JAXBElement<ListarPrevision> createListarPrevision(ListarPrevision value) {
        return new JAXBElement<ListarPrevision>(_ListarPrevision_QNAME, ListarPrevision.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarPrevisionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "listarPrevisionResponse")
    public JAXBElement<ListarPrevisionResponse> createListarPrevisionResponse(ListarPrevisionResponse value) {
        return new JAXBElement<ListarPrevisionResponse>(_ListarPrevisionResponse_QNAME, ListarPrevisionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarReceta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "listarReceta")
    public JAXBElement<ListarReceta> createListarReceta(ListarReceta value) {
        return new JAXBElement<ListarReceta>(_ListarReceta_QNAME, ListarReceta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarRecetaByRut }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "listarRecetaByRut")
    public JAXBElement<ListarRecetaByRut> createListarRecetaByRut(ListarRecetaByRut value) {
        return new JAXBElement<ListarRecetaByRut>(_ListarRecetaByRut_QNAME, ListarRecetaByRut.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarRecetaByRutResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "listarRecetaByRutResponse")
    public JAXBElement<ListarRecetaByRutResponse> createListarRecetaByRutResponse(ListarRecetaByRutResponse value) {
        return new JAXBElement<ListarRecetaByRutResponse>(_ListarRecetaByRutResponse_QNAME, ListarRecetaByRutResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarRecetaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "listarRecetaResponse")
    public JAXBElement<ListarRecetaResponse> createListarRecetaResponse(ListarRecetaResponse value) {
        return new JAXBElement<ListarRecetaResponse>(_ListarRecetaResponse_QNAME, ListarRecetaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerComprobantePorIdPago }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "obtenerComprobantePorIdPago")
    public JAXBElement<ObtenerComprobantePorIdPago> createObtenerComprobantePorIdPago(ObtenerComprobantePorIdPago value) {
        return new JAXBElement<ObtenerComprobantePorIdPago>(_ObtenerComprobantePorIdPago_QNAME, ObtenerComprobantePorIdPago.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerComprobantePorIdPagoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "obtenerComprobantePorIdPagoResponse")
    public JAXBElement<ObtenerComprobantePorIdPagoResponse> createObtenerComprobantePorIdPagoResponse(ObtenerComprobantePorIdPagoResponse value) {
        return new JAXBElement<ObtenerComprobantePorIdPagoResponse>(_ObtenerComprobantePorIdPagoResponse_QNAME, ObtenerComprobantePorIdPagoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerDetalleAtencionPorRutPaciente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "obtenerDetalleAtencionPorRutPaciente")
    public JAXBElement<ObtenerDetalleAtencionPorRutPaciente> createObtenerDetalleAtencionPorRutPaciente(ObtenerDetalleAtencionPorRutPaciente value) {
        return new JAXBElement<ObtenerDetalleAtencionPorRutPaciente>(_ObtenerDetalleAtencionPorRutPaciente_QNAME, ObtenerDetalleAtencionPorRutPaciente.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerDetalleAtencionPorRutPacienteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "obtenerDetalleAtencionPorRutPacienteResponse")
    public JAXBElement<ObtenerDetalleAtencionPorRutPacienteResponse> createObtenerDetalleAtencionPorRutPacienteResponse(ObtenerDetalleAtencionPorRutPacienteResponse value) {
        return new JAXBElement<ObtenerDetalleAtencionPorRutPacienteResponse>(_ObtenerDetalleAtencionPorRutPacienteResponse_QNAME, ObtenerDetalleAtencionPorRutPacienteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerDeudasPorRut }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "obtenerDeudasPorRut")
    public JAXBElement<ObtenerDeudasPorRut> createObtenerDeudasPorRut(ObtenerDeudasPorRut value) {
        return new JAXBElement<ObtenerDeudasPorRut>(_ObtenerDeudasPorRut_QNAME, ObtenerDeudasPorRut.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerDeudasPorRutResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "obtenerDeudasPorRutResponse")
    public JAXBElement<ObtenerDeudasPorRutResponse> createObtenerDeudasPorRutResponse(ObtenerDeudasPorRutResponse value) {
        return new JAXBElement<ObtenerDeudasPorRutResponse>(_ObtenerDeudasPorRutResponse_QNAME, ObtenerDeudasPorRutResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerReserva }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "obtenerReserva")
    public JAXBElement<ObtenerReserva> createObtenerReserva(ObtenerReserva value) {
        return new JAXBElement<ObtenerReserva>(_ObtenerReserva_QNAME, ObtenerReserva.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerReservaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "obtenerReservaResponse")
    public JAXBElement<ObtenerReservaResponse> createObtenerReservaResponse(ObtenerReservaResponse value) {
        return new JAXBElement<ObtenerReservaResponse>(_ObtenerReservaResponse_QNAME, ObtenerReservaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProfesionalesPorEspecialidad }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "profesionalesPorEspecialidad")
    public JAXBElement<ProfesionalesPorEspecialidad> createProfesionalesPorEspecialidad(ProfesionalesPorEspecialidad value) {
        return new JAXBElement<ProfesionalesPorEspecialidad>(_ProfesionalesPorEspecialidad_QNAME, ProfesionalesPorEspecialidad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProfesionalesPorEspecialidadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "profesionalesPorEspecialidadResponse")
    public JAXBElement<ProfesionalesPorEspecialidadResponse> createProfesionalesPorEspecialidadResponse(ProfesionalesPorEspecialidadResponse value) {
        return new JAXBElement<ProfesionalesPorEspecialidadResponse>(_ProfesionalesPorEspecialidadResponse_QNAME, ProfesionalesPorEspecialidadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReservasPorPaciente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "reservasPorPaciente")
    public JAXBElement<ReservasPorPaciente> createReservasPorPaciente(ReservasPorPaciente value) {
        return new JAXBElement<ReservasPorPaciente>(_ReservasPorPaciente_QNAME, ReservasPorPaciente.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReservasPorPacienteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "reservasPorPacienteResponse")
    public JAXBElement<ReservasPorPacienteResponse> createReservasPorPacienteResponse(ReservasPorPacienteResponse value) {
        return new JAXBElement<ReservasPorPacienteResponse>(_ReservasPorPacienteResponse_QNAME, ReservasPorPacienteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SolicitarHce }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "solicitarHce")
    public JAXBElement<SolicitarHce> createSolicitarHce(SolicitarHce value) {
        return new JAXBElement<SolicitarHce>(_SolicitarHce_QNAME, SolicitarHce.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SolicitarHceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "solicitarHceResponse")
    public JAXBElement<SolicitarHceResponse> createSolicitarHceResponse(SolicitarHceResponse value) {
        return new JAXBElement<SolicitarHceResponse>(_SolicitarHceResponse_QNAME, SolicitarHceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidarUsuario }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "validarUsuario")
    public JAXBElement<ValidarUsuario> createValidarUsuario(ValidarUsuario value) {
        return new JAXBElement<ValidarUsuario>(_ValidarUsuario_QNAME, ValidarUsuario.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidarUsuarioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "validarUsuarioResponse")
    public JAXBElement<ValidarUsuarioResponse> createValidarUsuarioResponse(ValidarUsuarioResponse value) {
        return new JAXBElement<ValidarUsuarioResponse>(_ValidarUsuarioResponse_QNAME, ValidarUsuarioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerBusquedaHcePorPaciente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "verBusquedaHcePorPaciente")
    public JAXBElement<VerBusquedaHcePorPaciente> createVerBusquedaHcePorPaciente(VerBusquedaHcePorPaciente value) {
        return new JAXBElement<VerBusquedaHcePorPaciente>(_VerBusquedaHcePorPaciente_QNAME, VerBusquedaHcePorPaciente.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerBusquedaHcePorPacienteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "verBusquedaHcePorPacienteResponse")
    public JAXBElement<VerBusquedaHcePorPacienteResponse> createVerBusquedaHcePorPacienteResponse(VerBusquedaHcePorPacienteResponse value) {
        return new JAXBElement<VerBusquedaHcePorPacienteResponse>(_VerBusquedaHcePorPacienteResponse_QNAME, VerBusquedaHcePorPacienteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerBusquedaHcePorProfesional }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "verBusquedaHcePorProfesional")
    public JAXBElement<VerBusquedaHcePorProfesional> createVerBusquedaHcePorProfesional(VerBusquedaHcePorProfesional value) {
        return new JAXBElement<VerBusquedaHcePorProfesional>(_VerBusquedaHcePorProfesional_QNAME, VerBusquedaHcePorProfesional.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerBusquedaHcePorProfesionalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://capaServicio/", name = "verBusquedaHcePorProfesionalResponse")
    public JAXBElement<VerBusquedaHcePorProfesionalResponse> createVerBusquedaHcePorProfesionalResponse(VerBusquedaHcePorProfesionalResponse value) {
        return new JAXBElement<VerBusquedaHcePorProfesionalResponse>(_VerBusquedaHcePorProfesionalResponse_QNAME, VerBusquedaHcePorProfesionalResponse.class, null, value);
    }

}
