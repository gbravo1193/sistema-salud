
package capaservicio;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para recetarce complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="recetarce"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="rce" type="{http://capaServicio/}rce" minOccurs="0"/&gt;
 *         &lt;element name="receta" type="{http://capaServicio/}receta" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "recetarce", propOrder = {
    "id",
    "rce",
    "receta"
})
public class Recetarce {

    protected int id;
    protected Rce rce;
    protected Receta receta;

    /**
     * Obtiene el valor de la propiedad id.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad rce.
     * 
     * @return
     *     possible object is
     *     {@link Rce }
     *     
     */
    public Rce getRce() {
        return rce;
    }

    /**
     * Define el valor de la propiedad rce.
     * 
     * @param value
     *     allowed object is
     *     {@link Rce }
     *     
     */
    public void setRce(Rce value) {
        this.rce = value;
    }

    /**
     * Obtiene el valor de la propiedad receta.
     * 
     * @return
     *     possible object is
     *     {@link Receta }
     *     
     */
    public Receta getReceta() {
        return receta;
    }

    /**
     * Define el valor de la propiedad receta.
     * 
     * @param value
     *     allowed object is
     *     {@link Receta }
     *     
     */
    public void setReceta(Receta value) {
        this.receta = value;
    }

}
