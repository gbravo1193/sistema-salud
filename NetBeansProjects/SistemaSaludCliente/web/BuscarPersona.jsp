<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="i"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="js/jquery-1.12.0.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/Style.css">
<title>Buscar Persona</title>
</head>
<body>
  <jsp:include page="barraNavegacion.jsp"></jsp:include>



<div class="container">
<h1> Busqueda Persona</h1>
<form action="BuscarPersona" id=""BuscarPersona" method="post">
	<div class="form-group">
		<div class="row">
			<div class="col-md-3">
				<label>Rut Persona: </label>
				<input type="text" name="rut" class="form-control" required>
				
				<input type="submit" value="Buscar" class="btn btn-default">
			</div>
		</div>
	</div>
</form>

<table class="table table-bordered table-hover table-responsive">
	<tr class="success">
		<th>Rut</th>
		<th>Nombres</th>
		<th>Apellido Paterno</th>
		<th>Apellido Materno</th>
		<th>E-mail</th>
		<th>Fecha Nacimiento</th>
		<th>ID Facebook</th>
		<th>Acciones</th>
	</tr>

	<tr>
		<td>${rut}</td>
		<td>${nombres}</td>
		<td>${apellido_paterno}</td>
		<td>${apellido_materno}</td>
		<td>${email}</td>
		<td>${fecha_nacimiento}</td>
		<td>${identificadorfacebook}</td>
		<td>
			<table>
				<tr>
					<td><form action="EditarPersona.jsp" method="post">
						<input type="hidden" value="${id}" name="id">
						<input type="hidden" value="${rut}" name="rut">
						<input type="hidden" value="${nombres}" name="nombres">
						<input type="hidden" value="${apellido_paterno}" name="apellido_paterno">
						<input type="hidden" value="${apellido_materno}" name="apellido_materno">
						<input type="hidden" value="${email}" name="email">
						<input type="hidden" value="${fecha_nacimiento}" name="fecha_nacimiento">
						<input type="hidden" value="${identificadorfacebook}" name="idFacebook">
						<input type="hidden" value="${activo}" name="activo">
						<input type="hidden" value="${password}" name="password">
						<input type="submit" value="Editar" class="btn btn-warning">	
					</form>
					</td>
					<td>
					<form action="EliminarPersona" method="post">
						<input type="hidden" value="${id}" name="id">
						<input type="submit" value="Eliminar" class="btn btn-danger">	
					</form>
					</td>
					<td>
					<form action="CrearProfesional.jsp" method="post">
						<input type="hidden" value="${id}" name="idPersona">
						<input type="hidden" value="${nombres}" name="nombres">
						<input type="hidden" value="${apellido_paterno}" name="apellido_paterno">
						<input type="submit" value="Crear Profesional" class="btn btn-success">	
					</form>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<a href="Index.jsp" class="btn btn-default"><img src="icons/volver.png">Volver</a>

</div>
<script type="text/javascript" src="js/validador.js"></script>
			<script src="//oss.maxcdn.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->

<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script  type="text/javascript" src="ValidacionFrontend.js" ></script>

</body>
</html>