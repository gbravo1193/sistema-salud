<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="i"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="./css/bootstrap.min.css" rel="stylesheet">
        <link href="./css/customcss.css" rel="stylesheet">
        <script src="http://code.jquery.com/jquery-latest.js"></script>
        <script src="./js/bootstrap.min.js"></script>
        <script src="./js/validator.js"></script>
        <script type="text/javascript" src="./js/validarRUT.js"></script>

        <link rel="stylesheet" type="text/css" media="screen" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <link href="./css/prettify-1.0.css" rel="stylesheet">
        <link href="./css/base.css" rel="stylesheet">
        <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css"
              rel="stylesheet">

        <script type="text/javascript" src="//code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>

        <script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>

        <title>Agregar Reserva Medica</title>
    </head>
    <body>
  <jsp:include page="barraNavegacion.jsp"></jsp:include>

    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapse" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.jsp">Menu</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="index.jsp">Index<span class="sr-only">(current)</span></a></li>
                    <!--<li><a href="crearReservaMedica.jsp">Crear Reserva</a></li>-->
                </ul>
                <ul class="nav navbar-nav">
                    <li><a href="agregarReservaMedica.jsp">Reservar Hora<span class="sr-only">(current)</span></a></li>
                    <!--<li><a href="crearReservaMedica.jsp">Crear Reserva</a></li>-->
                </ul>
                <ul class="nav navbar-nav">
                    <li><a href="agregarTipoPrevision.jsp"><span class="sr-only">(current)</span></a></li>
                    <!--<li><a href="crearReservaMedica.jsp">Crear Reserva</a></li>-->
                </ul>
                <ul class="nav navbar-nav">
                    <li><a href="buscarReservasPorProfesional.jsp">Buscar Reservas<span class="sr-only">(current)</span></a></li>
                    <!--<li><a href="crearReservaMedica.jsp">Crear Reserva</a></li>-->
                </ul>
                <ul class="nav navbar-nav">
                    <li><a href="ServletListarEspecialidad"><span class="sr-only">(current)</span></a></li>
                    <!--<li><a href="crearReservaMedica.jsp">Crear Reserva</a></li>-->
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <form role="form" action="Login" method="get">
                            <button type="submit" class="li-btn li-btn-primary">
                                <span class="glyphicon glyphicon-log-out"> </span> Cerrar sesion
                            </button>
                        </form>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>
    </nav>

    <form class="form-horizontal" action="ServletAgregarReservaMedica" method="post" id="ServletAgregarReservaMedica">
        <center>
            <div class="row">
                <div class="col-sm-12">
                    <center>
                        <h2>
                            <h1 class="tituloForm"><span class="label label-info">Agregar Reserva Medica</span></h1>
                        </h2>
                    </center>
                </div>
            </div>
            <div class = "row">
                <div class="col-sm-2">
                </div>
                <div class="col-sm-8">
                    <div class="panel panel-primary">

                        <div class="panel-body">
                            <div class="row">
                                <!-- FORMULARIO Agregar Hora Medica-->
                                <form class="form-horizontal" action="ServletBuscarReservasPorPaciente" method="post" id="ServletBuscarReservasPorPaciente">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="rutProfesional" class="col-sm-4 control-label"> Rut profesional: </label>
                                            <div class="col-sm-8">
                                                <input type="text" placeholder="Ingrese Rut Profesional" required oninput="checkRut(this)" maxlength="10" onkeypress="return validarNumero(event)" class="form-control" id="rutProfesional" name="rutProfesional" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="rutAdministrador" class="col-sm-4 control-label"> Rut administrador: </label>
                                            <div class="col-sm-8">
                                                <input type="text" placeholder="Ingrese Rut Administrador" required oninput="checkRut(this)" class="form-control" maxlength="10" onkeypress="return validarNumero(event)" id="rutAdministrador" name="rutAdministrador" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="rutPaciente" class="col-sm-4 control-label"> Rut Paciente: </label>
                                            <div class="col-sm-8">
                                                <input type="text" placeholder="Ingrese Rut Paciente" required oninput="checkRut(this)" class="form-control" id="rutPaciente" maxlength="10" onkeypress="return validarNumero(event)" name="rutPaciente" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="fechaReserva" class="col-sm-4 control-label">Fecha Reserva: </label>

                                            <div class="container">
                                                <div class="row">
                                                    <div class="container">
                                                        <div class='col-sm-6'>
                                                            <div class="form-group">
                                                                <div class='input-group date' id="fechaReserva" >
                                                                    <input type='text' class="form-control" name="fechaReserva" id="fechaReserva" maxlength="10" onkeypress="return validarNumero(event)" placeholder="Ingrese fecha reserva" required/>
                                                                    <span class="input-group-addon">
                                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <script type="text/javascript">
                                                        $(function () {
                                                            $('#fechaReserva').datetimepicker();
                                                        });
                                                    </script>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                            </div>
                            ${respuesta}
                        </div>
                    </div>
                </div>
            </div>

            <div class = "row">
                <div class="col-sm-2">
                </div>
                <div class="col-sm-8">
                    <div class="panel panel-primary">

                        <div class="panel-body">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label for="comment">Comentario:</label>
                                    <textarea class="form-control" rows="5" id="motivoConsulta" maxlength="200" name="motivoConsulta" placeholder="Comentar motivo de la consulta"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </center>
        <center><div class="row">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-success">Agregar Reserva</button>
                    <button type="button" id="buttonClean" class="btn btn-default">Limpiar</button>
                </div>
            </div></center>
    </form>
    <script  type="text/javascript" src="ValidacionFrontend.js" ></script>
    <script type="text/javascript" src="js/validador.js"></script>
    <script src="//oss.maxcdn.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>
</body>

</html>