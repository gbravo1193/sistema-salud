<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/bootstrap.min.css" rel="stylesheet">
<link href="./css/customcss.css" rel="stylesheet">
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script src="./js/bootstrap.min.js"></script>
<script src="./js/validator.js"></script>
<title>Comprobante de Reserva</title>
</head>
<body>
	  <jsp:include page="barraNavegacion.jsp"></jsp:include>

	
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h2 class="panel-title">Comprobante de Reserva</h2>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-9">
							<div class="form-group">
								<label for="rutPaciente" class="control-label">Rut: ${rutPaciente}"</label>
							</div>
							<div class="form-group">
								<label for="nombrePaciente" class="control-label">Nombre Paciente: ${nombrePaciente}</label>
							</div>
							<div class="form-group">
								<label for="motivoConsulta" class="control-label">Motivo: ${motivoConsulta}</label>
							</div>
													
							<div class="form-group">
								<label for="nombreProfesional" class="control-label"> Rut Profesional: ${rutProfesional}</label>
							</div>
							<div class="form-group">
								<label for="horario" class="control-label">Fecha: ${fechaConsulta}</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>