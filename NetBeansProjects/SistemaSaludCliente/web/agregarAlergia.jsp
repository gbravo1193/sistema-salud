<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Agregar Alergia</title>
<link rel="stylesheet" href="css/estiloVistas.css">

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet"></link>


</head>
<body>
  <jsp:include page="barraNavegacion.jsp"></jsp:include>

 <div class="container" >


<center><h1 class=""><span class="label label-info">Nueva Alergia</span></h1></center><br>

<div id="formAddAlergia">

<form id="formulario" action="ServletAgregarAlergia" method="post" class="form-horizontal" role="form" autocomplete="off">
	<div class="form-group">
	
	<label for="pacienteid">Paciente ID</label>
	
	
	<input type="text" name="pacienteid" id="pacienteid" class="form-control required" onkeypress="return validarNumero(event)" placeholder="Ingrese ID paciente">
	
	</div>
	<div class="form-group">
	
	<label for="rceid">RCE ID</label>
	
	
	<input type="text" name="rceid" id="rceid" class="form-control required" onkeypress="return validarNumero(event)" placeholder="Ingrese RCE ID">
	
	</div>
	<div class="form-group">
	
	<label for="detalle">Detalle Alergia</label>
	
	
	<textarea row="6" col="50"  name="detalle" id="detalle" class="form-control required" placeholder="Ingrese detalle alergia." maxlength="150"></textarea>
	
	</div>
	
	<center><div class="form-group">
	<button type="submit" class="btn btn-default">Agregar</button>
    <button type="button" id="buttonClean" class="btn btn-default">Limpiar</button>
	</div></center>
	
</form>

</div>

</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->

<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script  type="text/javascript" src="ValidacionFrontend.js" ></script>




</div>


</body>
</html>
