<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="i"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript" src="js/jquery-1.12.0.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/Style.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Crear Paciente</title>
</head>
<body>
  <jsp:include page="barraNavegacion.jsp"></jsp:include>

	<%
	String idPersona=(request.getParameter("idPersona"));
	String nombres= request.getParameter("nombres");
	String apellido_paterno= request.getParameter("apellido_paterno");
	%>
	<div class="container" >
		<h1 class="tituloForm"><span class="label label-info">Crear Paciente</span></h1>
		<div class="col-md-5">
	<form action="CrearPaciente" id="CrearPaciente" method="post">
	<div class="form-group">
		<label>ID Persona:</label>
               <input type="text"  name="rutPersona" class="form-control">
                <input type="hidden"  name="idPersona">
	</div>
	
	<div class="form-group">
		<label>Nombre:</label>
		<input type="text" name="nombrePersona" class="form-control"  disabled>
	</div>
	
	<div class="form-group">
		<label>Previsión:</label>
		<select name="tipoPrevision" class="form-control">
			<option value="1">Fonasa</option>
			<option value="2">Isapre</option>
		</select>
	</div>
	
	<div class="form-group">
		<label>Descripción Paciente:</label>
		<input type="text" name="descripcion" class="form-control">
	</div>
	
	<div class="form-group">
		<label>Activo:</label>
		<select name="activo" class="form-control">
			<option value="1">Si</option>
			<option value="0">No</option>
		</select>
	</div>
	
	
	<div class="btnForm form-group">
		
	<input type="submit" value="Ingresar" class="btn btn-default">
	<a class="btn btn-default" href="Index.jsp">Cancelar</a>
	</div>
	
	</form>
	</div>
	</div>
                <script type="text/javascript" src="js/creadorPaciente.js"></script>

</body>
</html>