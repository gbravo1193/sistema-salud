<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="bootstrap-3.3.6/dist/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="bootstrap-3.3.6/dist/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script  type="text/javascript" src="bootstrap-3.3.6/dist/js/bootstrap.min.js" ></script>

<!--  css  -->
<link rel="stylesheet" href="css/style.css">