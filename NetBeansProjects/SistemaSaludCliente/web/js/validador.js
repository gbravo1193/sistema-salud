$(document).ready(function() {
	$('#ServletAgregarTipoPrevision').bootstrapValidator({
		feedbackIcons : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			tipoPrevision : {
				validators : {
					notEmpty : {
						message : 'El tipo de previsión es requerido'
					},
					stringLength : {
						max : 12,
						message : 'El tipo de previsión debe contener máximo 12 caracteres'
					}
				}
			}
		}
	});	
	
	$('#ServletRegistrarHoraMedica').bootstrapValidator({
		feedbackIcons : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			rutAdministrador : {
				validators : {
					notEmpty : {
						message : 'El rut del admnistrador es requerido'
					},
					stringLength : {
						max : 12,
						message : 'El rut debe contener máximo 12 caracteres'
					}
				}
			},
			rutProfesional : {
				validators : {
					notEmpty : {
						message : 'El rut del profesional es requerido'
					},
					stringLength : {
						max : 12,
						message : 'El rut debe contener máximo 12 caracteres'
					}
				}
			},
			datetimepicker7 : {
				validators : {
					notEmpty : {
						message : 'La fecha y hora es requerida'
					},
					stringLength : {
						max : 21,
						message : 'la fecha y hora deben contener máximo 21 caracteres'
					}
				}
			}
		}
	});
});
	