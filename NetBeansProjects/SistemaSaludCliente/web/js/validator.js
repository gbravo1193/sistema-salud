$(document).ready(function() {
	$('#ServletRegistrarHoraMedica').bootstrapValidator({
		feedbackIcons : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			rutAdministrador : {
				validators : {
					notEmpty : {
						message : 'El rut del admnistrador es requerido'
					},
					stringLength : {
						max : 12,
						message : 'El rut debe contener máximo 12 caracteres'
					}
				}
			},
			rutProfesional : {
				validators : {
					notEmpty : {
						message : 'El rut del profesional es requerido'
					},
					stringLength : {
						max : 12,
						message : 'El rut debe contener máximo 12 caracteres'
					}
				}
			},
			datetimepicker7 : {
				validators : {
					notEmpty : {
						message : 'La fecha y hora es requerida'
					},
					stringLength : {
						max : 21,
						message : 'la fecha y hora deben contener máximo 21 caracteres'
					}
				}
			}
		}
	});
	
	$('#ServletReservarHoraMedica').bootstrapValidator({
		feedbackIcons : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			rut : {
				validators : {
					notEmpty : {
						message : 'El rut es requerido'
					},
					stringLength : {
						max : 12,
						message : 'El rut debe contener máximo 12 caracteres'
					}
				}
			}
		}
	});
	
	$('#ServletAnularReservaMedica').bootstrapValidator({
		feedbackIcons : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			codigo: {
                validators: {
                    notEmpty: {
                        message: 'El codigo es requerido'
                    },
                    stringLength: {
                        max: 4,
                        message: 'El id debe contener maximo 4 caracteres'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: 'El id solo puede contener números'
                    }
                }
			}
		}
	});
	
	$('#ServletCrearReservarMedica').bootstrapValidator({
		feedbackIcons : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			comentario : {
				validators : {
					notEmpty : {
						message : 'El comentario es requerido'
					},
					stringLength : {
						max : 500,
						message : 'El comentario debe contener máximo 500 caracteres'
					}
				}
			}
		}
	});
	
	$('#ServletLogin').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
        	rut : {
				validators : {
					notEmpty : {
						message : 'El rut es requerido'
					},
					stringLength : {
						max : 12,
						message : 'El rut debe contener máximo 12 caracteres'
					}
				}
			},
            password: {
                validators: {
                    notEmpty: {
                        message: 'La contraseña es requerida'
                    },
                    stringLength: {
                        max: 20,
                        message: 'La contraseña debe contener máximo 20 caracteres'
                    }
                }
            }
        }
	});
	
});