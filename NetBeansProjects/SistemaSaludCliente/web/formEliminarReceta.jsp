<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Form Eliminar Receta</title>
<link rel="stylesheet" href="css/estiloVistas.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">


</head>

<body>
  <jsp:include page="barraNavegacion.jsp"></jsp:include>


<div class="container">
<center><h1><span class="label label-info">Eliminar Receta</span></h1></center><br>


<div id="formDeleteAlergia">

<form id="formulario" action="ServletEliminarReceta" method="post" class="form-horizontal" role="form" autocomplete="off">
	<div class="form-group">
	
	<label for="pacienteid">Paciente ID</label>
	
	
	<input type="text" name="pacienteid" id="pacienteid" class="form-control required" onkeypress="return validarNumero(event)" placeholder="Ingrese alergia ID.">
	
	</div>
	<div class="form-group">
	
	<label for="rceid">RCE ID</label>
	
	
	<input type="text" name="rceid" id="rceid" class="form-control required" onkeypress="return validarNumero(event)" placeholder="Ingrese RCE ID.">
	
	</div>
	
	<div class="form-group">
	
	<label for="recetaid">Receta ID</label>
	
	
	<input type="text"  name="recetaid" id="recetaid" class="form-control required" onkeypress="return validarNumero(event)" placeholder="Ingrese Receta ID.">
	
	</div>
	
	<center><div class="form-group">
	<button type="submit" class="btn btn-default">Eliminar</button>
    <button type="button" id="buttonClean" class="btn btn-default">Limpiar</button>
	</div></center>
	
</form>

</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->

<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script  type="text/javascript" src="ValidacionFrontend.js" ></script>


</div>


</body>
</html>
