<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="i"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript" src="js/jquery-1.12.0.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/Style.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Crear Paciente</title>
</head>
<body>
      <jsp:include page="barraNavegacion.jsp"></jsp:include>

<h1>Crear Paciente</h1>
<form action="BuscarPersonaPaciente" method="post">
	<div class="form-group">
		<div class="row">
			<div class="col-md-3">
				<label>Rut Persona: </label>
				<input type="text" name="rut" class="form-control" required>
				<br>
				<input type="submit" value="Buscar" class="btn btn-default">
			</div>
		</div>
	</div>
</form>

<table class="table table-bordered table-hover table-responsive">
	<tr class="success">
		<th>Rut</th>
		<th>Nombres</th>
		<th>Apellido Paterno</th>
	</tr>

	<tr>
		<td>${rut}</td>
		<td>${nombres}</td>
		<td>${apellido_paterno}</td>
		<td>
			<table>
				<tr>
					<td>
					<form action="CreadorPaciente.jsp" method="post">
						<input type="hidden" value="${id}" name="idPersona">
						<input type="hidden" value="${nombres}" name="nombres">
						<input type="hidden" value="${apellido_paterno}" name="apellido_paterno">
						<input type="submit" value="Crear Profesional" class="btn btn-default">	
					</form>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<a href="Index.jsp" class="btn btn-default"><img src="icons/volver.png">Volver</a>
</body>
</html>