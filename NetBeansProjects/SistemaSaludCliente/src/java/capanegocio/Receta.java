package capanegocio;


public class Receta {
	
	private int id;
	private int pacienteid;
	private int rceid;
	private String detalle;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the pacienteid
	 */
	public int getPacienteid() {
		return pacienteid;
	}
	/**
	 * @param pacienteid the pacienteid to set
	 */
	public void setPacienteid(int pacienteid) {
		this.pacienteid = pacienteid;
	}
	/**
	 * @return the rceid
	 */
	public int getRceid() {
		return rceid;
	}
	/**
	 * @param rceid the rceid to set
	 */
	public void setRceid(int rceid) {
		this.rceid = rceid;
	}
	/**
	 * @return the detalle
	 */
	public String getDetalle() {
		return detalle;
	}
	/**
	 * @param detalle the detalle to set
	 */
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	
	
	//contructores 
	public Receta(){
	  super();
	}
	public Receta(int pacienteId,int rceId,String detalle){
            this.pacienteid=pacienteId;
            this.rceid=rceId;
            this.detalle= detalle;
	}


}