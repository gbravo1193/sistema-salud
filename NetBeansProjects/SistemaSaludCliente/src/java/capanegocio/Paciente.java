package capanegocio;

public class Paciente {

	private int id;
	private int id_persona;
    private Persona persona;

	public int getId() {
		return this.id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	public Persona getPersona() {
            return this.persona;
	}

	/**
	 * 
	 * @param persona
	 */
	public void setPersona(Persona persona) {
                this.persona = persona;
	}

	public int getId_persona() {
		return this.id_persona;
	}

	/**
	 * 
	 * @param id_persona
	 */
	public void setId_persona(int id_persona) {
		this.id_persona = id_persona;
	}

}