package capaNegocio;

import java.util.Collection;

/**
 * 
 * @authors César Barriga - Boris Morales
 * 
 * Clase relacionada a la lógica de negocio de la aplicación referente a especialidades
 *
 */

public class Especialidad {

	Collection<Profesional> profesional;
	private int id;
	private String especialidad;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getEspecialidad() {
		return especialidad;
	}
	
	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}

	public Collection<Profesional> getProfesional() {
		return profesional;
	}

	public void setProfesional(Collection<Profesional> profesional) {
		this.profesional = profesional;
	}
	
	
}
