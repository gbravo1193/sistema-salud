package capanegocio;

import java.util.Date;

public class Reserva {

	private int id;
	private Date fecha;
	private String motivoConsulta;
	private int id_paciente;
    private Paciente paciente;
    private HoraMedica horaMedica;
    private int hora_medica_id;

	public int getId() {
		return this.id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	public Date getFecha() {
		return this.fecha;
	}

	/**
	 * 
	 * @param fecha
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getMotivoConsulta() {
		return this.motivoConsulta;
	}

	/**
	 * 
	 * @param motivoConsulta
	 */
	public void setMotivoConsulta(String motivoConsulta) {
		this.motivoConsulta = motivoConsulta;
	}

	public Paciente getPaciente() {
                return this.paciente;
	}

	/**
	 * 
	 * @param paciente
	 */
	public void setPaciente(Paciente paciente) {
             this.paciente = paciente;
	}

	public HoraMedica getHoraMedica() {
                return this.horaMedica;
	}

	/**
	 * 
	 * @param horaMedica
	 */
	public void setHoraMedica(HoraMedica horaMedica) {
            this.horaMedica = horaMedica;
	}

	public int getId_paciente() {
		return this.id_paciente;
	}

	/**
	 * 
	 * @param id_paciente
	 */
	public void setId_paciente(int id_paciente) {
		this.id_paciente = id_paciente;
	}
        
        public int getHora_medica_id() {
		return this.hora_medica_id;
	}

	/**
	 * 
	 * @param hora_medica_id
	 */
	public void setHora_medica_id(int hora_medica_id) {
		this.hora_medica_id = hora_medica_id;
	}
	
	public Reserva IngresarReservaLista(String[] x){
		
		String[] ContenidoReserva = x;
		Reserva reserva = new Reserva();
		//System.out.println("Comienzo Reserva");
		
		for(int i =0;i<ContenidoReserva.length;i++){
			ContenidoReserva[i] = ContenidoReserva[i].replace("{", "");
        	ContenidoReserva[i] = ContenidoReserva[i].replace("}", "");
        	ContenidoReserva[i] = ContenidoReserva[i].replace("[", "");
        	ContenidoReserva[i] = ContenidoReserva[i].replace("]", "");
        	//System.out.println(ContenidoReserva[i]);
		}
		
		
		for(int i = 0 ; i<ContenidoReserva.length;i++){
			 String[] contenido = ContenidoReserva[i].split(":");
			//System.out.println(ContenidoReserva[i]);
			if(i == 3){
				reserva.setMotivoConsulta(contenido[1]);
			}
			
			
			
		}
		
		return reserva;
	}

}