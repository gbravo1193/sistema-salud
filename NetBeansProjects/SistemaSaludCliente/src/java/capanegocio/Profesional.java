package capanegocio;

public class Profesional {

	private int id;
        private Persona persona;
        private int persona_id;

    public int getPersona_id() {
        return persona_id;
    }

    public void setPersona_id(int persona_id) {
        this.persona_id = persona_id;
    }

	public int getId() {
		return this.id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	public Persona getPersona() {
            return this.persona;
	}

	/**
	 * 
	 * @param persona
	 */
	public void setPersona(Persona persona) {
            this.persona = persona;
	}

}