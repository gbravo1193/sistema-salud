package capanegocio;

import java.util.Date;

public class HoraMedica {

    private int id;
    private Date hora_inicio;
    private Profesional profesional;
    private int profesional_id;

    public int getProfesional_id() {
        return profesional_id;
    }

    public void setProfesional_id(int profesional_id) {
        this.profesional_id = profesional_id;
    }

    public int getId() {
        return this.id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    public Date getHora_inicio() {
        return this.hora_inicio;
    }

    /**
     *
     * @param hora_inicio
     */
    public void setHora_inicio(Date hora_inicio) {
        this.hora_inicio = hora_inicio;
    }

    public Profesional getProfesional() {
        return this.profesional;
    }

    /**
     *
     * @param profesional
     */
    public void setProfesional(Profesional profesional) {
        this.profesional = profesional;
    }

}
