package capanegocio;

public class Box {

	private int id;
	private String codigoBox;

	public int getId() {
		return this.id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	public String getCodigoBox() {
		return this.codigoBox;
	}

	/**
	 * 
	 * @param codigoBox
	 */
	public void setCodigoBox(String codigoBox) {
		this.codigoBox = codigoBox;
	}

}