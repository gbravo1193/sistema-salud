/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package capanegocio;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;



public class Consulta_hce {

    private int id;
    private int personaid_profesional;
    private int personaid_paciente;
    private Date fecha;
    private Persona persona_profesional;
    private Persona persona_paciente;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPersonaid_profesional() {
        return personaid_profesional;
    }

    public void setPersonaid_profesional(int personaid_profesional) {
        this.personaid_profesional = personaid_profesional;
    }

    public int getPersonaid_paciente() {
        return personaid_paciente;
    }

    public void setPersonaid_paciente(int personaid_paciente) {
        this.personaid_paciente = personaid_paciente;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Persona getPersona_profesional() {
        return persona_profesional;
    }

    public void setPersona_profesional(Persona persona_profesional) {
        this.persona_profesional = persona_profesional;
    }

    public Persona getPersona_paciente() {
        return persona_paciente;
    }

    public void setPersona_paciente(Persona persona_paciente) {
        this.persona_paciente = persona_paciente;
    }

   
}
