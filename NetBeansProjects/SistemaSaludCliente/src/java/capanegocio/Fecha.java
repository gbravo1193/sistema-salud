/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package capanegocio;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Manuel
 */
public class Fecha {
    
    public Date convertStingToDate(String s) {
        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy");
        Date fecha = null;
        try {

            fecha = formatoDelTexto.parse(s);

        } catch (ParseException ex) {

            ex.printStackTrace();

        }

        return fecha;
    }
    
}
