package capanegocio;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Rce {

    private int id;
    private int persona_id_realiza_examen;
    private int cerrado;
    private int reserva_id;
    private int box_id;
    private int procedencia_id;
    private int destino_id;
    private String indicaciones;
    private String observaciones;
    private String anamnesis;
    private Date fechaCierre;
    private Reserva reserva;
    private Box box;
    private Destino destino;
    private Procedencia procedencia;
    private String busqueda = "";

    public int getId() {
        return this.id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    public int getReserva_id() {
        return this.reserva_id;
    }

    /**
     *
     * @param reserva_id
     */
    public void setReserva_id(int reserva_id) {
        this.reserva_id = reserva_id;
    }

    public int getPersona_id_realiza_examen() {
        return this.persona_id_realiza_examen;
    }

    /**
     *
     * @param persona_id_realiza_examen
     */
    public void setPersona_id_realiza_examen(int persona_id_realiza_examen) {
        this.persona_id_realiza_examen = persona_id_realiza_examen;
    }

    public int getCerrado() {
        return this.cerrado;
    }

    /**
     *
     * @param cerrado
     */
    public void setCerrado(int cerrado) {
        this.cerrado = cerrado;
    }

    public String getIndicaciones() {
        return this.indicaciones;
    }

    /**
     *
     * @param indicaciones
     */
    public void setIndicaciones(String indicaciones) {
        this.indicaciones = indicaciones;
    }

    public String getObservaciones() {
        return this.observaciones;
    }

    /**
     *
     * @param observaciones
     */
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getAnamnesis() {
        return this.anamnesis;
    }

    /**
     *
     * @param anamnesis
     */
    public void setAnamnesis(String anamnesis) {
        this.anamnesis = anamnesis;
    }

    public Date getFechaCierre() {
        return this.fechaCierre;
    }

    /**
     *
     * @param fechaCierre
     */
    public void setFechaCierre(Date fechaCierre) {
        this.fechaCierre = fechaCierre;
    }

    public Reserva getReserva() {
        return this.reserva;
    }

    /**
     *
     * @param reserva
     */
    public void setReserva(Reserva reserva) {
        this.reserva = reserva;
    }

    public Box getBox() {
        return this.box;
    }

    /**
     *
     * @param box
     */
    public void setBox(Box box) {
        this.box = box;
    }

    public Destino getDestino() {
        return this.destino;
    }

    /**
     *
     * @param destino
     */
    public void setDestino(Destino destino) {
        this.destino = destino;
    }

    public Procedencia getProcedencia() {
        return this.procedencia;
    }

    /**
     *
     * @param procedencia
     */
    public void setProcedencia(Procedencia procedencia) {
        this.procedencia = procedencia;
    }

    public void setBox_id(int box_id) {
        this.box_id = box_id;
    }

    public void setDestino_id(int destino_id) {
        this.destino_id = destino_id;
    }

    public void setProcedencia_id(int procedencia_id) {
        this.procedencia_id = procedencia_id;
    }

    public int getBox_id() {
        return this.box_id;
    }

    public int getDestino_id() {
        return this.destino_id;
    }

    public int getProcedencia_id() {
        return this.procedencia_id;
    }
    

    
    

}
