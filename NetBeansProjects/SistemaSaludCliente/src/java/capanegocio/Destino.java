package capanegocio;

public class Destino {

	private int id;
	private String detalle;

	public int getId() {
		return this.id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	public String getDetalle() {
		return this.detalle;
	}

	/**
	 * 
	 * @param detalle
	 */
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

}