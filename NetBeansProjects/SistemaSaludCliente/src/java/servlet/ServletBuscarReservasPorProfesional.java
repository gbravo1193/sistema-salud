package servlet;

import capaservicio.PersistentException_Exception;
import capaservicio.Service;
import capaservicio.Service_Service;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



/**
 * Servlet implementation class ServletBuscarReservasPorProfesional
 */
@WebServlet("/ServletBuscarReservasPorProfesional")
public class ServletBuscarReservasPorProfesional extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletBuscarReservasPorProfesional() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		HttpSession session = request.getSession();
		session.invalidate();
		RequestDispatcher rs = request.getRequestDispatcher("Login.jsp");
		request.setAttribute("LoginStatus",	" Error, No se aceptan peticiones GET");
		rs.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String resultadoJson = "";
		String respuesta = "";
		
		String rutProfesional = request.getParameter("rutProfesional");
		String fechaInicio = request.getParameter("fechaInicio");
		String fechaTermino = request.getParameter("fechaTermino");
				
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");
		String fechaInicioFormat = sdf.format(new Date(fechaInicio));
		String fechaTerminoFormat = sdf2.format(new Date(fechaTermino));
		
		Service_Service res = new Service_Service();
		Service port = res.getServicePort();
            try {
                //try {
                resultadoJson = port.buscarReservasPorProfesional(rutProfesional, fechaInicio, fechaTermino);
                
            } catch (PersistentException_Exception ex) {
                Logger.getLogger(ServletBuscarReservasPorProfesional.class.getName()).log(Level.SEVERE, null, ex);
            }
                                    request.setAttribute("respBuscResProf", resultadoJson);

                        request.getRequestDispatcher("buscarReservasPorProfesional.jsp").forward(request, response);

			System.out.println("Resultado: \n" + resultadoJson);
		//} catch (ParseException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		//}
		
		/*Gson gson = new Gson();
		List<Reserva> res = gson.fromJson(resultadoJson, new TypeToken <List<Reserva>>(){}.getType());
		
		for (Reserva reserva:res){
			System.out.println("Id: " + reserva.getId());
			//System.out.println("Id: " + reserva.getPaciente());
		}*/
		
	}

}
