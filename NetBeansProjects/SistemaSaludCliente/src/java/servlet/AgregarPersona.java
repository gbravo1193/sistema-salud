package servlet;

import capaservicio.PersistentException_Exception;
import capaservicio.Service;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import capaservicio.Service_Service;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * Servlet implementation class AgregarPersona
 */
@WebServlet("/AgregarPersona")
public class AgregarPersona extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AgregarPersona() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		capaservicio.Service_Service sp= new Service_Service();
		Service s = sp.getServicePort();
		String rut= request.getParameter("rut");
		String nombres= request.getParameter("nombres");
		String apellido_paterno= request.getParameter("apellidoPaterno");
		String apellido_materno= request.getParameter("apellidoMaterno");
		String email= request.getParameter("email");
		String password= request.getParameter("password");
		
		SimpleDateFormat sdf= new SimpleDateFormat("yyyy-mm-dd");
		
		String fString= request.getParameter("fecha_nacimiento");
		
		
		
		String identificadorfacebook= request.getParameter("idFacebook");
		int activo= Integer.parseInt(request.getParameter("activo"));
		
		
		if(rut==null || rut.trim().length()==0 || nombres==null || nombres.trim().length()==0 || 
				apellido_paterno==null || apellido_paterno.trim().length()==0 || apellido_materno==null || apellido_materno.trim().length()==0 ||
				email==null || email.trim().length()==0 || password==null || password.trim().length()==0 || fString==null || fString.trim().length()==0 ||
				identificadorfacebook==null || identificadorfacebook.trim().length()==0){
			
			System.out.println("Existen valores nulos");
		}
		else{	
                    try {
                        GregorianCalendar c = new GregorianCalendar();
                        c.setTime(sdf.parse(fString));
                        XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
                        s.agregarPersona(rut, nombres, apellido_paterno, apellido_materno, email, date2, identificadorfacebook, activo, password);
                    } catch (ParseException ex) {
                        Logger.getLogger(AgregarPersona.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (DatatypeConfigurationException ex) {
                        Logger.getLogger(AgregarPersona.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (PersistentException_Exception ex) {
                        Logger.getLogger(AgregarPersona.class.getName()).log(Level.SEVERE, null, ex);
                    }
		}
		
		request.getRequestDispatcher("/index.jsp").forward(request, response);
	}
}
