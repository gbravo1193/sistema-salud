package servlet;

import capaservicio.PersistentException_Exception;
import capaservicio.Service;
import capaservicio.Service_Service;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ServletEliminarReceta
 */
@WebServlet("/ServletEliminarReceta")
public class ServletEliminarReceta extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletEliminarReceta() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		//formulario
		String pacienteid=request.getParameter("pacienteid");
		String rceid=request.getParameter("rceid");
		String recetaid=request.getParameter("recetaid");
		
		//procesamiento
                Service_Service ss= new Service_Service();
                Service s = ss.getServicePort();
	                String resultado=null;
            try {
                resultado = s.eliminarReceta(Integer.parseInt(recetaid));
            } catch (PersistentException_Exception ex) {
                Logger.getLogger(ServletEliminarReceta.class.getName()).log(Level.SEVERE, null, ex);
            }
       
       
       request.getSession().setAttribute("r", resultado);
	   response.sendRedirect("index.jsp");
		 
       
		
		
		
		
	
		
		
	
	
	
	
	
	
	}

}
