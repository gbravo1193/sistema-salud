package servlet;

import java.io.IOException;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import capanegocio.Alergia;
import capaservicio.Service;
import capaservicio.Service_Service;



/**
 * Servlet implementation class agregarAlergia
 */
@WebServlet("/ServletAgregarAlergia")
public class ServletAgregarAlergia extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletAgregarAlergia() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
	
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
		 
      /*  RequestDispatcher dispatcher= request.getRequestDispatcher("");
        dispatcher.forward(request, response);*/
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		//PARAMETROS DEL FORMULARIO
		String alergia=request.getParameter("detalle");
		String rceid=request.getParameter("rceid");
		String pacienteid=request.getParameter("pacienteid");
		
		if (alergia!=null && rceid!=null && pacienteid!=null)
		{
		
		
		//NUEVA ALERGIA
		Alergia nuevaAlergia=new Alergia();

	
		
		nuevaAlergia.setDetalle(alergia);

		int id_rce;
		id_rce=Integer.parseInt(rceid);
		
		nuevaAlergia.setRceid(id_rce);
		
		int id_paciente;
		id_paciente=Integer.parseInt(pacienteid);
		nuevaAlergia.setPacienteid(id_paciente);
		
		// invocando proxy  en la capa servicio  desde la capa de negocio
                Service_Service ss= new Service_Service();
                Service s = ss.getServicePort();
                
		String r= s.agregarAlergia(Integer.parseInt(rceid),alergia);
		request.getSession().setAttribute("respuesta", r);
        response.sendRedirect("index.jsp");
		}
		
		else  {
			 String error="Operacion no Valida";
		request.getSession().setAttribute("respuesta", error);
        response.sendRedirect("index.jsp");
		}
			
	}

}
