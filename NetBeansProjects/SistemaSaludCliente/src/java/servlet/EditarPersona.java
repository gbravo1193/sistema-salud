package servlet;

import capaservicio.PersistentException_Exception;
import capaservicio.Service;
import capaservicio.Service_Service;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * Servlet implementation class EditarPersona
 */
@WebServlet("/EditarPersona")
public class EditarPersona extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditarPersona() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        response.getWriter().append("Served at: ").append(request.getContextPath());
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPut(request, response);
    }

    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Service_Service sp = new Service_Service();
        Service s = sp.getServicePort();
        int id = Integer.parseInt(request.getParameter("id"));
        String rut = request.getParameter("rut");
        String nombres = request.getParameter("nombres");
        String apellido_paterno = request.getParameter("apellidoPaterno");
        String apellido_materno = request.getParameter("apellidoMaterno");
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");

        String fString = request.getParameter("fecha_nacimiento");
        GregorianCalendar c = new GregorianCalendar();
        try {
            c.setTime(sdf.parse(fString));
        } catch (ParseException ex) {
            Logger.getLogger(EditarPersona.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
             String identificadorfacebook = request.getParameter("idFacebook");
        int activo = Integer.parseInt(request.getParameter("activo"));

        s.editarPersona(id, rut, nombres, apellido_paterno, apellido_materno, email, date2, identificadorfacebook, activo, password);
        
        request.getRequestDispatcher("/index.jsp").forward(request, response);
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(EditarPersona.class.getName()).log(Level.SEVERE, null, ex);
        } catch (PersistentException_Exception ex) {
            Logger.getLogger(EditarPersona.class.getName()).log(Level.SEVERE, null, ex);
        }

     

       
    }

    public static Calendar DateToCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }
}
