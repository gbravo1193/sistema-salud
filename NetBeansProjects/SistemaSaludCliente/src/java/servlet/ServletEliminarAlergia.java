package servlet;

import capaservicio.Service;
import capaservicio.Service_Service;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import capanegocio.Alergia;


/**
 * Servlet implementation class ServeletEliminarAlergia
 */
@WebServlet("/ServletEliminarAlergia")
public class ServletEliminarAlergia extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletEliminarAlergia() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
				  
		  //PARAMETROS FORMULARIO
		  String rceid=null;
		  rceid=request.getParameter("rceid");
		  
		  String alergiaid=null;
		  alergiaid=request.getParameter("alergiaid");
		  
		  Service_Service ss= new Service_Service();
                Service s = ss.getServicePort();
                String r=s.eliminarAlergiaID(Integer.parseInt(rceid), Integer.parseInt(alergiaid));
                 


		 request.getSession().setAttribute("r", r);
	     response.sendRedirect("index.jsp");
		 
	}

}
