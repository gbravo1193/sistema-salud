package servlet;

import capaservicio.Service;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import capaservicio.Service_Service;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Servlet implementation class BuscarPersona
 */
@WebServlet("/BuscarPersona")
public class BuscarPersona extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuscarPersona() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String rut= request.getParameter("rut");
		Service_Service res= new Service_Service();
                Service port = res.getServicePort();
		
		String resultado= port.buscarPersonaPorRut(rut);
		
		try {
				final JSONObject jsonObject= new JSONObject(resultado);
				request.setAttribute("id", jsonObject.getInt("id"));
				request.setAttribute("rut", jsonObject.getString("rut"));
				request.setAttribute("nombres", jsonObject.getString("nombres"));
				request.setAttribute("apellido_paterno", jsonObject.getString("apellido_paterno"));
				request.setAttribute("apellido_materno", jsonObject.getString("apellido_materno"));
				request.setAttribute("email", jsonObject.getString("email"));
				request.setAttribute("fecha_nacimiento", jsonObject.getString("fecha_nacimiento"));
				request.setAttribute("identificadorfacebook", jsonObject.getString("identificadorfacebook"));
				request.setAttribute("activo", jsonObject.getInt("activo"));
				request.setAttribute("password", jsonObject.getString("password"));
				
				System.out.println(jsonObject.getInt("id"));
				System.out.println(jsonObject.getString("rut"));
				System.out.println(jsonObject.getString("nombres"));
				System.out.println(jsonObject.getString("apellido_paterno"));
				System.out.println(jsonObject.getString("apellido_materno"));
				System.out.println(jsonObject.getString("email"));
				System.out.println(jsonObject.getString("fecha_nacimiento"));
				System.out.println(jsonObject.getString("identificadorfacebook"));
				System.out.println(jsonObject.getInt("activo"));
				System.out.println(jsonObject.getString("password"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		request.getRequestDispatcher("/BuscarPersona.jsp").forward(request, response);
	}

}
