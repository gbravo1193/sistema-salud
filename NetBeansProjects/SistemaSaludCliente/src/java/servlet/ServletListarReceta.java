package servlet;

import capanegocio.Receta;
import capaservicio.PersistentException_Exception;
import capaservicio.Service;
import capaservicio.Service_Service;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ServletListarReceta
 */
@WebServlet("/ServletListarReceta")
public class ServletListarReceta extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletListarReceta() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
				//datos de formulario
				String pacienteid=request.getParameter("pacienteid");
				if ( pacienteid==null	)
				{
					
				     response.sendRedirect("error.jsp");
				
				}
				
				else 
				{
                                    Service_Service ss= new Service_Service();
                                    Service s = ss.getServicePort();
                                    List<Receta> listaReceta = null;
                                    //servicio.RecetaVO listaReceta[]=null;
                                    try {
                                        s.listarReceta(Integer.parseInt(pacienteid));
                                    } catch (PersistentException_Exception ex) {
                                        Logger.getLogger(ServletListarReceta.class.getName()).log(Level.SEVERE, null, ex);
                                    }
					
					
					if (	listaReceta!= null	)
					   {
						
						// debug consola
						int i=0;
						for (i=0; i< listaReceta.size(); i++)
						{
							System.out.println( listaReceta.get(i).getDetalle() );
							System.out.println(listaReceta.get(i).getId() );
							System.out.println("ORIGEN: SERVLET...");
						}
			    
							//session.setAttribute("listaReceta", listaReceta);
							request.setAttribute("listaReceta", listaReceta);
							request.getRequestDispatcher("/listaRecetas.jsp").forward(request, response);
					   }
					else
						{
						 	response.sendRedirect("error.jsp");
						}   
					   
				}  
				
	
	}

	
}
