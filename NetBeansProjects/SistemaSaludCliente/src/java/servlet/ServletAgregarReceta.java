package servlet;

import capaservicio.Service;
import capaservicio.Service_Service;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ServletAgregarReceta
 */
@WebServlet("/ServletAgregarReceta")
public class ServletAgregarReceta extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletAgregarReceta() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

	//formulario
	String pacienteid=request.getParameter("pacienteid");
	String rceid=request.getParameter("rceid");
	String detalle=request.getParameter("detalle");
	
	
	if( pacienteid!= null && rceid!=null && detalle!=null)
	{
		if ( pacienteid.equals(" ") || rceid.equals(" ") || detalle.equals(" ") )
		{	
			response.sendRedirect("error.jsp");
		}
		else
		{			
			
			capanegocio.Receta receta=new capanegocio.Receta(Integer.parseInt(pacienteid),
					Integer.parseInt(rceid),
					detalle);
			String respuesta="";
                        
                        Service_Service ss= new Service_Service();
                Service s = ss.getServicePort();
                
		String r= s.agregarReceta(receta.getDetalle());
			if ( respuesta!=null )
				{
					request.setAttribute("respuesta", respuesta);
					response.sendRedirect("tareaExitosa.jsp");
				}
			else
					{

						response.sendRedirect("error.jsp");
					}
			
			
		}
	}
	
	else
		{
			response.sendRedirect("error.jsp");	
	}
	
} // fin doPost

}
