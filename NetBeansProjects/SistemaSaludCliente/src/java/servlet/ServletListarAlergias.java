package servlet;

import capanegocio.Alergia;
import capaservicio.AlergiaVo;
import capaservicio.PersistentException_Exception;
import capaservicio.Service;
import capaservicio.Service_Service;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class ServletListarAlergias
 */
@WebServlet("/ServletListarAlergias")
public class ServletListarAlergias extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletListarAlergias() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		//datos de formulario
		String pacienteid=request.getParameter("pacienteid");
		String rceid=request.getParameter("rceid");
		
		Service_Service ss= new Service_Service();
                Service s = ss.getServicePort();
                List<AlergiaVo> lista=null;

            try {
               lista=  s.listarAlergia(Integer.parseInt(pacienteid), Integer.parseInt(rceid));
               int i=0;
		for (i=0; i< lista.size(); i++)
		{
                       
			System.out.println(lista.get(i).getDetalle());
			System.out.println("SERVLET");
		}
	
		request.setAttribute("lista",lista);
      //  response.sendRedirect("listaAlergia.jsp");
        
		//request.setAttribute("lista", lista);
		request.getRequestDispatcher("/listaAlergia.jsp").forward(request, response);
            } catch (PersistentException_Exception ex) {
                Logger.getLogger(ServletListarAlergias.class.getName()).log(Level.SEVERE, null, ex);
            }
            
		
		
	}

}
