package servlet;

import capaservicio.Service;
import capaservicio.Service_Service;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;


/**
 * Servlet implementation class ListarPrevisiones
 */
@WebServlet("/ListarPrevisiones")
public class ListarPrevisiones extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListarPrevisiones() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String lista="";
		Service_Service sp= new Service_Service();
                Service s = sp.getServicePort();
		lista= s.listarPrevision();
		
		try {
			JSONObject jo= new JSONObject(lista);
			request.setAttribute("id", jo.getInt("id"));
			request.setAttribute("descripcion", jo.getString("descripcion"));
		} catch (JSONException e) {
			
			e.printStackTrace();
		}
		
		request.getRequestDispatcher("/CreadorPaciente.jsp").forward(request, response);
	}

}
