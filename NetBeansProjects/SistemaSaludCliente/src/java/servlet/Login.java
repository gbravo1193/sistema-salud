package servlet;

import capaservicio.Service;
import capaservicio.Service_Service;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.google.gson.Gson;

//import capaServicio.RegistroMedicoProxy;

/**
 * Servlet implementation class ServletLogin
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    //Metodo para hacer logout
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String msg = "";
		HttpSession sesion = request.getSession();
		if(sesion.getAttribute("usuario")!=null){
			sesion.invalidate();
			msg = "Sesion Cerrada!";
			System.out.print(msg);
		}else{
			msg="Error: No hay una sesion iniciada";
		}	
		request.setAttribute("msg", msg);
		request.getRequestDispatcher("/login.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	//Metodo para hacer login
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 boolean valido=false;
	        HttpSession sesion = request.getSession();
	        String rut, pass, msg = "";
	        try{
	            rut = request.getParameter("rut");
	            pass = request.getParameter("password");
	            
	            //Sysos para comprobar que llegan las variables.
	            System.out.println("Captura de Datos....");
	            System.out.println("Rut: " + rut);
	            System.out.println("Password: " + pass);
	            
	            Service_Service lsp = new Service_Service();
                    Service s = lsp.getServicePort();
	            try {
	                String aux=s.validarUsuario(rut, pass);
	                if (aux != null){
	                    valido = true;
	                } else {
	                    valido = false;
	                }
	            } catch (Exception e) {
	                msg = "Error: No se pudo verificar el Usuario!";
	            }
	            if (valido!=false){
	                if(sesion.getAttribute("usuario") == null){
	                    sesion.setAttribute("usuario", rut);
	                    msg = "Login Exitoso!";
	                }else {
	                    msg = "Error: sesion ya iniciada!";
	                }
	            } else {
	                msg = "Error: Datos incorrectos!";
	            }
	        }catch(Exception e){
	            msg ="Error: No se pudo leer informacion del formulario!";
	        }   
	        request.setAttribute("msg", msg);
	        if(msg.toLowerCase().contains("exitoso".toLowerCase())){
                    request.setAttribute("login", "true");
	            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/index.jsp");
	            requestDispatcher.forward(request, response);
	            //return;
	        }else{
                    request.setAttribute("login", "false");
	            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/login.jsp");
	            requestDispatcher.forward(request, response);
	            //return;
	        }
	}
}
