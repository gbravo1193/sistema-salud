package servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import capanegocio.Reserva;
import capaservicio.PersistentException_Exception;
import capaservicio.Service;
import capaservicio.Service_Service;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Servlet implementation class ServletAgregarReservaMedica
 */
@WebServlet("/ServletAgregarReservaMedica")
public class ServletAgregarReservaMedica extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletAgregarReservaMedica() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String respuesta = "";
		
		String rutProfesional = request.getParameter("rutProfesional");
		String rutAdministrador = request.getParameter("rutAdministrador");
		String rutPaciente = request.getParameter("rutPaciente");
		String fechaReserva = request.getParameter("fechaReserva");
		String motivoConsulta = request.getParameter("motivoConsulta");
		
		//Sysos para comprobar que llegan las variables.
		System.out.println("Captura de Datos....");
		System.out.println("rutProfesional: " + rutProfesional);
		System.out.println("rutAdministrador: " + rutAdministrador);
		System.out.println("rutPaciente: " + rutPaciente);
		System.out.println("fechaConsulta: " + fechaReserva);
		System.out.println("motivoConsulta: " + motivoConsulta);
		
		if (rutProfesional != null && rutAdministrador != null && rutPaciente != null 
				&& fechaReserva != null || motivoConsulta != null){
			
			if (rutProfesional.equals("") || rutAdministrador.equals("") || rutPaciente.equals("") 
					|| fechaReserva.equals("") || motivoConsulta.equals("")){
				respuesta = "Ingreso fallido";
				RequestDispatcher rs = request.getRequestDispatcher("/agregarReservaMedica.jsp");
				request.setAttribute("respuesta", respuesta);
				rs.forward(request, response);
			} else {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");
				
				@SuppressWarnings("deprecation")
				String fechaReservaFormat = sdf.format(new Date(fechaReserva));
				System.out.println("fechaConsultatoWS: " + fechaReservaFormat);
				String result = null;
				
				Service_Service res = new Service_Service();
                                Service port = res.getServicePort();
                            try {
                                result = port.agregarReservaMedica(rutProfesional, rutAdministrador, rutPaciente, fechaReservaFormat, motivoConsulta);
                            } catch (PersistentException_Exception ex) {
                                Logger.getLogger(ServletAgregarReservaMedica.class.getName()).log(Level.SEVERE, null, ex);
                            }
				
				//si respuesta
				if(result != null){
					
					Gson gson = new GsonBuilder().create();
					Reserva reserva = new Reserva();
					reserva = gson.fromJson(result, Reserva.class);
					Date fechaConsulta = reserva.getFecha();
					request.setAttribute("rutPaciente", rutPaciente);
					request.setAttribute("fechaConsulta", fechaConsulta);
					request.setAttribute("motivoConsulta", motivoConsulta);
					request.setAttribute("rutProfesional", rutProfesional);
					//request.setAttribute("", "obtener");
					//respuesta = result;//<-----Resultados
					request.setAttribute("respuesta", respuesta);
					
					//RequestDispatcher rs = request.getRequestDispatcher("/agregarReservaMedica.jsp");
					RequestDispatcher rs = request.getRequestDispatcher("/comprobanteReserva.jsp");	
					rs.forward(request, response);
					/*
					 * 
					 */
				}else{
					respuesta = "No se almacenaron los datos en la BD";
					RequestDispatcher rs = request.getRequestDispatcher("/agregarReservaMedica.jsp");
					request.setAttribute("respuesta", respuesta);
					rs.forward(request, response);
				}
			}
		} else {
			respuesta = "Ingreso fallido de datos en formulario";
			RequestDispatcher rs = request.getRequestDispatcher("/agregarReservaMedica.jsp");
			request.setAttribute("respuesta", respuesta);
			rs.forward(request, response);
		}
	}

}
