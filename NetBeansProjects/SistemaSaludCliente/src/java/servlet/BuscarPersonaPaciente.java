package servlet;

import capaservicio.Service;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import capaservicio.Service_Service;

/**
 * Servlet implementation class BuscarPersonaPaciente
 */
@WebServlet("/BuscarPersonaPaciente")
public class BuscarPersonaPaciente extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuscarPersonaPaciente() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String rut= request.getParameter("rut");
		Service_Service res= new Service_Service();
		Service s = res.getServicePort();
		String resultado= s.buscarPacientePorRut(rut);
		
		try {
				final JSONObject jsonObject= new JSONObject(resultado);
				request.setAttribute("id", jsonObject.getInt("id"));
				request.setAttribute("rut", jsonObject.getString("rut"));
				request.setAttribute("nombres", jsonObject.getString("nombres"));
				request.setAttribute("apellido_paterno", jsonObject.getString("apellido_paterno"));
				request.setAttribute("apellido_materno", jsonObject.getString("apellido_materno"));
				
				
				System.out.println(jsonObject.getInt("id"));
				System.out.println(jsonObject.getString("rut"));
				System.out.println(jsonObject.getString("nombres"));
				System.out.println(jsonObject.getString("apellido_paterno"));
				System.out.println(jsonObject.getString("apellido_materno"));
				
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		request.getRequestDispatcher("/CrearPaciente.jsp").forward(request, response);
	}

}
