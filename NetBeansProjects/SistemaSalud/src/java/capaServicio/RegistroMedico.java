/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package capaServicio;

import capaNegocio.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import orm.Insumo;

/**
 *
 * @author Sergioh
 */
@WebService(serviceName = "RegistroMedico")
public class RegistroMedico {

    /**
     * Metodo Web que añade un registro a la base de datos
     * @param rce - Gson - Gson con los datos del rce
     * @return String - Salida del ingreso
     */
    @WebMethod(operationName = "AgregarRegistroMedico")
    public String AgregarRegistroMedico(@WebParam(name = "rce") String rce) {
        String respuesta = "no se pudo almacenar el registro";
        Gson gson = new Gson();
        Rce rce1 = new Rce();
        rce1 = gson.fromJson(rce, Rce.class);
        respuesta = rce1.crearRCE(rce1);
        return respuesta;
    }

    /**
     * Metodo que ingresa alergias al Registro clinico con la id especificada
     * @param rce - int - Id del registro al cual se le agregaran las alergias
     * @param a - ArrayList - Array con las alergias a ingresar
     * @return String . Salida del ingreso
     */
    @WebMethod(operationName = "IngresarAlergias")
    public String IngresarAlergias(@WebParam(name = "Rce") int rce, @WebParam(name = "Alergia") ArrayList a) {
        //TODO write your implementation code here:
        String respuesta = "";
        Rce rce1 = new Rce();
        rce1.setId(rce);
        Alergias alergias = new Alergias();
        alergias.setRce(rce1);
        try {
            for (int i = 0; i < a.size(); i++) {
                alergias.setDetalle(a.get(i).toString());
                String resultado = alergias.crearAlergia(alergias);
                if (resultado.toLowerCase().equals("Ingreso Exitoso!".toLowerCase())) {
                    respuesta = "Creación del Registro exitoso";
                }
            }
        } catch (Exception ex) {
            respuesta = "Error: No se guardaron las alegias!";
        }
        return respuesta;
    }

    /**
     * Metodo que ingresa insunos al Registro clinico con la id especificada
     * @param rce - int - Id del registro al cual se le agregaran los insumos
     * @param a - ArrayList - Array con los insumos a ingresar
     * @return String . Salida del ingreso
     */
    @WebMethod(operationName = "IngresarRceInsumo")
    public String IngresarRceInsumo(@WebParam(name = "Rce") int rce, @WebParam(name = "Insumo") ArrayList a) {
        //TODO write your implementation code here:
        String respuesta = "";
        Rce_insumo rceInsumo = new Rce_insumo();

        Rce rce1 = new Rce();
        rce1.setId(rce);
        rceInsumo.setRce(rce1);
        Insumo insumo = new Insumo();
        try {
            for (int i = 0; i < a.size(); i++) {
                insumo.setNombre(a.get(i).toString());
                rceInsumo.setInsumo(insumo);
                String resultado = rceInsumo.CrearRce_insumo(rceInsumo);
                if (resultado.toLowerCase().equals("Ingreso Exitoso!".toLowerCase())) {
                    respuesta = "Creación del Registro exitoso";
                }
            }
        } catch (Exception ex) {
            respuesta = "Error: No se guardaron los insumos!";
        }
        return respuesta;
    }

    /**
     * Metodo que ingresa un diagnosticoa al Registro clinico con la id especificada
     * @param rce - int - Id del registro al cual se le agregara el diagnostico
     * @param a - Diagnostico - diagnostico a ingresar
     * @return String . Salida del ingreso
     */
    @WebMethod(operationName = "IngresarRce_Diagnostico")
    public String IngresarRce_Diagnostico(@WebParam(name = "Rce") int rce, @WebParam(name = "Diagnostico") Diagnostico a) {
        //TODO write your implementation code here:
        String respuesta = "";

        Rce_diagnostico rceDiagnostico = new Rce_diagnostico();
        Rce rce1 = new Rce();
        rce1.setId(rce);
        rceDiagnostico.setRce(rce1);
        Diagnostico diagnostico = new Diagnostico();
        diagnostico.setCodigo(a.getCodigo());
        rceDiagnostico.setDiagnostico(diagnostico);
        try {
            String resultado = rceDiagnostico.CrearRce_diagnostico(rceDiagnostico);
            if (resultado.toLowerCase().equals("Ingreso Exitoso!".toLowerCase())) {
                respuesta = "Creación del Registro exitoso";
            }
        } catch (Exception ex) {
            respuesta = "Error: No se guardaron los insumos!";
        }
        return respuesta;
    }

    /**
     * Metodo que ingresa recertas al Registro clinico con la id especificada
     * @param rce - int - Id del registro al cual se le agregaran las recetas
     * @param a - ArrayList - Array con las recetas a ingresar
     * @return String . Salida del ingreso
     */
    @WebMethod(operationName = "IngresarRecetarce")
    public String IngresarRecetarce(@WebParam(name = "Rce") int rce, @WebParam(name = "Receta") ArrayList a) {
        //TODO write your implementation code here:
        String respuesta = "";
        Recetarce recetarce = new Recetarce();
        Rce rce1 = new Rce();
        rce1.setId(rce);
        Receta receta = new Receta();
        recetarce.setRce(rce1);

        try {
            for (int i = 0; i < a.size(); i++) {
                receta.setDetalle(a.get(i).toString());
                recetarce.setReceta(receta);
                String resultado = recetarce.crearResetarse(recetarce);
                if (resultado.toLowerCase().equals("Ingreso Exitoso!".toLowerCase())) {
                    respuesta = "Creación del Registro exitoso";
                }
            }
        } catch (Exception ex) {
            respuesta = "Error: No se guardaron los insumos!";
        }

        return respuesta;
    }
    /**
     * Devuelve el listado de registros clinicos
     * @return Gson - registros en formato Gson
     */
    @WebMethod(operationName = "listarRegistros")
    public String listarRegistros() {
        //TODO write your implementation code here:
        String respuesta = "";
        Rce rce= new Rce();
        Gson gson= new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        Rce[] salida= null;
        try {
           salida=rce.listarRce();
           respuesta=gson.toJson(salida);
        } catch (Exception ex) {
            respuesta = "Error: No se guardaron los insumos!";
        }
        return respuesta;
    }
}
