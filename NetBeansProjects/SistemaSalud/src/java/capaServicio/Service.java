/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package capaServicio;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import capaNegocio.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import org.orm.PersistentException;
import vo.AlergiaVo;
import vo.EspecialidadVo;
import vo.PacienteVo;
import vo.ProfesionalVo;
import vo.TipoPrevisionVo;


@WebService(serviceName = "Service")
public class Service {
    
    /**
     * 
     * @param rut
     * @param password
     * @return 
     */
    @WebMethod(operationName = "validarUsuario")
    public String validarUsuario(@WebParam(name = "rut") String rut,
            @WebParam(name = "password") String password){
        
        String result = null;
        Persona pers = new Persona();                
        Gson gson = new GsonBuilder().create();
        
        //pers = pers.validarPersonaRegistrada(rut, password); //poco eficiente
        pers = pers.buscarPersonaPorRut(rut);
        if( pers != null){            
            if(pers.getPassword().equals(password)){
                return gson.toJson(pers);
            }else{
                return null;
            }
        }else{
            return null;
        }
    }
    
    /**
     * Solicitar HCE por Rut
     * 
     * @param rut Paciente
     * @param RutPersonaProfesional
     * @return String Json List
     */        
    @WebMethod(operationName = "solicitarHce")
    public String solicitarHce(
            @WebParam(name = "rut") String rut, 
            @WebParam(name = "RutPersonaProfesional") String RutPersonaProfesional) {
        
        capaNegocio.Rce rce = new capaNegocio.Rce();
        List<capaNegocio.Rce> hce = new ArrayList<capaNegocio.Rce>();
        String hceResultado = "";

        Gson gson = new GsonBuilder().create();

        try {
            //hce = rce.ObtenerHceCapaNegocio(rut);
            hceResultado = gson.toJson(hce);

            //Guarda Busqueda
            capaNegocio.Consulta_hce consulta_hce = new capaNegocio.Consulta_hce();
            if (consulta_hce.agregarConsulta_hce(rut, RutPersonaProfesional) == false) {
                hceResultado = "[]";

            }

        } catch (PersistentException ex) {
            hceResultado = null;
        }
        return hceResultado;
    }

    /**
     * Busca HCE por Profesional a traves del rut
     * 
     * @param rut
     * @return Strin json List
     */
    @WebMethod(operationName = "verBusquedaHcePorProfesional")
    public String verBusquedaHcePorProfesional(@WebParam(name = "rut") String rut) {
        
        capaNegocio.Consulta_hce consultahce = new capaNegocio.Consulta_hce();
        List<capaNegocio.Consulta_hce> ListaConsultahce = new ArrayList<capaNegocio.Consulta_hce>();
        String ListaBusquedaResultado = "";

        Gson gson = new GsonBuilder().create();

        try {
            ListaConsultahce = consultahce.verBusquedaPorProfesional(rut);
            ListaBusquedaResultado = gson.toJson(ListaConsultahce);

        } catch (PersistentException ex) {
            ListaBusquedaResultado = null;
        }
        return ListaBusquedaResultado;
    }

    /**
     * Busca HCE por rut de Paciente
     * @param rut
     * @return String json List
     */
    @WebMethod(operationName = "verBusquedaHcePorPaciente")
    public String verBusquedaHcePorPaciente(@WebParam(name = "rut") String rut) {
        
        capaNegocio.Consulta_hce consultahce = new capaNegocio.Consulta_hce();
        List<capaNegocio.Consulta_hce> ListaConsultahce = new ArrayList<capaNegocio.Consulta_hce>();
        String ListaBusquedaResultado = "";

        Gson gson = new GsonBuilder().create();

        try {
            ListaConsultahce = consultahce.verBusquedaporPaciente(rut);
            ListaBusquedaResultado = gson.toJson(ListaConsultahce);

        } catch (PersistentException ex) {
            ListaBusquedaResultado = null;
        }
        return ListaBusquedaResultado;
    }

    /**
     * Busca Reservas mediante datos de Paciente
     * 
     * @param rut
     * @param nombres
     * @param apellido_paterno
     * @param apellido_materno
     * @param fecha_inicio
     * @param fecha_termino
     * @return String json List
     * @throws org.orm.PersistentException 
     * @throws java.text.ParseException 
     */
    @WebMethod(operationName = "buscarReservasPorPaciente")
    public String buscarReservasPorPaciente(
            @WebParam(name = "rut") String rut, 
            @WebParam(name = "nombres") String nombres,
            @WebParam(name = "apellido_paterno") String apellido_paterno, 
            @WebParam(name = "apellido_materno") String apellido_materno,
            @WebParam(name = "fecha_inicio") String fecha_inicio, 
            @WebParam(name = "fecha_termino") String fecha_termino) throws PersistentException, ParseException {
        
        String resultado = "";
        List<capaNegocio.Reserva> lista = new ArrayList<capaNegocio.Reserva>();
        capaNegocio.Reserva reserva = new Reserva();
        capaNegocio.Persona persona = new capaNegocio.Persona();
        capaNegocio.Paciente paciente = new capaNegocio.Paciente();

        //DateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        if (rut.equals("") && nombres.equals("") && apellido_paterno.equals("") && apellido_materno.equals("")) {
            resultado += "Ingrese por lo menos uno de los 4 primeros campos";
        } else if (reserva.validarFecha(fecha_inicio) != 1 || reserva.validarFecha(fecha_termino) != 1) {
            resultado += "La fecha ingresada no es valida o no fue ingresada";
        } else {

            if (rut != null) {
                persona.setRut(rut);
            } else {
                persona.setRut("");
            }

            if (nombres != null) {
                persona.setNombres(nombres);
            } else {
                persona.setNombres("");
            }

            if (apellido_paterno != null) {
                persona.setApellido_paterno(apellido_paterno);
            } else {
                persona.setApellido_paterno("");
            }

            if (apellido_materno != null) {
                persona.setApellido_materno(apellido_materno);
            } else {
                persona.setApellido_materno("");
            }

            paciente.setPersona(persona);
            reserva.setPaciente(paciente);

            Gson gson = new GsonBuilder().create();

            lista = reserva.buscarPorPaciente(reserva, fecha_inicio, fecha_termino);
            resultado = gson.toJson(lista);

        }
        return resultado;
    }

    /**
     * Obtiene una reserva a traves de su ID
     * 
     * @param id
     * @return String json List
     * @throws org.orm.PersistentException
     */
    @WebMethod(operationName = "obtenerReserva")
    public String obtenerReserva(@WebParam(name = "id") int id) throws PersistentException {
        
        String resultado = "";
        capaNegocio.Reserva reservaId = new Reserva();
        reservaId = reservaId.obtenerReserva(id);

        if (reservaId != null) {
            Gson gson = new GsonBuilder().create();
            resultado = gson.toJson(reservaId);
        } else {
            resultado = "Ingrese un Id valido";
        }

        return resultado;
    }

    /**
     * Busca reservas a traves de datos de Profesional
     * 
     * @param rut
     * @param nombres
     * @param apellido_paterno
     * @param apellido_materno
     * @param fecha_inicio
     * @param fecha_termino
     * @return 
     * @throws org.orm.PersistentException 
     */
    @WebMethod(operationName = "buscarReservasPorProfesional")
    public String buscarReservasPorProfesional(
            @WebParam(name = "rut") String rut,
            @WebParam(name = "fecha_inicio") String fecha_inicio, 
            @WebParam(name = "fecha_termino") String fecha_termino) throws PersistentException {
        
        String resultado = "";
        List<capaNegocio.Reserva> lista = new ArrayList<capaNegocio.Reserva>();
        Reserva reserva = new Reserva();
        Persona persona = new capaNegocio.Persona();
        Horamedica hora = new Horamedica();
        Profesional medico = new Profesional();

        if (rut.equals("")) {
            resultado += "Ingrese el rut del Profesional";
        } else if (fecha_inicio.equals("") && fecha_termino.equals("")) {
            resultado += "La fecha ingresada no es valida o no fue ingresada";
        } else {
            
            persona = persona.buscarPersonaPorRut(rut);
            medico = medico.buscarProfesional(persona.getId());
            medico.setPersona_id(persona.getId());
            hora.setProfesional(medico);
            reserva.setHora_medica(hora);

            Gson gson = new GsonBuilder().create();

            lista = reserva.buscarPorProfesional(reserva, fecha_inicio, fecha_termino);
            resultado = gson.toJson(lista);
        }
        return resultado;
    }

    /**
     * Busca reservas segun Especialidad
     * 
     * @param especialidad
     * @param fecha_Termino
     * @param fecha_Inicio
     * @return String json List
     * @throws org.orm.PersistentException
     */
    @WebMethod(operationName = "buscarReservasPorEspecialidad")
    public String buscarReservasPorEspecialidad(
            @WebParam(name = "especialidad") String especialidad, 
            @WebParam(name = "fecha_Inicio") String fecha_Inicio, 
            @WebParam(name = "fecha_Termino") String fecha_Termino) throws PersistentException {
        
        String resultado = "";
        List<capaNegocio.Reserva> lista = new ArrayList<capaNegocio.Reserva>();
        capaNegocio.Reserva reserva = new Reserva();
        Gson gson = new GsonBuilder().create();

        if (especialidad.equals("") || (especialidad == null)) {
            resultado += "Ingrese una Especialidad";

        } else if (reserva.validarFecha(fecha_Inicio) != 1 || reserva.validarFecha(fecha_Termino) != 1) {
            resultado += "La fecha ingresada no es valida o no fue ingresada";

        } else {
            lista = reserva.buscarPorEspecialidad(especialidad, fecha_Inicio, fecha_Termino);

            if (lista == null) {
                resultado += "No se encontraron reservas con la especialidad ingresada";
            } else {
                resultado = gson.toJson(lista);
            }
        }
        return resultado;
    }

    /**
     * Lista las Especialidades
     * 
     * @return String json List
     * @throws org.orm.PersistentException
     */
    @WebMethod(operationName = "listarEspecialidades")
    public String listarEspecialidades() throws PersistentException {
        
        String listaResultado = "";
        List<capaNegocio.Especialidad> lista = new ArrayList<>();
        Gson gson = new GsonBuilder().create();
        Especialidad especialidad = new Especialidad();

        lista = especialidad.ListarEspecilidades();
        listaResultado = gson.toJson(lista);

        return listaResultado;
    }

    /**
     * Busca a Paciente mediante Rut
     *
     * @param rut
     * @return String json
     */
    @WebMethod(operationName = "buscarPacientePorRut")
    public String buscarPacientePorRut(@WebParam(name = "rutPaciente") String rut) {
        Pago pago = new Pago();
        Gson g = new GsonBuilder().create();
        String out = "";
        out = g.toJson(pago.obtenerPacientePorRut(rut));
        return out;
    }

    /**
     * Registra un pago
     *
     * @param idMedioPago
     * @param idRce
     * @param monto
     * @param comprobante
     * @param estadoPago
     * @param subsidio
     * @return String json
     */
    @WebMethod(operationName = "efectuarPago")
    public String efectuarPago(
            @WebParam(name = "idMedioPago") int idMedioPago,
            @WebParam(name = "idRce") int idRce,
            @WebParam(name = "monto") double monto,
            @WebParam(name = "comprobante") double comprobante,
            @WebParam(name = "estadoPago") int estadoPago,
            @WebParam(name = "subsidio") float subsidio) {
        
        Gson g = new Gson();
        Pago pago = new Pago();
        return g.toJson(pago.efectuarPago(idMedioPago, idRce, monto, comprobante, estadoPago, subsidio));
    }

    /**
     * Obtiene un comprobante de Pago
     *
     * @param idPago
     * @return String json
     */
    @WebMethod(operationName = "obtenerComprobantePorIdPago")
    public String obtenerComprobantePorIdPago(@WebParam(name = "idPago") int idPago) {
        Pago pago = new Pago();
        Gson g = new GsonBuilder().create();
        String out = "";
        out = g.toJson(pago.obtenerComprobantePorIdPago(idPago));
        return out;
    }

    /**
     * Web service operation
     * -Metodo Cliente Ismael G.-
     * @param desc
     * @return
     */
    @WebMethod(operationName = "ingresarMedioPago")
    public String ingresarMedioPago(@WebParam(name = "desc") String desc) {
        MedioPago mpago = new MedioPago();
        Gson g = new GsonBuilder().create();
        return g.toJson(mpago.ingresarMedioPago(desc));
    }

    /**
     * Web service operation
     * -Metodo Cliente Ismael G.-
     * @param desc
     * @return
     */
    @WebMethod(operationName = "ingresarTipoPrevision")
    public String ingresarTipoPrevision(@WebParam(name = "desc") String desc) {
        Pago pago = new Pago();
        Gson g = new GsonBuilder().create();
        return g.toJson(pago.ingresarTipoPrevision(desc));
    }

    /**
     * Obtiene detalles de RCE mediante el Rut de un Paciente
     *
     * @param rutPaciente
     * @return String json
     */
    @WebMethod(operationName = "obtenerDetalleAtencionPorRutPaciente")
    public String obtenerDetalleAtencionPorRutPaciente(@WebParam(name = "rutPaciente") String rutPaciente) {
        Pago pago = new Pago();
        Gson g = new GsonBuilder().create();
        String out = "";
        out = g.toJson(pago.obtenerRcePorRut(rutPaciente));
        return out;
    }

    /**
     * Obtiene las deudas de un paciente mediante el rut
     *
     * @param rutPaciente
     * @return String json
     */
    @WebMethod(operationName = "obtenerDeudasPorRut")
    public String obtenerDeudasPorRut(@WebParam(name = "rutPaciente") String rutPaciente) {
        Pago pago = new Pago();
        Gson g = new GsonBuilder().create();
        return g.toJson(pago.obtenerDeudas(rutPaciente));
    }

    /**
     * Metodo agrega una alergia al RCE
     *
     * @param detalle
     * @param rceid
     * @return out con estado operacion
     */
    @WebMethod(operationName = "agregarAlergia")
    public String agregarAlergia(
            @WebParam(name = "rceid") int rceid,
            @WebParam(name = "detalle") String detalle) {

        if (detalle != null && rceid != 0) {

            try {
                return new Alergia().agregarAlergia(rceid, detalle.toLowerCase());
            } catch (PersistentException ex) {
                java.util.logging.Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return "ERROR:WS-agregar-alergia";
    }

    /**
     * Lista de Alergias de un paciente a traves de ID de paciente y RCE
     *
     * @param pacienteid
     * @param rceid
     * @return out con estado operacion
     * @throws org.orm.PersistentException
     */
    @WebMethod(operationName = "listarAlergia")
    public List<AlergiaVo> listarAlergia(
            @WebParam(name = "pacienteid") int pacienteid,
            @WebParam(name = "rceid") int rceid)
            throws PersistentException {
        
        return new Alergia().listarAlergia(pacienteid, rceid);
    }
    
    /**
     * Elimina una Alergia a traves del ID de RCE y ID Alergia
     * 
     * @param rceid
     * @param AlergiaID
     * @return 
     */
    @WebMethod(operationName = "eliminarAlergiaID")
    public String eliminarAlergia(int rceid, int AlergiaID) {

        try {
            return new Alergia().EliminarAlergia(rceid, AlergiaID);
        } catch (PersistentException ex) {
            java.util.logging.Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "ERROR:ELIMINADO";
    }
    
    /**
     * Agrega receta
     * 
     * @param receta
     * @return String
     */
    @WebMethod(operationName = "agregarReceta")
    public String agregarReceta(@WebParam(name = "receta") String receta) {
        
        Receta r = new Receta();
        Gson gson = new Gson();
        r = gson.fromJson(receta, Receta.class);
        String salida = r.agregarReceta(r);
        return salida;
    }
    
    /**
     * WS listar receta
     * 
     * @param pacienteid
     * @return String
     * @throws PersistentException 
     */
    @WebMethod(operationName = "listarReceta")
    public List<Receta> listarReceta(@WebParam(name = "pacienteid") int pacienteid) throws PersistentException {
        Receta r= new Receta();
        String salida="";
        List<Receta>recetas=r.listarRecetas(pacienteid);
        return recetas;
    }
    
    /**
     * WS eliminar receta
     * 
     * @param recetaid
     * @return String
     * @throws PersistentException 
     */
    @WebMethod(operationName = "eliminarReceta")
    public String eliminarReceta(@WebParam(name = "recetaid") int recetaid) throws PersistentException {
        return new Receta().eliminarReceta(recetaid);
    }
    
    /**
     * WS listar receta por rut
     * 
     * @param rut
     * @return String
     */
    @WebMethod(operationName = "listarRecetaByRut")
    public String listarRecetaByRut(@WebParam(name = "rut") String rut) {
        List<Receta> recetas = new ArrayList<Receta>();
        Receta r = new Receta();
        recetas = r.listarRecetaPorRut(rut);
        Gson gson = new Gson();
        String salida = gson.toJson(recetas);
        return salida;
    }

    /**
     * Pacientes por prevision
     *
     * @return String
     */
    @WebMethod(operationName = "listarPrevision")
    public String listarPrevision() {
        TipoPrevisionVo[] previsiones = new Prevision().pacientesPorPrevision();

        String json;
        if (previsiones.length <= 0) {
            json = "no existen registros";
        } else if (previsiones == null) {
            json = "Error fatal";
        } else {
            Gson gson = new Gson();
            json = gson.toJson(previsiones);
        }
        System.out.println(json);
        return json;
    }

    /**
     * WS profesionales por especialidad
     *
     * @return String
     */
    @WebMethod(operationName = "profesionalesPorEspecialidad")
    public String profesionalesPorEspecialidad() {
        EspecialidadVo[] especialidades = new Especialidad().cantidadMedicosPorEspecialidad();

        String json;
        if (especialidades.length <= 0) {
            json = "no existen registros";
        } else if (especialidades == null) {
            json = "Error fatal";
        } else {
            Gson gson = new Gson();
            json = gson.toJson(especialidades);
        }
        System.out.println(json);
        return json;

    }

    /**
     * WS atenciones por profesional
     *
     * @return String [nombre, apellidoPaterno, cantidadHorasMedicas]
     */
    @WebMethod(operationName = "listarAtenciones")
    public String listarAtenciones() {
        ProfesionalVo[] profesionales = new Atencion().atencionesPorProfesional();

        String json;
        if (profesionales.length <= 0) {
            json = "no existen registros";
        } else if (profesionales == null) {
            json = "Error fatal";
        } else {
            Gson gson = new Gson();
            json = gson.toJson(profesionales);
        }
        System.out.println(json);
        return json;
    }

    /**
     * Lista de atenciones por Profesionales segun Fecha
     *
     * @param FechaInicio
     * @param FechaTermino
     * @return
     */
    @WebMethod(operationName = "atencionesPorProfesionalConFecha")
    public String atencionesPorProfesionalConFecha(
            @WebParam(name = "FechaInicio") String FechaInicio, 
            @WebParam(name = "FechaTermino") String FechaTermino) {
        
        ProfesionalVo[] profesionales = new Atencion().atencionesPorProfesionalConFecha(FechaInicio, FechaTermino);

        String json;
        if (profesionales.length <= 0) {
            json = "no existen registros";
        } else if (profesionales == null) {
            json = "Error fatal";
        } else {
            Gson gson = new Gson();
            json = gson.toJson(profesionales);
        }
        System.out.println(json);
        return json;
    }

    /**
     * Web service operation
     *
     * @return
     */
    @WebMethod(operationName = "reservasPorPaciente")
    public String reservasPorPacientes() {
        PacienteVo[] pacientes = new Reserva().reservasPorPaciente();

        String json;
        if (pacientes.length <= 0) {
            json = "no existen registros";
        } else if (pacientes == null) {
            json = "Error fatal";
        } else {
            Gson gson = new Gson();
            json = gson.toJson(pacientes);
        }
        System.out.println(json);
        return json;
    }

    /**
     * busca una persona segun su rut
     * 
     * @param rut
     * @return 
     */
    @WebMethod(operationName = "buscarPersonaPorRut")
    public String buscarPersonaPorRut(@WebParam(name = "rut") String rut) {
        
        Persona persona= new Persona();
        
        Gson json= new GsonBuilder().create();
        String resultado= json.toJson(persona.buscarPersonaPorRut(rut));
        
        System.out.println("RUT Persona: "+rut);
        System.out.println(resultado);
        
        return resultado;
    }
    
    /**
     * WS Agregar una Persona
     * 
     * @param rut
     * @param nombres
     * @param apellido_paterno
     * @param apellido_materno
     * @param email
     * @param fecha_nacimiento
     * @param identificadorfacebook
     * @param activo
     * @param password
     * @return String
     * @throws PersistentException
     */
    @WebMethod(operationName = "agregarPersona")
    public String agregarPersona(@WebParam(name= "rut") String rut, 
            @WebParam(name= "nombres") String nombres, @WebParam(name= "apellido_paterno") String apellido_paterno,
            @WebParam(name= "apellido_materno") String apellido_materno, @WebParam(name= "email") String email, 
            @WebParam(name= "fecha_nacimiento") Date fecha_nacimiento, @WebParam(name= "identificadorfacebook") String identificadorfacebook,
            @WebParam(name= "activo") int activo, @WebParam(name= "password")String password) throws PersistentException {
       
        String resultado="";   
        capaNegocio.Persona persona= new capaNegocio.Persona();

        persona.setRut(rut.toLowerCase());
        persona.setNombres(nombres.toLowerCase());
        persona.setApellido_paterno(apellido_paterno.toLowerCase());
        persona.setApellido_materno(apellido_materno.toLowerCase());
        persona.setEmail(email.toLowerCase());
        persona.setFecha_nacimiento(fecha_nacimiento);
        persona.setIdentificadorfacebook(identificadorfacebook.toLowerCase());
        persona.setActivo(activo);
        persona.setPassword(password);
        persona.agregarPersona(persona);
        System.out.println(persona.getRut());

        return resultado;
    }
    
    /**
     * WS Editar persona
     * 
     * @param id
     * @param rut
     * @param nombres
     * @param apellido_paterno
     * @param apellido_materno
     * @param email
     * @param fecha_nacimiento
     * @param identificadorfacebook
     * @param activo
     * @param password
     * @return String
     * @throws PersistentException
     */
    @WebMethod(operationName = "editarPersona")
    public String editarPersona(@WebParam(name= "id")int id, 
            @WebParam(name= "rut") String rut, 
            @WebParam(name= "nombres") String nombres, 
            @WebParam(name= "apellido_paterno") String apellido_paterno,
            @WebParam(name= "apellido_materno") String apellido_materno, 
            @WebParam(name= "email") String email, 
            @WebParam(name= "fecha_nacimiento") Date fecha_nacimiento, 
            @WebParam(name= "identificadorfacebook") String identificadorfacebook,
            @WebParam(name= "activo") int activo, 
            @WebParam(name= "password")String password) 
            throws PersistentException {
            
        capaNegocio.Persona persona= new capaNegocio.Persona();
            
      
        persona.editarPersona(id, 
                rut, 
                nombres.toLowerCase(), 
                apellido_paterno.toLowerCase(), 
                apellido_materno.toLowerCase(), 
                email.toLowerCase(), 
                fecha_nacimiento, 
                identificadorfacebook.toLowerCase(), 
                activo, 
                password);
        return "Datos Actualizados";   
    }
    
    /**
     * WS eliminar persona
     * 
     * @param id
     * @return String
     * @throws PersistentException
     */
    @WebMethod(operationName= "eliminarPersona")
    public String eliminarPersona(@WebParam(name= "id")int id)throws PersistentException{
        capaNegocio.Persona persona= new capaNegocio.Persona();
        
        persona.setId(id);
        persona.eliminarPersona(id);
        return "Persona eliminada";
    }
    
    /**
     * Agrega un Administrador
     * 
     * @param id_persona
     * @param activo
     * @return String
     * @throws PersistentException 
     */
    @WebMethod(operationName= "agregarAdministrador")
    public String agregarAdministrador(@WebParam(name= "id_persona")int id_persona,
            @WebParam(name= "activo")int activo)throws PersistentException{
        String result = null;
        Gson gson = new GsonBuilder().create();
        
        capaNegocio.Administrador administrador= new capaNegocio.Administrador();
        
        administrador.setPersona_id(id_persona);
        administrador.setActivo(activo);
        administrador = administrador.agregarAdministrador(administrador);
        
        result = gson.toJson(administrador);
        
        return result;
    }
    
    /**
     * WS Agrega un Profesional
     * 
     * @param id_persona
     * @param activo
     * @return
     * @throws PersistentException
     */
    @WebMethod(operationName= "agregarProfesional")
    public String agregarProfesional(@WebParam(name= "id_persona")int id_persona,
            @WebParam(name= "activo")int activo)throws PersistentException{
        
        String rsult = null;
        capaNegocio.Profesional profesional= new capaNegocio.Profesional();
        
        profesional.setPersona_id(id_persona);
        profesional.setActivo(activo);
        profesional = profesional.agregarProfesional(profesional);
        
        Gson gson = new GsonBuilder().create();        
        rsult = gson.toJson(profesional);
        return rsult;
        
    }
    
    /**
     * Busca un Administrador
     * 
     * @param id
     * @return String json Administrador
     * @throws PersistentException 
     */
    @WebMethod(operationName = "buscarAdministrador")
    public String buscarAdministrador(@WebParam(name = "id") int id) throws PersistentException{
        
        Administrador administrador= new Administrador();
        
        Gson json= new GsonBuilder().create();
       
        administrador.setId(id);
        String resultado= json.toJson(administrador.buscarAdministrador(id));
              
        return resultado;
    }
    
    /**
     * WS buscar profesional
     * 
     * @param id
     * @return
     * @throws PersistentException 
     */
    @WebMethod(operationName = "buscarProfesional")
    public String buscarProfesional(@WebParam(name = "id") int id) throws PersistentException{
        
        Profesional profesional= new Profesional();
        //orm.Profesional lormProfesional = orm.ProfesionalDAO.loadProfesionalByORMID(id);
        Gson json= new GsonBuilder().create();
       
        profesional.setId(id);
        String resultado= json.toJson(profesional.buscarProfesional(id));
        //String resultado= "{'id':"+lormProfesional.getId()+",'persona_id':"+lormProfesional.getPersona().getId()+",'activo':"+lormProfesional.getActivo()+"}";
        System.out.println("ID: "+id);
        System.out.println(resultado);
       
        return resultado;
    }
    
    /**
     * Elimina un Administrador mediante ID
     * @param id
     * @return String
     * @throws PersistentException 
     */
    @WebMethod(operationName= "eliminarAdministrador")
    public String eliminarAdministrador(@WebParam(name= "id")int id)throws PersistentException{
        capaNegocio.Administrador administrador= new capaNegocio.Administrador();
        
        try{
            administrador.setId(id);
            administrador.eliminarAdministrador(id);
            return "Admin eliminado"; 
        }catch(Exception e){
            e.printStackTrace();
            return "Error";
        }  
    }
    
    /**
     * Elimina Profesional
     * 
     * @param id
     * @return String
     * @throws PersistentException 
     */
    @WebMethod(operationName= "eliminarProfesional")
    public String eliminarProfesional(@WebParam(name= "id")int id)throws PersistentException{
        capaNegocio.Profesional profesional= new capaNegocio.Profesional();
        
        try{
            profesional.setId(id);
            profesional.eliminarProfesional(id);
            return "Profesional eliminado"; 
        }catch(Exception e){
            e.printStackTrace();
            return "Error";
        }  
    }
    
    /**
     * WS Agregar Especialidad
     * 
     * @param nombreEspecialidad
     * @return String
     * @throws PersistentException 
     */
    @WebMethod(operationName= "agregarEspecialidad")
    public String agregarEspecialidad(@WebParam(name= "nombreEspecialidad")String nombreEspecialidad)throws PersistentException{
        Especialidad especialidad = new Especialidad();
        if(especialidad.agregarEspecialidad(nombreEspecialidad.toLowerCase())){
            return "Especialidad agregada";
        }else{
            return "No se pudo agregar especialidad";
        }        
    }
    /**
     * WS Agregar prevision a lista de previsiones
     * 
     * @param nombrePrevision
     * @return String
     * @throws PersistentException 
     */
    @WebMethod(operationName = "agregarTipoPrevision")
    public String agregarTipoPrevision(
            @WebParam(name = "nombrePrevision")String nombrePrevision) 
            throws PersistentException{
        
        Tipo_prevision prevision = new Tipo_prevision();
        Gson json = new GsonBuilder().create();
        return json.toJson(prevision.ingresarTipoPrevision(nombrePrevision.toLowerCase()));
    }
    
    

    /**
     * WS crear una hora medica
     * 
     * @param rutAdministrador
     * @param rutProfesional
     * @param horaInicio
     * @return String
     * @throws org.orm.PersistentException 
     */
    @WebMethod(operationName = "agregarHoramedica")
    public String agregarHoramedica(@WebParam(name = "rutProfesional") String rutProfesional, 
            @WebParam(name = "rutAdministrador") String rutAdministrador, 
            @WebParam(name = "horaInicio") String horaInicio) 
            throws PersistentException {
        
        Gson gson = new GsonBuilder().create();
        String result = "";
        Horamedica horamedica = new Horamedica();
        
        horamedica = horamedica.crearHoraMedica(rutProfesional.toLowerCase(), 
                rutAdministrador.toLowerCase(), 
                horaInicio);
        result = gson.toJson(horamedica);
        return result;        
    }
    
    /**
     * Agregar una Hora medica con la correspondiente Reserva al Sistema
     * 
     * @param rutProfesional
     * @param rutAdministrador
     * @param rutPaciente
     * @param fechaReserva
     * @param motivoConsulta
     * @return String Reserva 
     */
    @WebMethod(operationName = "agregarReservaMedica")
    public String agregarReservaMedica(@WebParam(name = "rutProfesional") String rutProfesional,
            @WebParam(name = "rutAdministrador") String rutAdministrador,
            @WebParam(name = "rutPaciente") String rutPaciente,
            @WebParam(name = "fechaReserva") String fechaReserva,
            @WebParam(name = "motivoConsulta") String motivoConsulta) throws PersistentException{
        
        Gson gson = new GsonBuilder().create();        
        Horamedica horamedica = new Horamedica();
        Reserva reserva = new Reserva();
        
        //creacion de hora medica 
        horamedica = horamedica.crearHoraMedica(rutProfesional, rutAdministrador, fechaReserva);
        //creacion de reserva
        reserva = reserva.agregarReservaByRuts(rutPaciente, rutAdministrador, horamedica, fechaReserva, motivoConsulta);
        
        return gson.toJson(reserva);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "agregarPaciente")
    public String agregarPaciente(@WebParam(name = "id_persona") int id_persona, @WebParam(name = "descripcion") String descripcion, @WebParam(name = "tipo_prevision") int tipo_prevision, @WebParam(name = "activo") int activo) throws PersistentException {
        //TODO write your implementation code here:
        String rsult = null;
        capaNegocio.Paciente paciente= new capaNegocio.Paciente();
        
        paciente.setPersona_id(id_persona);
        paciente.setActivo(activo);
        paciente.setDescripcion(descripcion);
        Tipo_prevision s = new Tipo_prevision();
        s.setId(tipo_prevision);
        paciente.setTipo_prevision(s);
        paciente = paciente.agregarPaciente(paciente);
        
        Gson gson = new GsonBuilder().create();        
        rsult = gson.toJson(paciente);
        return rsult;
    }

}
