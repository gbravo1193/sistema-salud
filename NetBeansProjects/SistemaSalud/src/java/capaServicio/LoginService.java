package capaServicio;

import capaNegocio.*;
import com.google.gson.Gson;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;


@WebService(serviceName = "LoginService")
public class LoginService {
    /**
     * Metodo que verifica la valides de un usuario y contraseña ingresado con los datos 
     * de la base de datos.
     * @param rut - String - Rut que funciona como usuario
     * @param password - String - Contraseña asociada a la persona
     * @return 
     */
    @WebMethod(operationName = "verificarLogin")
    public String verificarLogin(
            @WebParam(name = "rut") String rut, 
            @WebParam(name = "password") String password) {
        String respuesta = "";
        Usuario user = new Usuario();
        boolean aux = user.verificarUsuario(rut, password);
        respuesta = new Gson().toJson(aux);
        return respuesta;
    }
    /**
     * Metodo para buscar una persona por su rut
     * @param rut - String - Rut de la persona a buscar
     * @return Gson - Persona  o Mensaje resultante
     */
    
    @WebMethod(operationName = "Persona")    
    public String buscarPersonaPorRut(@WebParam(name = "rut") String rut) {
        String respuesta = "";
        Persona persona= new Persona();
        persona= persona.buscarPersonaPorRut(rut);
        respuesta= new Gson().toJson(persona);
        return respuesta;
    }
}
