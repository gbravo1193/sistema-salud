/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class DeleteSaludData {
	public void deleteTestData() throws PersistentException {
		PersistentTransaction t = orm.SaludPersistentManager.instance().getSession().beginTransaction();
		try {
			orm.Paciente lormPaciente = orm.PacienteDAO.loadPacienteByQuery(null, null);
			// Delete the persistent object
			orm.PacienteDAO.delete(lormPaciente);
			orm.Persona lormPersona = orm.PersonaDAO.loadPersonaByQuery(null, null);
			// Delete the persistent object
			orm.PersonaDAO.delete(lormPersona);
			orm.Administrador lormAdministrador = orm.AdministradorDAO.loadAdministradorByQuery(null, null);
			// Delete the persistent object
			orm.AdministradorDAO.delete(lormAdministrador);
			orm.Horamedica lormHoramedica = orm.HoramedicaDAO.loadHoramedicaByQuery(null, null);
			// Delete the persistent object
			orm.HoramedicaDAO.delete(lormHoramedica);
			orm.Profesional lormProfesional = orm.ProfesionalDAO.loadProfesionalByQuery(null, null);
			// Delete the persistent object
			orm.ProfesionalDAO.delete(lormProfesional);
			orm.Reserva lormReserva = orm.ReservaDAO.loadReservaByQuery(null, null);
			// Delete the persistent object
			orm.ReservaDAO.delete(lormReserva);
			orm.Hora_examen_solicitado lormHora_examen_solicitado = orm.Hora_examen_solicitadoDAO.loadHora_examen_solicitadoByQuery(null, null);
			// Delete the persistent object
			orm.Hora_examen_solicitadoDAO.delete(lormHora_examen_solicitado);
			orm.Tipo_prevision lormTipo_prevision = orm.Tipo_previsionDAO.loadTipo_previsionByQuery(null, null);
			// Delete the persistent object
			orm.Tipo_previsionDAO.delete(lormTipo_prevision);
			orm.Especialidad lormEspecialidad = orm.EspecialidadDAO.loadEspecialidadByQuery(null, null);
			// Delete the persistent object
			orm.EspecialidadDAO.delete(lormEspecialidad);
			orm.Consulta_hce lormConsulta_hce = orm.Consulta_hceDAO.loadConsulta_hceByQuery(null, null);
			// Delete the persistent object
			orm.Consulta_hceDAO.delete(lormConsulta_hce);
			orm.Fichapaciente lormFichapaciente = orm.FichapacienteDAO.loadFichapacienteByQuery(null, null);
			// Delete the persistent object
			orm.FichapacienteDAO.delete(lormFichapaciente);
			orm.Examen lormExamen = orm.ExamenDAO.loadExamenByQuery(null, null);
			// Delete the persistent object
			orm.ExamenDAO.delete(lormExamen);
			orm.Procedencia lormProcedencia = orm.ProcedenciaDAO.loadProcedenciaByQuery(null, null);
			// Delete the persistent object
			orm.ProcedenciaDAO.delete(lormProcedencia);
			orm.Pago lormPago = orm.PagoDAO.loadPagoByQuery(null, null);
			// Delete the persistent object
			orm.PagoDAO.delete(lormPago);
			orm.Medio_pago lormMedio_pago = orm.Medio_pagoDAO.loadMedio_pagoByQuery(null, null);
			// Delete the persistent object
			orm.Medio_pagoDAO.delete(lormMedio_pago);
			orm.Rce lormRce = orm.RceDAO.loadRceByQuery(null, null);
			// Delete the persistent object
			orm.RceDAO.delete(lormRce);
			orm.Rce_examen_examen lormRce_examen_examen = orm.Rce_examen_examenDAO.loadRce_examen_examenByQuery(null, null);
			// Delete the persistent object
			orm.Rce_examen_examenDAO.delete(lormRce_examen_examen);
			orm.Bitacora lormBitacora = orm.BitacoraDAO.loadBitacoraByQuery(null, null);
			// Delete the persistent object
			orm.BitacoraDAO.delete(lormBitacora);
			orm.Estado_rce_examen lormEstado_rce_examen = orm.Estado_rce_examenDAO.loadEstado_rce_examenByQuery(null, null);
			// Delete the persistent object
			orm.Estado_rce_examenDAO.delete(lormEstado_rce_examen);
			orm.Box lormBox = orm.BoxDAO.loadBoxByQuery(null, null);
			// Delete the persistent object
			orm.BoxDAO.delete(lormBox);
			orm.Bono lormBono = orm.BonoDAO.loadBonoByQuery(null, null);
			// Delete the persistent object
			orm.BonoDAO.delete(lormBono);
			orm.Diagnostico lormDiagnostico = orm.DiagnosticoDAO.loadDiagnosticoByQuery(null, null);
			// Delete the persistent object
			orm.DiagnosticoDAO.delete(lormDiagnostico);
			orm.Rce_diagnostico lormRce_diagnostico = orm.Rce_diagnosticoDAO.loadRce_diagnosticoByQuery(null, null);
			// Delete the persistent object
			orm.Rce_diagnosticoDAO.delete(lormRce_diagnostico);
			orm.Alergias lormAlergias = orm.AlergiasDAO.loadAlergiasByQuery(null, null);
			// Delete the persistent object
			orm.AlergiasDAO.delete(lormAlergias);
			orm.Recetarce lormRecetarce = orm.RecetarceDAO.loadRecetarceByQuery(null, null);
			// Delete the persistent object
			orm.RecetarceDAO.delete(lormRecetarce);
			orm.Receta lormReceta = orm.RecetaDAO.loadRecetaByQuery(null, null);
			// Delete the persistent object
			orm.RecetaDAO.delete(lormReceta);
			orm.Insumo lormInsumo = orm.InsumoDAO.loadInsumoByQuery(null, null);
			// Delete the persistent object
			orm.InsumoDAO.delete(lormInsumo);
			orm.Rce_insumo lormRce_insumo = orm.Rce_insumoDAO.loadRce_insumoByQuery(null, null);
			// Delete the persistent object
			orm.Rce_insumoDAO.delete(lormRce_insumo);
			orm.Destino lormDestino = orm.DestinoDAO.loadDestinoByQuery(null, null);
			// Delete the persistent object
			orm.DestinoDAO.delete(lormDestino);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			DeleteSaludData deleteSaludData = new DeleteSaludData();
			try {
				deleteSaludData.deleteTestData();
			}
			finally {
				orm.SaludPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
