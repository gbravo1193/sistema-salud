/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class CreateSaludData {
	public void createTestData() throws PersistentException {
		PersistentTransaction t = orm.SaludPersistentManager.instance().getSession().beginTransaction();
		try {
			orm.Paciente lormPaciente = orm.PacienteDAO.createPaciente();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : receta, fichapaciente, reserva, tipo_prevision, persona
			orm.PacienteDAO.save(lormPaciente);
			orm.Persona lormPersona = orm.PersonaDAO.createPersona();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : bitacora, rce, consulta_hce1, consulta_hce, reserva, administrador, paciente, profesional, password, fecha_nacimiento, email, apellido_paterno, nombres, rut
			orm.PersonaDAO.save(lormPersona);
			orm.Administrador lormAdministrador = orm.AdministradorDAO.createAdministrador();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : horamedica, activo, persona
			orm.AdministradorDAO.save(lormAdministrador);
			orm.Horamedica lormHoramedica = orm.HoramedicaDAO.createHoramedica();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : hora_examen_solicitado, reserva, administrador, profesional
			orm.HoramedicaDAO.save(lormHoramedica);
			orm.Profesional lormProfesional = orm.ProfesionalDAO.createProfesional();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : horamedica, especialidad, persona
			orm.ProfesionalDAO.save(lormProfesional);
			orm.Reserva lormReserva = orm.ReservaDAO.createReserva();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : rce, hora_medica, persona_id_ingresa_reserva, paciente
			orm.ReservaDAO.save(lormReserva);
			orm.Hora_examen_solicitado lormHora_examen_solicitado = orm.Hora_examen_solicitadoDAO.createHora_examen_solicitado();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : examen, horamedica
			orm.Hora_examen_solicitadoDAO.save(lormHora_examen_solicitado);
			orm.Tipo_prevision lormTipo_prevision = orm.Tipo_previsionDAO.createTipo_prevision();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : bono, paciente
			orm.Tipo_previsionDAO.save(lormTipo_prevision);
			orm.Especialidad lormEspecialidad = orm.EspecialidadDAO.createEspecialidad();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : profesional
			orm.EspecialidadDAO.save(lormEspecialidad);
			orm.Consulta_hce lormConsulta_hce = orm.Consulta_hceDAO.createConsulta_hce();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : rce, personaid_paciente, personaid_profesional
			orm.Consulta_hceDAO.save(lormConsulta_hce);
			orm.Fichapaciente lormFichapaciente = orm.FichapacienteDAO.createFichapaciente();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : paciente
			orm.FichapacienteDAO.save(lormFichapaciente);
			orm.Examen lormExamen = orm.ExamenDAO.createExamen();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : rce_examen_examen, hora_examen_solicitado
			orm.ExamenDAO.save(lormExamen);
			orm.Procedencia lormProcedencia = orm.ProcedenciaDAO.createProcedencia();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : rce
			orm.ProcedenciaDAO.save(lormProcedencia);
			orm.Pago lormPago = orm.PagoDAO.createPago();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : bono, monto, rce_examen, medio_pago
			orm.PagoDAO.save(lormPago);
			orm.Medio_pago lormMedio_pago = orm.Medio_pagoDAO.createMedio_pago();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : pago
			orm.Medio_pagoDAO.save(lormMedio_pago);
			orm.Rce lormRce = orm.RceDAO.createRce();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : rce_insumo, recetarce, alergias, rce_diagnostico, bitacora, rce_examen_examen, pago, consulta_hce, indicaciones, cerrado, destino, box, persona_id_realiza_examen, reserva, procedencia
			orm.RceDAO.save(lormRce);
			orm.Rce_examen_examen lormRce_examen_examen = orm.Rce_examen_examenDAO.createRce_examen_examen();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : examen, rce
			orm.Rce_examen_examenDAO.save(lormRce_examen_examen);
			orm.Bitacora lormBitacora = orm.BitacoraDAO.createBitacora();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : persona_id_ingresa_bitacora, rce, estado_rce_examen
			orm.BitacoraDAO.save(lormBitacora);
			orm.Estado_rce_examen lormEstado_rce_examen = orm.Estado_rce_examenDAO.createEstado_rce_examen();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : bitacora
			orm.Estado_rce_examenDAO.save(lormEstado_rce_examen);
			orm.Box lormBox = orm.BoxDAO.createBox();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : rce
			orm.BoxDAO.save(lormBox);
			orm.Bono lormBono = orm.BonoDAO.createBono();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : tipo_prevision, pago
			orm.BonoDAO.save(lormBono);
			orm.Diagnostico lormDiagnostico = orm.DiagnosticoDAO.createDiagnostico();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : rce_diagnostico
			orm.DiagnosticoDAO.save(lormDiagnostico);
			orm.Rce_diagnostico lormRce_diagnostico = orm.Rce_diagnosticoDAO.createRce_diagnostico();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : diagnostico, rce
			orm.Rce_diagnosticoDAO.save(lormRce_diagnostico);
			orm.Alergias lormAlergias = orm.AlergiasDAO.createAlergias();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : rce
			orm.AlergiasDAO.save(lormAlergias);
			orm.Recetarce lormRecetarce = orm.RecetarceDAO.createRecetarce();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : receta, rce
			orm.RecetarceDAO.save(lormRecetarce);
			orm.Receta lormReceta = orm.RecetaDAO.createReceta();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : recetarce, paciente
			orm.RecetaDAO.save(lormReceta);
			orm.Insumo lormInsumo = orm.InsumoDAO.createInsumo();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : rce_insumo
			orm.InsumoDAO.save(lormInsumo);
			orm.Rce_insumo lormRce_insumo = orm.Rce_insumoDAO.createRce_insumo();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : cantidad, insumo, rce
			orm.Rce_insumoDAO.save(lormRce_insumo);
			orm.Destino lormDestino = orm.DestinoDAO.createDestino();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : rce
			orm.DestinoDAO.save(lormDestino);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			CreateSaludData createSaludData = new CreateSaludData();
			try {
				createSaludData.createTestData();
			}
			finally {
				orm.SaludPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
