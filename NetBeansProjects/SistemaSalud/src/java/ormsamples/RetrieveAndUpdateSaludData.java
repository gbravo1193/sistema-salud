/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class RetrieveAndUpdateSaludData {
	public void retrieveAndUpdateTestData() throws PersistentException {
		PersistentTransaction t = orm.SaludPersistentManager.instance().getSession().beginTransaction();
		try {
			orm.Paciente lormPaciente = orm.PacienteDAO.loadPacienteByQuery(null, null);
			// Update the properties of the persistent object
			orm.PacienteDAO.save(lormPaciente);
			orm.Persona lormPersona = orm.PersonaDAO.loadPersonaByQuery(null, null);
			// Update the properties of the persistent object
			orm.PersonaDAO.save(lormPersona);
			orm.Administrador lormAdministrador = orm.AdministradorDAO.loadAdministradorByQuery(null, null);
			// Update the properties of the persistent object
			orm.AdministradorDAO.save(lormAdministrador);
			orm.Horamedica lormHoramedica = orm.HoramedicaDAO.loadHoramedicaByQuery(null, null);
			// Update the properties of the persistent object
			orm.HoramedicaDAO.save(lormHoramedica);
			orm.Profesional lormProfesional = orm.ProfesionalDAO.loadProfesionalByQuery(null, null);
			// Update the properties of the persistent object
			orm.ProfesionalDAO.save(lormProfesional);
			orm.Reserva lormReserva = orm.ReservaDAO.loadReservaByQuery(null, null);
			// Update the properties of the persistent object
			orm.ReservaDAO.save(lormReserva);
			orm.Hora_examen_solicitado lormHora_examen_solicitado = orm.Hora_examen_solicitadoDAO.loadHora_examen_solicitadoByQuery(null, null);
			// Update the properties of the persistent object
			orm.Hora_examen_solicitadoDAO.save(lormHora_examen_solicitado);
			orm.Tipo_prevision lormTipo_prevision = orm.Tipo_previsionDAO.loadTipo_previsionByQuery(null, null);
			// Update the properties of the persistent object
			orm.Tipo_previsionDAO.save(lormTipo_prevision);
			orm.Especialidad lormEspecialidad = orm.EspecialidadDAO.loadEspecialidadByQuery(null, null);
			// Update the properties of the persistent object
			orm.EspecialidadDAO.save(lormEspecialidad);
			orm.Consulta_hce lormConsulta_hce = orm.Consulta_hceDAO.loadConsulta_hceByQuery(null, null);
			// Update the properties of the persistent object
			orm.Consulta_hceDAO.save(lormConsulta_hce);
			orm.Fichapaciente lormFichapaciente = orm.FichapacienteDAO.loadFichapacienteByQuery(null, null);
			// Update the properties of the persistent object
			orm.FichapacienteDAO.save(lormFichapaciente);
			orm.Examen lormExamen = orm.ExamenDAO.loadExamenByQuery(null, null);
			// Update the properties of the persistent object
			orm.ExamenDAO.save(lormExamen);
			orm.Procedencia lormProcedencia = orm.ProcedenciaDAO.loadProcedenciaByQuery(null, null);
			// Update the properties of the persistent object
			orm.ProcedenciaDAO.save(lormProcedencia);
			orm.Pago lormPago = orm.PagoDAO.loadPagoByQuery(null, null);
			// Update the properties of the persistent object
			orm.PagoDAO.save(lormPago);
			orm.Medio_pago lormMedio_pago = orm.Medio_pagoDAO.loadMedio_pagoByQuery(null, null);
			// Update the properties of the persistent object
			orm.Medio_pagoDAO.save(lormMedio_pago);
			orm.Rce lormRce = orm.RceDAO.loadRceByQuery(null, null);
			// Update the properties of the persistent object
			orm.RceDAO.save(lormRce);
			orm.Rce_examen_examen lormRce_examen_examen = orm.Rce_examen_examenDAO.loadRce_examen_examenByQuery(null, null);
			// Update the properties of the persistent object
			orm.Rce_examen_examenDAO.save(lormRce_examen_examen);
			orm.Bitacora lormBitacora = orm.BitacoraDAO.loadBitacoraByQuery(null, null);
			// Update the properties of the persistent object
			orm.BitacoraDAO.save(lormBitacora);
			orm.Estado_rce_examen lormEstado_rce_examen = orm.Estado_rce_examenDAO.loadEstado_rce_examenByQuery(null, null);
			// Update the properties of the persistent object
			orm.Estado_rce_examenDAO.save(lormEstado_rce_examen);
			orm.Box lormBox = orm.BoxDAO.loadBoxByQuery(null, null);
			// Update the properties of the persistent object
			orm.BoxDAO.save(lormBox);
			orm.Bono lormBono = orm.BonoDAO.loadBonoByQuery(null, null);
			// Update the properties of the persistent object
			orm.BonoDAO.save(lormBono);
			orm.Diagnostico lormDiagnostico = orm.DiagnosticoDAO.loadDiagnosticoByQuery(null, null);
			// Update the properties of the persistent object
			orm.DiagnosticoDAO.save(lormDiagnostico);
			orm.Rce_diagnostico lormRce_diagnostico = orm.Rce_diagnosticoDAO.loadRce_diagnosticoByQuery(null, null);
			// Update the properties of the persistent object
			orm.Rce_diagnosticoDAO.save(lormRce_diagnostico);
			orm.Alergias lormAlergias = orm.AlergiasDAO.loadAlergiasByQuery(null, null);
			// Update the properties of the persistent object
			orm.AlergiasDAO.save(lormAlergias);
			orm.Recetarce lormRecetarce = orm.RecetarceDAO.loadRecetarceByQuery(null, null);
			// Update the properties of the persistent object
			orm.RecetarceDAO.save(lormRecetarce);
			orm.Receta lormReceta = orm.RecetaDAO.loadRecetaByQuery(null, null);
			// Update the properties of the persistent object
			orm.RecetaDAO.save(lormReceta);
			orm.Insumo lormInsumo = orm.InsumoDAO.loadInsumoByQuery(null, null);
			// Update the properties of the persistent object
			orm.InsumoDAO.save(lormInsumo);
			orm.Rce_insumo lormRce_insumo = orm.Rce_insumoDAO.loadRce_insumoByQuery(null, null);
			// Update the properties of the persistent object
			orm.Rce_insumoDAO.save(lormRce_insumo);
			orm.Destino lormDestino = orm.DestinoDAO.loadDestinoByQuery(null, null);
			// Update the properties of the persistent object
			orm.DestinoDAO.save(lormDestino);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public void retrieveByCriteria() throws PersistentException {
		System.out.println("Retrieving Paciente by PacienteCriteria");
		orm.PacienteCriteria lormPacienteCriteria = new orm.PacienteCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormPacienteCriteria.id.eq();
		System.out.println(lormPacienteCriteria.uniquePaciente());
		
		System.out.println("Retrieving Persona by PersonaCriteria");
		orm.PersonaCriteria lormPersonaCriteria = new orm.PersonaCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormPersonaCriteria.id.eq();
		System.out.println(lormPersonaCriteria.uniquePersona());
		
		System.out.println("Retrieving Administrador by AdministradorCriteria");
		orm.AdministradorCriteria lormAdministradorCriteria = new orm.AdministradorCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormAdministradorCriteria.id.eq();
		System.out.println(lormAdministradorCriteria.uniqueAdministrador());
		
		System.out.println("Retrieving Horamedica by HoramedicaCriteria");
		orm.HoramedicaCriteria lormHoramedicaCriteria = new orm.HoramedicaCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormHoramedicaCriteria.id.eq();
		System.out.println(lormHoramedicaCriteria.uniqueHoramedica());
		
		System.out.println("Retrieving Profesional by ProfesionalCriteria");
		orm.ProfesionalCriteria lormProfesionalCriteria = new orm.ProfesionalCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormProfesionalCriteria.id.eq();
		System.out.println(lormProfesionalCriteria.uniqueProfesional());
		
		System.out.println("Retrieving Reserva by ReservaCriteria");
		orm.ReservaCriteria lormReservaCriteria = new orm.ReservaCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormReservaCriteria.id.eq();
		System.out.println(lormReservaCriteria.uniqueReserva());
		
		System.out.println("Retrieving Hora_examen_solicitado by Hora_examen_solicitadoCriteria");
		orm.Hora_examen_solicitadoCriteria lormHora_examen_solicitadoCriteria = new orm.Hora_examen_solicitadoCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormHora_examen_solicitadoCriteria.id.eq();
		System.out.println(lormHora_examen_solicitadoCriteria.uniqueHora_examen_solicitado());
		
		System.out.println("Retrieving Tipo_prevision by Tipo_previsionCriteria");
		orm.Tipo_previsionCriteria lormTipo_previsionCriteria = new orm.Tipo_previsionCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormTipo_previsionCriteria.id.eq();
		System.out.println(lormTipo_previsionCriteria.uniqueTipo_prevision());
		
		System.out.println("Retrieving Especialidad by EspecialidadCriteria");
		orm.EspecialidadCriteria lormEspecialidadCriteria = new orm.EspecialidadCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormEspecialidadCriteria.id.eq();
		System.out.println(lormEspecialidadCriteria.uniqueEspecialidad());
		
		System.out.println("Retrieving Consulta_hce by Consulta_hceCriteria");
		orm.Consulta_hceCriteria lormConsulta_hceCriteria = new orm.Consulta_hceCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormConsulta_hceCriteria.id.eq();
		System.out.println(lormConsulta_hceCriteria.uniqueConsulta_hce());
		
		System.out.println("Retrieving Fichapaciente by FichapacienteCriteria");
		orm.FichapacienteCriteria lormFichapacienteCriteria = new orm.FichapacienteCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormFichapacienteCriteria.id.eq();
		System.out.println(lormFichapacienteCriteria.uniqueFichapaciente());
		
		System.out.println("Retrieving Examen by ExamenCriteria");
		orm.ExamenCriteria lormExamenCriteria = new orm.ExamenCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormExamenCriteria.id.eq();
		System.out.println(lormExamenCriteria.uniqueExamen());
		
		System.out.println("Retrieving Procedencia by ProcedenciaCriteria");
		orm.ProcedenciaCriteria lormProcedenciaCriteria = new orm.ProcedenciaCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormProcedenciaCriteria.id.eq();
		System.out.println(lormProcedenciaCriteria.uniqueProcedencia());
		
		System.out.println("Retrieving Pago by PagoCriteria");
		orm.PagoCriteria lormPagoCriteria = new orm.PagoCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormPagoCriteria.id.eq();
		System.out.println(lormPagoCriteria.uniquePago());
		
		System.out.println("Retrieving Medio_pago by Medio_pagoCriteria");
		orm.Medio_pagoCriteria lormMedio_pagoCriteria = new orm.Medio_pagoCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormMedio_pagoCriteria.id.eq();
		System.out.println(lormMedio_pagoCriteria.uniqueMedio_pago());
		
		System.out.println("Retrieving Rce by RceCriteria");
		orm.RceCriteria lormRceCriteria = new orm.RceCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormRceCriteria.id.eq();
		System.out.println(lormRceCriteria.uniqueRce());
		
		System.out.println("Retrieving Rce_examen_examen by Rce_examen_examenCriteria");
		orm.Rce_examen_examenCriteria lormRce_examen_examenCriteria = new orm.Rce_examen_examenCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormRce_examen_examenCriteria.id.eq();
		System.out.println(lormRce_examen_examenCriteria.uniqueRce_examen_examen());
		
		System.out.println("Retrieving Bitacora by BitacoraCriteria");
		orm.BitacoraCriteria lormBitacoraCriteria = new orm.BitacoraCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormBitacoraCriteria.id.eq();
		System.out.println(lormBitacoraCriteria.uniqueBitacora());
		
		System.out.println("Retrieving Estado_rce_examen by Estado_rce_examenCriteria");
		orm.Estado_rce_examenCriteria lormEstado_rce_examenCriteria = new orm.Estado_rce_examenCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormEstado_rce_examenCriteria.id.eq();
		System.out.println(lormEstado_rce_examenCriteria.uniqueEstado_rce_examen());
		
		System.out.println("Retrieving Box by BoxCriteria");
		orm.BoxCriteria lormBoxCriteria = new orm.BoxCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormBoxCriteria.id.eq();
		System.out.println(lormBoxCriteria.uniqueBox());
		
		System.out.println("Retrieving Bono by BonoCriteria");
		orm.BonoCriteria lormBonoCriteria = new orm.BonoCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormBonoCriteria.id.eq();
		System.out.println(lormBonoCriteria.uniqueBono());
		
		System.out.println("Retrieving Diagnostico by DiagnosticoCriteria");
		orm.DiagnosticoCriteria lormDiagnosticoCriteria = new orm.DiagnosticoCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormDiagnosticoCriteria.id.eq();
		System.out.println(lormDiagnosticoCriteria.uniqueDiagnostico());
		
		System.out.println("Retrieving Rce_diagnostico by Rce_diagnosticoCriteria");
		orm.Rce_diagnosticoCriteria lormRce_diagnosticoCriteria = new orm.Rce_diagnosticoCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormRce_diagnosticoCriteria.id.eq();
		System.out.println(lormRce_diagnosticoCriteria.uniqueRce_diagnostico());
		
		System.out.println("Retrieving Alergias by AlergiasCriteria");
		orm.AlergiasCriteria lormAlergiasCriteria = new orm.AlergiasCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormAlergiasCriteria.id.eq();
		System.out.println(lormAlergiasCriteria.uniqueAlergias());
		
		System.out.println("Retrieving Recetarce by RecetarceCriteria");
		orm.RecetarceCriteria lormRecetarceCriteria = new orm.RecetarceCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormRecetarceCriteria.id.eq();
		System.out.println(lormRecetarceCriteria.uniqueRecetarce());
		
		System.out.println("Retrieving Receta by RecetaCriteria");
		orm.RecetaCriteria lormRecetaCriteria = new orm.RecetaCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormRecetaCriteria.id.eq();
		System.out.println(lormRecetaCriteria.uniqueReceta());
		
		System.out.println("Retrieving Insumo by InsumoCriteria");
		orm.InsumoCriteria lormInsumoCriteria = new orm.InsumoCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormInsumoCriteria.id.eq();
		System.out.println(lormInsumoCriteria.uniqueInsumo());
		
		System.out.println("Retrieving Rce_insumo by Rce_insumoCriteria");
		orm.Rce_insumoCriteria lormRce_insumoCriteria = new orm.Rce_insumoCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormRce_insumoCriteria.id.eq();
		System.out.println(lormRce_insumoCriteria.uniqueRce_insumo());
		
		System.out.println("Retrieving Destino by DestinoCriteria");
		orm.DestinoCriteria lormDestinoCriteria = new orm.DestinoCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormDestinoCriteria.id.eq();
		System.out.println(lormDestinoCriteria.uniqueDestino());
		
	}
	
	
	public static void main(String[] args) {
		try {
			RetrieveAndUpdateSaludData retrieveAndUpdateSaludData = new RetrieveAndUpdateSaludData();
			try {
				retrieveAndUpdateSaludData.retrieveAndUpdateTestData();
				//retrieveAndUpdateSaludData.retrieveByCriteria();
			}
			finally {
				orm.SaludPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
