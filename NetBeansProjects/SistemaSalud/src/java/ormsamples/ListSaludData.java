/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class ListSaludData {
	private static final int ROW_COUNT = 100;
	
	public void listTestData() throws PersistentException {
		System.out.println("Listing Paciente...");
		orm.Paciente[] ormPacientes = orm.PacienteDAO.listPacienteByQuery(null, null);
		int length = Math.min(ormPacientes.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormPacientes[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Persona...");
		orm.Persona[] ormPersonas = orm.PersonaDAO.listPersonaByQuery(null, null);
		length = Math.min(ormPersonas.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormPersonas[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Administrador...");
		orm.Administrador[] ormAdministradors = orm.AdministradorDAO.listAdministradorByQuery(null, null);
		length = Math.min(ormAdministradors.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormAdministradors[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Horamedica...");
		orm.Horamedica[] ormHoramedicas = orm.HoramedicaDAO.listHoramedicaByQuery(null, null);
		length = Math.min(ormHoramedicas.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormHoramedicas[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Profesional...");
		orm.Profesional[] ormProfesionals = orm.ProfesionalDAO.listProfesionalByQuery(null, null);
		length = Math.min(ormProfesionals.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormProfesionals[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Reserva...");
		orm.Reserva[] ormReservas = orm.ReservaDAO.listReservaByQuery(null, null);
		length = Math.min(ormReservas.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormReservas[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Hora_examen_solicitado...");
		orm.Hora_examen_solicitado[] ormHora_examen_solicitados = orm.Hora_examen_solicitadoDAO.listHora_examen_solicitadoByQuery(null, null);
		length = Math.min(ormHora_examen_solicitados.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormHora_examen_solicitados[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Tipo_prevision...");
		orm.Tipo_prevision[] ormTipo_previsions = orm.Tipo_previsionDAO.listTipo_previsionByQuery(null, null);
		length = Math.min(ormTipo_previsions.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormTipo_previsions[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Especialidad...");
		orm.Especialidad[] ormEspecialidads = orm.EspecialidadDAO.listEspecialidadByQuery(null, null);
		length = Math.min(ormEspecialidads.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormEspecialidads[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Consulta_hce...");
		orm.Consulta_hce[] ormConsulta_hces = orm.Consulta_hceDAO.listConsulta_hceByQuery(null, null);
		length = Math.min(ormConsulta_hces.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormConsulta_hces[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Fichapaciente...");
		orm.Fichapaciente[] ormFichapacientes = orm.FichapacienteDAO.listFichapacienteByQuery(null, null);
		length = Math.min(ormFichapacientes.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormFichapacientes[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Examen...");
		orm.Examen[] ormExamens = orm.ExamenDAO.listExamenByQuery(null, null);
		length = Math.min(ormExamens.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormExamens[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Procedencia...");
		orm.Procedencia[] ormProcedencias = orm.ProcedenciaDAO.listProcedenciaByQuery(null, null);
		length = Math.min(ormProcedencias.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormProcedencias[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Pago...");
		orm.Pago[] ormPagos = orm.PagoDAO.listPagoByQuery(null, null);
		length = Math.min(ormPagos.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormPagos[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Medio_pago...");
		orm.Medio_pago[] ormMedio_pagos = orm.Medio_pagoDAO.listMedio_pagoByQuery(null, null);
		length = Math.min(ormMedio_pagos.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormMedio_pagos[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Rce...");
		orm.Rce[] ormRces = orm.RceDAO.listRceByQuery(null, null);
		length = Math.min(ormRces.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormRces[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Rce_examen_examen...");
		orm.Rce_examen_examen[] ormRce_examen_examens = orm.Rce_examen_examenDAO.listRce_examen_examenByQuery(null, null);
		length = Math.min(ormRce_examen_examens.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormRce_examen_examens[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Bitacora...");
		orm.Bitacora[] ormBitacoras = orm.BitacoraDAO.listBitacoraByQuery(null, null);
		length = Math.min(ormBitacoras.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormBitacoras[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Estado_rce_examen...");
		orm.Estado_rce_examen[] ormEstado_rce_examens = orm.Estado_rce_examenDAO.listEstado_rce_examenByQuery(null, null);
		length = Math.min(ormEstado_rce_examens.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormEstado_rce_examens[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Box...");
		orm.Box[] ormBoxs = orm.BoxDAO.listBoxByQuery(null, null);
		length = Math.min(ormBoxs.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormBoxs[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Bono...");
		orm.Bono[] ormBonos = orm.BonoDAO.listBonoByQuery(null, null);
		length = Math.min(ormBonos.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormBonos[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Diagnostico...");
		orm.Diagnostico[] ormDiagnosticos = orm.DiagnosticoDAO.listDiagnosticoByQuery(null, null);
		length = Math.min(ormDiagnosticos.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormDiagnosticos[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Rce_diagnostico...");
		orm.Rce_diagnostico[] ormRce_diagnosticos = orm.Rce_diagnosticoDAO.listRce_diagnosticoByQuery(null, null);
		length = Math.min(ormRce_diagnosticos.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormRce_diagnosticos[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Alergias...");
		orm.Alergias[] ormAlergiases = orm.AlergiasDAO.listAlergiasByQuery(null, null);
		length = Math.min(ormAlergiases.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormAlergiases[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Recetarce...");
		orm.Recetarce[] ormRecetarces = orm.RecetarceDAO.listRecetarceByQuery(null, null);
		length = Math.min(ormRecetarces.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormRecetarces[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Receta...");
		orm.Receta[] ormRecetas = orm.RecetaDAO.listRecetaByQuery(null, null);
		length = Math.min(ormRecetas.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormRecetas[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Insumo...");
		orm.Insumo[] ormInsumos = orm.InsumoDAO.listInsumoByQuery(null, null);
		length = Math.min(ormInsumos.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormInsumos[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Rce_insumo...");
		orm.Rce_insumo[] ormRce_insumos = orm.Rce_insumoDAO.listRce_insumoByQuery(null, null);
		length = Math.min(ormRce_insumos.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormRce_insumos[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Destino...");
		orm.Destino[] ormDestinos = orm.DestinoDAO.listDestinoByQuery(null, null);
		length = Math.min(ormDestinos.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormDestinos[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
	}
	
	public void listByCriteria() throws PersistentException {
		System.out.println("Listing Paciente by Criteria...");
		orm.PacienteCriteria lormPacienteCriteria = new orm.PacienteCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormPacienteCriteria.id.eq();
		lormPacienteCriteria.setMaxResults(ROW_COUNT);
		orm.Paciente[] ormPacientes = lormPacienteCriteria.listPaciente();
		int length =ormPacientes== null ? 0 : Math.min(ormPacientes.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormPacientes[i]);
		}
		System.out.println(length + " Paciente record(s) retrieved."); 
		
		System.out.println("Listing Persona by Criteria...");
		orm.PersonaCriteria lormPersonaCriteria = new orm.PersonaCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormPersonaCriteria.id.eq();
		lormPersonaCriteria.setMaxResults(ROW_COUNT);
		orm.Persona[] ormPersonas = lormPersonaCriteria.listPersona();
		length =ormPersonas== null ? 0 : Math.min(ormPersonas.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormPersonas[i]);
		}
		System.out.println(length + " Persona record(s) retrieved."); 
		
		System.out.println("Listing Administrador by Criteria...");
		orm.AdministradorCriteria lormAdministradorCriteria = new orm.AdministradorCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormAdministradorCriteria.id.eq();
		lormAdministradorCriteria.setMaxResults(ROW_COUNT);
		orm.Administrador[] ormAdministradors = lormAdministradorCriteria.listAdministrador();
		length =ormAdministradors== null ? 0 : Math.min(ormAdministradors.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormAdministradors[i]);
		}
		System.out.println(length + " Administrador record(s) retrieved."); 
		
		System.out.println("Listing Horamedica by Criteria...");
		orm.HoramedicaCriteria lormHoramedicaCriteria = new orm.HoramedicaCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormHoramedicaCriteria.id.eq();
		lormHoramedicaCriteria.setMaxResults(ROW_COUNT);
		orm.Horamedica[] ormHoramedicas = lormHoramedicaCriteria.listHoramedica();
		length =ormHoramedicas== null ? 0 : Math.min(ormHoramedicas.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormHoramedicas[i]);
		}
		System.out.println(length + " Horamedica record(s) retrieved."); 
		
		System.out.println("Listing Profesional by Criteria...");
		orm.ProfesionalCriteria lormProfesionalCriteria = new orm.ProfesionalCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormProfesionalCriteria.id.eq();
		lormProfesionalCriteria.setMaxResults(ROW_COUNT);
		orm.Profesional[] ormProfesionals = lormProfesionalCriteria.listProfesional();
		length =ormProfesionals== null ? 0 : Math.min(ormProfesionals.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormProfesionals[i]);
		}
		System.out.println(length + " Profesional record(s) retrieved."); 
		
		System.out.println("Listing Reserva by Criteria...");
		orm.ReservaCriteria lormReservaCriteria = new orm.ReservaCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormReservaCriteria.id.eq();
		lormReservaCriteria.setMaxResults(ROW_COUNT);
		orm.Reserva[] ormReservas = lormReservaCriteria.listReserva();
		length =ormReservas== null ? 0 : Math.min(ormReservas.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormReservas[i]);
		}
		System.out.println(length + " Reserva record(s) retrieved."); 
		
		System.out.println("Listing Hora_examen_solicitado by Criteria...");
		orm.Hora_examen_solicitadoCriteria lormHora_examen_solicitadoCriteria = new orm.Hora_examen_solicitadoCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormHora_examen_solicitadoCriteria.id.eq();
		lormHora_examen_solicitadoCriteria.setMaxResults(ROW_COUNT);
		orm.Hora_examen_solicitado[] ormHora_examen_solicitados = lormHora_examen_solicitadoCriteria.listHora_examen_solicitado();
		length =ormHora_examen_solicitados== null ? 0 : Math.min(ormHora_examen_solicitados.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormHora_examen_solicitados[i]);
		}
		System.out.println(length + " Hora_examen_solicitado record(s) retrieved."); 
		
		System.out.println("Listing Tipo_prevision by Criteria...");
		orm.Tipo_previsionCriteria lormTipo_previsionCriteria = new orm.Tipo_previsionCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormTipo_previsionCriteria.id.eq();
		lormTipo_previsionCriteria.setMaxResults(ROW_COUNT);
		orm.Tipo_prevision[] ormTipo_previsions = lormTipo_previsionCriteria.listTipo_prevision();
		length =ormTipo_previsions== null ? 0 : Math.min(ormTipo_previsions.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormTipo_previsions[i]);
		}
		System.out.println(length + " Tipo_prevision record(s) retrieved."); 
		
		System.out.println("Listing Especialidad by Criteria...");
		orm.EspecialidadCriteria lormEspecialidadCriteria = new orm.EspecialidadCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormEspecialidadCriteria.id.eq();
		lormEspecialidadCriteria.setMaxResults(ROW_COUNT);
		orm.Especialidad[] ormEspecialidads = lormEspecialidadCriteria.listEspecialidad();
		length =ormEspecialidads== null ? 0 : Math.min(ormEspecialidads.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormEspecialidads[i]);
		}
		System.out.println(length + " Especialidad record(s) retrieved."); 
		
		System.out.println("Listing Consulta_hce by Criteria...");
		orm.Consulta_hceCriteria lormConsulta_hceCriteria = new orm.Consulta_hceCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormConsulta_hceCriteria.id.eq();
		lormConsulta_hceCriteria.setMaxResults(ROW_COUNT);
		orm.Consulta_hce[] ormConsulta_hces = lormConsulta_hceCriteria.listConsulta_hce();
		length =ormConsulta_hces== null ? 0 : Math.min(ormConsulta_hces.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormConsulta_hces[i]);
		}
		System.out.println(length + " Consulta_hce record(s) retrieved."); 
		
		System.out.println("Listing Fichapaciente by Criteria...");
		orm.FichapacienteCriteria lormFichapacienteCriteria = new orm.FichapacienteCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormFichapacienteCriteria.id.eq();
		lormFichapacienteCriteria.setMaxResults(ROW_COUNT);
		orm.Fichapaciente[] ormFichapacientes = lormFichapacienteCriteria.listFichapaciente();
		length =ormFichapacientes== null ? 0 : Math.min(ormFichapacientes.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormFichapacientes[i]);
		}
		System.out.println(length + " Fichapaciente record(s) retrieved."); 
		
		System.out.println("Listing Examen by Criteria...");
		orm.ExamenCriteria lormExamenCriteria = new orm.ExamenCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormExamenCriteria.id.eq();
		lormExamenCriteria.setMaxResults(ROW_COUNT);
		orm.Examen[] ormExamens = lormExamenCriteria.listExamen();
		length =ormExamens== null ? 0 : Math.min(ormExamens.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormExamens[i]);
		}
		System.out.println(length + " Examen record(s) retrieved."); 
		
		System.out.println("Listing Procedencia by Criteria...");
		orm.ProcedenciaCriteria lormProcedenciaCriteria = new orm.ProcedenciaCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormProcedenciaCriteria.id.eq();
		lormProcedenciaCriteria.setMaxResults(ROW_COUNT);
		orm.Procedencia[] ormProcedencias = lormProcedenciaCriteria.listProcedencia();
		length =ormProcedencias== null ? 0 : Math.min(ormProcedencias.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormProcedencias[i]);
		}
		System.out.println(length + " Procedencia record(s) retrieved."); 
		
		System.out.println("Listing Pago by Criteria...");
		orm.PagoCriteria lormPagoCriteria = new orm.PagoCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormPagoCriteria.id.eq();
		lormPagoCriteria.setMaxResults(ROW_COUNT);
		orm.Pago[] ormPagos = lormPagoCriteria.listPago();
		length =ormPagos== null ? 0 : Math.min(ormPagos.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormPagos[i]);
		}
		System.out.println(length + " Pago record(s) retrieved."); 
		
		System.out.println("Listing Medio_pago by Criteria...");
		orm.Medio_pagoCriteria lormMedio_pagoCriteria = new orm.Medio_pagoCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormMedio_pagoCriteria.id.eq();
		lormMedio_pagoCriteria.setMaxResults(ROW_COUNT);
		orm.Medio_pago[] ormMedio_pagos = lormMedio_pagoCriteria.listMedio_pago();
		length =ormMedio_pagos== null ? 0 : Math.min(ormMedio_pagos.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormMedio_pagos[i]);
		}
		System.out.println(length + " Medio_pago record(s) retrieved."); 
		
		System.out.println("Listing Rce by Criteria...");
		orm.RceCriteria lormRceCriteria = new orm.RceCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormRceCriteria.id.eq();
		lormRceCriteria.setMaxResults(ROW_COUNT);
		orm.Rce[] ormRces = lormRceCriteria.listRce();
		length =ormRces== null ? 0 : Math.min(ormRces.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormRces[i]);
		}
		System.out.println(length + " Rce record(s) retrieved."); 
		
		System.out.println("Listing Rce_examen_examen by Criteria...");
		orm.Rce_examen_examenCriteria lormRce_examen_examenCriteria = new orm.Rce_examen_examenCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormRce_examen_examenCriteria.id.eq();
		lormRce_examen_examenCriteria.setMaxResults(ROW_COUNT);
		orm.Rce_examen_examen[] ormRce_examen_examens = lormRce_examen_examenCriteria.listRce_examen_examen();
		length =ormRce_examen_examens== null ? 0 : Math.min(ormRce_examen_examens.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormRce_examen_examens[i]);
		}
		System.out.println(length + " Rce_examen_examen record(s) retrieved."); 
		
		System.out.println("Listing Bitacora by Criteria...");
		orm.BitacoraCriteria lormBitacoraCriteria = new orm.BitacoraCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormBitacoraCriteria.id.eq();
		lormBitacoraCriteria.setMaxResults(ROW_COUNT);
		orm.Bitacora[] ormBitacoras = lormBitacoraCriteria.listBitacora();
		length =ormBitacoras== null ? 0 : Math.min(ormBitacoras.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormBitacoras[i]);
		}
		System.out.println(length + " Bitacora record(s) retrieved."); 
		
		System.out.println("Listing Estado_rce_examen by Criteria...");
		orm.Estado_rce_examenCriteria lormEstado_rce_examenCriteria = new orm.Estado_rce_examenCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormEstado_rce_examenCriteria.id.eq();
		lormEstado_rce_examenCriteria.setMaxResults(ROW_COUNT);
		orm.Estado_rce_examen[] ormEstado_rce_examens = lormEstado_rce_examenCriteria.listEstado_rce_examen();
		length =ormEstado_rce_examens== null ? 0 : Math.min(ormEstado_rce_examens.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormEstado_rce_examens[i]);
		}
		System.out.println(length + " Estado_rce_examen record(s) retrieved."); 
		
		System.out.println("Listing Box by Criteria...");
		orm.BoxCriteria lormBoxCriteria = new orm.BoxCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormBoxCriteria.id.eq();
		lormBoxCriteria.setMaxResults(ROW_COUNT);
		orm.Box[] ormBoxs = lormBoxCriteria.listBox();
		length =ormBoxs== null ? 0 : Math.min(ormBoxs.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormBoxs[i]);
		}
		System.out.println(length + " Box record(s) retrieved."); 
		
		System.out.println("Listing Bono by Criteria...");
		orm.BonoCriteria lormBonoCriteria = new orm.BonoCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormBonoCriteria.id.eq();
		lormBonoCriteria.setMaxResults(ROW_COUNT);
		orm.Bono[] ormBonos = lormBonoCriteria.listBono();
		length =ormBonos== null ? 0 : Math.min(ormBonos.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormBonos[i]);
		}
		System.out.println(length + " Bono record(s) retrieved."); 
		
		System.out.println("Listing Diagnostico by Criteria...");
		orm.DiagnosticoCriteria lormDiagnosticoCriteria = new orm.DiagnosticoCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormDiagnosticoCriteria.id.eq();
		lormDiagnosticoCriteria.setMaxResults(ROW_COUNT);
		orm.Diagnostico[] ormDiagnosticos = lormDiagnosticoCriteria.listDiagnostico();
		length =ormDiagnosticos== null ? 0 : Math.min(ormDiagnosticos.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormDiagnosticos[i]);
		}
		System.out.println(length + " Diagnostico record(s) retrieved."); 
		
		System.out.println("Listing Rce_diagnostico by Criteria...");
		orm.Rce_diagnosticoCriteria lormRce_diagnosticoCriteria = new orm.Rce_diagnosticoCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormRce_diagnosticoCriteria.id.eq();
		lormRce_diagnosticoCriteria.setMaxResults(ROW_COUNT);
		orm.Rce_diagnostico[] ormRce_diagnosticos = lormRce_diagnosticoCriteria.listRce_diagnostico();
		length =ormRce_diagnosticos== null ? 0 : Math.min(ormRce_diagnosticos.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormRce_diagnosticos[i]);
		}
		System.out.println(length + " Rce_diagnostico record(s) retrieved."); 
		
		System.out.println("Listing Alergias by Criteria...");
		orm.AlergiasCriteria lormAlergiasCriteria = new orm.AlergiasCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormAlergiasCriteria.id.eq();
		lormAlergiasCriteria.setMaxResults(ROW_COUNT);
		orm.Alergias[] ormAlergiases = lormAlergiasCriteria.listAlergias();
		length =ormAlergiases== null ? 0 : Math.min(ormAlergiases.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormAlergiases[i]);
		}
		System.out.println(length + " Alergias record(s) retrieved."); 
		
		System.out.println("Listing Recetarce by Criteria...");
		orm.RecetarceCriteria lormRecetarceCriteria = new orm.RecetarceCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormRecetarceCriteria.id.eq();
		lormRecetarceCriteria.setMaxResults(ROW_COUNT);
		orm.Recetarce[] ormRecetarces = lormRecetarceCriteria.listRecetarce();
		length =ormRecetarces== null ? 0 : Math.min(ormRecetarces.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormRecetarces[i]);
		}
		System.out.println(length + " Recetarce record(s) retrieved."); 
		
		System.out.println("Listing Receta by Criteria...");
		orm.RecetaCriteria lormRecetaCriteria = new orm.RecetaCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormRecetaCriteria.id.eq();
		lormRecetaCriteria.setMaxResults(ROW_COUNT);
		orm.Receta[] ormRecetas = lormRecetaCriteria.listReceta();
		length =ormRecetas== null ? 0 : Math.min(ormRecetas.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormRecetas[i]);
		}
		System.out.println(length + " Receta record(s) retrieved."); 
		
		System.out.println("Listing Insumo by Criteria...");
		orm.InsumoCriteria lormInsumoCriteria = new orm.InsumoCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormInsumoCriteria.id.eq();
		lormInsumoCriteria.setMaxResults(ROW_COUNT);
		orm.Insumo[] ormInsumos = lormInsumoCriteria.listInsumo();
		length =ormInsumos== null ? 0 : Math.min(ormInsumos.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormInsumos[i]);
		}
		System.out.println(length + " Insumo record(s) retrieved."); 
		
		System.out.println("Listing Rce_insumo by Criteria...");
		orm.Rce_insumoCriteria lormRce_insumoCriteria = new orm.Rce_insumoCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormRce_insumoCriteria.id.eq();
		lormRce_insumoCriteria.setMaxResults(ROW_COUNT);
		orm.Rce_insumo[] ormRce_insumos = lormRce_insumoCriteria.listRce_insumo();
		length =ormRce_insumos== null ? 0 : Math.min(ormRce_insumos.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormRce_insumos[i]);
		}
		System.out.println(length + " Rce_insumo record(s) retrieved."); 
		
		System.out.println("Listing Destino by Criteria...");
		orm.DestinoCriteria lormDestinoCriteria = new orm.DestinoCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormDestinoCriteria.id.eq();
		lormDestinoCriteria.setMaxResults(ROW_COUNT);
		orm.Destino[] ormDestinos = lormDestinoCriteria.listDestino();
		length =ormDestinos== null ? 0 : Math.min(ormDestinos.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormDestinos[i]);
		}
		System.out.println(length + " Destino record(s) retrieved."); 
		
	}
	
	public static void main(String[] args) {
		try {
			ListSaludData listSaludData = new ListSaludData();
			try {
				listSaludData.listTestData();
				//listSaludData.listByCriteria();
			}
			finally {
				orm.SaludPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
