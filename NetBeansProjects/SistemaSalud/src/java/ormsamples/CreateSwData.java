/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class CreateSwData {
	public void createTestData() throws PersistentException {
		PersistentTransaction t = orm.SwPersistentManager.instance().getSession().beginTransaction();
		try {
			orm.Contacto lormContacto = orm.ContactoDAO.createContacto();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : ciudadu, empresau
			orm.ContactoDAO.save(lormContacto);
			orm.Ciudad lormCiudad = orm.CiudadDAO.createCiudad();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : contacto, nombre
			orm.CiudadDAO.save(lormCiudad);
			orm.Empresa lormEmpresa = orm.EmpresaDAO.createEmpresa();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : contacto, nombre
			orm.EmpresaDAO.save(lormEmpresa);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			CreateSwData createSwData = new CreateSwData();
			try {
				createSwData.createTestData();
			}
			finally {
				Utility.shutdownDerby(orm.SwPersistentManager.instance());
				orm.SwPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
