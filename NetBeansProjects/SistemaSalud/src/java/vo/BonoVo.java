/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vo;

/**
 *
 * @author Ismael
 */
public class BonoVo {
    private int id;
    private int pagoId;
    private int tipo_previsionId;
    private String bono;

    public BonoVo(int id, int pagoId, int tipo_previsionId, String bono) {
        this.id = id;
        this.pagoId = pagoId;
        this.tipo_previsionId = tipo_previsionId;
        this.bono = bono;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPagoId() {
        return pagoId;
    }

    public void setPagoId(int pagoId) {
        this.pagoId = pagoId;
    }

    public int getTipo_previsionId() {
        return tipo_previsionId;
    }

    public void setTipo_previsionId(int tipo_previsionId) {
        this.tipo_previsionId = tipo_previsionId;
    }

    public String getBono() {
        return bono;
    }

    public void setBono(String bono) {
        this.bono = bono;
    }
    
    public static BonoVo fromORM(orm.Bono bono) {
                        BonoVo bonovo = new BonoVo(bono.getId(), 1, 1, bono.getBono());
		return bonovo;
    }
}
