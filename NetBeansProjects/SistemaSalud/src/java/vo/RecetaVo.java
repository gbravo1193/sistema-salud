/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vo;

/**
 *
 * @author dci
 */
public class RecetaVo {
     private int id;
    private int pacienteid;
    private String detalle;

    
    
    public RecetaVo()
            
    {   
        this.id=0;
        this.pacienteid=0;
        this.detalle="";
    }
     
    
    public RecetaVo(int rceid, int pacienteid,String detalle)
    {
       
        this.id=rceid;
        this.pacienteid=pacienteid;
        this.detalle=detalle;
    }

  //------------------------GETID ----   
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPacienteid() {
        return pacienteid;
    }

    public void setPacienteid(int pacienteid) {
        this.pacienteid = pacienteid;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }
   
}
