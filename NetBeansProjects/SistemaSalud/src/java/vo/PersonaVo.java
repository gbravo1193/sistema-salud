/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vo;

/**
 *
 * @author Ismael
 */
import java.util.Date;

/**
 * 
 * Clase PersonaVo
 * Genera un VO de Persona con los datos de Persona, constructor con parámetros, 
 * getters y método para usar con ORM.
 *
 */
public class PersonaVo {
    private int id;
    private String rut;
    private String nombres;
    private String apellido_paterno;
    private String apellido_materno;
    private String email;
    private Date fecha_nacimiento;
    private String id_facebook;
    private int activo;

    public PersonaVo() {
    }

    public PersonaVo(int id, String rut, String nombres, String apellido_paterno,
		String apellido_materno, String email, Date fecha_nacimiento,
                String id_facebook, int activo){
            super();
            this.id = id;
            this.rut = rut;
            this.nombres = nombres;
            this.apellido_paterno = apellido_paterno;
            this.apellido_materno = apellido_materno;
            this.email = email;
            this.fecha_nacimiento = fecha_nacimiento;
            this.id_facebook = id_facebook;
            this.activo = activo;
    }

    public int getId() {
        return id;
    }

    public String getRut() {
        return rut;
    }

    public String getNombres() {
        return nombres;
    }

    public String getApellido_paterno() {
        return apellido_paterno;
    }

    public String getApellido_materno() {
        return apellido_materno;
    }

    public String getEmail() {
        return email;
    }

    public Date getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public String getId_facebook() {
        return id_facebook;
    }

    public int getActivo() {
        return activo;
    }

        

    public static PersonaVo fromORM(orm.Persona p) {
	//
	PersonaVo pe = new PersonaVo(p.getId(), p.getRut(), p.getNombres(),
			p.getApellido_paterno(), p.getApellido_materno(), p.getEmail(), 
                        p.getFecha_nacimiento(), p.getIdentificadorfacebook(),
			p.getActivo());
	return pe;
	}
}
