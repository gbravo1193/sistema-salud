/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vo;

/**
 *
 * @author Ismael
 */
public class PacienteVo {
    private int id;
    private PersonaVo personavo;
    private String desc;
    private TipoPrevisionVo tipo_prevision_vo;
    private int activo;
    private String rut;
    private String nombre;
    private String fNacimiento;
    private int reservas;

    public PacienteVo(int id, PersonaVo personavo, String desc, TipoPrevisionVo tipo_prevision_vo, int activo) {
        this.id = id;
        this.personavo = personavo;
        this.desc = desc;
        this.tipo_prevision_vo = tipo_prevision_vo;
        this.activo = activo;
    }
    public PacienteVo(String rut, String nombre, int reservas) {
        this.rut = rut;
        this.nombre = nombre;
        this.reservas = reservas;
    }

    public PacienteVo(String rut, String nombre, String fNacimiento) {
        this.rut = rut;
        this.nombre = nombre;
        this.fNacimiento = fNacimiento;
    }
    
    public int getId() {
        return id;
    }

    public PersonaVo getPersonavo() {
        return personavo;
    }

    public String getDesc() {
        return desc;
    }

    public TipoPrevisionVo getTipo_prevision_vo() {
        return tipo_prevision_vo;
    }

    public int getActivo() {
        return activo;
    }

    public String getRut() {
        return rut;
    }

    public String getNombre() {
        return nombre;
    }

    public String getfNacimiento() {
        return fNacimiento;
    }

    public int getReservas() {
        return reservas;
    }
    
    public static PacienteVo fromORM(orm.Paciente paciente) {
        PersonaVo pe = PersonaVo.fromORM(paciente.getPersona());
        TipoPrevisionVo prev = TipoPrevisionVo.fromORM(paciente.getTipo_prevision());
        PacienteVo pa = new PacienteVo(paciente.getId(), pe,
        paciente.getDescripcion(), prev, paciente.getActivo());
        return pa;
    }
}
