/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vo;

import java.util.Date;

/**
 *
 * @author Ismael
 */
public class RceVo {
    private int id;
    private int procedencia_id;
    private int reserva_id;
    private int persona_id_realiza_examen;
    private int box_id;
    private int destino_id;
    private int cerrado;
    private String indicaciones;
    private String observaciones;
    private String anamnesis;
    private Date fecha_cierre;
    private int tipo_cierre;

    public RceVo(int id, int procedencia_id, int reserva_id, int persona_id_realiza_examen, 
            int box_id, int destino_id, int cerrado, String indicaciones, 
            String observaciones, String anamnesis, Date fecha_cierre, int tipo_cierre) {
        this.id = id;
        this.procedencia_id = procedencia_id;
        this.reserva_id = reserva_id;
        this.persona_id_realiza_examen = persona_id_realiza_examen;
        this.box_id = box_id;
        this.destino_id = destino_id;
        this.cerrado = cerrado;
        this.indicaciones = indicaciones;
        this.observaciones = observaciones;
        this.anamnesis = anamnesis;
        this.fecha_cierre = fecha_cierre;
        this.tipo_cierre = tipo_cierre;
    }

    public int getId() {
        return id;
    }

    public int getProcedencia_id() {
        return procedencia_id;
    }

    public int getReserva_id() {
        return reserva_id;
    }

    public int getPersona_id_realiza_examen() {
        return persona_id_realiza_examen;
    }

    public int getBox_id() {
        return box_id;
    }

    public int getDestino_id() {
        return destino_id;
    }

    public int getCerrado() {
        return cerrado;
    }

    public String getIndicaciones() {
        return indicaciones;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public String getAnamnesis() {
        return anamnesis;
    }

    public Date getFecha_cierre() {
        return fecha_cierre;
    }

    public int getTipo_cierre() {
        return tipo_cierre;
    }

    public static RceVo fromORM(orm.Rce rce) {
                        RceVo rcevo = new RceVo(rce.getId(), 1, 1, 1, 1, 1,
                        rce.getCerrado(), rce.getIndicaciones(), rce.getObservaciones(),
                        rce.getAnamnesis(), rce.getFechacierre(), rce.getTipocierre());
		return rcevo;
    }
}