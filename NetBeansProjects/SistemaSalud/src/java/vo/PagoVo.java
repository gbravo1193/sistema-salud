/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vo;

/**
 *
 * @author Ismael
 */
public class PagoVo {
    private int id;
    private MedioPagoVo medioPagoVo;
    private double monto;
    private double comprobante;
    private int estadoPago;
    private float subsidio;

    public PagoVo(int id, MedioPagoVo medioPagoVo, double monto, double comprobante, int estadoPago, float subsidio) {
        this.id = id;
        this.medioPagoVo = medioPagoVo;
        this.monto = monto;
        this.comprobante = comprobante;
        this.estadoPago = estadoPago;
        this.subsidio = subsidio;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public MedioPagoVo getMedioPagoVo() {
        return medioPagoVo;
    }

    public void setMedioPagoVo(MedioPagoVo medioPagoVo) {
        this.medioPagoVo = medioPagoVo;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public double getComprobante() {
        return comprobante;
    }

    public void setComprobante(double comprobante) {
        this.comprobante = comprobante;
    }

    public int getEstadoPago() {
        return estadoPago;
    }

    public void setEstadoPago(int estadoPago) {
        this.estadoPago = estadoPago;
    }

    public float getSubsidio() {
        return subsidio;
    }

    public void setSubsidio(float subsidio) {
        this.subsidio = subsidio;
    }
    
    public static PagoVo fromORM(orm.Pago pago){
        MedioPagoVo mp = MedioPagoVo.fromORM(pago.getMedio_pago());
        PagoVo pa = new PagoVo(pago.getId(), mp, pago.getMonto(), pago.getComprobante(), 
                        pago.getEstadopago(), pago.getSubsidio());
        return pa;
    }
}
