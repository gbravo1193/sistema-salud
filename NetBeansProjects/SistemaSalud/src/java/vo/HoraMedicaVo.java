/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vo;

import java.io.Serializable;
import vo.TipoHoraMedicaVo;

/**
 *
 * @author Luis
 */
public class HoraMedicaVo implements Serializable {

    private String fecha;
    private TipoHoraMedicaVo tipoHoraMedica;
    

    public HoraMedicaVo(String fecha) {
        this.fecha = fecha;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
    public TipoHoraMedicaVo getTipoHoraMedica() {
        return tipoHoraMedica;
    }

    public void setHoraMedica(TipoHoraMedicaVo TipoHoraMedica) {
        this.tipoHoraMedica = TipoHoraMedica;
    }

}
