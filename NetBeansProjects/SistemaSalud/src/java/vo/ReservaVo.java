package vo;

public class ReservaVo {
    private String paciente;
    private String  persona;
    private int fecha;

    public ReservaVo(String paciente, String persona, int fecha) {
        this.paciente = paciente;
        this.persona = persona;
        this.fecha = fecha;
    }

    public String getPaciente() {
        return paciente;
    }

    public String getPersona() {
        return persona;
    }

    public int getFecha() {
        return fecha;
    }    
}
