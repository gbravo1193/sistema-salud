/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vo;

/**
 *
 * @author Luis
 */
public class EspecialidadVo {
    private String nombreEspecialidad;
    private int profesionales;

    public EspecialidadVo(String nombreEspecialidad, int profesionales) {
        this.nombreEspecialidad = nombreEspecialidad;
        this.profesionales = profesionales;
    }

    public String getNombreEspecialidad() {
        return nombreEspecialidad;
    }

    public void setNombreEspecialidad(String nombreEspecialidad) {
        this.nombreEspecialidad = nombreEspecialidad;
    }

    public int getProfesionales() {
        return profesionales;
    }

    public void setProfesionales(int profesionales) {
        this.profesionales = profesionales;
    }

}
