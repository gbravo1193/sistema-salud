package vo;

public class AlergiaVo {
    private int id;
    private int rceid;
    private String detalle;    
        
    public AlergiaVo()     
    {
        this.id=0;
        this.rceid=0;
        this.detalle="";
    }
    
    
    public  AlergiaVo(int id , int rceid, String detalle){       
        this.id=id;
        this.rceid=rceid;
        this.detalle=detalle;
    }
    
    public int getId() {
        return id;
    }

    public int getRceid() {
        return rceid;
    }

    public String getDetalle() {
        return detalle;
    }
}
