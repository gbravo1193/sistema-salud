/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vo;

/**
 *
 * @author Ismael
 */
public class TipoPrevisionVo {
    private int id;
    private String desc;

    public TipoPrevisionVo(int id, String desc) {
        this.id = id;
        this.desc = desc;
    }

    public int getId() {
        return id;
    }

    public String getDesc() {
        return desc;
    }
    
    public static TipoPrevisionVo fromORM(orm.Tipo_prevision tp){
        TipoPrevisionVo prev = new TipoPrevisionVo(tp.getId(), tp.getDescripcion());
        return prev;
    }
    
}
