/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vo;

/**
 *
 * @author Ismael
 */
public class MedioPagoVo {
    private int id;
    private String desc;

    public MedioPagoVo(int id, String desc) {
        this.id = id;
        this.desc = desc;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
    
    public static MedioPagoVo fromORM(orm.Medio_pago mpago){
        MedioPagoVo mp = new MedioPagoVo(mpago.getId(), mpago.getDescripcion());
        return mp;
    }
}
