/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vo;

import java.io.Serializable;

/**
 *
 * @author Luis
 */
public class ProfesionalVo implements Serializable{

    private String nombre;
    private String apellido;
    private int horaMedica;

    public ProfesionalVo(String nombre, String apellido, int horaMedica) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.horaMedica = horaMedica;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int  getHoraMedica() {
        return horaMedica;
    }

    public void setHoraMedica(int horaMedica) {
        this.horaMedica = horaMedica;
    }

    
}
