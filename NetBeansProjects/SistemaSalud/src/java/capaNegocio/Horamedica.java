package capaNegocio;

import java.io.Serializable;
import java.sql.Timestamp;
import org.orm.PersistentException;
import org.orm.PersistentTransaction;

public class Horamedica implements Serializable {

    private int id;
    private Profesional profesional;
    private Administrador administrador;
    private java.sql.Timestamp hora_inicio;
    private Integer tiempo_periodo = 15;
    private Reserva reserva;
    
    public Horamedica() {
    
    }
    
    public Horamedica(Timestamp hora_inicio) {
        this.hora_inicio = hora_inicio;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public capaNegocio.Profesional getProfesional() {
        return profesional;
    }

    public void setProfesional(capaNegocio.Profesional profesional) {
        this.profesional = profesional;
    }

    public capaNegocio.Administrador getAdministrador() {
        return administrador;
    }

    public void setAdministrador(capaNegocio.Administrador administrador) {
        this.administrador = administrador;
    }

    public Timestamp getHora_inicio() {
        return hora_inicio;
    }

    public void setHora_inicio(Timestamp hora_inicio) {
        this.hora_inicio = hora_inicio;
    }

    public Integer getTiempo_periodo() {
        return tiempo_periodo;
    }

    public void setTiempo_periodo(Integer tiempo_periodo) {
        this.tiempo_periodo = tiempo_periodo;
    }

    public capaNegocio.Reserva getReserva() {
        return reserva;
    }

    public void setReserva(capaNegocio.Reserva reserva) {
        this.reserva = reserva;
    }

    @Override
    public String toString() {
        return String.valueOf(getId());
    }
    
    /**
     * Transforma el tipo orm.Horamedica a el tipo capaNegocio.Horamedica
     * Esto es necesario para el manejo y envio de datos sin utilizar la capa de persistencia
     * para las transacciones.
     * @param hora_medica - orm.Horamedica- Horamedica a transformar
     * @return capaNegocio.Horamedica - Salida de la transformacion
     */
    public Horamedica toCNHoramedica(orm.Horamedica hora_medica) {
        Horamedica salida= new Horamedica();
        salida.setId(hora_medica.getId());
        Profesional p = new Profesional();
        salida.setProfesional(p.toCNProfesional(hora_medica.getProfesional()));
        Administrador a= new Administrador();
        salida.setAdministrador(a.toCNAdministrador(hora_medica.getAdministrador()));
        salida.setHora_inicio(hora_medica.getHora_inicio());
        salida.setTiempo_periodo(hora_medica.getTiempo_periodo());
        return salida;
    }
    
    /**
     * transforma el tipo capaNegocio.Horamedica a el tipo orm.Horamedica
     * Esto es necesario para el manejo y envio de datos sin utilizar la capa de persistencia
     * para las transacciones.
     * 
     * @param hora_medica  - capaNegocio.Horamedica - Horamedica a transformar
     * @return orm.Horamedica - Salida de la transformacion
     * 
     */
    public orm.Horamedica toORMHoramedica(Horamedica hora_medica){
        orm.Horamedica salida = orm.HoramedicaDAO.createHoramedica();        
        salida.setId(hora_medica.getId());
        Profesional p = new Profesional();
        salida.setProfesional(p.toORMProfesional(hora_medica.getProfesional()));
        Administrador a= new Administrador();
        salida.setAdministrador(a.toORMAdministrador(hora_medica.getAdministrador()));
        salida.setHora_inicio(hora_medica.getHora_inicio());
        salida.setTiempo_periodo(hora_medica.getTiempo_periodo());
        return salida;
    }
    
    
    /**
     * Crea una Horamedica y la almacena en la BD
     * 
     * @param rutProfesional
     * @param rutAdmin
     * @param tsHoraInicio
     * @return booelan True si hora es creada
     * @throws org.orm.PersistentException
     */
    public Horamedica crearHoraMedica(String rutProfesional, String rutAdmin, String tsHoraInicio ) 
            throws PersistentException{
        
        PersistentTransaction t= orm.SaludPersistentManager.instance().getSession().beginTransaction();
        Horamedica hm = new Horamedica();
        
        
            orm.Persona lormPersonaProfesional = orm.PersonaDAO.loadPersonaByQuery("Persona.rut='"+rutProfesional+"'", null);
            orm.Persona lormPersonaAdministrador = orm.PersonaDAO.loadPersonaByQuery("Persona.rut='"+rutAdmin+"'", null);
            System.out.println("KIKI"+lormPersonaAdministrador.getId());
            orm.Horamedica lormHoramedica = orm.HoramedicaDAO.createHoramedica();
            
            if(lormPersonaProfesional != null){
                lormHoramedica.setProfesional(lormPersonaProfesional.getProfesional());
            }
                        
            if(lormPersonaAdministrador != null){
                lormHoramedica.setAdministrador(lormPersonaAdministrador.getAdministrador());
            }
            Timestamp ts = Timestamp.valueOf(tsHoraInicio);
            lormHoramedica.setHora_inicio(ts);
            lormHoramedica.setTiempo_periodo(15);
            orm.HoramedicaDAO.save(lormHoramedica);
            t.commit();
            if(t.wasCommitted()){
                return hm.toCNHoramedica(lormHoramedica);
            }
        return hm.toCNHoramedica(lormHoramedica);
    }

}
