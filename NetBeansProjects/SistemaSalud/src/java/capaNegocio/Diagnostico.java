package capaNegocio;

import java.io.Serializable;

public class Diagnostico implements Serializable {

    private int id;
    private String codigo;
    
    public Diagnostico() {
    }

    public Diagnostico(String codigo) {
        this.codigo = codigo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Override
    public String toString() {
        return String.valueOf(getId());
    }
    
    /**
     * Transforma el tipo capaNegocio.Diagnostico a el tipo orm.Diagnostico
     * Esto es necesario para el manejo y envio de datos sin utilizar la capa de persistencia
     * para las transacciones.
     * @param diagnostico - capaNegocio.Diagnostico - Diagnostico a transformar
     * @return orm.Diagnostico - Salida de la transformacion
     */
    public orm.Diagnostico toORMDiagnostico(Diagnostico diagnostico) {
        orm.Diagnostico salida = orm.DiagnosticoDAO.createDiagnostico();
        salida.setId(diagnostico.getId());
        salida.setCodigo(diagnostico.getCodigo());
        return salida;
    }
}
