package capaNegocio;

public class Procedencia {

    private int id;
    private String descripcion;

    public Procedencia() {
    }
    
    public Procedencia(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String toString() {
        return String.valueOf(getId());
    }
    
    /**
     * Transforma el tipo orm.Procedencia a el tipo capaNegocio.Procedencia
     * Esto es necesario para el manejo y envio de datos sin utilizar la capa de persistencia
     * para las transacciones.
     * @param procedencia  - orm.Procedencia - Procedencia a transformar
     * @return capaNegocio.Procedencia - Salida de la transformacion
     */
    public Procedencia toCNProcedencia(orm.Procedencia procedencia) {
        Procedencia salida= new Procedencia();
        salida.setDescripcion(procedencia.getDescripcion());
        salida.setId(procedencia.getId());
        return salida;
    }
}
