package capaNegocio;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.orm.PersistentException;
import org.orm.PersistentTransaction;
import orm.*;

public class Rce implements Serializable {

    public Rce() {
    }

    private int id;
    private Procedencia procedencia;
    private Reserva reserva;
    private Persona persona_id_realiza_examen;
    private Box box;
    private Destino destino;
    private int cerrado = 0;
    private String indicaciones;
    private String observaciones;
    private String anamnesis;
    private java.util.Date fechacierre;
    private Integer tipocierre;

    public Rce(Procedencia procedencia, 
            Reserva reserva, 
            Persona persona_id_realiza_examen, 
            Box box, 
            Destino destino, 
            String indicaciones, 
            String observaciones, 
            String anamnesis, 
            Date fechacierre, 
            Integer tipocierre) {
        
        this.procedencia = procedencia;
        this.reserva = reserva;
        this.persona_id_realiza_examen = persona_id_realiza_examen;
        this.box = box;
        this.destino = destino;
        this.indicaciones = indicaciones;
        this.observaciones = observaciones;
        this.anamnesis = anamnesis;
        this.fechacierre = fechacierre;
        this.tipocierre = tipocierre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public capaNegocio.Procedencia getProcedencia() {
        return procedencia;
    }

    public void setProcedencia(capaNegocio.Procedencia procedencia) {
        this.procedencia = procedencia;
    }

    public Reserva getReserva() {
        return reserva;
    }

    public void setReserva(Reserva reserva) {
        this.reserva = reserva;
    }

    public capaNegocio.Persona getPersona_id_realiza_examen() {
        return persona_id_realiza_examen;
    }

    public void setPersona_id_realiza_examen(capaNegocio.Persona persona_id_realiza_examen) {
        this.persona_id_realiza_examen = persona_id_realiza_examen;
    }

    public Box getBox() {
        return box;
    }

    public void setBox(Box box) {
        this.box = box;
    }

    public capaNegocio.Destino getDestino() {
        return destino;
    }

    public void setDestino(capaNegocio.Destino destino) {
        this.destino = destino;
    }

    public int getCerrado() {
        return cerrado;
    }

    public void setCerrado(int cerrado) {
        this.cerrado = cerrado;
    }

    public String getIndicaciones() {
        return indicaciones;
    }

    public void setIndicaciones(String indicaciones) {
        this.indicaciones = indicaciones;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getAnamnesis() {
        return anamnesis;
    }

    public void setAnamnesis(String anamnesis) {
        this.anamnesis = anamnesis;
    }

    public Date getFechacierre() {
        return fechacierre;
    }

    public void setFechacierre(Date fechacierre) {
        this.fechacierre = fechacierre;
    }

    public Integer getTipocierre() {
        return tipocierre;
    }

    public void setTipocierre(Integer tipocierre) {
        this.tipocierre = tipocierre;
    }

    public String toString() {
        return String.valueOf(getId());
    }

    /**
     * Crear un Registro en la base de datos
     *
     * @param rce - Rce - Objeto Rce con los parametros para crear el registro
     * @return String - Salida del ingreso
     */
    public String crearRCE(Rce rce) {
        String salida = "";
        PersistentTransaction t = null;
        try {
            t = SaludPersistentManager.instance().getSession().beginTransaction();
            orm.Rce reg = orm.RceDAO.createRce();
            reg = rce.toORMRce(rce);
            orm.RceDAO.save(reg);
            t.commit();
            salida = "Ingreso Exitoso!";
        } catch (Exception e) {
            try {
                t.rollback();
            } catch (PersistentException e1) {
                salida = "ERROR Volviendo todo atras.";
            }
            salida = "ERROR Creando contacto";
            e.printStackTrace();
        }
        return salida;
    }

    /**
     * Lista los registro existentes en la base de datos
     *
     * @return Rce[] - Arreglo de registros
     */
    public Rce[] listarRce() {
        Rce[] salida = null;
        orm.Rce[] ormRce = null;
        try {
            Rce rce = new Rce();
            ormRce = orm.RceDAO.listRceByQuery(null, null);
            salida = rce.toCNRceArray(ormRce);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return salida;
    }

    /**
     * Transforma un arreglo del tipo orm.Rce a un arreglo del tipo
     * capaNegocio.Rce Esto es necesario para el manejo y envio de datos sin
     * utilizar la capa de persistencia para las transacciones.
     *
     * @param rce - orm.Rce[]- Arreglo omr.Rce a transformar
     * @return capaNegocio.Rce[]- Salida de la transformacion
     */
    public Rce[] toCNRceArray(orm.Rce[] rce) {
        Rce[] salida = new Rce[rce.length];
        for (int i = 0; i < rce.length; i++) {
            Rce r = new Rce();
            salida[i] = r.toCNRce(rce[i]);
        }
        return salida;
    }

    /**
     * Transforma el tipo orm.Rce a el tipo capaNegocio.Rce Esto es necesario
     * para el manejo y envio de datos sin utilizar la capa de persistencia para
     * las transacciones.
     *
     * @param rce - orm.Rce- Rce a transformar
     * @return capaNegocio.Rce- Salida de la transformacion
     */
    public Rce toCNRce(orm.Rce rce) {
        Rce salida = new Rce();
        salida.setAnamnesis(rce.getAnamnesis());
        salida.setId(rce.getId());
        Persona persona = new Persona();
        salida.setPersona_id_realiza_examen(persona.toCNPersona(rce.getPersona_id_realiza_examen()));
        Procedencia procedencia = new Procedencia();
        salida.setProcedencia(procedencia.toCNProcedencia(rce.getProcedencia()));
        Reserva reserva = new Reserva();
        salida.setReserva(reserva.toCNReserva(rce.getReserva()));
        Box box = new Box();
        salida.setBox(box.toCNBox(rce.getBox()));
        salida.setCerrado(rce.getCerrado());
        Destino destino = new Destino();
        salida.setDestino(destino.toCNDestino(rce.getDestino()));
        salida.setFechacierre(rce.getFechacierre());
        salida.setIndicaciones(salida.getIndicaciones());
        salida.setObservaciones(rce.getObservaciones());
        salida.setTipocierre(rce.getTipocierre());
        return salida;
    }

    /**
     * Transforma el tipo capaNegocio.Rce a el tipo orm.Rce Esto es necesario
     * para el manejo y envio de datos sin utilizar la capa de persistencia para
     * las transacciones.
     *
     * @param rce - capaNegocio.Rce- Rce a transformar
     * @return orm.Rce - Salida de la transformacion
     */
    public orm.Rce toORMRce(Rce rce) {
        orm.Rce salida = orm.RceDAO.createRce();
        salida.setId(rce.getId());
        salida.setAnamnesis(rce.getAnamnesis());
        salida.setBox(rce.getBox().toORMBox(rce.getBox()));
        salida.setCerrado(rce.getCerrado());
        salida.setDestino(rce.getDestino().toORMDestino(rce.getDestino()));
        salida.setFechacierre(rce.getFechacierre());
        salida.setIndicaciones(salida.getIndicaciones());
        salida.setObservaciones(rce.getObservaciones());
        salida.setTipocierre(rce.getTipocierre());
        return salida;
    }

    public List<Rce> ObtenerHceCapaNegocio(String busqueda) throws PersistentException {

        List<Rce> listaRce = new ArrayList<Rce>();

        //Busca a la persona
        orm.Persona personaORM = orm.PersonaDAO.loadPersonaByQuery("rut = '" + busqueda + "'", null);
        if (personaORM != null) {
            Persona persona = new Persona();
            persona.setNombres(personaORM.getNombres());
            persona.setApellido_materno(personaORM.getApellido_materno());
            persona.setApellido_paterno(personaORM.getApellido_paterno());
            persona.setFecha_nacimiento(personaORM.getFecha_nacimiento());
            persona.setEmail(personaORM.getEmail());
            persona.setRut(personaORM.getRut());
            persona.setId(personaORM.getId());

            //Busca datos del paciente
            orm.Paciente pacienteOrm = orm.PacienteDAO.loadPacienteByQuery("persona_id = " + persona.getId(), null);
            Paciente paciente = new Paciente();
            paciente.setId(pacienteOrm.getId());
            paciente.setPersona(persona);

            //Busca Reservas asociadas al paciente
            List<orm.Reserva> listaReservaORM = new ArrayList<orm.Reserva>();
            List<Reserva> listaReserva = new ArrayList<Reserva>();
            listaReservaORM = orm.ReservaDAO.queryReserva("paciente_id = " + paciente.getId(), null);
            for (int i = 0; i < listaReservaORM.size(); i++) {
                Reserva reserva = new Reserva();
                reserva.setId(listaReservaORM.get(i).getId());
                reserva.setMotivoconsulta(listaReservaORM.get(i).getMotivoconsulta());
                reserva.setFecha(listaReservaORM.get(i).getFecha());
                Horamedica h= new Horamedica();
                reserva.setHora_medica(h.toCNHoramedica(listaReservaORM.get(i).getHora_medica()));
                reserva.setPaciente(paciente);

                //Busca Hora medica asociada
                orm.Horamedica horaMedicaOrm = orm.HoramedicaDAO.loadHoramedicaByQuery("id = " + reserva.getHora_medica().getId(), null);
                Horamedica horaMedica = new Horamedica();
                horaMedica.setId(horaMedicaOrm.getId());
                Profesional p = new Profesional();
                horaMedica.setProfesional(p.toCNProfesional(horaMedicaOrm.getProfesional()));

                //Busca profesional
                orm.Profesional profesionalOrm = orm.ProfesionalDAO.loadProfesionalByQuery("Profesional.id = " + horaMedica.getProfesional().getId(), null);
                Profesional profesional = new Profesional();
                profesional.setId(profesionalOrm.getId());
                profesional.setPersona_id(profesionalOrm.getPersona().getId());

                //Busca persona
                orm.Persona personaProfesionalOrm = orm.PersonaDAO.loadPersonaByQuery("Persona.id = " + profesional.getPersona_id(), null);
                Persona personaProfesional = new Persona();
                personaProfesional.setApellido_paterno(personaProfesionalOrm.getApellido_paterno());
                personaProfesional.setApellido_materno(personaProfesionalOrm.getApellido_materno());
                personaProfesional.setNombres(personaProfesionalOrm.getNombres());
                personaProfesional.setRut(personaProfesionalOrm.getRut());

                //setea el resto de los datos
                //profesional.setPersona(personaProfesional);
                horaMedica.setProfesional(profesional);
                reserva.setHora_medica(horaMedica);

                listaReserva.add(reserva);
            }

            //Busca todos los rce asociadas al paciente
            List<orm.Rce> listaRceORM = new ArrayList<orm.Rce>();

            for (int i = 0; i < listaReserva.size(); i++) {
                listaRceORM = orm.RceDAO.queryRce("reserva_id = " + listaReserva.get(i).getId(), null);

                for (int j = 0; j < listaRceORM.size(); j++) {
                    Persona p = new Persona();
                    Rce rce = new Rce();
                    rce.setId(listaRceORM.get(j).getId());
                    rce.setPersona_id_realiza_examen(p.toCNPersona(listaRceORM.get(j).getPersona_id_realiza_examen()));

                    rce.setCerrado(listaRceORM.get(j).getCerrado());
                    rce.setIndicaciones(listaRceORM.get(j).getIndicaciones());
                    rce.setObservaciones(listaRceORM.get(j).getObservaciones());
                    rce.setAnamnesis(listaRceORM.get(j).getAnamnesis());
                    rce.setFechacierre(listaRceORM.get(j).getFechacierre());
                    Reserva r= new Reserva();
                    rce.setReserva(r.toCNReserva(listaRceORM.get(j).getReserva()));
                    Box b= new Box();
                    rce.setBox(b.toCNBox(listaRceORM.get(j).getBox()));
                    Destino d= new Destino();
                    rce.setDestino(d.toCNDestino(listaRceORM.get(j).getDestino()));
                    Procedencia p1= new Procedencia();
                    rce.setProcedencia(p1.toCNProcedencia(listaRceORM.get(j).getProcedencia()));
                    rce.setReserva(listaReserva.get(j));

                    //Busca y Agrega box a rce
                    orm.Box boxOrm = orm.BoxDAO.loadBoxByORMID(rce.getBox().getId());
                    Box box = new Box();
                    box.setId(boxOrm.getId());
                    box.setCodigobox(boxOrm.getCodigobox());
                    rce.setBox(box);

                    //Busca y agrega destino a rce
                    orm.Destino destinoOrm = orm.DestinoDAO.loadDestinoByORMID(rce.getDestino().getId());
                    Destino destino = new Destino();
                    destino.setId(destinoOrm.getId());
                    destino.setDetalle(destinoOrm.getDetalle());
                    rce.setDestino(destino);

                    //Busca y agrega procedencia a rce
                    orm.Procedencia procedenciaOrm = orm.ProcedenciaDAO.loadProcedenciaByORMID(rce.getProcedencia().getId());
                    Procedencia procedencia = new Procedencia();
                    procedencia.setId(procedenciaOrm.getId());
                    procedencia.setDescripcion(procedenciaOrm.getDescripcion());
                    rce.setProcedencia(procedencia);

                    //Agrega a la lista de rce
                    listaRce.add(rce);
                }
            }
        }
        return listaRce;
    }
    
    
    public Date convertStingToDate(String s) {
        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy");
        Date fecha = null;
        try {
            fecha = formatoDelTexto.parse(s);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return fecha;
    }
}
