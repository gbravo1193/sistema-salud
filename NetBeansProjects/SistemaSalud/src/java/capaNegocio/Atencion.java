package capaNegocio;

import vo.HoraMedicaVo;
import vo.ProfesionalVo;
import org.orm.PersistentException;

public class Atencion {

    private static final int ROW_COUNT = 100;

    public ProfesionalVo[] atencionesPorProfesional() {
        int lengthProfesionales;
        orm.Profesional[] ormProfesionals = null;
        ProfesionalVo[] profesionalesVO = null;
        System.out.println("Listing Profesional...");
        
        try {
            ormProfesionals = orm.ProfesionalDAO.listProfesionalByQuery(null, null);
            lengthProfesionales = Math.min(ormProfesionals.length, ROW_COUNT);
            profesionalesVO = new ProfesionalVo[lengthProfesionales];
            for (int i = 0; i < lengthProfesionales; i++) {
                System.out.println("Listing Horamedica...");
                profesionalesVO[i] = new ProfesionalVo(ormProfesionals[i].getPersona().getNombres(), ormProfesionals[i].getPersona().getApellido_paterno(), ormProfesionals[i].horamedica.size());
            }
        } catch (PersistentException ex) {
            System.out.println("Error al listar profesionales");
        }
        return profesionalesVO;
    }

    public ProfesionalVo[] atencionesPorProfesionalConFecha(String fechaInicio, String fechaTermino) {
        int lengthProfesionales;
        orm.Profesional[] ormProfesionals;
        ProfesionalVo[] profesionales = null;
        System.out.println("Listing Profesional...");
        try {
            ormProfesionals = orm.ProfesionalDAO.listProfesionalByQuery(null, null);
            lengthProfesionales = Math.min(ormProfesionals.length, ROW_COUNT);
            for (int i = 0; i < lengthProfesionales; i++) {
                profesionales = new ProfesionalVo[lengthProfesionales];
                String consulta = "profesional_id = " + ormProfesionals[i].getId() + " and hora_inicio between '" + fechaInicio + " 00:00:00.0' and '" + fechaTermino + " 23:59:59.0'";
                orm.Horamedica[] ormHoramedicas = orm.HoramedicaDAO.listHoramedicaByQuery(consulta, null);
                int lengthHoraMedica = Math.min(ormHoramedicas.length, ROW_COUNT);
                HoraMedicaVo[] horasMedicas = new HoraMedicaVo[lengthHoraMedica];
                //System.out.println(consulta);
                //System.out.println(ormProfesionals[i].getPersona().getNombres());
                for (int j = 0; j < lengthHoraMedica; j++) {
                    horasMedicas[j] = new HoraMedicaVo(String.valueOf(ormHoramedicas[j].getHora_inicio()));
                    //System.out.println(horasMedicas[j].getFecha());
                }
                //System.out.println(lengthHoraMedica + " horas medicas encontradas.\n\n");
            }
        } catch (PersistentException ex) {
            System.out.println("Error al listar profesionales");
        }
        return profesionales;
    }
}
