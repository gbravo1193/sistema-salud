package capaNegocio;

import orm.Insumo;
import java.io.Serializable;
import org.orm.PersistentException;
import org.orm.PersistentTransaction;
import orm.SaludPersistentManager;

public class Rce_insumo implements Serializable {

    private int id;
    private Rce rce;
    private orm.Insumo insumo;
    private float cantidad;

    public Rce_insumo() {
    }
    
    public Rce_insumo(Insumo insumo, float cantidad) {
        this.insumo = insumo;
        this.cantidad = cantidad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Rce getRce() {
        return rce;
    }

    public void setRce(Rce rce) {
        this.rce = rce;
    }

    public Insumo getInsumo() {
        return insumo;
    }

    public void setInsumo(Insumo insumo) {
        this.insumo = insumo;
    }

    public float getCantidad() {
        return cantidad;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }

    public String toString() {
        return String.valueOf(getId());
    }
    /**
     * Transforma el tipo capaNegocio.Rce_insumo  a el tipo orm.Rce_insumo 
     * Esto es necesario para el manejo y envio de datos sin utilizar la capa de persistencia
     * para las transacciones.
     * @param rce_insumo  - capaNegocio.Rce_insumo - Rce_insumo  a transformar
     * @return orm.Rce_insumo  - Salida de la transformacion
     */
    public orm.Rce_insumo toORMRce_insumo(Rce_insumo rce_insumo) {
        orm.Rce_insumo salida = orm.Rce_insumoDAO.createRce_insumo();
        salida.setId(rce_insumo.getId());
        salida.setCantidad(rce_insumo.getCantidad());
        salida.setInsumo(rce_insumo.getInsumo());
        salida.setRce(rce_insumo.getRce().toORMRce(rce_insumo.getRce()));
        return salida;
    }
    /**
     * Crear un Insumo en la base de datos
     * @param rceInsumo - Rce_insumo - Objeto Rce_insumo a ingresar en la base de datos
     * @return String - Salida del ingreso
     */
    public String CrearRce_insumo(Rce_insumo rceInsumo) {
        String salida = "";
        PersistentTransaction t = null;
        try {
            t = SaludPersistentManager.instance().getSession().beginTransaction();
            orm.Rce_insumo reci = orm.Rce_insumoDAO.createRce_insumo();
            reci = rceInsumo.toORMRce_insumo(rceInsumo);
            orm.Rce_insumoDAO.save(reci);
            t.commit();
            salida = "Ingreso Exitoso!";
        } catch (Exception e) {
            try {
                t.rollback();
            } catch (PersistentException e1) {
                salida = "ERROR Volviendo todo atras.";
            }
            salida = "ERROR Creando contacto";
            e.printStackTrace();
        }
        return salida;
    }
}
