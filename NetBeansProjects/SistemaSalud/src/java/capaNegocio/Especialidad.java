package capaNegocio;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.orm.PersistentException;
import org.orm.PersistentTransaction;
import vo.EspecialidadVo;

public class Especialidad {

    Collection<Profesional> profesional;
    private int id;
    private String especialidad;
    
    private static final int ROW_COUNT = 100;

    public int getId() {
            return this.id;
    }

    /**
     * 
     * @param id
     */
    public void setId(int id) {
            this.id = id;
    }

    public String getEspecialidad() {
            return this.especialidad;
    }

    /**
     * 
     * @param especialidad
     */
    public void setEspecialidad(String especialidad) {
            this.especialidad = especialidad;
    }
    
    /**
     * Agrega la especialidad recibida a la BD
     * 
     * @param nombreEspecialidad
     * @return boolean
     * @throws PersistentException 
     */
    public boolean agregarEspecialidad(String nombreEspecialidad) 
            throws PersistentException{
        PersistentTransaction t = orm.SaludPersistentManager.instance().getSession().beginTransaction();
        
        orm.Especialidad specialidad = orm.EspecialidadDAO.createEspecialidad();
        specialidad.setEspecialidad(nombreEspecialidad);
        if(orm.EspecialidadDAO.save(specialidad)){
            t.commit();
            return true;
        }else{
            return false;
        }        
    }
    /**
     * Lista las especialidades Ingresadas
     * 
     * @return List
     * @throws PersistentException 
     */
    public List<Especialidad> ListarEspecilidades() throws PersistentException {
        List<Especialidad> lista = new ArrayList<Especialidad>();
        orm.Especialidad[] listaOrm = orm.EspecialidadDAO.listEspecialidadByQuery(null, null);

        for (int x = 0; x < listaOrm.length; x++) {
            Especialidad especialidad = new Especialidad();
            especialidad.setId(listaOrm[x].getId());
            especialidad.setEspecialidad(listaOrm[x].getEspecialidad());
            lista.add(especialidad);
        }
        return lista;
    }

    /**
     * Devuelve la cantidad de medicos por especialidad
     * 
     * Usa->Vo
     * 
     * @return Array EspecialidadVo
     */    
    public EspecialidadVo[] cantidadMedicosPorEspecialidad() {
        int length;
        System.out.println("Listing Especialidad...");
        orm.Especialidad[] ormEspecialidads = null;
        EspecialidadVo especialidadesVO[] = null;
        try {
            ormEspecialidads = orm.EspecialidadDAO.listEspecialidadByQuery(null, null);
            length = Math.min(ormEspecialidads.length, ROW_COUNT);
            especialidadesVO = new EspecialidadVo[length];
            for (int i = 0; i < length; i++) {
                especialidadesVO[i] = new EspecialidadVo(ormEspecialidads[i].getEspecialidad(), ormEspecialidads[i].profesional.size());
            }
        } catch (PersistentException ex) {
            Logger.getLogger(Especialidad.class.getName()).log(Level.SEVERE, null, ex);
        }
        return especialidadesVO;
    }
}