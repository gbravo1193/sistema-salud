package capaNegocio;

import java.io.Serializable;

public class Insumo implements Serializable {
    
    private int id;
    private String nombre;
    private Float monto_pmp;
    private String codigo;
    
    public Insumo() {
    }

    public Insumo(String nombre, Float monto_pmp, String codigo) {
        this.nombre = nombre;
        this.monto_pmp = monto_pmp;
        this.codigo = codigo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Float getMonto_pmp() {
        return monto_pmp;
    }

    public void setMonto_pmp(Float monto_pmp) {
        this.monto_pmp = monto_pmp;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
    @Override
    public String toString() {
        return String.valueOf(getId());
    }
}
