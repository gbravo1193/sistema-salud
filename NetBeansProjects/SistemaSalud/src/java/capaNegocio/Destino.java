package capaNegocio;

import java.io.Serializable;

public class Destino implements Serializable {

    private int id;
    private String detalle;
    
    public Destino() {
    }   

    public Destino(String detalle) {
        this.detalle = detalle;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String toString() {
        return String.valueOf(getId());
    }
    /**
     * Transforma el tipo capaNegocio.Destino a el tipo orm.Destino
     * Esto es necesario para el manejo y envio de datos sin utilizar la capa de persistencia
     * para las transacciones.
     * @param destino - capaNegocio.Destino - Destino a transformar
     * @return orm.Destino - Salida de la transformacion
     */
    public orm.Destino toORMDestino(Destino destino){
        orm.Destino salida = orm.DestinoDAO.createDestino();
        salida.setId(destino.getId());
        salida.setDetalle(destino.getDetalle());
        return salida;
    }
    /**
     * Transforma el tipo orm.Destino a el tipo capaNegocio.Destino
     * Esto es necesario para el manejo y envio de datos sin utilizar la capa de persistencia
     * para las transacciones.
     * @param destino - orm.Destino - Destino a transformar
     * @return capaNegocio.Destino - Salida de la transformacion
     */
    public Destino toCNDestino(orm.Destino destino) {
        Destino salida= new Destino();
        salida.setDetalle(destino.getDetalle());
        salida.setId(destino.getId());
        return salida;
    }
}
