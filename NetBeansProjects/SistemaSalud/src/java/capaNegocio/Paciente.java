package capaNegocio;

import java.io.Serializable;
import org.hibernate.criterion.Restrictions;
import org.orm.PersistentException;
import org.orm.PersistentTransaction;
import orm.PacienteCriteria;
import orm.PersonaCriteria;

public class Paciente implements Serializable {
    
    private int id;
    private Persona persona;
    private String descripcion;
    private int persona_id;

    public int getPersona_id() {
        return persona_id;
    }

    public void setPersona_id(int persona_id) {
        this.persona_id = persona_id;
    }
    private Tipo_prevision tipo_prevision;
    private Integer activo;
    
    public Paciente() {
    }
    
    public Paciente(String descripcion, Tipo_prevision tipo_prevision, Integer activo) {
        this.descripcion = descripcion;
        this.tipo_prevision = tipo_prevision;
        this.activo = activo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Tipo_prevision getTipo_prevision() {
        return tipo_prevision;
    }

    public void setTipo_prevision(Tipo_prevision tipo_prevision) {
        this.tipo_prevision = tipo_prevision;
    }

    public Integer getActivo() {
        return activo;
    }

    public void setActivo(Integer activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return String.valueOf(getId());
    }
    
    /**
     * Transforma el tipo capaNegocio.Paciente a el tipo orm.Paciente
     * Esto es necesario para el manejo y envio de datos sin utilizar la capa de persistencia
     * para las transacciones.
     * @param paciente - capaNegocio.Paciente - Destino a transformar
     * @return orm.Paciente - Salida de la transformacion
     */
    public orm.Paciente toORMPaciente (Paciente paciente){
        orm.Paciente salida= orm.PacienteDAO.createPaciente();
        salida.setId(paciente.getId());
        salida.setActivo(paciente.getActivo());
        salida.setDescripcion(paciente.getDescripcion());
        return salida;
    }
    /**
     * Transforma el tipo orm.Paciente a el tipo capaNegocio.Paciente
     * Esto es necesario para el manejo y envio de datos sin utilizar la capa de persistencia
     * para las transacciones.
     * @param paciente- orm.Paciente - Destino a transformar
     * @return capaNegocio.Paciente - Salida de la transformacion
     */
    public Paciente toCNPaciente(orm.Paciente paciente) {
        Paciente salida = new Paciente();
        salida.setActivo(paciente.getActivo());
        salida.setDescripcion(paciente.getDescripcion());
        salida.setId(paciente.getId());
        Tipo_prevision tp= new Tipo_prevision();
        salida.setTipo_prevision(tp.toCNTipo_prevision(paciente.getTipo_prevision()));
        return salida;
    }
    
    public Paciente agregarPaciente(Paciente paciente) throws PersistentException{
        
        PersistentTransaction t = orm.SaludPersistentManager.instance().getSession().beginTransaction();
        try{
            orm.Persona lormPersona = orm.PersonaDAO.loadPersonaByORMID(paciente.getPersona_id());
            orm.Tipo_prevision lormTipoPrevision = orm.Tipo_previsionDAO.loadTipo_previsionByORMID(paciente.getTipo_prevision().getId());

            orm.Paciente lormPaciente = orm.PacienteDAO.createPaciente();
            
            lormPaciente.setPersona(lormPersona);
            lormPaciente.setDescripcion(paciente.getDescripcion());
            lormPaciente.setActivo(paciente.getActivo());
            lormPaciente.setTipo_prevision(lormTipoPrevision);
            orm.PacienteDAO.save(lormPaciente);
            t.commit();
            return paciente.toCNPaciente(lormPaciente);

        }catch(PersistentException e){
            
        }
        /*
        try{           
            
            orm.Persona lormPersona= orm.PersonaDAO.loadPersonaByORMID(profesional.getPersona().getId());
            
            orm.Profesional lormProfesional= orm.ProfesionalDAO.createProfesional();
            
            profesional.setPersona_id(lormPersona.getId());
            lormProfesional.setPersona(lormPersona);
            
            lormProfesional.setActivo(profesional.getActivo());
            
            System.out.println(profesional.getActivo());
            orm.ProfesionalDAO.save(lormProfesional);      
            t.commit();
            return "Ingresado Correctamente";
        }
        catch(Exception e){
            t.rollback();
            return "Error al ingresar: "+e.getMessage();
        }
        */
        
        return null;
    }
    
    /**
    public Persona buscarPaciente(int idPersona){
        Paciente paciente=null;
        orm.Paciente aux=null;
        PacienteCriteria pcr=null;
        try{
            pcr= new PacienteCriteria();
            persona = new Paciente();
            pcr.add(Restrictions.ilike("rut", rut.trim().toLowerCase()));
            aux=orm.PersonaDAO.loadPersonaByCriteria(pcr);
            if(aux.getId()!=0){
                persona = persona.toCNPersona(aux);
            }else{
                return null;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return persona;
    }**/
}
