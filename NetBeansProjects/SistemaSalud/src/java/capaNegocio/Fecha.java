package capaNegocio;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Clase Utilitaria para formato de Fechas
 */
public class Fecha {
    
    public Date convertStingToDate(String s) {
        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy");
        Date fecha = null;
        try {
            fecha = formatoDelTexto.parse(s);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return fecha;
    }    
}
