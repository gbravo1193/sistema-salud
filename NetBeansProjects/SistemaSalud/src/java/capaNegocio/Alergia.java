package capaNegocio;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlTransient;
import org.orm.PersistentException;
import org.orm.PersistentTransaction;
import orm.Alergias;
import vo.AlergiaVo;

public class Alergia {
    
    int id;
    String detalle;
    int rceid;

    public Alergia(){
        this.id=0;
        this.rceid=0;
        this.detalle="";    
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public int getRceid() {
        return rceid;
    }

    public void setRceid(int rceid) {
        this.rceid = rceid;
    }
    
// agregar nueva alergia al RCE
    public String agregarAlergia(int rceid, String detalle) 
            throws PersistentException{
       
        if(detalle!=null && rceid!=0){
        
            PersistentTransaction t = orm.SaludPersistentManager.instance().getSession().beginTransaction();

            try {
                
                orm.Rce rce=orm.RceDAO.getRceByORMID(rceid);
                orm.Alergias alergia=orm.AlergiasDAO.createAlergias();
                
                alergia.setDetalle(detalle);
                alergia.setORM_Rce(rce);
                alergia.setRce(rce);
                orm.AlergiasDAO.save(alergia);
                t.commit();
                orm.AlergiasDAO.refresh(alergia);
                return "OK:ALERGIA";
            } catch (PersistentException ex) {
                t.rollback();
                Logger.getLogger(Alergia.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return "ERROR:ALERGIA";
    }

    @XmlTransient
    public List<AlergiaVo> listarAlergia(int pacienteid, int rce_id) throws PersistentException{
        
        //METODO INCOMPLETO, FUNCIONAL, PERO INCOMPLETO EN USABILIDAD Y FUNCIONALIDAD!!!!
      //  PersistentTransaction t = orm.SaludPersistentManager.instance().getSession().beginTransaction();
          
  /*
        SessionFactory sessionFactory=orm.SaludPersistentManager.instance().getSession().getSessionFactory().getC
        Session s=sessionFactory.openSession();
         
                                
                   //obtengo el rce   Es necesario¿?
                   orm.Rce  rce=orm.RceDAO.getRceByORMID(rceid);
                   if (rce!=null)
                   {
                       String hql="from Alergias as a inner join a.rce where a.rce.ide=1 ";
                       Query q=s.createQuery(hql);
                       List<Object[]> lista=q.list();
                       for(Object[] l:lista)
                       {
                           System.out.println(Arrays.toString(l));
                       }
                       
                   }
                   
        else System.out.println("ERROR:Listar Alergias");*/
                
        List<AlergiaVo> alergia=new ArrayList<AlergiaVo>();
        orm.Alergias[] ormAlergias=null;
        ormAlergias=orm.AlergiasDAO.listAlergiasByQuery("rceid="+rce_id, null);
         
        if (ormAlergias!=null) {
            for(int i=0;i< ormAlergias.length;i++)
            {
                alergia.add(new AlergiaVo(ormAlergias[i].getId(),
                        ormAlergias[i].getRce().getId(),
                        ormAlergias[i].getDetalle() ) );
                
                System.out.println(":"+alergia.get(i).getDetalle());
            }
             int i=0;
              System.out.println(ormAlergias[i].getId());
                          
             
            for (Alergias alergia_lista : ormAlergias)
            {
                System.out.println(alergia_lista.getId());
                System.out.println(alergia_lista.getRce().getId());
                System.out.println(alergia_lista.getDetalle());
            }
        
           // return "OK:LISTAR-ALERGIAS";
            //return Alergias; 
         
            return alergia;
         }
         
         return null;
       
    }
    
    
    public String EliminarAlergia(int rceid, int AlergiaID) throws PersistentException{
    
        PersistentTransaction t = orm.SaludPersistentManager.instance().getSession().beginTransaction();
        
        if(rceid!=0 && AlergiaID!=0){
            try {
                //orm.Rce rce=orm.RceDAO.getRceByORMID(rceid);
                orm.Alergias a=orm.AlergiasDAO.getAlergiasByORMID(AlergiaID);
                orm.AlergiasDAO.delete(a);
                t.commit();
                orm.AlergiasDAO.refresh(a);

                return "ALERGIA CON ID:"+AlergiaID+" Eliminado...";
            } catch (PersistentException ex) {
                t.rollback();
                Logger.getLogger(Alergia.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return "ERROR:ELIMINAR-ALERGIA";
    }    
} // FIN CLASE 
    
    


