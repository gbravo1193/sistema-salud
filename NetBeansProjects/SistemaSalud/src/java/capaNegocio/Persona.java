package capaNegocio;

import java.util.Date;
import org.hibernate.criterion.Restrictions;
import org.orm.PersistentException;
import org.orm.PersistentTransaction;
import orm.PersonaCriteria;

public class Persona {

    private int id;
    private String rut;
    private String nombres;
    private String apellido_paterno;
    private String apellido_materno;
    private String email;
    private java.util.Date fecha_nacimiento;
    private String identificadorfacebook;
    private Integer activo;
    private String password;
    
    private Profesional profesional;
    private Paciente paciente;
    private Administrador administrador;
    
    /**
     *  Constructor vacio Persona
     */
    public Persona() {
    
    }
    
    /**
     * Constructor de Persona con datos base
     * 
     * @param rut
     * @param nombres
     * @param apellido_paterno
     * @param apellido_materno
     * @param email
     * @param fecha_nacimiento
     * @param identificadorfacebook
     * @param activo
     * @param password 
     */
    public Persona(String rut, String nombres, String apellido_paterno, String apellido_materno, String email, Date fecha_nacimiento, String identificadorfacebook, Integer activo, String password){
        this.rut = rut;
        this.nombres = nombres;
        this.apellido_paterno = apellido_paterno;
        this.apellido_materno = apellido_materno;
        this.email = email;
        this.fecha_nacimiento = fecha_nacimiento;
        this.identificadorfacebook = identificadorfacebook;
        this.activo = activo;
        this.password = password;
    }
    
    /**
     * Constructor de Persona tipo Adminstrador
     * 
     * @param rut
     * @param nombres
     * @param apellido_paterno
     * @param apellido_materno
     * @param email
     * @param fecha_nacimiento
     * @param identificadorfacebook
     * @param activo
     * @param password
     * @param administrador 
     */
    public Persona(String rut, String nombres, String apellido_paterno, String apellido_materno, String email, Date fecha_nacimiento, String identificadorfacebook, Integer activo, String password, Administrador administrador) {
        this.rut = rut;
        this.nombres = nombres;
        this.apellido_paterno = apellido_paterno;
        this.apellido_materno = apellido_materno;
        this.email = email;
        this.fecha_nacimiento = fecha_nacimiento;
        this.identificadorfacebook = identificadorfacebook;
        this.activo = activo;
        this.password = password;
        this.administrador = administrador;
    }
    
    /**
     * Constructor de Persona tipo Profesional
     * 
     * @param rut
     * @param nombres
     * @param apellido_paterno
     * @param apellido_materno
     * @param email
     * @param fecha_nacimiento
     * @param identificadorfacebook
     * @param activo
     * @param password
     * @param profesional 
     */
    public Persona(String rut, String nombres, String apellido_paterno, String apellido_materno, String email, Date fecha_nacimiento, String identificadorfacebook, Integer activo, String password, Profesional profesional) {
        this.rut = rut;
        this.nombres = nombres;
        this.apellido_paterno = apellido_paterno;
        this.apellido_materno = apellido_materno;
        this.email = email;
        this.fecha_nacimiento = fecha_nacimiento;
        this.identificadorfacebook = identificadorfacebook;
        this.activo = activo;
        this.password = password;
        this.profesional = profesional;
    }
    
    /**
     * Constructor de Persona tipo Paciente, sin password
     * 
     * @param rut
     * @param nombres
     * @param apellido_paterno
     * @param apellido_materno
     * @param email
     * @param fecha_nacimiento
     * @param identificadorfacebook
     * @param activo
     * @param password
     * @param paciente 
     */
    public Persona(String rut, String nombres, String apellido_paterno, String apellido_materno, String email, Date fecha_nacimiento, String identificadorfacebook, Integer activo, String password, Paciente paciente) {
        this.rut = rut;
        this.nombres = nombres;
        this.apellido_paterno = apellido_paterno;
        this.apellido_materno = apellido_materno;
        this.email = email;
        this.fecha_nacimiento = fecha_nacimiento;
        this.identificadorfacebook = identificadorfacebook;
        this.activo = activo;
        this.paciente = paciente;
        this.password = null;
    }
    
    @Override
    public String toString() {
        return String.valueOf(getId());
    }
    
    /**
     * Busca Persona por el rut de esta
     * 
     * @param rut - String - Rut para realizar la busqueda
     * @return Persona - Resultado de la busqueda
     */
    public Persona buscarPersonaPorRut(String rut){
        Persona persona=null;
        orm.Persona aux=null;
        PersonaCriteria pcr=null;
        try{
            aux=orm.PersonaDAO.createPersona();
            pcr= new PersonaCriteria();
            persona = new Persona();
            pcr.add(Restrictions.ilike("rut", rut.trim().toLowerCase()));
            aux=orm.PersonaDAO.loadPersonaByCriteria(pcr);
            if(aux.getId()!=0){
                persona = persona.toCNPersona(aux);
            }else{
                return null;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return persona;
    }
    
    /**
     * Transforma el tipo orm.Persona a el tipo capaNegocio.Persona
     *  
     * @param persona  - orm.Persona - Persona a transformar
     * @return capaNegocio.Persona - Salida de la transformacion
     */
    public Persona toCNPersona(orm.Persona persona){
        Persona salida= new Persona();
        
        salida.setActivo(persona.getActivo());
        salida.setEmail(persona.getEmail());
        salida.setApellido_materno(persona.getApellido_materno());
        salida.setApellido_paterno(persona.getApellido_paterno());
        salida.setFecha_nacimiento(persona.getFecha_nacimiento());
        salida.setId(persona.getId());
        salida.setIdentificadorfacebook(persona.getIdentificadorfacebook());
        salida.setNombres(persona.getNombres());
        salida.setRut(persona.getRut());
        salida.setPassword(persona.getPassword());
        
        if(persona.getAdministrador()!=null && persona.getAdministrador().getId()!=0){
            Administrador a= new Administrador();
            salida.setAdministrador(a.toCNAdministrador(persona.getAdministrador()));
        }else if(persona.getPaciente()!=null && persona.getPaciente().getId()!=0){
            Paciente p= new Paciente();
            salida.setPaciente(p.toCNPaciente(persona.getPaciente())); 
        }else if(persona.getProfesional()!=null && persona.getProfesional().getId()!=0){
            Profesional pro= new Profesional();
            salida.setProfesional(pro.toCNProfesional(persona.getProfesional())); 
        }
        
        return salida;
    }
    
    /**
     * Transforma el tipo orm.Persona a el tipo capaNegocio.Persona     * 
     * 
     * @param persona - capaNegocio.Persona - Persona a transformar
     * @return orm.Persona - Salida de la transformacion
     */
    public orm.Persona toORMPersona(Persona persona) {
        orm.Persona salida = orm.PersonaDAO.createPersona();
        
        salida.setId(persona.getId());
        salida.setActivo(persona.getActivo());
        salida.setEmail(persona.getEmail());
        salida.setApellido_materno(persona.getApellido_materno());
        salida.setApellido_paterno(persona.getApellido_paterno());
        salida.setFecha_nacimiento(persona.getFecha_nacimiento());
        salida.setId(persona.getId());
        salida.setIdentificadorfacebook(persona.getIdentificadorfacebook());
        salida.setNombres(persona.getNombres());
        salida.setRut(persona.getRut());
        salida.setPassword(persona.getPassword());        
        
        if(persona.getAdministrador()!=null && persona.getAdministrador().getId()!=0){
            orm.Administrador lormAdminAux = persona.getAdministrador().toORMAdministrador(persona.getAdministrador());
            salida.setAdministrador(lormAdminAux);
        }else if(persona.getPaciente()!=null && persona.getPaciente().getId()!=0){
            orm.Paciente lormPaciAux = persona.getPaciente().toORMPaciente(persona.getPaciente());
            salida.setPaciente(lormPaciAux); 
        }else if(persona.getProfesional()!=null && persona.getProfesional().getId()!=0){
            orm.Profesional lorProfAux = persona.getProfesional().toORMProfesional(persona.getProfesional());
            salida.setProfesional(lorProfAux); 
        }
        
        return salida;
    }
    
    /**
     * Agrega una persona a la BD
     * 
     * @param persona
     * @return String
     * @throws PersistentException 
     */
    public String agregarPersona(Persona persona) throws PersistentException {
        PersistentTransaction t = orm.SaludPersistentManager.instance().getSession().beginTransaction();

        try {
                orm.Persona lormPersona= orm.PersonaDAO.createPersona();
                lormPersona.setRut(persona.getRut());
                lormPersona.setNombres(persona.getNombres());
                lormPersona.setApellido_paterno(persona.getApellido_paterno());
                lormPersona.setApellido_materno(persona.getApellido_materno());
                lormPersona.setEmail(persona.getEmail());
                lormPersona.setFecha_nacimiento(persona.getFecha_nacimiento());
                lormPersona.setIdentificadorfacebook(persona.getIdentificadorfacebook());
                lormPersona.setActivo(persona.getActivo());
                lormPersona.setPassword(persona.getPassword());
                orm.PersonaDAO.save(lormPersona);

                t.commit();
                return "Ingresado";
        }
        catch (Exception e) {
                t.rollback();
                return "Error: "+e.getMessage();
        }	
    }
    
    /**
     * Edita a una Persona
     * 
     * @param id
     * @param rut
     * @param nombres
     * @param apellido_paterno
     * @param apellido_materno
     * @param email
     * @param fecha_nacimiento
     * @param identificadorfacebook
     * @param activo
     * @param password
     * @return String
     * @throws PersistentException 
     */
    public String editarPersona(int id, String rut, String nombres, String apellido_paterno,
            String apellido_materno, String email, Date fecha_nacimiento, String identificadorfacebook, int activo, String password) 
            throws PersistentException {
        
        PersistentTransaction t= orm.SaludPersistentManager.instance().getSession().beginTransaction();
            
        try{
            orm.Persona lormPersona= orm.PersonaDAO.loadPersonaByORMID(id);
            lormPersona.setRut(rut);
            lormPersona.setNombres(nombres);
            lormPersona.setApellido_paterno(apellido_paterno);
            lormPersona.setApellido_materno(apellido_materno);
            lormPersona.setEmail(email);
            lormPersona.setFecha_nacimiento(fecha_nacimiento);
            lormPersona.setIdentificadorfacebook(identificadorfacebook);
            lormPersona.setActivo(activo);
            lormPersona.setPassword(password);
            orm.PersonaDAO.save(lormPersona);

            t.commit();
            return "Datos Actualizados";
        }catch(Exception e){
            t.rollback();
            return "Error a actualizar datos";
        }
    }
    
    /**
     * Elimina a una Persona mediante el ID
     * 
     * @param id
     * @return String mensaje
     * @throws PersistentException 
     */
    public String eliminarPersona(int id) throws PersistentException { 
        
        PersistentTransaction t = orm.SaludPersistentManager.instance().getSession().beginTransaction();
        
        try {
            System.out.println("id es :"+id);
            orm.Persona contacto= new orm.Persona();
            orm.Persona lormPersona = orm.PersonaDAO.loadPersonaByORMID(id);
            orm.PersonaDAO.delete(lormPersona);
            t.commit();
            return "Registro Eliminado";
        }
        catch (Exception e) {
                t.rollback();
                return "Error";
        }
    }
    
    /**
     * Valida a una persona registrada, debolviendo todos los datos de esta
     * de encontraste registrada
     * 
     * @param rut
     * @param password
     * @return Persona
     */
    public Persona validarPersonaRegistrada(String rut, String password){
        Persona personaValidar = new Persona();        
        personaValidar = personaValidar.buscarPersonaPorRut(rut);
        
        if(personaValidar.getPassword().equals(password)){
            return personaValidar;
        }else{
            return null;
        }
    }

// Setters y Getters
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellido_paterno() {
        return apellido_paterno;
    }

    public void setApellido_paterno(String apellido_paterno) {
        this.apellido_paterno = apellido_paterno;
    }

    public String getApellido_materno() {
        return apellido_materno;
    }

    public void setApellido_materno(String apellido_materno) {
        this.apellido_materno = apellido_materno;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(Date fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public String getIdentificadorfacebook() {
        return identificadorfacebook;
    }

    public void setIdentificadorfacebook(String identificadorfacebook) {
        this.identificadorfacebook = identificadorfacebook;
    }

    public Integer getActivo() {
        return activo;
    }

    public void setActivo(Integer activo) {
        this.activo = activo;
    }
    
    public String getPassword(){
        return password;
    }
    
    public void setPassword(String password){
        this.password = password;
    }

    public Profesional getProfesional() {
        return profesional;
    }

    public void setProfesional(Profesional profesional) {
        this.profesional = profesional;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Administrador getAdministrador() {
        return administrador;
    }

    public void setAdministrador(Administrador administrador) {
        this.administrador = administrador;
    }
    
//----FIN De Setters y Getters

    
    
}
