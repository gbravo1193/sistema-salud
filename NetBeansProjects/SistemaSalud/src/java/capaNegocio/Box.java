package capaNegocio;

public class Box {

    private int id;
    private String codigobox;
    
    public Box() {
    }
    
    public Box(String codigobox) {
        this.codigobox = codigobox;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigobox() {
        return codigobox;
    }

    public void setCodigobox(String codigobox) {
        this.codigobox = codigobox;
    }

    public String toString() {
        return String.valueOf(getId());
    }
    /**
     * Transforma el tipo capaNegocio.Box a el tipo orm.Boxr
     * Esto es necesario para el manejo y envio de datos sin utilizar la capa de persistencia
     * para las transacciones.
     * @param box - capaNegocio.Box - Box a transformar
     * @return orm.Box - Salida de la transformacion
     */
    public orm.Box toORMBox(Box box){
        orm.Box salida= orm.BoxDAO.createBox();
        salida.setId(box.getId());
        salida.setCodigobox(box.getCodigobox());
        return salida;
    }
    /**
     * Transforma el tipo orm.Box a el tipo capaNegocio.Boxr
     * Esto es necesario para el manejo y envio de datos sin utilizar la capa de persistencia
     * para las transacciones.
     * @param box - orm.Box - Box a transformar
     * @return capaNegocio.Box - Salida de la transformacion
     */
    public Box toCNBox(orm.Box box) {
        Box salida= new Box();
        salida.setCodigobox(box.getCodigobox());
        salida.setId(box.getId());
        return salida;
    }
}
