package capaNegocio;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

public class Profesional {

    private int id;
    private int persona_id;
    private int activo;

    public int getPersona_id() {
        return persona_id;
    }

    public void setPersona_id(int persona_id) {
        this.persona_id = persona_id;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }
    
    /**
     * Transforma el tipo orm.Profesional a el tipo capaNegocio.Profesional
     * Esto es necesario para el manejo y envio de datos sin utilizar la capa de persistencia
     * para las transacciones.
     * @param profesional  - orm.Profesional - Profesional a transformar
     * @return capaNegocio.Profesional - Salida de la transformacion
     */
    public Profesional toCNProfesional(orm.Profesional profesional) {
        Persona persAux = new Persona();
        Profesional salida= new Profesional();
        
        salida.setId(profesional.getId());
        salida.setActivo(profesional.getActivo());
        salida.setPersona_id(profesional.getPersona().getId());
        return salida;
    }
    
    public orm.Profesional toORMProfesional(Profesional profesional){
        orm.Profesional salida = orm.ProfesionalDAO.createProfesional();
        salida.setId(profesional.getId());
        salida.setActivo(profesional.getActivo());
        //salida.setPersona(profesional.getPersona().toORMPersona(profesional.getPersona()));
        return salida;
    }
    
    /**
     * Agrega un Profesional
     * 
     * @param profesional
     * @return String
     * @throws PersistentException 
     */
    public Profesional agregarProfesional(Profesional profesional)throws PersistentException{
        PersistentTransaction t= orm.SaludPersistentManager.instance().getSession().beginTransaction();
        try{           
            
            orm.Persona lormPersona= orm.PersonaDAO.loadPersonaByORMID(profesional.getPersona_id());
            
            orm.Profesional lormProfesional= orm.ProfesionalDAO.createProfesional();
            lormProfesional.setActivo(lormPersona.getActivo());
            lormProfesional.setPersona(lormPersona);
            orm.ProfesionalDAO.save(lormProfesional);
            
            t.commit();
            return profesional.toCNProfesional(lormProfesional);
        }
        catch(Exception e){
            t.rollback();
            return null;
        }
    }
    
    /**
     * Busca un profesional a traves del ID
     * 
     * @param id
     * @return Profesional
     * @throws PersistentException 
     */
    public Profesional buscarProfesional(int id)throws PersistentException{
        
        Profesional profesional= new Profesional();
       
        try{
            System.out.println("ESR"+id);
            orm.Profesional lormprofesional= orm.ProfesionalDAO.loadProfesionalByQuery("Profesional.persona.id="+id+"", null);
            
            if(lormprofesional.getId()!=0){
                profesional = profesional.toCNProfesional(lormprofesional);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return profesional;
    }
    
    /**
     * Elimina un profesional a traves del ID
     * 
     * @param id
     * @return String
     * @throws PersistentException 
     */
    public String eliminarProfesional(int id) throws PersistentException { 
        PersistentTransaction t = orm.SaludPersistentManager.instance().getSession().beginTransaction();
        
        try {
            orm.Profesional lormProfesional = orm.ProfesionalDAO.loadProfesionalByORMID(id);
            orm.ProfesionalDAO.delete(lormProfesional);
            t.commit();
            return "Registro Eliminado";
        }
        catch (Exception e) {
                t.rollback();
                return "Error";
        }
    }

}
