package capaNegocio;

import java.util.ArrayList;
import java.util.List;
import org.orm.PersistentException;
import orm.PacienteDAO;
import vo.PacienteVo;
import vo.PagoVo;
import vo.RceVo;
import vo.TipoPrevisionVo;

public class Pago {

    public PacienteVo obtenerPacientePorRut(String rut) {
        try {
            orm.PersonaCriteria pc = new orm.PersonaCriteria();
            pc.rut.eq(rut);
            orm.Persona personaResult = (orm.Persona) pc.uniqueResult();
            if (personaResult != null) {
                List lres;
                lres = PacienteDAO.queryPaciente("persona_id=" + personaResult.getId(), null);
                orm.Paciente pac = lres.isEmpty() ? null : (orm.Paciente) lres.get(0);
                if (pac != null) {
                    return PacienteVo.fromORM(pac);
                }
            }
        } catch (PersistentException e) {
            e.printStackTrace();
        }
        return null;
    }

    public PagoVo efectuarPago(int idMedioPago, int idRce, double monto, double comprobante, int estadoPago, float subsidio) {
        try {
            // Intenta setear todos los atributos heredados del orm pago
            orm.Pago pago = orm.PagoDAO.createPago();
            orm.Medio_pago med = orm.Medio_pagoDAO.getMedio_pagoByORMID(idMedioPago);
            orm.Rce rce = orm.RceDAO.getRceByORMID(idRce);
            pago.setMedio_pago(med);
            pago.setRce_examen(rce);
            pago.setMonto(monto);
            pago.setComprobante(comprobante);
            pago.setEstadopago(estadoPago);
            pago.setSubsidio(subsidio);
            //Si es guardado
            if (orm.PagoDAO.save(pago)) {
                orm.PagoDAO.refresh(pago);
                PagoVo pvo = PagoVo.fromORM(pago);
                return pvo;
            }
            
        } catch (PersistentException e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }
    
    public double obtenerComprobantePorIdPago(int id) {

        try {
            //Obtiene el comprobante por el id ingresado
            orm.Pago pag = orm.PagoDAO.getPagoByORMID(id);
            double comp = pag.getComprobante();
            if (pag != null) {
               return comp;
            }
        } catch (PersistentException e) {
            e.printStackTrace();
        }
        return 0.0;
    }

    
    public TipoPrevisionVo ingresarTipoPrevision(String desc) {

        try {
            orm.Tipo_prevision tprev = new orm.Tipo_prevision();
            //Setea la descripcion del tipo de previsión
            tprev.setDescripcion(desc);
            //Si logra persistir
            if (orm.Tipo_previsionDAO.save(tprev)) {
                orm.Tipo_previsionDAO.refresh(tprev);
                TipoPrevisionVo previ = TipoPrevisionVo.fromORM(tprev);
                return previ;
            }
        } catch (PersistentException e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }

    public RceVo obtenerRcePorRut(String rut) {
        try {
            orm.RceCriteria rc = new orm.RceCriteria();
            //Consulta el rut ingresado
            rc.createReservaCriteria().createPacienteCriteria().createPersonaCriteria().rut.eq(rut);
            orm.Rce rceResult = (orm.Rce) rc.uniqueResult();
            if (rceResult != null) {
                @SuppressWarnings("rawtypes")
                List lres = rc.list();
                //Si la lista esta vacia, retorna null, si no, retorna el primer valor.
                orm.Rce rcee = lres.isEmpty() ? null : (orm.Rce) lres.get(0);
                if (rcee != null) {
                    return RceVo.fromORM(rcee);
                }
            }
        } catch (PersistentException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public ArrayList<PagoVo> obtenerDeudas(String rutPaciente) {
		ArrayList<PagoVo> lpagos = new ArrayList<PagoVo>();
        try {
                orm.PagoCriteria pc = new orm.PagoCriteria();
                //Consulta por el rut ingresado
                pc.createRce_examenCriteria().createReservaCriteria()
                        .createPacienteCriteria().createPersonaCriteria().rut.eq(rutPaciente);
                @SuppressWarnings("unchecked")
                List<orm.Pago> deudas = pc.list();
                for (int i = 0; i < deudas.size(); i++) {
                        PagoVo pvo = PagoVo.fromORM(deudas.get(i));
                        //Solo añade a la lista si estadoPago es 0, es decir, si no está pagado.
                        if(deudas.get(i).getEstadopago() == 0){
                            lpagos.add(pvo);
                        }
                }// end for
                return lpagos;
        } catch (PersistentException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        }
        return null;
    }
}
