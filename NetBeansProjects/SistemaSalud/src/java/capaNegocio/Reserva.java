package capaNegocio;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.orm.PersistentException;
import org.orm.PersistentTransaction;
import vo.PacienteVo;

public class Reserva {

    public Reserva() {
    }

    private int id;
    private Paciente paciente;
    private Persona persona_id_ingresa_reserva;
    private Horamedica hora_medica;
    private java.sql.Timestamp fecha;
    private String motivoconsulta;
    private Rce rce;

    public Reserva(Paciente paciente, 
            Persona persona_id_ingresa_reserva, 
            Horamedica hora_medica, 
            Timestamp fecha, 
            String motivoconsulta, 
            Rce rce) {
        
        this.paciente = paciente;
        this.persona_id_ingresa_reserva = persona_id_ingresa_reserva;
        this.hora_medica = hora_medica;
        this.fecha = fecha;
        this.motivoconsulta = motivoconsulta;
        this.rce = rce;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Persona getPersona_id_ingresa_reserva() {
        return persona_id_ingresa_reserva;
    }

    public void setPersona_id_ingresa_reserva(Persona persona_id_ingresa_reserva) {
        this.persona_id_ingresa_reserva = persona_id_ingresa_reserva;
    }

    public Horamedica getHora_medica() {
        return hora_medica;
    }

    public void setHora_medica(Horamedica hora_medica) {
        this.hora_medica = hora_medica;
    }

    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    public String getMotivoconsulta() {
        return motivoconsulta;
    }

    public void setMotivoconsulta(String motivoconsulta) {
        this.motivoconsulta = motivoconsulta;
    }

    public Rce getRce() {
        return rce;
    }

    public void setRce(Rce rce) {
        this.rce = rce;
    }

    public String toString() {
        return String.valueOf(getId());
    }

    /**
     * Transforma el tipo orm.Reserva a el tipo capaNegocio.Reserva Esto es
     * necesario para el manejo y envio de datos sin utilizar la capa de
     * persistencia para las transacciones.
     *
     * @param reserva - orm.Reserva - Reserva a transformar
     * @return capaNegocio.Reserva - Salida de la transformacion
     */
    public Reserva toCNReserva(orm.Reserva reserva) {
        Reserva salida = new Reserva();
        salida.setId(reserva.getId());
        salida.setFecha(reserva.getFecha());
        Horamedica hm = new Horamedica();
        salida.setHora_medica(hm.toCNHoramedica(reserva.getHora_medica()));
        salida.setMotivoconsulta(reserva.getMotivoconsulta());
        Paciente p = new Paciente();
        salida.setPaciente(p.toCNPaciente(reserva.getPaciente()));
        Persona per = new Persona();
        salida.setPersona_id_ingresa_reserva(per.toCNPersona(reserva.getPersona_id_ingresa_reserva()));
        return salida;
    }
    
    //NO verificado
    public orm.Reserva toORMReserva (Reserva reserva){
        orm.Reserva salida = orm.ReservaDAO.createReserva();
        salida.setId(reserva.getId());
        salida.setFecha(reserva.getFecha());
        Horamedica hm = new Horamedica();
        salida.setHora_medica(hm.toORMHoramedica(reserva.getHora_medica()));
        salida.setMotivoconsulta(reserva.getMotivoconsulta());
        Paciente p = new Paciente();
        salida.setPaciente(p.toORMPaciente(reserva.getPaciente()));
        Persona per = new Persona();
        salida.setPersona_id_ingresa_reserva(per.toORMPersona(reserva.getPersona_id_ingresa_reserva()));
        return salida;
    }
    
    /**
     * Agrega una reserva medica medainte ruts y una Horamedica
     * 
     * @param rutPaciente
     * @param rutAdministrador
     * @param horamedica
     * @param fecha
     * @param motivoConsulta
     * @return Reserva
     * @throws PersistentException 
     */
    public Reserva agregarReservaByRuts(String rutPaciente, String rutAdministrador, Horamedica horamedica , String fecha, String motivoConsulta) throws PersistentException{
        PersistentTransaction t = orm.SaludPersistentManager.instance().getSession().beginTransaction();
        Reserva reserva = new Reserva();
        
        try {
                      
            orm.Reserva lormReserva = orm.ReservaDAO.createReserva();
            orm.Horamedica lormHoramedica = orm.HoramedicaDAO.loadHoramedicaByORMID(horamedica.getId());          
            
            //cargarPersonas
            orm.Persona lormPerAdmin = orm.PersonaDAO.loadPersonaByQuery("Persona.rut='"+rutAdministrador+"'", null);
            orm.Persona lormPerPac = orm.PersonaDAO.loadPersonaByQuery("Persona.rut='"+rutPaciente+"'", null);
              
            if(lormPerAdmin != null){
                lormReserva.setPersona_id_ingresa_reserva(lormPerAdmin);
            }            
            if(lormPerPac != null){
                lormReserva.setPaciente(lormPerPac.getPaciente());
            }
            
            //SOLO INTEGRAR RCE en caso de Cliente Unico
            //orm.Rce rceTest = orm.RceDAO.createRce();
            //lormReserva.setRce(rceTest);
            
            lormReserva.setHora_medica(lormHoramedica);            
            lormReserva.setFecha(lormHoramedica.getHora_inicio());
            lormReserva.setMotivoconsulta(motivoConsulta);
            orm.ReservaDAO.save(lormReserva);
            t.commit();
            
            reserva = reserva.toCNReserva(lormReserva);
            return reserva;
        } catch (PersistentException ex) {
            return null;
        }
        
    }
    
    /**
     * 
     * @param reserva
     * @param fechaInicio
     * @param fechaTermino
     * @return List< Reserva>
     * @throws PersistentException 
     */
    public List<Reserva> buscarPorPaciente(Reserva reserva, String fechaInicio, String fechaTermino) throws PersistentException {
        // TODO - implement Reserva.buscarPorPaciente
        List<Reserva> lista = new ArrayList<Reserva>();
        List<orm.Reserva> listaAtenciones = new ArrayList<orm.Reserva>();
        String consulta = "";

        if (reserva.getPaciente().getPersona().getRut() != null && !reserva.getPaciente().getPersona().getRut().equals("")) {
            consulta += "Reserva.paciente.persona.rut='" + reserva.getPaciente().getPersona().getRut() + "' ";
        }

        if ((reserva.getPaciente().getPersona().getRut() != null && !reserva.getPaciente().getPersona().getRut().equals(""))
                && (reserva.getPaciente().getPersona().getNombres() != null && !reserva.getPaciente().getPersona().getNombres().equals(""))) {
            consulta += "and ";
        }

        if (reserva.getPaciente().getPersona().getNombres() != null && !reserva.getPaciente().getPersona().getNombres().trim().equals("")) {
            consulta = "Reserva.paciente.persona.nombres='" + reserva.getPaciente().getPersona().getNombres() + "' ";
        }

        if ((reserva.getPaciente().getPersona().getRut() != null && !reserva.getPaciente().getPersona().getRut().trim().equals("")
                || reserva.getPaciente().getPersona().getNombres() != null && !reserva.getPaciente().getPersona().getNombres().trim().equals(""))
                && (reserva.getPaciente().getPersona().getApellido_paterno() != null && !reserva.getPaciente().getPersona().getApellido_paterno().trim().equals(""))) {
            consulta += "and ";
        }

        if (reserva.getPaciente().getPersona().getApellido_paterno() != null && !reserva.getPaciente().getPersona().getApellido_paterno().trim().equals("")) {
            consulta += "Reserva.paciente.persona.apellido_paterno='" + reserva.getPaciente().getPersona().getApellido_paterno() + "' ";
        }

        if ((reserva.getPaciente().getPersona().getRut() != null && !reserva.getPaciente().getPersona().getRut().trim().equals("")
                || reserva.getPaciente().getPersona().getNombres() != null && !reserva.getPaciente().getPersona().getNombres().trim().equals("")
                || reserva.getPaciente().getPersona().getApellido_paterno() != null && !reserva.getPaciente().getPersona().getApellido_paterno().trim().equals(""))
                && reserva.getPaciente().getPersona().getApellido_materno() != null && !reserva.getPaciente().getPersona().getApellido_materno().trim().equals("")) {
            consulta += "and ";
        }

        if (reserva.getPaciente().getPersona().getApellido_materno() != null && !reserva.getPaciente().getPersona().getApellido_materno().trim().equals("")) {
            consulta += "Reserva.paciente.persona.apellido_materno='" + reserva.getPaciente().getPersona().getApellido_materno() + "' ";
        }

        consulta += "and Reserva.fecha between '" + fechaInicio + "' and '" + fechaTermino + "' ";

        listaAtenciones = orm.ReservaDAO.queryReserva(consulta, null);

        if (listaAtenciones != null) {
            for (orm.Reserva reservaOrm : listaAtenciones) {
                Reserva reservaNegocio = new Reserva();
                orm.Reserva reservaORM = orm.ReservaDAO.loadReservaByORMID(reservaOrm.getId());

                Persona personaNegocio = new Persona();
                orm.Persona personaORM = orm.PersonaDAO.loadPersonaByORMID(reservaOrm.getPaciente().getPersona().getId());
                personaNegocio.setRut(personaORM.getRut());
                personaNegocio.setNombres(personaORM.getNombres());
                personaNegocio.setApellido_paterno(personaORM.getApellido_paterno());
                personaNegocio.setApellido_materno(personaORM.getApellido_materno());
                personaNegocio.setEmail(personaORM.getEmail());

                Paciente pacienteNegocio = new Paciente();
                orm.Paciente pacienteORM = orm.PacienteDAO.loadPacienteByORMID(reservaOrm.getPaciente().getId());
                pacienteNegocio.setId(pacienteORM.getId());
                pacienteNegocio.setPersona(personaNegocio);
                pacienteNegocio.setDescripcion(pacienteORM.getDescripcion());

                Persona personaProfesionalNegocio = new Persona();
                orm.Persona personaProfesionalORM = orm.PersonaDAO.loadPersonaByORMID(reservaOrm.getHora_medica().getProfesional().getPersona().getId());
                personaProfesionalNegocio.setRut(personaProfesionalORM.getRut());
                personaProfesionalNegocio.setNombres(personaProfesionalORM.getNombres());
                personaProfesionalNegocio.setApellido_paterno(personaProfesionalORM.getApellido_paterno());
                personaProfesionalNegocio.setApellido_materno(personaProfesionalORM.getApellido_materno());
                personaProfesionalNegocio.setEmail(personaProfesionalORM.getEmail());

                Profesional medicoNegocio = new Profesional();
                orm.Profesional profesionalORM = orm.ProfesionalDAO.loadProfesionalByORMID(reservaOrm.getHora_medica().getProfesional().getId());
                medicoNegocio.setPersona_id(personaProfesionalNegocio.getId());

                Horamedica horaNegocio = new Horamedica();
                orm.Horamedica horaORM = orm.HoramedicaDAO.loadHoramedicaByORMID(reservaOrm.getHora_medica().getId());
                horaNegocio.setHora_inicio(horaORM.getHora_inicio());
                horaNegocio.setTiempo_periodo(horaORM.getTiempo_periodo());
                horaNegocio.setProfesional(medicoNegocio);

                reservaNegocio.setId(reservaORM.getId());
                reservaNegocio.setHora_medica(horaNegocio);
                reservaNegocio.setPaciente(pacienteNegocio);
                reservaNegocio.setMotivoconsulta(reservaORM.getMotivoconsulta());

                lista.add(reservaNegocio);
            }
        }
        return lista;
    }

    /**
     *
     * @param reserva
     * @param fechaInicio
     * @param fechaTermino
     * @return List< Reserva>
     * @throws org.orm.PersistentException
     */
    
    public List<Reserva> buscarPorProfesional(Reserva reserva, String fechaInicio, String fechaTermino) throws PersistentException {
        
        List<orm.Reserva> listaOrm = new ArrayList<orm.Reserva>();
        List<Reserva> listaReserva = new ArrayList<Reserva>();
        String consulta = "";
        
        Profesional profAux = new Profesional();
        Persona medico = new Persona();
        //medico = medico.buscarPersonaPorRut(reserva.getPersona_id_ingresa_reserva().getRut());
        
        int a = reserva.getHora_medica().getProfesional().getPersona_id();
        medico = medico.toCNPersona(orm.PersonaDAO.getPersonaByORMID(a));
        
        String rutMedico = medico.getRut();
        String nombresMedico = medico.getNombres();
        String apellidoPaterno = medico.getApellido_paterno();
        String apellidoMaterno = medico.getApellido_materno();
        
        if (rutMedico != null && !rutMedico.equals("")) {
            consulta += "Reserva.hora_medica.profesional.persona.rut='" + rutMedico + "' ";
        }

        consulta += "and Reserva.fecha between '" + fechaInicio + "' and '" + fechaTermino + "' ";

        listaOrm = orm.ReservaDAO.queryReserva(consulta, null);

        if (listaOrm != null) {
            for (orm.Reserva reservaOrm : listaOrm) {
                Reserva reservaNegocio = new Reserva();
                orm.Reserva reservaORM = orm.ReservaDAO.loadReservaByORMID(reservaOrm.getId());

                Persona personaPacienteNegocio = new Persona();
                orm.Persona personaPacienteORM = orm.PersonaDAO.loadPersonaByORMID(reservaOrm.getPaciente().getPersona().getId());
                personaPacienteNegocio.setRut(personaPacienteORM.getRut());
                personaPacienteNegocio.setNombres(personaPacienteORM.getNombres());
                personaPacienteNegocio.setApellido_paterno(personaPacienteORM.getApellido_paterno());
                personaPacienteNegocio.setApellido_materno(personaPacienteORM.getApellido_materno());
                personaPacienteNegocio.setEmail(personaPacienteORM.getEmail());

                Persona personaMedicoNegocio = new Persona();
                orm.Persona personaMedicoORM = orm.PersonaDAO.loadPersonaByORMID(reservaOrm.getHora_medica().getProfesional().getPersona().getId());
                personaMedicoNegocio.setRut(personaMedicoORM.getRut());
                personaMedicoNegocio.setNombres(personaMedicoORM.getNombres());
                personaMedicoNegocio.setApellido_paterno(personaMedicoORM.getApellido_paterno());
                personaMedicoNegocio.setApellido_materno(personaMedicoORM.getApellido_materno());
                personaMedicoNegocio.setEmail(personaMedicoORM.getEmail());

                Profesional profesionalNegocio = new Profesional();
                orm.Profesional profesionalORM = orm.ProfesionalDAO.loadProfesionalByORMID(reservaOrm.getHora_medica().getProfesional().getId());
                profesionalNegocio.setId(profesionalORM.getId());
                profesionalNegocio.setPersona_id(personaMedicoNegocio.getId());
                
                Paciente pacienteNegocio = new Paciente();
                orm.Paciente pacienteORM = orm.PacienteDAO.loadPacienteByORMID(reservaOrm.getPaciente().getId());
                pacienteNegocio.setDescripcion(pacienteORM.getDescripcion());
                pacienteNegocio.setId(pacienteORM.getId());
                pacienteNegocio.setPersona(personaPacienteNegocio);

                Horamedica horaNegocio = new Horamedica();
                orm.Horamedica horaORM = orm.HoramedicaDAO.loadHoramedicaByORMID(reservaOrm.getHora_medica().getId());
                horaNegocio.setHora_inicio(horaORM.getHora_inicio());
                horaNegocio.setTiempo_periodo(horaORM.getTiempo_periodo());
                horaNegocio.setProfesional(profesionalNegocio);

                reservaNegocio.setId(reservaORM.getId());
                reservaNegocio.setFecha(reservaORM.getFecha());
                reservaNegocio.setMotivoconsulta(reservaORM.getMotivoconsulta());
                reservaNegocio.setHora_medica(horaNegocio);
                reservaNegocio.setPaciente(pacienteNegocio);

                listaReserva.add(reservaNegocio);
            }
        }
        return listaReserva;
    }

    /**
     *
     * @param especialidad
     * @param fechaInicio
     * @param id
     * @param fechaTermino
     * @return List< Reserva>
     * @throws org.orm.PersistentException
     */
    public List<Reserva> buscarPorEspecialidad(String especialidad, String fechaInicio, String fechaTermino) throws PersistentException {
        List<Reserva> listaReserva = new ArrayList<Reserva>();
        List<orm.Reserva> listaOrm = new ArrayList<orm.Reserva>();

        List<orm.Especialidad> especialidadOrm = new ArrayList<orm.Especialidad>();

        String consulta = "";
        String query = "";
        query = "Especialidad.especialidad='" + especialidad + "' ";
        especialidadOrm = orm.EspecialidadDAO.queryEspecialidad(query, null);
        orm.Especialidad especialidadId = orm.EspecialidadDAO.getEspecialidadByORMID(especialidadOrm.get(0).getId());

        Set<orm.Profesional> lista = new HashSet<orm.Profesional>();
        lista = especialidadId.getORM_Profesional();

        int[] listaIndices = new int[lista.size()];
        int x = 0;

        orm.Profesional e = new orm.Profesional();

        for (Iterator iterador = lista.iterator(); iterador.hasNext();) {
            e = (orm.Profesional) iterador.next();
            listaIndices[x] = e.getId();
            x += 1;
        }

        for (int i = 0; i < listaIndices.length; i++) {
            if (i < listaIndices.length - 1) {
                consulta += "Reserva.hora_medica.profesional.id='" + listaIndices[i] + "' or ";
            } else {
                consulta += "Reserva.hora_medica.profesional.id='" + listaIndices[i] + "' ";
            }
        }

        consulta += "and Reserva.fecha between '" + fechaInicio + "' and '" + fechaTermino + "' ";

        listaOrm = orm.ReservaDAO.queryReserva(consulta, null);

        if (listaOrm != null) {
            for (orm.Reserva reservaOrm : listaOrm) {
                Reserva reservaNegocio = new Reserva();
                orm.Reserva reservaORM = orm.ReservaDAO.loadReservaByORMID(reservaOrm.getId());

                Persona personaPacienteNegocio = new Persona();
                orm.Persona personaPacienteORM = orm.PersonaDAO.loadPersonaByORMID(reservaOrm.getPaciente().getPersona().getId());
                personaPacienteNegocio.setRut(personaPacienteORM.getRut());
                personaPacienteNegocio.setNombres(personaPacienteORM.getNombres());
                personaPacienteNegocio.setApellido_paterno(personaPacienteORM.getApellido_paterno());
                personaPacienteNegocio.setApellido_materno(personaPacienteORM.getApellido_materno());
                personaPacienteNegocio.setEmail(personaPacienteORM.getEmail());

                Persona personaMedicoNegocio = new Persona();
                orm.Persona personaMedicoORM = orm.PersonaDAO.loadPersonaByORMID(reservaOrm.getHora_medica().getProfesional().getPersona().getId());
                personaMedicoNegocio.setRut(personaMedicoORM.getRut());
                personaMedicoNegocio.setNombres(personaMedicoORM.getNombres());
                personaMedicoNegocio.setApellido_paterno(personaMedicoORM.getApellido_paterno());
                personaMedicoNegocio.setApellido_materno(personaMedicoORM.getApellido_materno());
                personaMedicoNegocio.setEmail(personaMedicoORM.getEmail());

                Profesional profesionalNegocio = new Profesional();
                orm.Profesional profesionalORM = orm.ProfesionalDAO.loadProfesionalByORMID(reservaOrm.getHora_medica().getProfesional().getId());
                profesionalNegocio.setId(profesionalORM.getId());
                profesionalNegocio.setPersona_id(personaMedicoNegocio.getId());
                //profesionalNegocio.setDescripcion(pacienteORM.getDescripcion());

                Paciente pacienteNegocio = new Paciente();
                orm.Paciente pacienteORM = orm.PacienteDAO.loadPacienteByORMID(reservaOrm.getPaciente().getId());
                pacienteNegocio.setDescripcion(pacienteORM.getDescripcion());
                pacienteNegocio.setId(pacienteORM.getId());
                pacienteNegocio.setPersona(personaPacienteNegocio);

                Horamedica horaNegocio = new Horamedica();
                orm.Horamedica horaORM = orm.HoramedicaDAO.loadHoramedicaByORMID(reservaOrm.getHora_medica().getId());
                horaNegocio.setTiempo_periodo(horaORM.getTiempo_periodo());
                horaNegocio.setProfesional(profesionalNegocio);

                reservaNegocio.setId(reservaORM.getId());
                reservaNegocio.setFecha(reservaORM.getFecha());
                reservaNegocio.setMotivoconsulta(reservaORM.getMotivoconsulta());
                reservaNegocio.setHora_medica(horaNegocio);
                reservaNegocio.setPaciente(pacienteNegocio);

                listaReserva.add(reservaNegocio);
            }
        }

        return listaReserva;
    }

    public List<Especialidad> ListarEspecilidades() throws PersistentException {
        List<Especialidad> lista = new ArrayList<Especialidad>();
        orm.Especialidad[] listaOrm = orm.EspecialidadDAO.listEspecialidadByQuery(null, null);

        for (int x = 0; x < listaOrm.length; x++) {
            Especialidad especialidad = new Especialidad();
            especialidad.setId(listaOrm[x].getId());
            especialidad.setEspecialidad(listaOrm[x].getEspecialidad());
            lista.add(especialidad);
        }
        return lista;
    }
    
    /**
     * 
     * @param id
     * @return Reserva
     * @throws PersistentException 
     */
    public Reserva obtenerReserva(int id) throws PersistentException {
        Reserva reservaNegocio = new Reserva();
        Paciente paciente = new Paciente();
        Profesional profesional = new Profesional();
        Persona personaPaciente = new Persona();
        Persona personaProfesional = new Persona();
        Horamedica hora = new Horamedica();

        try {
            orm.Reserva reservaOrm = orm.ReservaDAO.loadReservaByORMID(id);

            personaPaciente.setId(reservaOrm.getPaciente().getPersona().getId());
            personaPaciente.setRut(reservaOrm.getPaciente().getPersona().getRut());
            personaPaciente.setNombres(reservaOrm.getPaciente().getPersona().getNombres());
            personaPaciente.setApellido_paterno(reservaOrm.getPaciente().getPersona().getApellido_paterno());
            personaPaciente.setApellido_materno(reservaOrm.getPaciente().getPersona().getApellido_materno());
            personaPaciente.setEmail(reservaOrm.getPaciente().getPersona().getEmail());
            personaPaciente.setFecha_nacimiento(reservaOrm.getPaciente().getPersona().getFecha_nacimiento());

            paciente.setId(reservaOrm.getPaciente().getId());
            paciente.setPersona(personaPaciente);

            personaProfesional.setId(reservaOrm.getPaciente().getPersona().getId());
            personaProfesional.setRut(reservaOrm.getPaciente().getPersona().getRut());
            personaProfesional.setNombres(reservaOrm.getHora_medica().getProfesional().getPersona().getNombres());
            personaProfesional.setApellido_paterno(reservaOrm.getHora_medica().getProfesional().getPersona().getApellido_paterno());
            personaProfesional.setEmail(reservaOrm.getPaciente().getPersona().getEmail());
            personaProfesional.setFecha_nacimiento(reservaOrm.getPaciente().getPersona().getFecha_nacimiento());

            profesional.setId(reservaOrm.getHora_medica().getProfesional().getId());
            profesional.setPersona_id(personaProfesional.getId());

            hora.setId(reservaOrm.getHora_medica().getId());
            hora.setProfesional(profesional);

            reservaNegocio.setId(reservaOrm.getId());
            reservaNegocio.setFecha(reservaOrm.getFecha());
            reservaNegocio.setMotivoconsulta(reservaOrm.getMotivoconsulta());
            reservaNegocio.setPaciente(paciente);
            reservaNegocio.setHora_medica(hora);

        } catch (PersistentException e) {
            reservaNegocio = null;
        }

        return reservaNegocio;
    }
    
    /**
     * 
     * @param fecha
     * @return int 0=invalida, 1=valida
     */
    public int validarFecha(String fecha) {
        int fechaValida = 0;
        try {
            String fechaR = fecha;
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date fechaC = dateFormat.parse(fechaR);
            fechaValida = 1;
        } catch (ParseException e) {
            fechaValida = 0;
        }
        return fechaValida;
    }

    private static final int ROW_COUNT = 100;
    
    /**
     * 
     * @return PacienteVo
     */
    public PacienteVo[] reservasPorPaciente() {
        int length;
        System.out.println("Listing Paciente...");
        orm.Paciente[] ormPacientes = null;
        PacienteVo pacientesVO[] = null;
        try {
            ormPacientes = orm.PacienteDAO.listPacienteByQuery(null, null);
            length = Math.min(ormPacientes.length, ROW_COUNT);
            pacientesVO = new PacienteVo[length];
            for (int i = 0; i < length; i++) {
                pacientesVO[i] = new PacienteVo(ormPacientes[i].getPersona().getRut(), ormPacientes[i].getPersona().getNombres(), ormPacientes[i].reserva.size());
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Reserva.class.getName()).log(Level.SEVERE, null, ex);
        }

        return pacientesVO;
    }

}
