package capaNegocio;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

public class Contacto {

	private int uid;
	private String nombre;
	private String apellido;
	private String mail;
	private String telefono;
        private Empresa empresa;
        private Ciudad ciudad;

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

	public String getTelefono() {
		return this.telefono;
	}

	/**
	 * 
	 * @param telefono
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getMail() {
		return this.mail;
	}

	/**
	 * 
	 * @param mail
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getApellido() {
		return this.apellido;
	}

	/**
	 * 
	 * @param apellido
	 */
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombre() {
		return this.nombre;
	}

	/**
	 * 
	 * @param nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getUid() {
		return this.uid;
	}

	/**
	 * 
	 * @param uid
	 */
	public void setUid(int uid) {
		this.uid = uid;
	}

	/**
	 * 
	 * @param contacto
	 */
	public int addContact(Contacto contacto) throws PersistentException {
		// TODO - implement Contacto.addContact
                int respuesta = 0;
                PersistentTransaction t = orm.SwPersistentManager.instance().getSession().beginTransaction();
                try {
                    orm.Ciudad lormCiudad = orm.CiudadDAO.createCiudad();
                    lormCiudad.setNombre(contacto.getCiudad().getNombre());
                    orm.CiudadDAO.save(lormCiudad);
                    
                    orm.Empresa lormEmpresa = orm.EmpresaDAO.createEmpresa();
                    lormEmpresa.setNombre(contacto.getEmpresa().getNombre());
                    orm.EmpresaDAO.save(lormEmpresa);
                    
                     orm.Contacto lormContacto = orm.ContactoDAO.createContacto();
                    lormContacto.setNombre(contacto.getNombre());
                    lormContacto.setApellido(contacto.getApellido());
                    lormContacto.setMail(contacto.getMail());
                    lormContacto.setTelefono(contacto.getTelefono());
                    lormContacto.setORM_Ciudadu(lormCiudad);
                    lormContacto.setORM_Empresau(lormEmpresa);
                    orm.ContactoDAO.save(lormContacto);
                    orm.ContactoDAO.refresh(lormContacto);
                    t.commit();
                    respuesta = lormContacto.getUid();

                    
                }catch(Exception e){
                    t.rollback();
                }
                return respuesta;
	}

	/**
	 * 
	 * @param contacto
	 */
	public int updateContact(Contacto contacto) throws PersistentException {
		// TODO - implement Contacto.updateContact
		int respuesta = 0;
                PersistentTransaction t = orm.SwPersistentManager.instance().getSession().beginTransaction();
                try {
                     orm.Ciudad lormCiudad = orm.CiudadDAO.loadCiudadByORMID(contacto.getCiudad().getUid());
                     lormCiudad.setNombre(contacto.getCiudad().getNombre());
                     
                     orm.Empresa lormEmpresa = orm.EmpresaDAO.loadEmpresaByORMID(contacto.getEmpresa().getUid());
                     lormEmpresa.setNombre(contacto.getEmpresa().getNombre());
                     orm.Contacto lormContacto = orm.ContactoDAO.loadContactoByORMID(contacto.getUid());
                     lormContacto.setNombre(contacto.getNombre());
                     lormContacto.setApellido(contacto.getApellido());
                     lormContacto.setTelefono(contacto.getTelefono());
                     lormContacto.setMail(contacto.getMail());
                     lormContacto.setORM_Ciudadu(lormCiudad);
                     lormContacto.setORM_Empresau(lormEmpresa);
                     t.commit();
                     respuesta = lormContacto.getUid();
                     
                }catch(Exception e){
                    t.rollback();
                }
                return respuesta;
	}

	/**
	 * 
	 * @param contacto
	 */
	public int removeContact(Contacto contacto) throws PersistentException {
		// TODO - implement Contacto.removeContact
		int respuesta = 0;
                PersistentTransaction t = orm.SwPersistentManager.instance().getSession().beginTransaction();
                try {
                     orm.Ciudad lormCiudad = orm.CiudadDAO.loadCiudadByORMID(contacto.getCiudad().getUid());
                     orm.CiudadDAO.delete(lormCiudad);
                     
                     orm.Empresa lormEmpresa = orm.EmpresaDAO.loadEmpresaByORMID(contacto.getEmpresa().getUid());
                     orm.EmpresaDAO.delete(lormEmpresa);
                     orm.Contacto lormContacto = orm.ContactoDAO.loadContactoByORMID(contacto.getUid());
                     orm.ContactoDAO.delete(lormContacto);
                    
                     t.commit();
                     return lormContacto.getUid();
                     
                }catch(Exception e){
                    t.rollback();
                }
                return respuesta;
	}

}