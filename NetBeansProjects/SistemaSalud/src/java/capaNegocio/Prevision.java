package capaNegocio;

import vo.TipoPrevisionVo;
import org.orm.PersistentException;

public class Prevision {

    private static final int ROW_COUNT = 100;

    public TipoPrevisionVo[] pacientesPorPrevision() {

        int lengthPrevisiones;
        orm.Tipo_prevision[] ormTipo_previsions;
        TipoPrevisionVo[] previsiones = null;
        try {
            ormTipo_previsions = orm.Tipo_previsionDAO.listTipo_previsionByQuery(null, null);
            lengthPrevisiones = Math.min(ormTipo_previsions.length, ROW_COUNT);
            previsiones = new TipoPrevisionVo[lengthPrevisiones];
            
            for (int i = 0; i < lengthPrevisiones; i++) {
                orm.Paciente[] ormPacientes = orm.PacienteDAO.listPacienteByQuery("tipo_previsionid = " + ormTipo_previsions[i].getId(), null);
                int lengthPacientes = Math.min(ormPacientes.length, ROW_COUNT);
                previsiones[i] = new TipoPrevisionVo(lengthPacientes ,ormTipo_previsions[i].getDescripcion());
            }
        } catch (PersistentException ex) {
            System.out.println("Error al listar previsiones.");
        }    
        return previsiones;
    }
}