package capaNegocio;

import orm.*;
import java.io.Serializable;
import org.orm.PersistentException;
import org.orm.PersistentTransaction;

public class Recetarce implements Serializable {

    private int id;
    private Rce rce;
    private Receta receta;

    public Recetarce() {
    }

    public Recetarce(Receta receta) {
        this.receta = receta;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public capaNegocio.Rce getRce() {
        return rce;
    }

    public void setRce(capaNegocio.Rce rce) {
        this.rce = rce;
    }

    public Receta getReceta() {
        return receta;
    }

    public void setReceta(Receta receta) {
        this.receta = receta;
    }

    @Override
    public String toString() {
        return String.valueOf(getId());
    }

    /**
     * Transforma el tipo capaNegocio.Recetarce a el tipo orm.Recetarce Esto es
     * necesario para el manejo y envio de datos sin utilizar la capa de
     * persistencia para las transacciones.
     *
     * @param recetarce - capaNegocio.Recetarce - Destino a transformar
     * @return orm.Recetarce - Salida de la transformacion
     */
    public orm.Recetarce toORMRecetarse(Recetarce recetarce) {
        orm.Recetarce salida = orm.RecetarceDAO.createRecetarce();
        salida.setId(recetarce.getId());
        salida.setRce(recetarce.getRce().toORMRce(recetarce.getRce()));
        return salida;
    }

    /**
     * Crear un registro Recetarse en la base de datos
     *
     * @param recetarce - Recetarce - Objeto Recetarce a ingresar en la base de
     * datos
     * @return String - Salida del ingreso
     */
    public String crearResetarse(Recetarce recetarce) {
        String salida = "";
        PersistentTransaction t = null;
        try {
            t = SaludPersistentManager.instance().getSession().beginTransaction();
            orm.Recetarce rec = orm.RecetarceDAO.createRecetarce();
            rec = recetarce.toORMRecetarse(recetarce);
            orm.RecetarceDAO.save(rec);
            t.commit();
            salida = "Ingreso Exitoso!";
        } catch (Exception e) {
            try {
                t.rollback();
            } catch (PersistentException e1) {
                salida = "ERROR Volviendo todo atras.";
            }
            salida = "ERROR Creando contacto";
            e.printStackTrace();
        }
        return salida;
    }
 
    /**
     *
     * @param recetarce
     * @return
     */
    public Recetarce toCNRecetarse(orm.Recetarce recetarce) {
        Recetarce salida = new Recetarce();
        salida.setId(recetarce.getId());
        Rce r = new Rce();
        salida.setRce(r.toCNRce(recetarce.getRce()));
        return salida;
    }
}
