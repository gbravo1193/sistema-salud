
package capaNegocio;

import java.io.Serializable;

public class Usuario implements Serializable {

    public Usuario() {
    }

    private int id;

    private String password;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return String.valueOf(getId());
    }
    
    /**
     * Verifica la valides de los parametros para permitir el ingreso al sistema
     * @param rut - String - Rut de la persona.
     * @param password - String - Contraseña de la persona
     * @return boolean - true, si permite el ingreso. false, rechaza el ingreso con datos invalidos.
     */
    
    public boolean verificarUsuario(String rut, String password) {
        Persona persona = new Persona();
        persona = persona.buscarPersonaPorRut(rut);
        //orm.Usuario user = null;
        boolean valido = true;
        /*
        if (persona != null) {
            int profId = 0;
            int adminId = 0;
            if (persona.getProfesional() != null) {
                profId = persona.getProfesional().getId();
            } else if (persona.getAdministrador() != null) {
                adminId = persona.getAdministrador().getId();
            }
            
            try {
                UsuarioCriteria ucr = new UsuarioCriteria();
                Criterion cr = Restrictions.eq("id", adminId);
                Criterion cr2 = Restrictions.eq("id", profId);
                LogicalExpression dis = Restrictions.or(cr, cr2);
                ucr.add(dis);
                user = orm.UsuarioDAO.loadUsuarioByCriteria(ucr);
                if (user.getId() != 0) {
                    if (password.trim().toLowerCase().equals(user.getPassword().trim().toLowerCase())) {
                        valido = true;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/
        return valido;
    }
}
