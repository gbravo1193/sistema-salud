package capaNegocio;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.orm.PersistentException;
import org.orm.PersistentTransaction;

public class Consulta_hce {
    
    private int id;
    private int personaid_profesional;
    private int personaid_paciente;
    private String fecha;
    private Persona persona_profesional;
    private Persona persona_paciente;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPersonaid_profesional() {
        return personaid_profesional;
    }

    public void setPersonaid_profesional(int personaid_profesional) {
        this.personaid_profesional = personaid_profesional;
    }

    public int getPersonaid_paciente() {
        return personaid_paciente;
    }

    public void setPersonaid_paciente(int personaid_paciente) {
        this.personaid_paciente = personaid_paciente;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Persona getPersona_profesional() {
        return persona_profesional;
    }

    public void setPersona_profesional(Persona persona_profesional) {
        this.persona_profesional = persona_profesional;
    }

    public Persona getPersona_paciente() {
        return persona_paciente;
    }

    public void setPersona_paciente(Persona persona_paciente) {
        this.persona_paciente = persona_paciente;
    }
    
    /**
     * Agrega una consulta HCE
     * 
     * @param rutPacientePersona
     * @param RutProfesionalPersona
     * @return boolean
     * @throws PersistentException 
     */
    public boolean agregarConsulta_hce(String rutPacientePersona,  String RutProfesionalPersona) throws PersistentException {
        PersistentTransaction t = orm.SaludPersistentManager.instance().getSession().beginTransaction();
        boolean a = false;
        try {

            orm.Persona lormPacientePersona = orm.PersonaDAO.loadPersonaByQuery("Persona.rut = '" + rutPacientePersona + "'", null);
            orm.Persona lormProfesionalPersona = orm.PersonaDAO.loadPersonaByQuery("Persona.rut= '" + RutProfesionalPersona +"'", null);

            //Calcula fecha Actual
            Calendar c = Calendar.getInstance();
            String dia = Integer.toString(c.get(Calendar.DATE));
            String mes = Integer.toString(c.get(Calendar.MONTH));
            String anio = Integer.toString(c.get(Calendar.YEAR));
            String fechaBusqueda = dia + "-" + mes + "-" + anio;
            Fecha fechaConvertir = new Fecha();
            Date RegistroFecha = fechaConvertir.convertStingToDate(fechaBusqueda);

            orm.Consulta_hce lormConsulta_hce = orm.Consulta_hceDAO.createConsulta_hce();

            lormConsulta_hce.setFecha(RegistroFecha);
            lormConsulta_hce.setPersonaid_profesional(lormProfesionalPersona);
            lormConsulta_hce.setPersonaid_paciente(lormPacientePersona);

            orm.Consulta_hceDAO.save(lormConsulta_hce);
            orm.Consulta_hceDAO.refresh(lormConsulta_hce);
            t.commit();
            if(!lormConsulta_hce.getPersonaid_profesional().getRut().equalsIgnoreCase("")){
                a = true;
            }

        } catch (Exception e) {
            t.rollback();
        }
        return a;

    }

    /**
     * Ver busqueda por profesional
     * 
     * @param rutProfesional
     * @return List
     * @throws PersistentException 
     */
    public List<Consulta_hce> verBusquedaPorProfesional(String rutProfesional) throws PersistentException {
        //Lista a devolver
        List<Consulta_hce> ListaConsulta_hce = new ArrayList<Consulta_hce>();
        orm.Persona personaProfesionalORM = orm.PersonaDAO.loadPersonaByQuery("Persona.rut = '" + rutProfesional + "'", null);

        if (personaProfesionalORM != null) {

            List<orm.Consulta_hce> listaConsultaOrm = new ArrayList<orm.Consulta_hce>();
            listaConsultaOrm = orm.Consulta_hceDAO.queryConsulta_hce("personaid_profesional = " + personaProfesionalORM.getId(), null);

            for (int i = 0; i < listaConsultaOrm.size(); i++) {
                //Carga datos del profesional que busca
                Persona personaProfesional = new Persona();
                personaProfesional.setNombres(personaProfesionalORM.getNombres());
                personaProfesional.setApellido_paterno(personaProfesionalORM.getApellido_paterno());
                personaProfesional.setRut(personaProfesional.getRut());

                //Carga datos de la persona buscada
                Persona personaPaciente = new Persona();
                personaPaciente.setNombres(listaConsultaOrm.get(i).getPersonaid_paciente().getNombres());
                personaPaciente.setApellido_paterno(listaConsultaOrm.get(i).getPersonaid_paciente().getApellido_paterno());
                personaPaciente.setRut(listaConsultaOrm.get(i).getPersonaid_paciente().getRut());

                Consulta_hce consulta_hce = new Consulta_hce();
                consulta_hce.setPersona_paciente(personaPaciente);
                consulta_hce.setPersona_profesional(personaProfesional);
                consulta_hce.setFecha(listaConsultaOrm.get(i).getFecha().toString());

                ListaConsulta_hce.add(consulta_hce);
            }

        }
        return ListaConsulta_hce;
    }

    /**
     * Ver busqueda por rut de Paciente
     * 
     * @param rutPaciente
     * @return List
     * @throws PersistentException 
     */
    public List<Consulta_hce> verBusquedaporPaciente(String rutPaciente) throws PersistentException {
        List<Consulta_hce> ListaConsulta_hce = new ArrayList<Consulta_hce>();
        orm.Persona personaPacienteORM = orm.PersonaDAO.loadPersonaByQuery("Persona.rut = '" + rutPaciente + "'", null);

        if (personaPacienteORM != null) {

            List<orm.Consulta_hce> listaConsultaOrm = new ArrayList<orm.Consulta_hce>();
            listaConsultaOrm = orm.Consulta_hceDAO.queryConsulta_hce("personaid_paciente = " + personaPacienteORM.getId(), null);

            for (int i = 0; i < listaConsultaOrm.size(); i++) {
                //Carga datos del profesional que busca
                Persona personaPaciente = new Persona();
                personaPaciente.setNombres(personaPacienteORM.getNombres());
                personaPaciente.setApellido_paterno(personaPacienteORM.getApellido_paterno());
                personaPaciente.setRut(personaPaciente.getRut());

                //Carga datos de la persona buscada
                Persona personaProfesional = new Persona();
                personaProfesional.setNombres(listaConsultaOrm.get(i).getPersonaid_profesional().getNombres());
                personaProfesional.setApellido_paterno(listaConsultaOrm.get(i).getPersonaid_profesional().getApellido_paterno());
                personaPaciente.setRut(listaConsultaOrm.get(i).getPersonaid_profesional().getRut());

                Consulta_hce consulta_hce = new Consulta_hce();
                consulta_hce.setPersona_paciente(personaPaciente);
                    consulta_hce.setPersona_profesional(personaProfesional);
                consulta_hce.setFecha(listaConsultaOrm.get(i).getFecha().toString());

                ListaConsulta_hce.add(consulta_hce);
            }

        }
        return ListaConsulta_hce;
    }
}
