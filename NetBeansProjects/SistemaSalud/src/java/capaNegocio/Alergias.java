package capaNegocio;

import orm.*;
import java.io.Serializable;
import org.orm.PersistentException;
import org.orm.PersistentTransaction;

public class Alergias implements Serializable {

    public Alergias() {
    }

    private int id;

    private String detalle;

    private Rce rce;

    public Alergias(String detalle) {
        this.detalle = detalle;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public Rce getRce() {
        return rce;
    }

    public void setRce(Rce rce) {
        this.rce = rce;
    }

    public String toString() {
        return String.valueOf(getId());
    }
    
    /**
     * Crear alergia en la base de datos.
     * @param alergia - Alergia - Alergia a ingresar
     * @return  String - salida del ingreso
     */
    public String crearAlergia(Alergias alergia) {
        String salida = "";
        PersistentTransaction t = null;
        try {
            t = SaludPersistentManager.instance().getSession().beginTransaction();
            orm.Alergias alg = orm.AlergiasDAO.createAlergias();
            alg = alergia.toORMAlergia(alergia);
            orm.AlergiasDAO.save(alg);
            t.commit();
            salida = "Ingreso Exitoso!";
        } catch (Exception e) {
            try {
                t.rollback();
            } catch (PersistentException e1) {
                salida = "ERROR Volviendo todo atras.";
            }
            salida = "ERROR Creando contacto";
            e.printStackTrace();
        }
        return salida;
    }
    /**
     * Transforma el tipo capaNegocio.Alergias a el tipo orm.Alergias
     * Esto es necesario para el manejo y envio de datos sin utilizar la capa de persistencia
     * para las transacciones.
     * @param alergias  - capaNegocio.Aletgias - Alergias a transformar
     * @return orm.Alergias - Salida de la transformacion
     */
    public orm.Alergias toORMAlergia(Alergias alergias){
        orm.Alergias salida= orm.AlergiasDAO.createAlergias();
        salida.setId(alergias.getId());
        salida.setDetalle(alergias.getDetalle());
        salida.setRce(alergias.getRce().toORMRce(alergias.getRce()));
        return salida;
    }    
}