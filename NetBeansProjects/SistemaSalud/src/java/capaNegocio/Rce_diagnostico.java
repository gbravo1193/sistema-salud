package capaNegocio;

import orm.*;
import java.io.Serializable;
import org.orm.PersistentException;
import org.orm.PersistentTransaction;

public class Rce_diagnostico implements Serializable {

    private int id;
    private Rce rce;
    private Diagnostico diagnostico;
    
    public Rce_diagnostico() {
    }

    public Rce_diagnostico(Diagnostico diagnostico) {
        this.diagnostico = diagnostico;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public capaNegocio.Rce getRce() {
        return rce;
    }

    public void setRce(capaNegocio.Rce rce) {
        this.rce = rce;
    }

    public Diagnostico getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(Diagnostico diagnostico) {
        this.diagnostico = diagnostico;
    }

    public String toString() {
        return String.valueOf(getId());
    }

    /**
     * Transforma el tipo capaNegocio.Rce_diagnostico a el tipo
     * orm.Rce_diagnostico Esto es necesario para el manejo y envio de datos sin
     * utilizar la capa de persistencia para las transacciones.
     *
     * @param rce_diagnostico - capaNegocio.Rce_diagnostico - Rce_diagnostico a
     * transformar
     * @return orm.Rce_diagnostico - Salida de la transformacion
     */
    public orm.Rce_diagnostico toORMRce_diagnostico(Rce_diagnostico rce_diagnostico) {
        orm.Rce_diagnostico salida = orm.Rce_diagnosticoDAO.createRce_diagnostico();
        salida.setDiagnostico(rce_diagnostico.getDiagnostico().toORMDiagnostico(rce_diagnostico.getDiagnostico()));
        salida.setRce(rce_diagnostico.getRce().toORMRce(rce_diagnostico.getRce()));
        return salida;
    }

    /**
     * Crear un diagnostico en la base de datos
     *
     * @param rceDiag - Rce_diagnostico - Objeto Rce_diagnostico a ingresar en
     * la base de datos
     * @return String - Salida del ingreso
     */
    public String CrearRce_diagnostico(Rce_diagnostico rceDiag) {
        String salida = "";
        PersistentTransaction t = null;
        try {
            t = SaludPersistentManager.instance().getSession().beginTransaction();
            orm.Rce_diagnostico rced = orm.Rce_diagnosticoDAO.createRce_diagnostico();
            rced = rceDiag.toORMRce_diagnostico(rceDiag);
            orm.Rce_diagnosticoDAO.save(rced);
            t.commit();
            salida = "Ingreso Exitoso!";
        } catch (Exception e) {
            try {
                t.rollback();
            } catch (PersistentException e1) {
                salida = "ERROR Volviendo todo atras.";
            }
            salida = "ERROR Creando contacto";
            e.printStackTrace();
        }
        return salida;
    }
}
