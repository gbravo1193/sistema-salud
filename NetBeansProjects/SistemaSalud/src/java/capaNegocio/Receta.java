package capaNegocio;

import orm.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.orm.PersistentException;
import org.orm.PersistentTransaction;
import vo.RecetaVo;

public class Receta implements Serializable {

    private int id;
    private String detalle;
    private Paciente paciente;
    private Recetarce recetarce;

    public Receta() {
    }

    public Receta(String detalle) {
        this.detalle = detalle;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public capaNegocio.Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(capaNegocio.Paciente paciente) {
        this.paciente = paciente;
    }

    public capaNegocio.Recetarce getRecetarce() {
        return recetarce;
    }

    public void setRecetarce(capaNegocio.Recetarce recetarce) {
        this.recetarce = recetarce;
    }

    public String toString() {
        return String.valueOf(getId());
    }

    /**
     * Transforma el tipo capaNegocio.Receta a el tipo orm.Receta Esto es
     * necesario para el manejo y envio de datos sin utilizar la capa de
     * persistencia para las transacciones.
     *
     * @param receta - capaNegocio.Receta - Receta a transformar
     * @return orm.Receta - Salida de la transformacion
     */
    public orm.Receta toORMReceta(Receta receta) {
        orm.Receta salida = orm.RecetaDAO.createReceta();
        salida.setId(receta.getId());
        salida.setDetalle(receta.getDetalle());
        salida.setPaciente(receta.getPaciente().toORMPaciente(receta.getPaciente()));
        salida.setRecetarce(receta.getRecetarce().toORMRecetarse(receta.getRecetarce()));
        return salida;
    }

    /**
     * Transforma el tipo capaNegocio.Receta a el tipo orm.Receta Esto es
     * necesario para el manejo y envio de datos sin utilizar la capa de
     * persistencia para las transacciones.
     *
     * @param receta - capaNegocio.Receta - Receta a transformar
     * @return orm.Receta - Salida de la transformacion
     */
    public Receta toCNReceta(orm.Receta receta) {
        Receta salida = new Receta();
        salida.setId(receta.getId());
        salida.setDetalle(receta.getDetalle());
        Paciente p = new Paciente();
        salida.setPaciente(p.toCNPaciente(receta.getPaciente()));
        Recetarce r = new Recetarce();
        salida.setRecetarce(r.toCNRecetarse(receta.getRecetarce()));
        return salida;
    }

    /**
     * Transforma un arreglo orm.Receta a un arreglo del tipo capaNegocio.Receta
     * Esto es necesario para el manejo y envio de datos sin utilizar la capa de
     * persistencia para las transacciones.
     *
     * @param receta - orm.Receta[] - Receta a transformar
     * @return capaNegocio.Receta[] - Salida de la transformacion
     */
    public Receta[] toCNRecetaArray(orm.Receta[] receta) {
        Receta[] salida = new Receta[receta.length];
        for (int i = 0; i < receta.length; i++) {
            Receta r = new Receta();
            salida[i] = r.toCNReceta(receta[i]);
        }
        return salida;
    }

    /**
     * Crea un receta en la base de datos
     *
     * @param receta - Receta - Receta para ser ingresado en la base de datos
     * @return String out - Resultado del ingreso
     * @throws PersistentException
     */
    public String agregarReceta(Receta receta) {
        PersistentTransaction t = null;
        String out = "";
        try {
            t = SaludPersistentManager.instance().getSession().beginTransaction();
            orm.Receta rec = orm.RecetaDAO.createReceta();
            rec = receta.toORMReceta(receta);
            orm.RecetaDAO.save(rec);
            t.commit();
            out = "Ingreso Exitoso!";
        } catch (Exception e) {
            try {
                t.rollback();
            } catch (PersistentException e1) {
                out = "ERROR Volviendo todo atras.";
            }
            out = "ERROR Creando Receta";
            e.printStackTrace();
        }
        return out;
    }

    /**
     * Obtiene la lista de recetas de la base de datos.
     *
     * @return List<Receta> - Lista de recetas en forma de List
     */
    public List<Receta> listarRecetas(int id) {
        orm.Receta[] aux = null;
        Receta[] out = null;
        List<Receta> salida = null;
        try {
            RecetaCriteria receta = new RecetaCriteria();
            Criterion c = Restrictions.eq("pacienteid", id);
            aux = orm.RecetaDAO.listRecetaByCriteria(receta);
            out = toCNRecetaArray(aux);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            salida = Arrays.asList(out);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return salida;
    }

    /**
     * Elimina un receta de la base de datos
     *
     * @param id - int - id de la receta a eliminar
     * @return salida - String - Mensaje con el resultado de la eliminacion
     */
    public String eliminarReceta(int id) {
        PersistentTransaction t = null;
        String out = "";
        try {
            t = SaludPersistentManager.instance().getSession().beginTransaction();
            orm.Receta receta = orm.RecetaDAO.loadRecetaByQuery("id=" + id, null);
            orm.RecetaDAO.delete(receta);
            t.commit();
            out = "Receta Eliminado!";
        } catch (Exception e) {
            try {
                t.rollback();
            } catch (PersistentException e1) {
                out = "ERROR Volviendo todo atras.";
                e1.printStackTrace();
            }
            out = "ERROR Eliminando al Receta";
            e.printStackTrace();
        }
        return out;
    }

    /**
     * Obtiene una lista de recetas por el rut del paciente
     *
     * @param rut - String - Rut del paciente para realizar la busqueda
     * @return List<Receta> - Lista de Recetas.
     */
    public List<Receta> listarRecetaPorRut(String rut) {
        orm.Receta[] aux = null;
        Receta[] out = null;
        List<Receta> salida = null;
        try {
            Persona p = new Persona();
            p = p.buscarPersonaPorRut(rut);
            RecetaCriteria rcr = new RecetaCriteria();
            Criterion c = Restrictions.eq("pacienteid", p.getPaciente().getId());
            rcr.add(c);
            aux = orm.RecetaDAO.listRecetaByCriteria(rcr);
            out = toCNRecetaArray(aux);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            salida = Arrays.asList(out);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return salida;
    }

    public String agregarReceta(int pacienteid, int rceid, String detalle) throws PersistentException {

        PersistentTransaction t = orm.SaludPersistentManager.instance().getSession().beginTransaction();

        try {
            orm.Rce rce;

            rce = orm.RceDAO.getRceByORMID(rceid);
            orm.Paciente paciente = orm.PacienteDAO.getPacienteByORMID(pacienteid);
            // orm.Recetarce recetaRCE=orm.RecetarceDAO.getRecetarceByORMID(rce.getId());
            System.out.println("VALOR ID:" + rce.getId());

            if (rce != null) {

                orm.Receta nuevaReceta = orm.RecetaDAO.createReceta();
                //NUEVA RECETA
                nuevaReceta.setPaciente(paciente);
                nuevaReceta.setDetalle(detalle);
                nuevaReceta.setORM_Paciente(paciente);

                orm.Recetarce recetaRCE = orm.RecetarceDAO.createRecetarce();
                recetaRCE.setORM_Rce(rce);
                recetaRCE.setRce(rce);

                nuevaReceta.setRecetarce(recetaRCE);
                orm.RecetaDAO.save(nuevaReceta);

                // RECETA_RCE
                recetaRCE.setORM_Rce(rce);
                recetaRCE.setRce(rce);
                recetaRCE.setReceta(nuevaReceta);
                orm.RecetaDAO.save(nuevaReceta);
                t.commit();
                orm.RecetaDAO.refresh(nuevaReceta);

                orm.SaludPersistentManager.instance().disposePersistentManager();

                return "OK: AGREGADA RECETA...";

            }

        } catch (PersistentException ex) {
            Logger.getLogger(Receta.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "ERROR:AGREGANDO RECETA...";
    }

    //-------------------------------------------------------------------------
    /**
     *
     * @param paciente_id
     * @return
     * @throws PersistentException
     */
    public List<RecetaVo> listarReceta(int paciente_id) throws PersistentException {

        List<RecetaVo> Receta = new ArrayList<RecetaVo>();
        orm.Receta[] ormReceta = null;
        ormReceta = orm.RecetaDAO.listRecetaByQuery("pacienteid=" + paciente_id, null);

        if (ormReceta != null) {

            for (int i = 0; i < ormReceta.length; i++) {

                Receta.add(new RecetaVo(ormReceta[i].getId(),
                        ormReceta[i].getPaciente().getId(),
                        ormReceta[i].getDetalle()));

                System.out.println(":" + Receta.get(i).getDetalle());

            }

            int i = 0;
            System.out.println(ormReceta[i].getId());

            for (orm.Receta Receta_lista : ormReceta) {
                System.out.println(Receta_lista.getDetalle());
            }

            return Receta;
        }

        return null;

    }

    /**
     *
     * @param rut
     * @return
     * @throws PersistentException
     */
    //metodo listarreceta utilizando RUT del paciente
    public String listarRecetaByRut(String rut) throws PersistentException {
        orm.Persona persona = PersonaDAO.loadPersonaByQuery("rut='" + rut + "'", null);

        int idpac = persona.getId();

        orm.Paciente pac = PacienteDAO.loadPacienteByQuery("persona_id=" + idpac, null);

        idpac = pac.getId();

        orm.Receta r = RecetaDAO.loadRecetaByQuery("pacienteid=" + idpac, null);

        String ret = r.getId() + "\n" + r.getDetalle() + "\n" + r.getPaciente() + "\n" + r.getRecetarce();

        return ret;
    }

    /**
     *
     * @param pacienteid
     * @param rceid
     * @param recetaid
     * @return
     * @throws PersistentException
     */
    public String EliminarReceta(int pacienteid, int rceid, int recetaid) throws PersistentException {

        PersistentTransaction t = orm.SaludPersistentManager.instance().getSession().beginTransaction();

        if (rceid != 0 && pacienteid != 0 && recetaid != 0) {
            //PRIMERO ELIMINAR LAS REFERENCIAS A LA TABLA  A BORRAR...

            int receta_id = recetaid;
            String cBusqueda = "";
            cBusqueda += "recetaid=";
            cBusqueda += receta_id;

            //NOTA:  ORDENA LOS PARAMETROS PARA LA CONSULTA SQL....
            // ELIMINAR LINEAS COMENTADAS NO UTILIZADAS...
            orm.Recetarce recetaRCE = orm.RecetarceDAO.loadRecetarceByQuery(cBusqueda, null);

            //DEBUG CONSOLA
            System.out.println("ID-RECETARCE" + recetaRCE.getId());

            orm.RecetarceDAO.delete(recetaRCE);
            orm.Receta r = orm.RecetaDAO.getRecetaByORMID(recetaid);
            orm.RecetaDAO.delete(r);

            t.commit();
            orm.RecetaDAO.refresh(r);
            orm.SaludPersistentManager.instance().disposePersistentManager();

            return "OK:ELIMINAR-RECETA...";

        }

        return "FALSE-RVP";
    }
}


