package capaNegocio;

import java.io.Serializable;
import org.orm.PersistentException;
import org.orm.PersistentTransaction;
import vo.TipoPrevisionVo;

public class Tipo_prevision implements Serializable {

    private int id;
    private String descripcion;

    public Tipo_prevision() {
    }
    public Tipo_prevision(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    
    
    @Override
    public String toString() {
        return String.valueOf(getId());
    }
    
    /**
     * Agrega un tipo de prevision a la BD
     * 
     * @param tpDescripcion String
     * @return String
     * @throws PersistentException 
     */
    public boolean agregarTipoPrevision(String tpDescripcion)throws PersistentException {
        
        PersistentTransaction t = orm.SaludPersistentManager.instance().getSession().beginTransaction();        
        try{
            orm.Tipo_prevision lormTipo_prevision = orm.Tipo_previsionDAO.createTipo_prevision();
            lormTipo_prevision.setDescripcion(tpDescripcion);
            orm.Tipo_previsionDAO.save(lormTipo_prevision);
            t.commit();
            return true;
        }catch(PersistentException e){
            return false;
        }
    }
    
    //METODO CON VO - Ismael G.
    public TipoPrevisionVo ingresarTipoPrevision(String desc) {

        try {
            orm.Tipo_prevision tprev = new orm.Tipo_prevision();
            //Setea la descripcion del tipo de previsión
            tprev.setDescripcion(desc);
            //Si logra persistir
            if (orm.Tipo_previsionDAO.save(tprev)) {
                orm.Tipo_previsionDAO.refresh(tprev);
                TipoPrevisionVo previ = TipoPrevisionVo.fromORM(tprev);
                return previ;
            }
        } catch (PersistentException e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }
    
    /**
     * Transforma el tipo orm.Tipo_prevision a el tipo capaNegocio.Tipo_prevision
     * Esto es necesario para el manejo y envio de datos sin utilizar la capa de persistencia
     * para las transacciones.
     * @param tipo_prevision - orm.Tipo_prevision - Tipo_prevision a transformar
     * @return capaNegocio.Tipo_prevision- Salida de la transformacion
     */
    public Tipo_prevision toCNTipo_prevision(orm.Tipo_prevision tipo_prevision){
        Tipo_prevision salida= new Tipo_prevision();
        salida.setDescripcion(tipo_prevision.getDescripcion());
        salida.setId(tipo_prevision.getId());
        return salida;
    }
}
