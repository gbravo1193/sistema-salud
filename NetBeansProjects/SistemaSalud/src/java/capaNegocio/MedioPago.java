package capaNegocio;

import org.orm.PersistentException;
import vo.MedioPagoVo;

public class MedioPago {
    
    public MedioPagoVo ingresarMedioPago(String desc) {
        try {
            // Intenta setear todos los atributos heredados del orm Persona

            orm.Medio_pago mpago = new orm.Medio_pago();
            mpago.setDescripcion(desc);

            if (orm.Medio_pagoDAO.save(mpago)) {
                // Si la persona se logra persistir, se asigna como M�dico
                orm.Medio_pagoDAO.refresh(mpago);
                MedioPagoVo mpvo = MedioPagoVo.fromORM(mpago);
                return mpvo;
            }
        } catch (PersistentException e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }
}

