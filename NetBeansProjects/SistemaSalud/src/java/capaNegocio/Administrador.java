package capaNegocio;

import java.io.Serializable;
import org.orm.PersistentException;
import org.orm.PersistentTransaction;

public class Administrador implements Serializable {

    private int id;
    private int activo;
    private int persona_id;
    
    public Administrador() {
    
    }
    
    public Administrador(int activo) {
        this.activo = activo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }
    
    public int getPersona_id() {
        return id;
    }

    public void setPersona_id(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return String.valueOf(getId());
    }
    /**
     * Transforma el tipo orm.Administrador a el tipo capaNegocio.Administrador
     * Esto es necesario para el manejo y envio de datos sin utilizar la capa de persistencia
     * para las transacciones.
     * @param administrador
     * @return capaNegocio.Administrador - Salida de la transformacion
     */
    public Administrador toCNAdministrador(orm.Administrador administrador) {
        Administrador salida= new Administrador();
        salida.setId(administrador.getId());
        salida.setActivo(administrador.getActivo());
        salida.setPersona_id(administrador.getPersona().getId());
        return salida;
    }
    
    /**
     * Transforma el tipo capaNegocio.Administrador a el tipo orm.Administrador
     * Esto es necesario para el manejo y envio de datos sin utilizar la capa de persistencia
     * para las transacciones.
     * @param administrador - capaNegocio.Administrador - Destino a transformar
     * @return orm.Administrador - Salida de la transformacion
     */
    public orm.Administrador toORMAdministrador(Administrador administrador){
        orm.Administrador salida = orm.AdministradorDAO.createAdministrador();
        salida.setId(administrador.getId());
        salida.setActivo(administrador.getActivo());
        return salida;
    }
    
    /**
     * Agrega un administrador a traves de un objeto Administrador
     * 
     * @param administrador
     * @return String
     * @throws PersistentException 
     */
    public Administrador agregarAdministrador(Administrador administrador)throws PersistentException{
        PersistentTransaction t= orm.SaludPersistentManager.instance().getSession().beginTransaction();
        Administrador admin = new Administrador();
        try{              
            orm.Persona lormPersona= orm.PersonaDAO.loadPersonaByORMID(administrador.getPersona_id());
            orm.Administrador lormAdministrador= orm.AdministradorDAO.createAdministrador();
            
            lormAdministrador.setActivo(administrador.getActivo());
            lormAdministrador.setPersona(lormPersona);
            
            orm.AdministradorDAO.save(lormAdministrador);      
            t.commit();            
            return admin.toCNAdministrador(lormAdministrador);
        }
        catch(Exception e){
            t.rollback();
            return null;
        }
    }
    
    /**
     * Busca un administrador
     * 
     * @param id
     * @return Adminstrador
     * @throws PersistentException 
     */
    public Administrador buscarAdministrador(int id)throws PersistentException{
        
        Administrador administrador= new Administrador();
       
        try{
            administrador.setId(id);
            orm.Administrador lormAdministrador= orm.AdministradorDAO.loadAdministradorByORMID(administrador.getId());
            
            if(lormAdministrador.getId()!=0){
                administrador = administrador.toCNAdministrador(lormAdministrador);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return administrador;
    }
    
    /**
     * Elimina un administrador a traves del ID
     * 
     * @param id
     * @return String
     * @throws PersistentException 
     */
    public String eliminarAdministrador(int id) throws PersistentException { 
        PersistentTransaction t = orm.SaludPersistentManager.instance().getSession().beginTransaction();
        
        try {
            orm.Administrador lormAdministrador = orm.AdministradorDAO.loadAdministradorByORMID(id);
            orm.AdministradorDAO.delete(lormAdministrador);
            t.commit();
            return "Registro Eliminado";
        }
        catch (Exception e) {
                t.rollback();
                return "Error";
        }
    }
}
