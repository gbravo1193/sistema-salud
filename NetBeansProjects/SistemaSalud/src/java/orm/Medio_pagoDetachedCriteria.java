/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Medio_pagoDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final StringExpression descripcion;
	public final CollectionExpression pago;
	
	public Medio_pagoDetachedCriteria() {
		super(orm.Medio_pago.class, orm.Medio_pagoCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		descripcion = new StringExpression("descripcion", this.getDetachedCriteria());
		pago = new CollectionExpression("ORM_Pago", this.getDetachedCriteria());
	}
	
	public Medio_pagoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.Medio_pagoCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		descripcion = new StringExpression("descripcion", this.getDetachedCriteria());
		pago = new CollectionExpression("ORM_Pago", this.getDetachedCriteria());
	}
	
	public PagoDetachedCriteria createPagoCriteria() {
		return new PagoDetachedCriteria(createCriteria("ORM_Pago"));
	}
	
	public Medio_pago uniqueMedio_pago(PersistentSession session) {
		return (Medio_pago) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Medio_pago[] listMedio_pago(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Medio_pago[]) list.toArray(new Medio_pago[list.size()]);
	}
}

