/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Pago {
	public Pago() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_PAGO_BONO) {
			return ORM_bono;
		}
		
		return null;
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_PAGO_MEDIO_PAGO) {
			this.medio_pago = (orm.Medio_pago) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_PAGO_RCE_EXAMEN) {
			this.rce_examen = (orm.Rce) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private orm.Medio_pago medio_pago;
	
	private orm.Rce rce_examen;
	
	private double monto;
	
	private Double comprobante;
	
	private Integer estadopago = new Integer(0);
	
	private Float subsidio;
	
	private java.util.Set ORM_bono = new java.util.HashSet();
	
	/**
	 * Clave primaria
	 */
	public void setId(int value) {
		this.id = value;
	}
	
	/**
	 * Clave primaria
	 */
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	/**
	 * Monto
	 */
	public void setMonto(double value) {
		this.monto = value;
	}
	
	/**
	 * Monto
	 */
	public double getMonto() {
		return monto;
	}
	
	/**
	 * Comprobante
	 */
	public void setComprobante(double value) {
		setComprobante(new Double(value));
	}
	
	/**
	 * Comprobante
	 */
	public void setComprobante(Double value) {
		this.comprobante = value;
	}
	
	/**
	 * Comprobante
	 */
	public Double getComprobante() {
		return comprobante;
	}
	
	/**
	 * Estado de Pago 0:Pendiente y 1:Pagado
	 */
	public void setEstadopago(int value) {
		setEstadopago(new Integer(value));
	}
	
	/**
	 * Estado de Pago 0:Pendiente y 1:Pagado
	 */
	public void setEstadopago(Integer value) {
		this.estadopago = value;
	}
	
	/**
	 * Estado de Pago 0:Pendiente y 1:Pagado
	 */
	public Integer getEstadopago() {
		return estadopago;
	}
	
	/**
	 * Subsidio Pago
	 */
	public void setSubsidio(float value) {
		setSubsidio(new Float(value));
	}
	
	/**
	 * Subsidio Pago
	 */
	public void setSubsidio(Float value) {
		this.subsidio = value;
	}
	
	/**
	 * Subsidio Pago
	 */
	public Float getSubsidio() {
		return subsidio;
	}
	
	public void setMedio_pago(orm.Medio_pago value) {
		if (medio_pago != null) {
			medio_pago.pago.remove(this);
		}
		if (value != null) {
			value.pago.add(this);
		}
	}
	
	public orm.Medio_pago getMedio_pago() {
		return medio_pago;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Medio_pago(orm.Medio_pago value) {
		this.medio_pago = value;
	}
	
	private orm.Medio_pago getORM_Medio_pago() {
		return medio_pago;
	}
	
	public void setRce_examen(orm.Rce value) {
		if (rce_examen != null) {
			rce_examen.pago.remove(this);
		}
		if (value != null) {
			value.pago.add(this);
		}
	}
	
	public orm.Rce getRce_examen() {
		return rce_examen;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Rce_examen(orm.Rce value) {
		this.rce_examen = value;
	}
	
	private orm.Rce getORM_Rce_examen() {
		return rce_examen;
	}
	
	private void setORM_Bono(java.util.Set value) {
		this.ORM_bono = value;
	}
	
	private java.util.Set getORM_Bono() {
		return ORM_bono;
	}
	
	public final orm.BonoSetCollection bono = new orm.BonoSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_PAGO_BONO, orm.ORMConstants.KEY_BONO_PAGO, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
