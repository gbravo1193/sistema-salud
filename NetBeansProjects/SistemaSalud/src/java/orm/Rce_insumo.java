/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Rce_insumo {
	public Rce_insumo() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_RCE_INSUMO_RCE) {
			this.rce = (orm.Rce) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_RCE_INSUMO_INSUMO) {
			this.insumo = (orm.Insumo) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private orm.Rce rce;
	
	private orm.Insumo insumo;
	
	private float cantidad;
	
	/**
	 * Clave Primaria
	 */
	public void setId(int value) {
		this.id = value;
	}
	
	/**
	 * Clave Primaria
	 */
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	/**
	 * Cantidad
	 */
	public void setCantidad(float value) {
		this.cantidad = value;
	}
	
	/**
	 * Cantidad
	 */
	public float getCantidad() {
		return cantidad;
	}
	
	public void setRce(orm.Rce value) {
		if (rce != null) {
			rce.rce_insumo.remove(this);
		}
		if (value != null) {
			value.rce_insumo.add(this);
		}
	}
	
	public orm.Rce getRce() {
		return rce;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Rce(orm.Rce value) {
		this.rce = value;
	}
	
	private orm.Rce getORM_Rce() {
		return rce;
	}
	
	public void setInsumo(orm.Insumo value) {
		if (insumo != null) {
			insumo.rce_insumo.remove(this);
		}
		if (value != null) {
			value.rce_insumo.add(this);
		}
	}
	
	public orm.Insumo getInsumo() {
		return insumo;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Insumo(orm.Insumo value) {
		this.insumo = value;
	}
	
	private orm.Insumo getORM_Insumo() {
		return insumo;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
