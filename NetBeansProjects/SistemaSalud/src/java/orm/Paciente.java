/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Paciente {
	public Paciente() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_PACIENTE_RESERVA) {
			return ORM_reserva;
		}
		else if (key == orm.ORMConstants.KEY_PACIENTE_FICHAPACIENTE) {
			return ORM_fichapaciente;
		}
		else if (key == orm.ORMConstants.KEY_PACIENTE_RECETA) {
			return ORM_receta;
		}
		
		return null;
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_PACIENTE_PERSONA) {
			this.persona = (orm.Persona) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_PACIENTE_TIPO_PREVISION) {
			this.tipo_prevision = (orm.Tipo_prevision) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private orm.Persona persona;
	
	private String descripcion;
	
	private orm.Tipo_prevision tipo_prevision;
	
	private Integer activo;
	
	private java.util.Set ORM_reserva = new java.util.HashSet();
	
	private java.util.Set ORM_fichapaciente = new java.util.HashSet();
	
	private java.util.Set ORM_receta = new java.util.HashSet();
	
	public void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setDescripcion(String value) {
		this.descripcion = value;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	/**
	 * Indica si esta activo o no
	 */
	public void setActivo(int value) {
		setActivo(new Integer(value));
	}
	
	/**
	 * Indica si esta activo o no
	 */
	public void setActivo(Integer value) {
		this.activo = value;
	}
	
	/**
	 * Indica si esta activo o no
	 */
	public Integer getActivo() {
		return activo;
	}
	
	public void setPersona(orm.Persona value) {
		if (this.persona != value) {
			orm.Persona lpersona = this.persona;
			this.persona = value;
			if (value != null) {
				persona.setPaciente(this);
			}
			if (lpersona != null && lpersona.getPaciente() == this) {
				lpersona.setPaciente(null);
			}
		}
	}
	
	public orm.Persona getPersona() {
		return persona;
	}
	
	public void setTipo_prevision(orm.Tipo_prevision value) {
		if (tipo_prevision != null) {
			tipo_prevision.paciente.remove(this);
		}
		if (value != null) {
			value.paciente.add(this);
		}
	}
	
	public orm.Tipo_prevision getTipo_prevision() {
		return tipo_prevision;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Tipo_prevision(orm.Tipo_prevision value) {
		this.tipo_prevision = value;
	}
	
	private orm.Tipo_prevision getORM_Tipo_prevision() {
		return tipo_prevision;
	}
	
	private void setORM_Reserva(java.util.Set value) {
		this.ORM_reserva = value;
	}
	
	private java.util.Set getORM_Reserva() {
		return ORM_reserva;
	}
	
	public final orm.ReservaSetCollection reserva = new orm.ReservaSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_PACIENTE_RESERVA, orm.ORMConstants.KEY_RESERVA_PACIENTE, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Fichapaciente(java.util.Set value) {
		this.ORM_fichapaciente = value;
	}
	
	private java.util.Set getORM_Fichapaciente() {
		return ORM_fichapaciente;
	}
	
	public final orm.FichapacienteSetCollection fichapaciente = new orm.FichapacienteSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_PACIENTE_FICHAPACIENTE, orm.ORMConstants.KEY_FICHAPACIENTE_PACIENTE, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Receta(java.util.Set value) {
		this.ORM_receta = value;
	}
	
	private java.util.Set getORM_Receta() {
		return ORM_receta;
	}
	
	public final orm.RecetaSetCollection receta = new orm.RecetaSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_PACIENTE_RECETA, orm.ORMConstants.KEY_RECETA_PACIENTE, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
