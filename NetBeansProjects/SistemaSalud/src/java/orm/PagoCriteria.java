/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class PagoCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final IntegerExpression medio_pagoId;
	public final AssociationExpression medio_pago;
	public final IntegerExpression rce_examenId;
	public final AssociationExpression rce_examen;
	public final DoubleExpression monto;
	public final DoubleExpression comprobante;
	public final IntegerExpression estadopago;
	public final FloatExpression subsidio;
	public final CollectionExpression bono;
	
	public PagoCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		medio_pagoId = new IntegerExpression("medio_pago.id", this);
		medio_pago = new AssociationExpression("medio_pago", this);
		rce_examenId = new IntegerExpression("rce_examen.id", this);
		rce_examen = new AssociationExpression("rce_examen", this);
		monto = new DoubleExpression("monto", this);
		comprobante = new DoubleExpression("comprobante", this);
		estadopago = new IntegerExpression("estadopago", this);
		subsidio = new FloatExpression("subsidio", this);
		bono = new CollectionExpression("ORM_Bono", this);
	}
	
	public PagoCriteria(PersistentSession session) {
		this(session.createCriteria(Pago.class));
	}
	
	public PagoCriteria() throws PersistentException {
		this(orm.SaludPersistentManager.instance().getSession());
	}
	
	public Medio_pagoCriteria createMedio_pagoCriteria() {
		return new Medio_pagoCriteria(createCriteria("medio_pago"));
	}
	
	public RceCriteria createRce_examenCriteria() {
		return new RceCriteria(createCriteria("rce_examen"));
	}
	
	public BonoCriteria createBonoCriteria() {
		return new BonoCriteria(createCriteria("ORM_Bono"));
	}
	
	public Pago uniquePago() {
		return (Pago) super.uniqueResult();
	}
	
	public Pago[] listPago() {
		java.util.List list = super.list();
		return (Pago[]) list.toArray(new Pago[list.size()]);
	}
}

