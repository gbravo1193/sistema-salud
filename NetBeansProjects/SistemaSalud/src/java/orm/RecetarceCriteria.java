/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class RecetarceCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final IntegerExpression rceId;
	public final AssociationExpression rce;
	public final IntegerExpression recetaId;
	public final AssociationExpression receta;
	
	public RecetarceCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		rceId = new IntegerExpression("rce.id", this);
		rce = new AssociationExpression("rce", this);
		recetaId = new IntegerExpression("receta.id", this);
		receta = new AssociationExpression("receta", this);
	}
	
	public RecetarceCriteria(PersistentSession session) {
		this(session.createCriteria(Recetarce.class));
	}
	
	public RecetarceCriteria() throws PersistentException {
		this(orm.SaludPersistentManager.instance().getSession());
	}
	
	public RceCriteria createRceCriteria() {
		return new RceCriteria(createCriteria("rce"));
	}
	
	public RecetaCriteria createRecetaCriteria() {
		return new RecetaCriteria(createCriteria("receta"));
	}
	
	public Recetarce uniqueRecetarce() {
		return (Recetarce) super.uniqueResult();
	}
	
	public Recetarce[] listRecetarce() {
		java.util.List list = super.list();
		return (Recetarce[]) list.toArray(new Recetarce[list.size()]);
	}
}

