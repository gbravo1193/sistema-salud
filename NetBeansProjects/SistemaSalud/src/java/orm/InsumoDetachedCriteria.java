/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class InsumoDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final StringExpression nombre;
	public final FloatExpression monto_pmp;
	public final StringExpression codigo;
	public final CollectionExpression rce_insumo;
	
	public InsumoDetachedCriteria() {
		super(orm.Insumo.class, orm.InsumoCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		nombre = new StringExpression("nombre", this.getDetachedCriteria());
		monto_pmp = new FloatExpression("monto_pmp", this.getDetachedCriteria());
		codigo = new StringExpression("codigo", this.getDetachedCriteria());
		rce_insumo = new CollectionExpression("ORM_Rce_insumo", this.getDetachedCriteria());
	}
	
	public InsumoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.InsumoCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		nombre = new StringExpression("nombre", this.getDetachedCriteria());
		monto_pmp = new FloatExpression("monto_pmp", this.getDetachedCriteria());
		codigo = new StringExpression("codigo", this.getDetachedCriteria());
		rce_insumo = new CollectionExpression("ORM_Rce_insumo", this.getDetachedCriteria());
	}
	
	public Rce_insumoDetachedCriteria createRce_insumoCriteria() {
		return new Rce_insumoDetachedCriteria(createCriteria("ORM_Rce_insumo"));
	}
	
	public Insumo uniqueInsumo(PersistentSession session) {
		return (Insumo) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Insumo[] listInsumo(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Insumo[]) list.toArray(new Insumo[list.size()]);
	}
}

