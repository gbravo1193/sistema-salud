/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ExamenDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final StringExpression descripcion;
	public final DoubleExpression monto;
	public final StringExpression observaciones;
	public final CollectionExpression hora_examen_solicitado;
	public final CollectionExpression rce_examen_examen;
	
	public ExamenDetachedCriteria() {
		super(orm.Examen.class, orm.ExamenCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		descripcion = new StringExpression("descripcion", this.getDetachedCriteria());
		monto = new DoubleExpression("monto", this.getDetachedCriteria());
		observaciones = new StringExpression("observaciones", this.getDetachedCriteria());
		hora_examen_solicitado = new CollectionExpression("ORM_Hora_examen_solicitado", this.getDetachedCriteria());
		rce_examen_examen = new CollectionExpression("ORM_Rce_examen_examen", this.getDetachedCriteria());
	}
	
	public ExamenDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.ExamenCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		descripcion = new StringExpression("descripcion", this.getDetachedCriteria());
		monto = new DoubleExpression("monto", this.getDetachedCriteria());
		observaciones = new StringExpression("observaciones", this.getDetachedCriteria());
		hora_examen_solicitado = new CollectionExpression("ORM_Hora_examen_solicitado", this.getDetachedCriteria());
		rce_examen_examen = new CollectionExpression("ORM_Rce_examen_examen", this.getDetachedCriteria());
	}
	
	public Hora_examen_solicitadoDetachedCriteria createHora_examen_solicitadoCriteria() {
		return new Hora_examen_solicitadoDetachedCriteria(createCriteria("ORM_Hora_examen_solicitado"));
	}
	
	public Rce_examen_examenDetachedCriteria createRce_examen_examenCriteria() {
		return new Rce_examen_examenDetachedCriteria(createCriteria("ORM_Rce_examen_examen"));
	}
	
	public Examen uniqueExamen(PersistentSession session) {
		return (Examen) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Examen[] listExamen(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Examen[]) list.toArray(new Examen[list.size()]);
	}
}

