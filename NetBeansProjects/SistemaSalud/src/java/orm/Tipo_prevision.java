/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Tipo_prevision {
	public Tipo_prevision() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_TIPO_PREVISION_PACIENTE) {
			return ORM_paciente;
		}
		else if (key == orm.ORMConstants.KEY_TIPO_PREVISION_BONO) {
			return ORM_bono;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private int id;
	
	private String descripcion;
	
	private java.util.Set ORM_paciente = new java.util.HashSet();
	
	private java.util.Set ORM_bono = new java.util.HashSet();
	
	/**
	 * Clave Primaria
	 */
	public void setId(int value) {
		this.id = value;
	}
	
	/**
	 * Clave Primaria
	 */
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	/**
	 * Descripcion de la prevision
	 */
	public void setDescripcion(String value) {
		this.descripcion = value;
	}
	
	/**
	 * Descripcion de la prevision
	 */
	public String getDescripcion() {
		return descripcion;
	}
	
	private void setORM_Paciente(java.util.Set value) {
		this.ORM_paciente = value;
	}
	
	private java.util.Set getORM_Paciente() {
		return ORM_paciente;
	}
	
	public final orm.PacienteSetCollection paciente = new orm.PacienteSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_TIPO_PREVISION_PACIENTE, orm.ORMConstants.KEY_PACIENTE_TIPO_PREVISION, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Bono(java.util.Set value) {
		this.ORM_bono = value;
	}
	
	private java.util.Set getORM_Bono() {
		return ORM_bono;
	}
	
	public final orm.BonoSetCollection bono = new orm.BonoSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_TIPO_PREVISION_BONO, orm.ORMConstants.KEY_BONO_TIPO_PREVISION, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
