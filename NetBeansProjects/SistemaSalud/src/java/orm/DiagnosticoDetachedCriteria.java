/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class DiagnosticoDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final StringExpression codigo;
	public final CollectionExpression rce_diagnostico;
	
	public DiagnosticoDetachedCriteria() {
		super(orm.Diagnostico.class, orm.DiagnosticoCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		codigo = new StringExpression("codigo", this.getDetachedCriteria());
		rce_diagnostico = new CollectionExpression("ORM_Rce_diagnostico", this.getDetachedCriteria());
	}
	
	public DiagnosticoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.DiagnosticoCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		codigo = new StringExpression("codigo", this.getDetachedCriteria());
		rce_diagnostico = new CollectionExpression("ORM_Rce_diagnostico", this.getDetachedCriteria());
	}
	
	public Rce_diagnosticoDetachedCriteria createRce_diagnosticoCriteria() {
		return new Rce_diagnosticoDetachedCriteria(createCriteria("ORM_Rce_diagnostico"));
	}
	
	public Diagnostico uniqueDiagnostico(PersistentSession session) {
		return (Diagnostico) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Diagnostico[] listDiagnostico(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Diagnostico[]) list.toArray(new Diagnostico[list.size()]);
	}
}

