/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Rce_diagnostico {
	public Rce_diagnostico() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_RCE_DIAGNOSTICO_RCE) {
			this.rce = (orm.Rce) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_RCE_DIAGNOSTICO_DIAGNOSTICO) {
			this.diagnostico = (orm.Diagnostico) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private orm.Rce rce;
	
	private orm.Diagnostico diagnostico;
	
	/**
	 * Clave Primaria
	 */
	public void setId(int value) {
		this.id = value;
	}
	
	/**
	 * Clave Primaria
	 */
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setRce(orm.Rce value) {
		if (rce != null) {
			rce.rce_diagnostico.remove(this);
		}
		if (value != null) {
			value.rce_diagnostico.add(this);
		}
	}
	
	public orm.Rce getRce() {
		return rce;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Rce(orm.Rce value) {
		this.rce = value;
	}
	
	private orm.Rce getORM_Rce() {
		return rce;
	}
	
	public void setDiagnostico(orm.Diagnostico value) {
		if (diagnostico != null) {
			diagnostico.rce_diagnostico.remove(this);
		}
		if (value != null) {
			value.rce_diagnostico.add(this);
		}
	}
	
	public orm.Diagnostico getDiagnostico() {
		return diagnostico;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Diagnostico(orm.Diagnostico value) {
		this.diagnostico = value;
	}
	
	private orm.Diagnostico getORM_Diagnostico() {
		return diagnostico;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
