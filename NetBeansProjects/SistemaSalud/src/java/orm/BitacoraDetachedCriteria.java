/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class BitacoraDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final IntegerExpression estado_rce_examenId;
	public final AssociationExpression estado_rce_examen;
	public final IntegerExpression rceId;
	public final AssociationExpression rce;
	public final IntegerExpression persona_id_ingresa_bitacoraId;
	public final AssociationExpression persona_id_ingresa_bitacora;
	public final StringExpression observacion;
	
	public BitacoraDetachedCriteria() {
		super(orm.Bitacora.class, orm.BitacoraCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		estado_rce_examenId = new IntegerExpression("estado_rce_examen.id", this.getDetachedCriteria());
		estado_rce_examen = new AssociationExpression("estado_rce_examen", this.getDetachedCriteria());
		rceId = new IntegerExpression("rce.id", this.getDetachedCriteria());
		rce = new AssociationExpression("rce", this.getDetachedCriteria());
		persona_id_ingresa_bitacoraId = new IntegerExpression("persona_id_ingresa_bitacora.id", this.getDetachedCriteria());
		persona_id_ingresa_bitacora = new AssociationExpression("persona_id_ingresa_bitacora", this.getDetachedCriteria());
		observacion = new StringExpression("observacion", this.getDetachedCriteria());
	}
	
	public BitacoraDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.BitacoraCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		estado_rce_examenId = new IntegerExpression("estado_rce_examen.id", this.getDetachedCriteria());
		estado_rce_examen = new AssociationExpression("estado_rce_examen", this.getDetachedCriteria());
		rceId = new IntegerExpression("rce.id", this.getDetachedCriteria());
		rce = new AssociationExpression("rce", this.getDetachedCriteria());
		persona_id_ingresa_bitacoraId = new IntegerExpression("persona_id_ingresa_bitacora.id", this.getDetachedCriteria());
		persona_id_ingresa_bitacora = new AssociationExpression("persona_id_ingresa_bitacora", this.getDetachedCriteria());
		observacion = new StringExpression("observacion", this.getDetachedCriteria());
	}
	
	public Estado_rce_examenDetachedCriteria createEstado_rce_examenCriteria() {
		return new Estado_rce_examenDetachedCriteria(createCriteria("estado_rce_examen"));
	}
	
	public RceDetachedCriteria createRceCriteria() {
		return new RceDetachedCriteria(createCriteria("rce"));
	}
	
	public PersonaDetachedCriteria createPersona_id_ingresa_bitacoraCriteria() {
		return new PersonaDetachedCriteria(createCriteria("persona_id_ingresa_bitacora"));
	}
	
	public Bitacora uniqueBitacora(PersistentSession session) {
		return (Bitacora) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Bitacora[] listBitacora(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Bitacora[]) list.toArray(new Bitacora[list.size()]);
	}
}

