/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class BitacoraCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final IntegerExpression estado_rce_examenId;
	public final AssociationExpression estado_rce_examen;
	public final IntegerExpression rceId;
	public final AssociationExpression rce;
	public final IntegerExpression persona_id_ingresa_bitacoraId;
	public final AssociationExpression persona_id_ingresa_bitacora;
	public final StringExpression observacion;
	
	public BitacoraCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		estado_rce_examenId = new IntegerExpression("estado_rce_examen.id", this);
		estado_rce_examen = new AssociationExpression("estado_rce_examen", this);
		rceId = new IntegerExpression("rce.id", this);
		rce = new AssociationExpression("rce", this);
		persona_id_ingresa_bitacoraId = new IntegerExpression("persona_id_ingresa_bitacora.id", this);
		persona_id_ingresa_bitacora = new AssociationExpression("persona_id_ingresa_bitacora", this);
		observacion = new StringExpression("observacion", this);
	}
	
	public BitacoraCriteria(PersistentSession session) {
		this(session.createCriteria(Bitacora.class));
	}
	
	public BitacoraCriteria() throws PersistentException {
		this(orm.SaludPersistentManager.instance().getSession());
	}
	
	public Estado_rce_examenCriteria createEstado_rce_examenCriteria() {
		return new Estado_rce_examenCriteria(createCriteria("estado_rce_examen"));
	}
	
	public RceCriteria createRceCriteria() {
		return new RceCriteria(createCriteria("rce"));
	}
	
	public PersonaCriteria createPersona_id_ingresa_bitacoraCriteria() {
		return new PersonaCriteria(createCriteria("persona_id_ingresa_bitacora"));
	}
	
	public Bitacora uniqueBitacora() {
		return (Bitacora) super.uniqueResult();
	}
	
	public Bitacora[] listBitacora() {
		java.util.List list = super.list();
		return (Bitacora[]) list.toArray(new Bitacora[list.size()]);
	}
}

