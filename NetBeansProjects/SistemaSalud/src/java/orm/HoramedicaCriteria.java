/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class HoramedicaCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final IntegerExpression profesionalId;
	public final AssociationExpression profesional;
	public final IntegerExpression administradorId;
	public final AssociationExpression administrador;
	public final TimestampExpression hora_inicio;
	public final IntegerExpression tiempo_periodo;
	public final IntegerExpression reservaId;
	public final AssociationExpression reserva;
	public final CollectionExpression hora_examen_solicitado;
	
	public HoramedicaCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		profesionalId = new IntegerExpression("profesional.id", this);
		profesional = new AssociationExpression("profesional", this);
		administradorId = new IntegerExpression("administrador.id", this);
		administrador = new AssociationExpression("administrador", this);
		hora_inicio = new TimestampExpression("hora_inicio", this);
		tiempo_periodo = new IntegerExpression("tiempo_periodo", this);
		reservaId = new IntegerExpression("reserva.id", this);
		reserva = new AssociationExpression("reserva", this);
		hora_examen_solicitado = new CollectionExpression("ORM_Hora_examen_solicitado", this);
	}
	
	public HoramedicaCriteria(PersistentSession session) {
		this(session.createCriteria(Horamedica.class));
	}
	
	public HoramedicaCriteria() throws PersistentException {
		this(orm.SaludPersistentManager.instance().getSession());
	}
	
	public ProfesionalCriteria createProfesionalCriteria() {
		return new ProfesionalCriteria(createCriteria("profesional"));
	}
	
	public AdministradorCriteria createAdministradorCriteria() {
		return new AdministradorCriteria(createCriteria("administrador"));
	}
	
	public ReservaCriteria createReservaCriteria() {
		return new ReservaCriteria(createCriteria("reserva"));
	}
	
	public Hora_examen_solicitadoCriteria createHora_examen_solicitadoCriteria() {
		return new Hora_examen_solicitadoCriteria(createCriteria("ORM_Hora_examen_solicitado"));
	}
	
	public Horamedica uniqueHoramedica() {
		return (Horamedica) super.uniqueResult();
	}
	
	public Horamedica[] listHoramedica() {
		java.util.List list = super.list();
		return (Horamedica[]) list.toArray(new Horamedica[list.size()]);
	}
}

