/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class BonoDAO {
	public static Bono loadBonoByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadBonoByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Bono getBonoByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return getBonoByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Bono loadBonoByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadBonoByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Bono getBonoByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return getBonoByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Bono loadBonoByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Bono) session.load(orm.Bono.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Bono getBonoByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Bono) session.get(orm.Bono.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Bono loadBonoByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Bono) session.load(orm.Bono.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Bono getBonoByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Bono) session.get(orm.Bono.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryBono(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return queryBono(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryBono(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return queryBono(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Bono[] listBonoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return listBonoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Bono[] listBonoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return listBonoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryBono(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Bono as Bono");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryBono(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Bono as Bono");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Bono", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Bono[] listBonoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryBono(session, condition, orderBy);
			return (Bono[]) list.toArray(new Bono[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Bono[] listBonoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryBono(session, condition, orderBy, lockMode);
			return (Bono[]) list.toArray(new Bono[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Bono loadBonoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadBonoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Bono loadBonoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadBonoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Bono loadBonoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Bono[] bonos = listBonoByQuery(session, condition, orderBy);
		if (bonos != null && bonos.length > 0)
			return bonos[0];
		else
			return null;
	}
	
	public static Bono loadBonoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Bono[] bonos = listBonoByQuery(session, condition, orderBy, lockMode);
		if (bonos != null && bonos.length > 0)
			return bonos[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateBonoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return iterateBonoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateBonoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return iterateBonoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateBonoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Bono as Bono");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateBonoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Bono as Bono");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Bono", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Bono createBono() {
		return new orm.Bono();
	}
	
	public static boolean save(orm.Bono bono) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().saveObject(bono);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(orm.Bono bono) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().deleteObject(bono);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Bono bono)throws PersistentException {
		try {
			if (bono.getPago() != null) {
				bono.getPago().bono.remove(bono);
			}
			
			if (bono.getTipo_prevision() != null) {
				bono.getTipo_prevision().bono.remove(bono);
			}
			
			return delete(bono);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Bono bono, org.orm.PersistentSession session)throws PersistentException {
		try {
			if (bono.getPago() != null) {
				bono.getPago().bono.remove(bono);
			}
			
			if (bono.getTipo_prevision() != null) {
				bono.getTipo_prevision().bono.remove(bono);
			}
			
			try {
				session.delete(bono);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(orm.Bono bono) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().getSession().refresh(bono);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(orm.Bono bono) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().getSession().evict(bono);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Bono loadBonoByCriteria(BonoCriteria bonoCriteria) {
		Bono[] bonos = listBonoByCriteria(bonoCriteria);
		if(bonos == null || bonos.length == 0) {
			return null;
		}
		return bonos[0];
	}
	
	public static Bono[] listBonoByCriteria(BonoCriteria bonoCriteria) {
		return bonoCriteria.listBono();
	}
}
