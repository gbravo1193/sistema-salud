/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public interface ORMConstants extends org.orm.util.ORMBaseConstants {
	final int KEY_CIUDAD_CONTACTO = -426405834;
	
	final int KEY_CONTACTO_CIUDADU = 1427438413;
	
	final int KEY_CONTACTO_EMPRESAU = -383464740;
	
	final int KEY_EMPRESA_CONTACTO = 1968643941;
        
        final int KEY_ADMINISTRADOR_HORAMEDICA = -1918411501;
	
	final int KEY_ADMINISTRADOR_PERSONA = 1673543690;
	
	final int KEY_ALERGIAS_RCE = 1444437089;
	
	final int KEY_BITACORA_ESTADO_RCE_EXAMEN = 650932796;
	
	final int KEY_BITACORA_PERSONA_ID_INGRESA_BITACORA = 936160428;
	
	final int KEY_BITACORA_RCE = -792488252;
	
	final int KEY_BONO_PAGO = -1039298230;
	
	final int KEY_BONO_TIPO_PREVISION = -1617139087;
	
	final int KEY_BOX_RCE = 1737301952;
	
	final int KEY_CONSULTA_HCE_PERSONAID_PACIENTE = 2061451240;
	
	final int KEY_CONSULTA_HCE_PERSONAID_PROFESIONAL = -1103923979;
	
	final int KEY_CONSULTA_HCE_RCE = 505261365;
	
	final int KEY_DESTINO_RCE = 250333565;
	
	final int KEY_DIAGNOSTICO_RCE_DIAGNOSTICO = 2096912774;
	
	final int KEY_ESPECIALIDAD_PROFESIONAL = 31000927;
	
	final int KEY_ESTADO_RCE_EXAMEN_BITACORA = 1072395714;
	
	final int KEY_EXAMEN_HORA_EXAMEN_SOLICITADO = 1261407548;
	
	final int KEY_EXAMEN_RCE_EXAMEN_EXAMEN = -1026703331;
	
	final int KEY_FICHAPACIENTE_PACIENTE = 678921414;
	
	final int KEY_HORA_EXAMEN_SOLICITADO_EXAMEN = -614949342;
	
	final int KEY_HORA_EXAMEN_SOLICITADO_HORAMEDICA = 1118657099;
	
	final int KEY_HORAMEDICA_ADMINISTRADOR = 1312187727;
	
	final int KEY_HORAMEDICA_HORA_EXAMEN_SOLICITADO = -1292925453;
	
	final int KEY_HORAMEDICA_PROFESIONAL = -1288281122;
	
	final int KEY_HORAMEDICA_RESERVA = -1906196278;
	
	final int KEY_INSUMO_RCE_INSUMO = 530403370;
	
	final int KEY_MEDIO_PAGO_PAGO = -223163310;
	
	final int KEY_PACIENTE_FICHAPACIENTE = -861414596;
	
	final int KEY_PACIENTE_PERSONA = 1308413182;
	
	final int KEY_PACIENTE_RECETA = 1484478320;
	
	final int KEY_PACIENTE_RESERVA = -1211037174;
	
	final int KEY_PACIENTE_TIPO_PREVISION = 374466414;
	
	final int KEY_PAGO_BONO = -1121359724;
	
	final int KEY_PAGO_MEDIO_PAGO = -1028058100;
	
	final int KEY_PAGO_RCE_EXAMEN = -1496614759;
	
	final int KEY_PERSONA_ADMINISTRADOR = -755786934;
	
	final int KEY_PERSONA_BITACORA = -765214238;
	
	final int KEY_PERSONA_CONSULTA_HCE = 2146035539;
	
	final int KEY_PERSONA_CONSULTA_HCE1 = 2102592318;
	
	final int KEY_PERSONA_PACIENTE = -1125016892;
	
	final int KEY_PERSONA_PROFESIONAL = -1808868199;
	
	final int KEY_PERSONA_RCE = 1243523361;
	
	final int KEY_PERSONA_RESERVA = 1590808325;
	
	final int KEY_PROCEDENCIA_RCE = -2009260270;
	
	final int KEY_PROFESIONAL_ESPECIALIDAD = 495053285;
	
	final int KEY_PROFESIONAL_HORAMEDICA = -850406492;
	
	final int KEY_PROFESIONAL_PERSONA = 923319065;
	
	final int KEY_RCE_ALERGIAS = -772128169;
	
	final int KEY_RCE_BITACORA = -1278204358;
	
	final int KEY_RCE_BOX = -1603620032;
	
	final int KEY_RCE_CONSULTA_HCE = 1365008299;
	
	final int KEY_RCE_DESTINO = -1844776707;
	
	final int KEY_RCE_DIAGNOSTICO_DIAGNOSTICO = 959266438;
	
	final int KEY_RCE_DIAGNOSTICO_RCE = -1753605326;
	
	final int KEY_RCE_EXAMEN_EXAMEN_EXAMEN = -857824173;
	
	final int KEY_RCE_EXAMEN_EXAMEN_RCE = -1731786519;
	
	final int KEY_RCE_INSUMO_INSUMO = -2018044876;
	
	final int KEY_RCE_INSUMO_RCE = 1591432745;
	
	final int KEY_RCE_PAGO = 1827789764;
	
	final int KEY_RCE_PERSONA_ID_REALIZA_EXAMEN = -1065796485;
	
	final int KEY_RCE_PROCEDENCIA = 124438162;
	
	final int KEY_RCE_RCE_DIAGNOSTICO = -1843757006;
	
	final int KEY_RCE_RCE_EXAMEN_EXAMEN = -1206669271;
	
	final int KEY_RCE_RCE_INSUMO = 1435347295;
	
	final int KEY_RCE_RECETARCE = -278297657;
	
	final int KEY_RCE_RESERVA = 1989902253;
	
	final int KEY_RECETA_PACIENTE = 1678473198;
	
	final int KEY_RECETA_RECETARCE = -546626251;
	
	final int KEY_RECETARCE_RCE = 754956423;
	
	final int KEY_RECETARCE_RECETA = -1835043345;
	
	final int KEY_RESERVA_HORA_MEDICA = 809075453;
	
	final int KEY_RESERVA_PACIENTE = 352537400;
	
	final int KEY_RESERVA_PERSONA_ID_INGRESA_RESERVA = 1974924146;
	
	final int KEY_RESERVA_RCE = 104169517;
	
	final int KEY_TIPO_PREVISION_BONO = 1472640653;
	
	final int KEY_TIPO_PREVISION_PACIENTE = -938633296;
	
}
