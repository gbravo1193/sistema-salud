/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class DestinoDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final StringExpression detalle;
	public final CollectionExpression rce;
	
	public DestinoDetachedCriteria() {
		super(orm.Destino.class, orm.DestinoCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		detalle = new StringExpression("detalle", this.getDetachedCriteria());
		rce = new CollectionExpression("ORM_Rce", this.getDetachedCriteria());
	}
	
	public DestinoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.DestinoCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		detalle = new StringExpression("detalle", this.getDetachedCriteria());
		rce = new CollectionExpression("ORM_Rce", this.getDetachedCriteria());
	}
	
	public RceDetachedCriteria createRceCriteria() {
		return new RceDetachedCriteria(createCriteria("ORM_Rce"));
	}
	
	public Destino uniqueDestino(PersistentSession session) {
		return (Destino) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Destino[] listDestino(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Destino[]) list.toArray(new Destino[list.size()]);
	}
}

