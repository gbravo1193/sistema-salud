/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class Rce_diagnosticoDAO {
	public static Rce_diagnostico loadRce_diagnosticoByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadRce_diagnosticoByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_diagnostico getRce_diagnosticoByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return getRce_diagnosticoByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_diagnostico loadRce_diagnosticoByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadRce_diagnosticoByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_diagnostico getRce_diagnosticoByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return getRce_diagnosticoByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_diagnostico loadRce_diagnosticoByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Rce_diagnostico) session.load(orm.Rce_diagnostico.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_diagnostico getRce_diagnosticoByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Rce_diagnostico) session.get(orm.Rce_diagnostico.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_diagnostico loadRce_diagnosticoByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Rce_diagnostico) session.load(orm.Rce_diagnostico.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_diagnostico getRce_diagnosticoByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Rce_diagnostico) session.get(orm.Rce_diagnostico.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRce_diagnostico(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return queryRce_diagnostico(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRce_diagnostico(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return queryRce_diagnostico(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_diagnostico[] listRce_diagnosticoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return listRce_diagnosticoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_diagnostico[] listRce_diagnosticoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return listRce_diagnosticoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRce_diagnostico(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Rce_diagnostico as Rce_diagnostico");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRce_diagnostico(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Rce_diagnostico as Rce_diagnostico");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Rce_diagnostico", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_diagnostico[] listRce_diagnosticoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryRce_diagnostico(session, condition, orderBy);
			return (Rce_diagnostico[]) list.toArray(new Rce_diagnostico[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_diagnostico[] listRce_diagnosticoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryRce_diagnostico(session, condition, orderBy, lockMode);
			return (Rce_diagnostico[]) list.toArray(new Rce_diagnostico[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_diagnostico loadRce_diagnosticoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadRce_diagnosticoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_diagnostico loadRce_diagnosticoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadRce_diagnosticoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_diagnostico loadRce_diagnosticoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Rce_diagnostico[] rce_diagnosticos = listRce_diagnosticoByQuery(session, condition, orderBy);
		if (rce_diagnosticos != null && rce_diagnosticos.length > 0)
			return rce_diagnosticos[0];
		else
			return null;
	}
	
	public static Rce_diagnostico loadRce_diagnosticoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Rce_diagnostico[] rce_diagnosticos = listRce_diagnosticoByQuery(session, condition, orderBy, lockMode);
		if (rce_diagnosticos != null && rce_diagnosticos.length > 0)
			return rce_diagnosticos[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateRce_diagnosticoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return iterateRce_diagnosticoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateRce_diagnosticoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return iterateRce_diagnosticoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateRce_diagnosticoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Rce_diagnostico as Rce_diagnostico");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateRce_diagnosticoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Rce_diagnostico as Rce_diagnostico");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Rce_diagnostico", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_diagnostico createRce_diagnostico() {
		return new orm.Rce_diagnostico();
	}
	
	public static boolean save(orm.Rce_diagnostico rce_diagnostico) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().saveObject(rce_diagnostico);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(orm.Rce_diagnostico rce_diagnostico) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().deleteObject(rce_diagnostico);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Rce_diagnostico rce_diagnostico)throws PersistentException {
		try {
			if (rce_diagnostico.getRce() != null) {
				rce_diagnostico.getRce().rce_diagnostico.remove(rce_diagnostico);
			}
			
			if (rce_diagnostico.getDiagnostico() != null) {
				rce_diagnostico.getDiagnostico().rce_diagnostico.remove(rce_diagnostico);
			}
			
			return delete(rce_diagnostico);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Rce_diagnostico rce_diagnostico, org.orm.PersistentSession session)throws PersistentException {
		try {
			if (rce_diagnostico.getRce() != null) {
				rce_diagnostico.getRce().rce_diagnostico.remove(rce_diagnostico);
			}
			
			if (rce_diagnostico.getDiagnostico() != null) {
				rce_diagnostico.getDiagnostico().rce_diagnostico.remove(rce_diagnostico);
			}
			
			try {
				session.delete(rce_diagnostico);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(orm.Rce_diagnostico rce_diagnostico) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().getSession().refresh(rce_diagnostico);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(orm.Rce_diagnostico rce_diagnostico) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().getSession().evict(rce_diagnostico);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_diagnostico loadRce_diagnosticoByCriteria(Rce_diagnosticoCriteria rce_diagnosticoCriteria) {
		Rce_diagnostico[] rce_diagnosticos = listRce_diagnosticoByCriteria(rce_diagnosticoCriteria);
		if(rce_diagnosticos == null || rce_diagnosticos.length == 0) {
			return null;
		}
		return rce_diagnosticos[0];
	}
	
	public static Rce_diagnostico[] listRce_diagnosticoByCriteria(Rce_diagnosticoCriteria rce_diagnosticoCriteria) {
		return rce_diagnosticoCriteria.listRce_diagnostico();
	}
}
