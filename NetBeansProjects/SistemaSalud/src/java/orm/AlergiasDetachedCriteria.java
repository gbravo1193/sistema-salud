/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class AlergiasDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final StringExpression detalle;
	public final IntegerExpression rceId;
	public final AssociationExpression rce;
	
	public AlergiasDetachedCriteria() {
		super(orm.Alergias.class, orm.AlergiasCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		detalle = new StringExpression("detalle", this.getDetachedCriteria());
		rceId = new IntegerExpression("rce.id", this.getDetachedCriteria());
		rce = new AssociationExpression("rce", this.getDetachedCriteria());
	}
	
	public AlergiasDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.AlergiasCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		detalle = new StringExpression("detalle", this.getDetachedCriteria());
		rceId = new IntegerExpression("rce.id", this.getDetachedCriteria());
		rce = new AssociationExpression("rce", this.getDetachedCriteria());
	}
	
	public RceDetachedCriteria createRceCriteria() {
		return new RceDetachedCriteria(createCriteria("rce"));
	}
	
	public Alergias uniqueAlergias(PersistentSession session) {
		return (Alergias) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Alergias[] listAlergias(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Alergias[]) list.toArray(new Alergias[list.size()]);
	}
}

