/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Reserva {
	public Reserva() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_RESERVA_PACIENTE) {
			this.paciente = (orm.Paciente) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_RESERVA_PERSONA_ID_INGRESA_RESERVA) {
			this.persona_id_ingresa_reserva = (orm.Persona) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_RESERVA_HORA_MEDICA) {
			this.hora_medica = (orm.Horamedica) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_RESERVA_RCE) {
			this.rce = (orm.Rce) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private orm.Paciente paciente;
	
	private orm.Persona persona_id_ingresa_reserva;
	
	private orm.Horamedica hora_medica;
	
	private java.sql.Timestamp fecha;
	
	private String motivoconsulta;
	
	private orm.Rce rce;
	
	public void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setFecha(java.sql.Timestamp value) {
		this.fecha = value;
	}
	
	public java.sql.Timestamp getFecha() {
		return fecha;
	}
	
	/**
	 * Motivo de consulta
	 */
	public void setMotivoconsulta(String value) {
		this.motivoconsulta = value;
	}
	
	/**
	 * Motivo de consulta
	 */
	public String getMotivoconsulta() {
		return motivoconsulta;
	}
	
	public void setPaciente(orm.Paciente value) {
		if (paciente != null) {
			paciente.reserva.remove(this);
		}
		if (value != null) {
			value.reserva.add(this);
		}
	}
	
	public orm.Paciente getPaciente() {
		return paciente;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Paciente(orm.Paciente value) {
		this.paciente = value;
	}
	
	private orm.Paciente getORM_Paciente() {
		return paciente;
	}
	
	public void setPersona_id_ingresa_reserva(orm.Persona value) {
		if (persona_id_ingresa_reserva != null) {
			persona_id_ingresa_reserva.reserva.remove(this);
		}
		if (value != null) {
			value.reserva.add(this);
		}
	}
	
	public orm.Persona getPersona_id_ingresa_reserva() {
		return persona_id_ingresa_reserva;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Persona_id_ingresa_reserva(orm.Persona value) {
		this.persona_id_ingresa_reserva = value;
	}
	
	private orm.Persona getORM_Persona_id_ingresa_reserva() {
		return persona_id_ingresa_reserva;
	}
	
	public void setHora_medica(orm.Horamedica value) {
		if (this.hora_medica != value) {
			orm.Horamedica lhora_medica = this.hora_medica;
			this.hora_medica = value;
			if (value != null) {
				hora_medica.setReserva(this);
			}
			if (lhora_medica != null && lhora_medica.getReserva() == this) {
				lhora_medica.setReserva(null);
			}
		}
	}
	
	public orm.Horamedica getHora_medica() {
		return hora_medica;
	}
	
	public void setRce(orm.Rce value) {
		if (this.rce != value) {
			orm.Rce lrce = this.rce;
			this.rce = value;
			if (value != null) {
				rce.setReserva(this);
			}
			if (lrce != null && lrce.getReserva() == this) {
				lrce.setReserva(null);
			}
		}
	}
	
	public orm.Rce getRce() {
		return rce;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
