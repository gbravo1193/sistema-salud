/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class DestinoCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression detalle;
	public final CollectionExpression rce;
	
	public DestinoCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		detalle = new StringExpression("detalle", this);
		rce = new CollectionExpression("ORM_Rce", this);
	}
	
	public DestinoCriteria(PersistentSession session) {
		this(session.createCriteria(Destino.class));
	}
	
	public DestinoCriteria() throws PersistentException {
		this(orm.SaludPersistentManager.instance().getSession());
	}
	
	public RceCriteria createRceCriteria() {
		return new RceCriteria(createCriteria("ORM_Rce"));
	}
	
	public Destino uniqueDestino() {
		return (Destino) super.uniqueResult();
	}
	
	public Destino[] listDestino() {
		java.util.List list = super.list();
		return (Destino[]) list.toArray(new Destino[list.size()]);
	}
}

