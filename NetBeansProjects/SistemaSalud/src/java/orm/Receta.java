/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Receta {
	public Receta() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_RECETA_PACIENTE) {
			this.paciente = (orm.Paciente) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_RECETA_RECETARCE) {
			this.recetarce = (orm.Recetarce) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private String detalle;
	
	private orm.Paciente paciente;
	
	private orm.Recetarce recetarce;
	
	/**
	 * Clave Primaria
	 */
	public void setId(int value) {
		this.id = value;
	}
	
	/**
	 * Clave Primaria
	 */
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	/**
	 * Detalle de Receta
	 */
	public void setDetalle(String value) {
		this.detalle = value;
	}
	
	/**
	 * Detalle de Receta
	 */
	public String getDetalle() {
		return detalle;
	}
	
	public void setPaciente(orm.Paciente value) {
		if (paciente != null) {
			paciente.receta.remove(this);
		}
		if (value != null) {
			value.receta.add(this);
		}
	}
	
	public orm.Paciente getPaciente() {
		return paciente;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Paciente(orm.Paciente value) {
		this.paciente = value;
	}
	
	private orm.Paciente getORM_Paciente() {
		return paciente;
	}
	
	public void setRecetarce(orm.Recetarce value) {
		if (this.recetarce != value) {
			orm.Recetarce lrecetarce = this.recetarce;
			this.recetarce = value;
			if (value != null) {
				recetarce.setReceta(this);
			}
			if (lrecetarce != null && lrecetarce.getReceta() == this) {
				lrecetarce.setReceta(null);
			}
		}
	}
	
	public orm.Recetarce getRecetarce() {
		return recetarce;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
