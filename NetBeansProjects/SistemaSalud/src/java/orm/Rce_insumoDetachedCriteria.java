/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Rce_insumoDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final IntegerExpression rceId;
	public final AssociationExpression rce;
	public final IntegerExpression insumoId;
	public final AssociationExpression insumo;
	public final FloatExpression cantidad;
	
	public Rce_insumoDetachedCriteria() {
		super(orm.Rce_insumo.class, orm.Rce_insumoCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		rceId = new IntegerExpression("rce.id", this.getDetachedCriteria());
		rce = new AssociationExpression("rce", this.getDetachedCriteria());
		insumoId = new IntegerExpression("insumo.id", this.getDetachedCriteria());
		insumo = new AssociationExpression("insumo", this.getDetachedCriteria());
		cantidad = new FloatExpression("cantidad", this.getDetachedCriteria());
	}
	
	public Rce_insumoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.Rce_insumoCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		rceId = new IntegerExpression("rce.id", this.getDetachedCriteria());
		rce = new AssociationExpression("rce", this.getDetachedCriteria());
		insumoId = new IntegerExpression("insumo.id", this.getDetachedCriteria());
		insumo = new AssociationExpression("insumo", this.getDetachedCriteria());
		cantidad = new FloatExpression("cantidad", this.getDetachedCriteria());
	}
	
	public RceDetachedCriteria createRceCriteria() {
		return new RceDetachedCriteria(createCriteria("rce"));
	}
	
	public InsumoDetachedCriteria createInsumoCriteria() {
		return new InsumoDetachedCriteria(createCriteria("insumo"));
	}
	
	public Rce_insumo uniqueRce_insumo(PersistentSession session) {
		return (Rce_insumo) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Rce_insumo[] listRce_insumo(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Rce_insumo[]) list.toArray(new Rce_insumo[list.size()]);
	}
}

