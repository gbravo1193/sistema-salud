/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Medio_pagoCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression descripcion;
	public final CollectionExpression pago;
	
	public Medio_pagoCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		descripcion = new StringExpression("descripcion", this);
		pago = new CollectionExpression("ORM_Pago", this);
	}
	
	public Medio_pagoCriteria(PersistentSession session) {
		this(session.createCriteria(Medio_pago.class));
	}
	
	public Medio_pagoCriteria() throws PersistentException {
		this(orm.SaludPersistentManager.instance().getSession());
	}
	
	public PagoCriteria createPagoCriteria() {
		return new PagoCriteria(createCriteria("ORM_Pago"));
	}
	
	public Medio_pago uniqueMedio_pago() {
		return (Medio_pago) super.uniqueResult();
	}
	
	public Medio_pago[] listMedio_pago() {
		java.util.List list = super.list();
		return (Medio_pago[]) list.toArray(new Medio_pago[list.size()]);
	}
}

