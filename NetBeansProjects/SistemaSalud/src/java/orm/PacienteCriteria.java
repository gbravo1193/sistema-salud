/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class PacienteCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final IntegerExpression personaId;
	public final AssociationExpression persona;
	public final StringExpression descripcion;
	public final IntegerExpression tipo_previsionId;
	public final AssociationExpression tipo_prevision;
	public final IntegerExpression activo;
	public final CollectionExpression reserva;
	public final CollectionExpression fichapaciente;
	public final CollectionExpression receta;
	
	public PacienteCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		personaId = new IntegerExpression("persona.id", this);
		persona = new AssociationExpression("persona", this);
		descripcion = new StringExpression("descripcion", this);
		tipo_previsionId = new IntegerExpression("tipo_prevision.id", this);
		tipo_prevision = new AssociationExpression("tipo_prevision", this);
		activo = new IntegerExpression("activo", this);
		reserva = new CollectionExpression("ORM_Reserva", this);
		fichapaciente = new CollectionExpression("ORM_Fichapaciente", this);
		receta = new CollectionExpression("ORM_Receta", this);
	}
	
	public PacienteCriteria(PersistentSession session) {
		this(session.createCriteria(Paciente.class));
	}
	
	public PacienteCriteria() throws PersistentException {
		this(orm.SaludPersistentManager.instance().getSession());
	}
	
	public PersonaCriteria createPersonaCriteria() {
		return new PersonaCriteria(createCriteria("persona"));
	}
	
	public Tipo_previsionCriteria createTipo_previsionCriteria() {
		return new Tipo_previsionCriteria(createCriteria("tipo_prevision"));
	}
	
	public ReservaCriteria createReservaCriteria() {
		return new ReservaCriteria(createCriteria("ORM_Reserva"));
	}
	
	public FichapacienteCriteria createFichapacienteCriteria() {
		return new FichapacienteCriteria(createCriteria("ORM_Fichapaciente"));
	}
	
	public RecetaCriteria createRecetaCriteria() {
		return new RecetaCriteria(createCriteria("ORM_Receta"));
	}
	
	public Paciente uniquePaciente() {
		return (Paciente) super.uniqueResult();
	}
	
	public Paciente[] listPaciente() {
		java.util.List list = super.list();
		return (Paciente[]) list.toArray(new Paciente[list.size()]);
	}
}

