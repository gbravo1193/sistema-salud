/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class HoramedicaDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final IntegerExpression profesionalId;
	public final AssociationExpression profesional;
	public final IntegerExpression administradorId;
	public final AssociationExpression administrador;
	public final TimestampExpression hora_inicio;
	public final IntegerExpression tiempo_periodo;
	public final IntegerExpression reservaId;
	public final AssociationExpression reserva;
	public final CollectionExpression hora_examen_solicitado;
	
	public HoramedicaDetachedCriteria() {
		super(orm.Horamedica.class, orm.HoramedicaCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		profesionalId = new IntegerExpression("profesional.id", this.getDetachedCriteria());
		profesional = new AssociationExpression("profesional", this.getDetachedCriteria());
		administradorId = new IntegerExpression("administrador.id", this.getDetachedCriteria());
		administrador = new AssociationExpression("administrador", this.getDetachedCriteria());
		hora_inicio = new TimestampExpression("hora_inicio", this.getDetachedCriteria());
		tiempo_periodo = new IntegerExpression("tiempo_periodo", this.getDetachedCriteria());
		reservaId = new IntegerExpression("reserva.id", this.getDetachedCriteria());
		reserva = new AssociationExpression("reserva", this.getDetachedCriteria());
		hora_examen_solicitado = new CollectionExpression("ORM_Hora_examen_solicitado", this.getDetachedCriteria());
	}
	
	public HoramedicaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.HoramedicaCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		profesionalId = new IntegerExpression("profesional.id", this.getDetachedCriteria());
		profesional = new AssociationExpression("profesional", this.getDetachedCriteria());
		administradorId = new IntegerExpression("administrador.id", this.getDetachedCriteria());
		administrador = new AssociationExpression("administrador", this.getDetachedCriteria());
		hora_inicio = new TimestampExpression("hora_inicio", this.getDetachedCriteria());
		tiempo_periodo = new IntegerExpression("tiempo_periodo", this.getDetachedCriteria());
		reservaId = new IntegerExpression("reserva.id", this.getDetachedCriteria());
		reserva = new AssociationExpression("reserva", this.getDetachedCriteria());
		hora_examen_solicitado = new CollectionExpression("ORM_Hora_examen_solicitado", this.getDetachedCriteria());
	}
	
	public ProfesionalDetachedCriteria createProfesionalCriteria() {
		return new ProfesionalDetachedCriteria(createCriteria("profesional"));
	}
	
	public AdministradorDetachedCriteria createAdministradorCriteria() {
		return new AdministradorDetachedCriteria(createCriteria("administrador"));
	}
	
	public ReservaDetachedCriteria createReservaCriteria() {
		return new ReservaDetachedCriteria(createCriteria("reserva"));
	}
	
	public Hora_examen_solicitadoDetachedCriteria createHora_examen_solicitadoCriteria() {
		return new Hora_examen_solicitadoDetachedCriteria(createCriteria("ORM_Hora_examen_solicitado"));
	}
	
	public Horamedica uniqueHoramedica(PersistentSession session) {
		return (Horamedica) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Horamedica[] listHoramedica(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Horamedica[]) list.toArray(new Horamedica[list.size()]);
	}
}

