/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Estado_rce_examenCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression descripcion;
	public final CollectionExpression bitacora;
	
	public Estado_rce_examenCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		descripcion = new StringExpression("descripcion", this);
		bitacora = new CollectionExpression("ORM_Bitacora", this);
	}
	
	public Estado_rce_examenCriteria(PersistentSession session) {
		this(session.createCriteria(Estado_rce_examen.class));
	}
	
	public Estado_rce_examenCriteria() throws PersistentException {
		this(orm.SaludPersistentManager.instance().getSession());
	}
	
	public BitacoraCriteria createBitacoraCriteria() {
		return new BitacoraCriteria(createCriteria("ORM_Bitacora"));
	}
	
	public Estado_rce_examen uniqueEstado_rce_examen() {
		return (Estado_rce_examen) super.uniqueResult();
	}
	
	public Estado_rce_examen[] listEstado_rce_examen() {
		java.util.List list = super.list();
		return (Estado_rce_examen[]) list.toArray(new Estado_rce_examen[list.size()]);
	}
}

