/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Diagnostico {
	public Diagnostico() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_DIAGNOSTICO_RCE_DIAGNOSTICO) {
			return ORM_rce_diagnostico;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private int id;
	
	private String codigo;
	
	private java.util.Set ORM_rce_diagnostico = new java.util.HashSet();
	
	/**
	 * Clave Primaria
	 */
	public void setId(int value) {
		this.id = value;
	}
	
	/**
	 * Clave Primaria
	 */
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	/**
	 * Codigo de Diagnostico
	 */
	public void setCodigo(String value) {
		this.codigo = value;
	}
	
	/**
	 * Codigo de Diagnostico
	 */
	public String getCodigo() {
		return codigo;
	}
	
	private void setORM_Rce_diagnostico(java.util.Set value) {
		this.ORM_rce_diagnostico = value;
	}
	
	private java.util.Set getORM_Rce_diagnostico() {
		return ORM_rce_diagnostico;
	}
	
	public final orm.Rce_diagnosticoSetCollection rce_diagnostico = new orm.Rce_diagnosticoSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_DIAGNOSTICO_RCE_DIAGNOSTICO, orm.ORMConstants.KEY_RCE_DIAGNOSTICO_DIAGNOSTICO, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
