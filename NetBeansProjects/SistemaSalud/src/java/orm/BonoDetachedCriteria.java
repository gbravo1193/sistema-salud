/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class BonoDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final IntegerExpression pagoId;
	public final AssociationExpression pago;
	public final IntegerExpression tipo_previsionId;
	public final AssociationExpression tipo_prevision;
	public final StringExpression bono;
	
	public BonoDetachedCriteria() {
		super(orm.Bono.class, orm.BonoCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		pagoId = new IntegerExpression("pago.id", this.getDetachedCriteria());
		pago = new AssociationExpression("pago", this.getDetachedCriteria());
		tipo_previsionId = new IntegerExpression("tipo_prevision.id", this.getDetachedCriteria());
		tipo_prevision = new AssociationExpression("tipo_prevision", this.getDetachedCriteria());
		bono = new StringExpression("bono", this.getDetachedCriteria());
	}
	
	public BonoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.BonoCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		pagoId = new IntegerExpression("pago.id", this.getDetachedCriteria());
		pago = new AssociationExpression("pago", this.getDetachedCriteria());
		tipo_previsionId = new IntegerExpression("tipo_prevision.id", this.getDetachedCriteria());
		tipo_prevision = new AssociationExpression("tipo_prevision", this.getDetachedCriteria());
		bono = new StringExpression("bono", this.getDetachedCriteria());
	}
	
	public PagoDetachedCriteria createPagoCriteria() {
		return new PagoDetachedCriteria(createCriteria("pago"));
	}
	
	public Tipo_previsionDetachedCriteria createTipo_previsionCriteria() {
		return new Tipo_previsionDetachedCriteria(createCriteria("tipo_prevision"));
	}
	
	public Bono uniqueBono(PersistentSession session) {
		return (Bono) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Bono[] listBono(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Bono[]) list.toArray(new Bono[list.size()]);
	}
}

