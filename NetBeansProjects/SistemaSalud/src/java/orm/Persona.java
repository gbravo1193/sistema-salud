/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Persona {
	public Persona() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_PERSONA_RESERVA) {
			return ORM_reserva;
		}
		else if (key == orm.ORMConstants.KEY_PERSONA_CONSULTA_HCE) {
			return ORM_consulta_hce;
		}
		else if (key == orm.ORMConstants.KEY_PERSONA_CONSULTA_HCE1) {
			return ORM_consulta_hce1;
		}
		else if (key == orm.ORMConstants.KEY_PERSONA_RCE) {
			return ORM_rce;
		}
		else if (key == orm.ORMConstants.KEY_PERSONA_BITACORA) {
			return ORM_bitacora;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private int id;
	
	private String rut;
	
	private String nombres;
	
	private String apellido_paterno;
	
	private String apellido_materno;
	
	private String email;
	
	private java.util.Date fecha_nacimiento;
	
	private String identificadorfacebook;
	
	private Integer activo;
	
	private String password;
	
	private orm.Profesional profesional;
	
	private orm.Paciente paciente;
	
	private orm.Administrador administrador;
	
	private java.util.Set ORM_reserva = new java.util.HashSet();
	
	private java.util.Set ORM_consulta_hce = new java.util.HashSet();
	
	private java.util.Set ORM_consulta_hce1 = new java.util.HashSet();
	
	private java.util.Set ORM_rce = new java.util.HashSet();
	
	private java.util.Set ORM_bitacora = new java.util.HashSet();
	
	public void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setRut(String value) {
		this.rut = value;
	}
	
	public String getRut() {
		return rut;
	}
	
	public void setNombres(String value) {
		this.nombres = value;
	}
	
	public String getNombres() {
		return nombres;
	}
	
	public void setApellido_paterno(String value) {
		this.apellido_paterno = value;
	}
	
	public String getApellido_paterno() {
		return apellido_paterno;
	}
	
	public void setApellido_materno(String value) {
		this.apellido_materno = value;
	}
	
	public String getApellido_materno() {
		return apellido_materno;
	}
	
	public void setEmail(String value) {
		this.email = value;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setFecha_nacimiento(java.util.Date value) {
		this.fecha_nacimiento = value;
	}
	
	public java.util.Date getFecha_nacimiento() {
		return fecha_nacimiento;
	}
	
	/**
	 * Identificador Facebook
	 */
	public void setIdentificadorfacebook(String value) {
		this.identificadorfacebook = value;
	}
	
	/**
	 * Identificador Facebook
	 */
	public String getIdentificadorfacebook() {
		return identificadorfacebook;
	}
	
	/**
	 * Indica si esta activo o no
	 */
	public void setActivo(int value) {
		setActivo(new Integer(value));
	}
	
	/**
	 * Indica si esta activo o no
	 */
	public void setActivo(Integer value) {
		this.activo = value;
	}
	
	/**
	 * Indica si esta activo o no
	 */
	public Integer getActivo() {
		return activo;
	}
	
	public void setPassword(String value) {
		this.password = value;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setProfesional(orm.Profesional value) {
		if (this.profesional != value) {
			orm.Profesional lprofesional = this.profesional;
			this.profesional = value;
			if (value != null) {
				profesional.setPersona(this);
			}
			if (lprofesional != null && lprofesional.getPersona() == this) {
				lprofesional.setPersona(null);
			}
		}
	}
	
	public orm.Profesional getProfesional() {
		return profesional;
	}
	
	public void setPaciente(orm.Paciente value) {
		if (this.paciente != value) {
			orm.Paciente lpaciente = this.paciente;
			this.paciente = value;
			if (value != null) {
				paciente.setPersona(this);
			}
			if (lpaciente != null && lpaciente.getPersona() == this) {
				lpaciente.setPersona(null);
			}
		}
	}
	
	public orm.Paciente getPaciente() {
		return paciente;
	}
	
	public void setAdministrador(orm.Administrador value) {
		if (this.administrador != value) {
			orm.Administrador ladministrador = this.administrador;
			this.administrador = value;
			if (value != null) {
				administrador.setPersona(this);
			}
			if (ladministrador != null && ladministrador.getPersona() == this) {
				ladministrador.setPersona(null);
			}
		}
	}
	
	public orm.Administrador getAdministrador() {
		return administrador;
	}
	
	private void setORM_Reserva(java.util.Set value) {
		this.ORM_reserva = value;
	}
	
	private java.util.Set getORM_Reserva() {
		return ORM_reserva;
	}
	
	public final orm.ReservaSetCollection reserva = new orm.ReservaSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_PERSONA_RESERVA, orm.ORMConstants.KEY_RESERVA_PERSONA_ID_INGRESA_RESERVA, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Consulta_hce(java.util.Set value) {
		this.ORM_consulta_hce = value;
	}
	
	private java.util.Set getORM_Consulta_hce() {
		return ORM_consulta_hce;
	}
	
	public final orm.Consulta_hceSetCollection consulta_hce = new orm.Consulta_hceSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_PERSONA_CONSULTA_HCE, orm.ORMConstants.KEY_CONSULTA_HCE_PERSONAID_PROFESIONAL, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Consulta_hce1(java.util.Set value) {
		this.ORM_consulta_hce1 = value;
	}
	
	private java.util.Set getORM_Consulta_hce1() {
		return ORM_consulta_hce1;
	}
	
	public final orm.Consulta_hceSetCollection consulta_hce1 = new orm.Consulta_hceSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_PERSONA_CONSULTA_HCE1, orm.ORMConstants.KEY_CONSULTA_HCE_PERSONAID_PACIENTE, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Rce(java.util.Set value) {
		this.ORM_rce = value;
	}
	
	private java.util.Set getORM_Rce() {
		return ORM_rce;
	}
	
	public final orm.RceSetCollection rce = new orm.RceSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_PERSONA_RCE, orm.ORMConstants.KEY_RCE_PERSONA_ID_REALIZA_EXAMEN, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Bitacora(java.util.Set value) {
		this.ORM_bitacora = value;
	}
	
	private java.util.Set getORM_Bitacora() {
		return ORM_bitacora;
	}
	
	public final orm.BitacoraSetCollection bitacora = new orm.BitacoraSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_PERSONA_BITACORA, orm.ORMConstants.KEY_BITACORA_PERSONA_ID_INGRESA_BITACORA, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
