/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Rce_examen_examenCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final IntegerExpression rceId;
	public final AssociationExpression rce;
	public final IntegerExpression examenId;
	public final AssociationExpression examen;
	public final DoubleExpression monto_a_pagar;
	
	public Rce_examen_examenCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		rceId = new IntegerExpression("rce.id", this);
		rce = new AssociationExpression("rce", this);
		examenId = new IntegerExpression("examen.id", this);
		examen = new AssociationExpression("examen", this);
		monto_a_pagar = new DoubleExpression("monto_a_pagar", this);
	}
	
	public Rce_examen_examenCriteria(PersistentSession session) {
		this(session.createCriteria(Rce_examen_examen.class));
	}
	
	public Rce_examen_examenCriteria() throws PersistentException {
		this(orm.SaludPersistentManager.instance().getSession());
	}
	
	public RceCriteria createRceCriteria() {
		return new RceCriteria(createCriteria("rce"));
	}
	
	public ExamenCriteria createExamenCriteria() {
		return new ExamenCriteria(createCriteria("examen"));
	}
	
	public Rce_examen_examen uniqueRce_examen_examen() {
		return (Rce_examen_examen) super.uniqueResult();
	}
	
	public Rce_examen_examen[] listRce_examen_examen() {
		java.util.List list = super.list();
		return (Rce_examen_examen[]) list.toArray(new Rce_examen_examen[list.size()]);
	}
}

