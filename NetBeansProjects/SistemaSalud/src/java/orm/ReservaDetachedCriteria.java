/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ReservaDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final IntegerExpression pacienteId;
	public final AssociationExpression paciente;
	public final IntegerExpression persona_id_ingresa_reservaId;
	public final AssociationExpression persona_id_ingresa_reserva;
	public final IntegerExpression hora_medicaId;
	public final AssociationExpression hora_medica;
	public final TimestampExpression fecha;
	public final StringExpression motivoconsulta;
	public final IntegerExpression rceId;
	public final AssociationExpression rce;
	
	public ReservaDetachedCriteria() {
		super(orm.Reserva.class, orm.ReservaCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		pacienteId = new IntegerExpression("paciente.id", this.getDetachedCriteria());
		paciente = new AssociationExpression("paciente", this.getDetachedCriteria());
		persona_id_ingresa_reservaId = new IntegerExpression("persona_id_ingresa_reserva.id", this.getDetachedCriteria());
		persona_id_ingresa_reserva = new AssociationExpression("persona_id_ingresa_reserva", this.getDetachedCriteria());
		hora_medicaId = new IntegerExpression("hora_medica.id", this.getDetachedCriteria());
		hora_medica = new AssociationExpression("hora_medica", this.getDetachedCriteria());
		fecha = new TimestampExpression("fecha", this.getDetachedCriteria());
		motivoconsulta = new StringExpression("motivoconsulta", this.getDetachedCriteria());
		rceId = new IntegerExpression("rce.id", this.getDetachedCriteria());
		rce = new AssociationExpression("rce", this.getDetachedCriteria());
	}
	
	public ReservaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.ReservaCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		pacienteId = new IntegerExpression("paciente.id", this.getDetachedCriteria());
		paciente = new AssociationExpression("paciente", this.getDetachedCriteria());
		persona_id_ingresa_reservaId = new IntegerExpression("persona_id_ingresa_reserva.id", this.getDetachedCriteria());
		persona_id_ingresa_reserva = new AssociationExpression("persona_id_ingresa_reserva", this.getDetachedCriteria());
		hora_medicaId = new IntegerExpression("hora_medica.id", this.getDetachedCriteria());
		hora_medica = new AssociationExpression("hora_medica", this.getDetachedCriteria());
		fecha = new TimestampExpression("fecha", this.getDetachedCriteria());
		motivoconsulta = new StringExpression("motivoconsulta", this.getDetachedCriteria());
		rceId = new IntegerExpression("rce.id", this.getDetachedCriteria());
		rce = new AssociationExpression("rce", this.getDetachedCriteria());
	}
	
	public PacienteDetachedCriteria createPacienteCriteria() {
		return new PacienteDetachedCriteria(createCriteria("paciente"));
	}
	
	public PersonaDetachedCriteria createPersona_id_ingresa_reservaCriteria() {
		return new PersonaDetachedCriteria(createCriteria("persona_id_ingresa_reserva"));
	}
	
	public HoramedicaDetachedCriteria createHora_medicaCriteria() {
		return new HoramedicaDetachedCriteria(createCriteria("hora_medica"));
	}
	
	public RceDetachedCriteria createRceCriteria() {
		return new RceDetachedCriteria(createCriteria("rce"));
	}
	
	public Reserva uniqueReserva(PersistentSession session) {
		return (Reserva) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Reserva[] listReserva(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Reserva[]) list.toArray(new Reserva[list.size()]);
	}
}

