/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Rce_diagnosticoCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final IntegerExpression rceId;
	public final AssociationExpression rce;
	public final IntegerExpression diagnosticoId;
	public final AssociationExpression diagnostico;
	
	public Rce_diagnosticoCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		rceId = new IntegerExpression("rce.id", this);
		rce = new AssociationExpression("rce", this);
		diagnosticoId = new IntegerExpression("diagnostico.id", this);
		diagnostico = new AssociationExpression("diagnostico", this);
	}
	
	public Rce_diagnosticoCriteria(PersistentSession session) {
		this(session.createCriteria(Rce_diagnostico.class));
	}
	
	public Rce_diagnosticoCriteria() throws PersistentException {
		this(orm.SaludPersistentManager.instance().getSession());
	}
	
	public RceCriteria createRceCriteria() {
		return new RceCriteria(createCriteria("rce"));
	}
	
	public DiagnosticoCriteria createDiagnosticoCriteria() {
		return new DiagnosticoCriteria(createCriteria("diagnostico"));
	}
	
	public Rce_diagnostico uniqueRce_diagnostico() {
		return (Rce_diagnostico) super.uniqueResult();
	}
	
	public Rce_diagnostico[] listRce_diagnostico() {
		java.util.List list = super.list();
		return (Rce_diagnostico[]) list.toArray(new Rce_diagnostico[list.size()]);
	}
}

