/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class RceDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final IntegerExpression procedenciaId;
	public final AssociationExpression procedencia;
	public final IntegerExpression reservaId;
	public final AssociationExpression reserva;
	public final IntegerExpression persona_id_realiza_examenId;
	public final AssociationExpression persona_id_realiza_examen;
	public final IntegerExpression boxId;
	public final AssociationExpression box;
	public final IntegerExpression destinoId;
	public final AssociationExpression destino;
	public final IntegerExpression cerrado;
	public final StringExpression indicaciones;
	public final StringExpression observaciones;
	public final StringExpression anamnesis;
	public final DateExpression fechacierre;
	public final IntegerExpression tipocierre;
	public final CollectionExpression consulta_hce;
	public final CollectionExpression pago;
	public final CollectionExpression rce_examen_examen;
	public final CollectionExpression bitacora;
	public final CollectionExpression rce_diagnostico;
	public final CollectionExpression alergias;
	public final CollectionExpression recetarce;
	public final CollectionExpression rce_insumo;
	
	public RceDetachedCriteria() {
		super(orm.Rce.class, orm.RceCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		procedenciaId = new IntegerExpression("procedencia.id", this.getDetachedCriteria());
		procedencia = new AssociationExpression("procedencia", this.getDetachedCriteria());
		reservaId = new IntegerExpression("reserva.id", this.getDetachedCriteria());
		reserva = new AssociationExpression("reserva", this.getDetachedCriteria());
		persona_id_realiza_examenId = new IntegerExpression("persona_id_realiza_examen.id", this.getDetachedCriteria());
		persona_id_realiza_examen = new AssociationExpression("persona_id_realiza_examen", this.getDetachedCriteria());
		boxId = new IntegerExpression("box.id", this.getDetachedCriteria());
		box = new AssociationExpression("box", this.getDetachedCriteria());
		destinoId = new IntegerExpression("destino.id", this.getDetachedCriteria());
		destino = new AssociationExpression("destino", this.getDetachedCriteria());
		cerrado = new IntegerExpression("cerrado", this.getDetachedCriteria());
		indicaciones = new StringExpression("indicaciones", this.getDetachedCriteria());
		observaciones = new StringExpression("observaciones", this.getDetachedCriteria());
		anamnesis = new StringExpression("anamnesis", this.getDetachedCriteria());
		fechacierre = new DateExpression("fechacierre", this.getDetachedCriteria());
		tipocierre = new IntegerExpression("tipocierre", this.getDetachedCriteria());
		consulta_hce = new CollectionExpression("ORM_Consulta_hce", this.getDetachedCriteria());
		pago = new CollectionExpression("ORM_Pago", this.getDetachedCriteria());
		rce_examen_examen = new CollectionExpression("ORM_Rce_examen_examen", this.getDetachedCriteria());
		bitacora = new CollectionExpression("ORM_Bitacora", this.getDetachedCriteria());
		rce_diagnostico = new CollectionExpression("ORM_Rce_diagnostico", this.getDetachedCriteria());
		alergias = new CollectionExpression("ORM_Alergias", this.getDetachedCriteria());
		recetarce = new CollectionExpression("ORM_Recetarce", this.getDetachedCriteria());
		rce_insumo = new CollectionExpression("ORM_Rce_insumo", this.getDetachedCriteria());
	}
	
	public RceDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.RceCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		procedenciaId = new IntegerExpression("procedencia.id", this.getDetachedCriteria());
		procedencia = new AssociationExpression("procedencia", this.getDetachedCriteria());
		reservaId = new IntegerExpression("reserva.id", this.getDetachedCriteria());
		reserva = new AssociationExpression("reserva", this.getDetachedCriteria());
		persona_id_realiza_examenId = new IntegerExpression("persona_id_realiza_examen.id", this.getDetachedCriteria());
		persona_id_realiza_examen = new AssociationExpression("persona_id_realiza_examen", this.getDetachedCriteria());
		boxId = new IntegerExpression("box.id", this.getDetachedCriteria());
		box = new AssociationExpression("box", this.getDetachedCriteria());
		destinoId = new IntegerExpression("destino.id", this.getDetachedCriteria());
		destino = new AssociationExpression("destino", this.getDetachedCriteria());
		cerrado = new IntegerExpression("cerrado", this.getDetachedCriteria());
		indicaciones = new StringExpression("indicaciones", this.getDetachedCriteria());
		observaciones = new StringExpression("observaciones", this.getDetachedCriteria());
		anamnesis = new StringExpression("anamnesis", this.getDetachedCriteria());
		fechacierre = new DateExpression("fechacierre", this.getDetachedCriteria());
		tipocierre = new IntegerExpression("tipocierre", this.getDetachedCriteria());
		consulta_hce = new CollectionExpression("ORM_Consulta_hce", this.getDetachedCriteria());
		pago = new CollectionExpression("ORM_Pago", this.getDetachedCriteria());
		rce_examen_examen = new CollectionExpression("ORM_Rce_examen_examen", this.getDetachedCriteria());
		bitacora = new CollectionExpression("ORM_Bitacora", this.getDetachedCriteria());
		rce_diagnostico = new CollectionExpression("ORM_Rce_diagnostico", this.getDetachedCriteria());
		alergias = new CollectionExpression("ORM_Alergias", this.getDetachedCriteria());
		recetarce = new CollectionExpression("ORM_Recetarce", this.getDetachedCriteria());
		rce_insumo = new CollectionExpression("ORM_Rce_insumo", this.getDetachedCriteria());
	}
	
	public ProcedenciaDetachedCriteria createProcedenciaCriteria() {
		return new ProcedenciaDetachedCriteria(createCriteria("procedencia"));
	}
	
	public ReservaDetachedCriteria createReservaCriteria() {
		return new ReservaDetachedCriteria(createCriteria("reserva"));
	}
	
	public PersonaDetachedCriteria createPersona_id_realiza_examenCriteria() {
		return new PersonaDetachedCriteria(createCriteria("persona_id_realiza_examen"));
	}
	
	public BoxDetachedCriteria createBoxCriteria() {
		return new BoxDetachedCriteria(createCriteria("box"));
	}
	
	public DestinoDetachedCriteria createDestinoCriteria() {
		return new DestinoDetachedCriteria(createCriteria("destino"));
	}
	
	public Consulta_hceDetachedCriteria createConsulta_hceCriteria() {
		return new Consulta_hceDetachedCriteria(createCriteria("ORM_Consulta_hce"));
	}
	
	public PagoDetachedCriteria createPagoCriteria() {
		return new PagoDetachedCriteria(createCriteria("ORM_Pago"));
	}
	
	public Rce_examen_examenDetachedCriteria createRce_examen_examenCriteria() {
		return new Rce_examen_examenDetachedCriteria(createCriteria("ORM_Rce_examen_examen"));
	}
	
	public BitacoraDetachedCriteria createBitacoraCriteria() {
		return new BitacoraDetachedCriteria(createCriteria("ORM_Bitacora"));
	}
	
	public Rce_diagnosticoDetachedCriteria createRce_diagnosticoCriteria() {
		return new Rce_diagnosticoDetachedCriteria(createCriteria("ORM_Rce_diagnostico"));
	}
	
	public AlergiasDetachedCriteria createAlergiasCriteria() {
		return new AlergiasDetachedCriteria(createCriteria("ORM_Alergias"));
	}
	
	public RecetarceDetachedCriteria createRecetarceCriteria() {
		return new RecetarceDetachedCriteria(createCriteria("ORM_Recetarce"));
	}
	
	public Rce_insumoDetachedCriteria createRce_insumoCriteria() {
		return new Rce_insumoDetachedCriteria(createCriteria("ORM_Rce_insumo"));
	}
	
	public Rce uniqueRce(PersistentSession session) {
		return (Rce) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Rce[] listRce(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Rce[]) list.toArray(new Rce[list.size()]);
	}
}

