/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class Hora_examen_solicitadoDAO {
	public static Hora_examen_solicitado loadHora_examen_solicitadoByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadHora_examen_solicitadoByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Hora_examen_solicitado getHora_examen_solicitadoByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return getHora_examen_solicitadoByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Hora_examen_solicitado loadHora_examen_solicitadoByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadHora_examen_solicitadoByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Hora_examen_solicitado getHora_examen_solicitadoByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return getHora_examen_solicitadoByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Hora_examen_solicitado loadHora_examen_solicitadoByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Hora_examen_solicitado) session.load(orm.Hora_examen_solicitado.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Hora_examen_solicitado getHora_examen_solicitadoByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Hora_examen_solicitado) session.get(orm.Hora_examen_solicitado.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Hora_examen_solicitado loadHora_examen_solicitadoByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Hora_examen_solicitado) session.load(orm.Hora_examen_solicitado.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Hora_examen_solicitado getHora_examen_solicitadoByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Hora_examen_solicitado) session.get(orm.Hora_examen_solicitado.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryHora_examen_solicitado(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return queryHora_examen_solicitado(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryHora_examen_solicitado(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return queryHora_examen_solicitado(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Hora_examen_solicitado[] listHora_examen_solicitadoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return listHora_examen_solicitadoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Hora_examen_solicitado[] listHora_examen_solicitadoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return listHora_examen_solicitadoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryHora_examen_solicitado(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Hora_examen_solicitado as Hora_examen_solicitado");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryHora_examen_solicitado(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Hora_examen_solicitado as Hora_examen_solicitado");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Hora_examen_solicitado", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Hora_examen_solicitado[] listHora_examen_solicitadoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryHora_examen_solicitado(session, condition, orderBy);
			return (Hora_examen_solicitado[]) list.toArray(new Hora_examen_solicitado[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Hora_examen_solicitado[] listHora_examen_solicitadoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryHora_examen_solicitado(session, condition, orderBy, lockMode);
			return (Hora_examen_solicitado[]) list.toArray(new Hora_examen_solicitado[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Hora_examen_solicitado loadHora_examen_solicitadoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadHora_examen_solicitadoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Hora_examen_solicitado loadHora_examen_solicitadoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadHora_examen_solicitadoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Hora_examen_solicitado loadHora_examen_solicitadoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Hora_examen_solicitado[] hora_examen_solicitados = listHora_examen_solicitadoByQuery(session, condition, orderBy);
		if (hora_examen_solicitados != null && hora_examen_solicitados.length > 0)
			return hora_examen_solicitados[0];
		else
			return null;
	}
	
	public static Hora_examen_solicitado loadHora_examen_solicitadoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Hora_examen_solicitado[] hora_examen_solicitados = listHora_examen_solicitadoByQuery(session, condition, orderBy, lockMode);
		if (hora_examen_solicitados != null && hora_examen_solicitados.length > 0)
			return hora_examen_solicitados[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateHora_examen_solicitadoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return iterateHora_examen_solicitadoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateHora_examen_solicitadoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return iterateHora_examen_solicitadoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateHora_examen_solicitadoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Hora_examen_solicitado as Hora_examen_solicitado");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateHora_examen_solicitadoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Hora_examen_solicitado as Hora_examen_solicitado");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Hora_examen_solicitado", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Hora_examen_solicitado createHora_examen_solicitado() {
		return new orm.Hora_examen_solicitado();
	}
	
	public static boolean save(orm.Hora_examen_solicitado hora_examen_solicitado) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().saveObject(hora_examen_solicitado);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(orm.Hora_examen_solicitado hora_examen_solicitado) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().deleteObject(hora_examen_solicitado);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Hora_examen_solicitado hora_examen_solicitado)throws PersistentException {
		try {
			if (hora_examen_solicitado.getHoramedica() != null) {
				hora_examen_solicitado.getHoramedica().hora_examen_solicitado.remove(hora_examen_solicitado);
			}
			
			if (hora_examen_solicitado.getExamen() != null) {
				hora_examen_solicitado.getExamen().hora_examen_solicitado.remove(hora_examen_solicitado);
			}
			
			return delete(hora_examen_solicitado);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Hora_examen_solicitado hora_examen_solicitado, org.orm.PersistentSession session)throws PersistentException {
		try {
			if (hora_examen_solicitado.getHoramedica() != null) {
				hora_examen_solicitado.getHoramedica().hora_examen_solicitado.remove(hora_examen_solicitado);
			}
			
			if (hora_examen_solicitado.getExamen() != null) {
				hora_examen_solicitado.getExamen().hora_examen_solicitado.remove(hora_examen_solicitado);
			}
			
			try {
				session.delete(hora_examen_solicitado);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(orm.Hora_examen_solicitado hora_examen_solicitado) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().getSession().refresh(hora_examen_solicitado);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(orm.Hora_examen_solicitado hora_examen_solicitado) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().getSession().evict(hora_examen_solicitado);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Hora_examen_solicitado loadHora_examen_solicitadoByCriteria(Hora_examen_solicitadoCriteria hora_examen_solicitadoCriteria) {
		Hora_examen_solicitado[] hora_examen_solicitados = listHora_examen_solicitadoByCriteria(hora_examen_solicitadoCriteria);
		if(hora_examen_solicitados == null || hora_examen_solicitados.length == 0) {
			return null;
		}
		return hora_examen_solicitados[0];
	}
	
	public static Hora_examen_solicitado[] listHora_examen_solicitadoByCriteria(Hora_examen_solicitadoCriteria hora_examen_solicitadoCriteria) {
		return hora_examen_solicitadoCriteria.listHora_examen_solicitado();
	}
}
