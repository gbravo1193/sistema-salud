/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Hora_examen_solicitadoCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final IntegerExpression horamedicaId;
	public final AssociationExpression horamedica;
	public final IntegerExpression examenId;
	public final AssociationExpression examen;
	
	public Hora_examen_solicitadoCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		horamedicaId = new IntegerExpression("horamedica.id", this);
		horamedica = new AssociationExpression("horamedica", this);
		examenId = new IntegerExpression("examen.id", this);
		examen = new AssociationExpression("examen", this);
	}
	
	public Hora_examen_solicitadoCriteria(PersistentSession session) {
		this(session.createCriteria(Hora_examen_solicitado.class));
	}
	
	public Hora_examen_solicitadoCriteria() throws PersistentException {
		this(orm.SaludPersistentManager.instance().getSession());
	}
	
	public HoramedicaCriteria createHoramedicaCriteria() {
		return new HoramedicaCriteria(createCriteria("horamedica"));
	}
	
	public ExamenCriteria createExamenCriteria() {
		return new ExamenCriteria(createCriteria("examen"));
	}
	
	public Hora_examen_solicitado uniqueHora_examen_solicitado() {
		return (Hora_examen_solicitado) super.uniqueResult();
	}
	
	public Hora_examen_solicitado[] listHora_examen_solicitado() {
		java.util.List list = super.list();
		return (Hora_examen_solicitado[]) list.toArray(new Hora_examen_solicitado[list.size()]);
	}
}

