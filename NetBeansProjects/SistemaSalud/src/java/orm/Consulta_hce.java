/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Consulta_hce {
	public Consulta_hce() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_CONSULTA_HCE_RCE) {
			return ORM_rce;
		}
		
		return null;
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_CONSULTA_HCE_PERSONAID_PROFESIONAL) {
			this.personaid_profesional = (orm.Persona) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_CONSULTA_HCE_PERSONAID_PACIENTE) {
			this.personaid_paciente = (orm.Persona) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private orm.Persona personaid_profesional;
	
	private orm.Persona personaid_paciente;
	
	private java.util.Date fecha;
	
	private java.util.Set ORM_rce = new java.util.HashSet();
	
	/**
	 * Clave Primaria
	 */
	public void setId(int value) {
		this.id = value;
	}
	
	/**
	 * Clave Primaria
	 */
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	/**
	 * Fecha de consulta
	 */
	public void setFecha(java.util.Date value) {
		this.fecha = value;
	}
	
	/**
	 * Fecha de consulta
	 */
	public java.util.Date getFecha() {
		return fecha;
	}
	
	public void setPersonaid_profesional(orm.Persona value) {
		if (personaid_profesional != null) {
			personaid_profesional.consulta_hce.remove(this);
		}
		if (value != null) {
			value.consulta_hce.add(this);
		}
	}
	
	public orm.Persona getPersonaid_profesional() {
		return personaid_profesional;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Personaid_profesional(orm.Persona value) {
		this.personaid_profesional = value;
	}
	
	private orm.Persona getORM_Personaid_profesional() {
		return personaid_profesional;
	}
	
	public void setPersonaid_paciente(orm.Persona value) {
		if (personaid_paciente != null) {
			personaid_paciente.consulta_hce1.remove(this);
		}
		if (value != null) {
			value.consulta_hce1.add(this);
		}
	}
	
	public orm.Persona getPersonaid_paciente() {
		return personaid_paciente;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Personaid_paciente(orm.Persona value) {
		this.personaid_paciente = value;
	}
	
	private orm.Persona getORM_Personaid_paciente() {
		return personaid_paciente;
	}
	
	private void setORM_Rce(java.util.Set value) {
		this.ORM_rce = value;
	}
	
	private java.util.Set getORM_Rce() {
		return ORM_rce;
	}
	
	public final orm.RceSetCollection rce = new orm.RceSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_CONSULTA_HCE_RCE, orm.ORMConstants.KEY_RCE_CONSULTA_HCE, orm.ORMConstants.KEY_MUL_MANY_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
