/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ProcedenciaCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression descripcion;
	public final CollectionExpression rce;
	
	public ProcedenciaCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		descripcion = new StringExpression("descripcion", this);
		rce = new CollectionExpression("ORM_Rce", this);
	}
	
	public ProcedenciaCriteria(PersistentSession session) {
		this(session.createCriteria(Procedencia.class));
	}
	
	public ProcedenciaCriteria() throws PersistentException {
		this(orm.SaludPersistentManager.instance().getSession());
	}
	
	public RceCriteria createRceCriteria() {
		return new RceCriteria(createCriteria("ORM_Rce"));
	}
	
	public Procedencia uniqueProcedencia() {
		return (Procedencia) super.uniqueResult();
	}
	
	public Procedencia[] listProcedencia() {
		java.util.List list = super.list();
		return (Procedencia[]) list.toArray(new Procedencia[list.size()]);
	}
}

