/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class PacienteDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final IntegerExpression personaId;
	public final AssociationExpression persona;
	public final StringExpression descripcion;
	public final IntegerExpression tipo_previsionId;
	public final AssociationExpression tipo_prevision;
	public final IntegerExpression activo;
	public final CollectionExpression reserva;
	public final CollectionExpression fichapaciente;
	public final CollectionExpression receta;
	
	public PacienteDetachedCriteria() {
		super(orm.Paciente.class, orm.PacienteCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		personaId = new IntegerExpression("persona.id", this.getDetachedCriteria());
		persona = new AssociationExpression("persona", this.getDetachedCriteria());
		descripcion = new StringExpression("descripcion", this.getDetachedCriteria());
		tipo_previsionId = new IntegerExpression("tipo_prevision.id", this.getDetachedCriteria());
		tipo_prevision = new AssociationExpression("tipo_prevision", this.getDetachedCriteria());
		activo = new IntegerExpression("activo", this.getDetachedCriteria());
		reserva = new CollectionExpression("ORM_Reserva", this.getDetachedCriteria());
		fichapaciente = new CollectionExpression("ORM_Fichapaciente", this.getDetachedCriteria());
		receta = new CollectionExpression("ORM_Receta", this.getDetachedCriteria());
	}
	
	public PacienteDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.PacienteCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		personaId = new IntegerExpression("persona.id", this.getDetachedCriteria());
		persona = new AssociationExpression("persona", this.getDetachedCriteria());
		descripcion = new StringExpression("descripcion", this.getDetachedCriteria());
		tipo_previsionId = new IntegerExpression("tipo_prevision.id", this.getDetachedCriteria());
		tipo_prevision = new AssociationExpression("tipo_prevision", this.getDetachedCriteria());
		activo = new IntegerExpression("activo", this.getDetachedCriteria());
		reserva = new CollectionExpression("ORM_Reserva", this.getDetachedCriteria());
		fichapaciente = new CollectionExpression("ORM_Fichapaciente", this.getDetachedCriteria());
		receta = new CollectionExpression("ORM_Receta", this.getDetachedCriteria());
	}
	
	public PersonaDetachedCriteria createPersonaCriteria() {
		return new PersonaDetachedCriteria(createCriteria("persona"));
	}
	
	public Tipo_previsionDetachedCriteria createTipo_previsionCriteria() {
		return new Tipo_previsionDetachedCriteria(createCriteria("tipo_prevision"));
	}
	
	public ReservaDetachedCriteria createReservaCriteria() {
		return new ReservaDetachedCriteria(createCriteria("ORM_Reserva"));
	}
	
	public FichapacienteDetachedCriteria createFichapacienteCriteria() {
		return new FichapacienteDetachedCriteria(createCriteria("ORM_Fichapaciente"));
	}
	
	public RecetaDetachedCriteria createRecetaCriteria() {
		return new RecetaDetachedCriteria(createCriteria("ORM_Receta"));
	}
	
	public Paciente uniquePaciente(PersistentSession session) {
		return (Paciente) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Paciente[] listPaciente(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Paciente[]) list.toArray(new Paciente[list.size()]);
	}
}

