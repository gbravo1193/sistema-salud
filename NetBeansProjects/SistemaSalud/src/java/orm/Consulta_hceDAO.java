/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class Consulta_hceDAO {
	public static Consulta_hce loadConsulta_hceByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadConsulta_hceByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Consulta_hce getConsulta_hceByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return getConsulta_hceByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Consulta_hce loadConsulta_hceByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadConsulta_hceByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Consulta_hce getConsulta_hceByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return getConsulta_hceByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Consulta_hce loadConsulta_hceByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Consulta_hce) session.load(orm.Consulta_hce.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Consulta_hce getConsulta_hceByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Consulta_hce) session.get(orm.Consulta_hce.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Consulta_hce loadConsulta_hceByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Consulta_hce) session.load(orm.Consulta_hce.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Consulta_hce getConsulta_hceByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Consulta_hce) session.get(orm.Consulta_hce.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryConsulta_hce(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return queryConsulta_hce(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryConsulta_hce(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return queryConsulta_hce(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Consulta_hce[] listConsulta_hceByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return listConsulta_hceByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Consulta_hce[] listConsulta_hceByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return listConsulta_hceByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryConsulta_hce(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Consulta_hce as Consulta_hce");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryConsulta_hce(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Consulta_hce as Consulta_hce");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Consulta_hce", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Consulta_hce[] listConsulta_hceByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryConsulta_hce(session, condition, orderBy);
			return (Consulta_hce[]) list.toArray(new Consulta_hce[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Consulta_hce[] listConsulta_hceByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryConsulta_hce(session, condition, orderBy, lockMode);
			return (Consulta_hce[]) list.toArray(new Consulta_hce[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Consulta_hce loadConsulta_hceByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadConsulta_hceByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Consulta_hce loadConsulta_hceByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadConsulta_hceByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Consulta_hce loadConsulta_hceByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Consulta_hce[] consulta_hces = listConsulta_hceByQuery(session, condition, orderBy);
		if (consulta_hces != null && consulta_hces.length > 0)
			return consulta_hces[0];
		else
			return null;
	}
	
	public static Consulta_hce loadConsulta_hceByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Consulta_hce[] consulta_hces = listConsulta_hceByQuery(session, condition, orderBy, lockMode);
		if (consulta_hces != null && consulta_hces.length > 0)
			return consulta_hces[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateConsulta_hceByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return iterateConsulta_hceByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateConsulta_hceByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return iterateConsulta_hceByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateConsulta_hceByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Consulta_hce as Consulta_hce");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateConsulta_hceByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Consulta_hce as Consulta_hce");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Consulta_hce", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Consulta_hce createConsulta_hce() {
		return new orm.Consulta_hce();
	}
	
	public static boolean save(orm.Consulta_hce consulta_hce) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().saveObject(consulta_hce);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(orm.Consulta_hce consulta_hce) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().deleteObject(consulta_hce);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Consulta_hce consulta_hce)throws PersistentException {
		try {
			if (consulta_hce.getPersonaid_profesional() != null) {
				consulta_hce.getPersonaid_profesional().consulta_hce.remove(consulta_hce);
			}
			
			if (consulta_hce.getPersonaid_paciente() != null) {
				consulta_hce.getPersonaid_paciente().consulta_hce1.remove(consulta_hce);
			}
			
			orm.Rce[] lRces = consulta_hce.rce.toArray();
			for(int i = 0; i < lRces.length; i++) {
				lRces[i].consulta_hce.remove(consulta_hce);
			}
			return delete(consulta_hce);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Consulta_hce consulta_hce, org.orm.PersistentSession session)throws PersistentException {
		try {
			if (consulta_hce.getPersonaid_profesional() != null) {
				consulta_hce.getPersonaid_profesional().consulta_hce.remove(consulta_hce);
			}
			
			if (consulta_hce.getPersonaid_paciente() != null) {
				consulta_hce.getPersonaid_paciente().consulta_hce1.remove(consulta_hce);
			}
			
			orm.Rce[] lRces = consulta_hce.rce.toArray();
			for(int i = 0; i < lRces.length; i++) {
				lRces[i].consulta_hce.remove(consulta_hce);
			}
			try {
				session.delete(consulta_hce);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(orm.Consulta_hce consulta_hce) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().getSession().refresh(consulta_hce);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(orm.Consulta_hce consulta_hce) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().getSession().evict(consulta_hce);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Consulta_hce loadConsulta_hceByCriteria(Consulta_hceCriteria consulta_hceCriteria) {
		Consulta_hce[] consulta_hces = listConsulta_hceByCriteria(consulta_hceCriteria);
		if(consulta_hces == null || consulta_hces.length == 0) {
			return null;
		}
		return consulta_hces[0];
	}
	
	public static Consulta_hce[] listConsulta_hceByCriteria(Consulta_hceCriteria consulta_hceCriteria) {
		return consulta_hceCriteria.listConsulta_hce();
	}
}
