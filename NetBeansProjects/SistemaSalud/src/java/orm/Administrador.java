/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Administrador {
	public Administrador() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_ADMINISTRADOR_HORAMEDICA) {
			return ORM_horamedica;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private int id;
	
	private orm.Persona persona;
	
	private int activo;
	
	private java.util.Set ORM_horamedica = new java.util.HashSet();
	
	public void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setActivo(int value) {
		this.activo = value;
	}
	
	public int getActivo() {
		return activo;
	}
	
	public void setPersona(orm.Persona value) {
		if (this.persona != value) {
			orm.Persona lpersona = this.persona;
			this.persona = value;
			if (value != null) {
				persona.setAdministrador(this);
			}
			if (lpersona != null && lpersona.getAdministrador() == this) {
				lpersona.setAdministrador(null);
			}
		}
	}
	
	public orm.Persona getPersona() {
		return persona;
	}
	
	private void setORM_Horamedica(java.util.Set value) {
		this.ORM_horamedica = value;
	}
	
	private java.util.Set getORM_Horamedica() {
		return ORM_horamedica;
	}
	
	public final orm.HoramedicaSetCollection horamedica = new orm.HoramedicaSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_ADMINISTRADOR_HORAMEDICA, orm.ORMConstants.KEY_HORAMEDICA_ADMINISTRADOR, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
