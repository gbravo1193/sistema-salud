/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ProfesionalDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final IntegerExpression personaId;
	public final AssociationExpression persona;
	public final IntegerExpression activo;
	public final CollectionExpression especialidad;
	public final CollectionExpression horamedica;
	
	public ProfesionalDetachedCriteria() {
		super(orm.Profesional.class, orm.ProfesionalCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		personaId = new IntegerExpression("persona.id", this.getDetachedCriteria());
		persona = new AssociationExpression("persona", this.getDetachedCriteria());
		activo = new IntegerExpression("activo", this.getDetachedCriteria());
		especialidad = new CollectionExpression("ORM_Especialidad", this.getDetachedCriteria());
		horamedica = new CollectionExpression("ORM_Horamedica", this.getDetachedCriteria());
	}
	
	public ProfesionalDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.ProfesionalCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		personaId = new IntegerExpression("persona.id", this.getDetachedCriteria());
		persona = new AssociationExpression("persona", this.getDetachedCriteria());
		activo = new IntegerExpression("activo", this.getDetachedCriteria());
		especialidad = new CollectionExpression("ORM_Especialidad", this.getDetachedCriteria());
		horamedica = new CollectionExpression("ORM_Horamedica", this.getDetachedCriteria());
	}
	
	public PersonaDetachedCriteria createPersonaCriteria() {
		return new PersonaDetachedCriteria(createCriteria("persona"));
	}
	
	public EspecialidadDetachedCriteria createEspecialidadCriteria() {
		return new EspecialidadDetachedCriteria(createCriteria("ORM_Especialidad"));
	}
	
	public HoramedicaDetachedCriteria createHoramedicaCriteria() {
		return new HoramedicaDetachedCriteria(createCriteria("ORM_Horamedica"));
	}
	
	public Profesional uniqueProfesional(PersistentSession session) {
		return (Profesional) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Profesional[] listProfesional(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Profesional[]) list.toArray(new Profesional[list.size()]);
	}
}

