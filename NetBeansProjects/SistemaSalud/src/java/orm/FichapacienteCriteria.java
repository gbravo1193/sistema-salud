/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class FichapacienteCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final IntegerExpression pacienteId;
	public final AssociationExpression paciente;
	public final StringExpression codigo;
	
	public FichapacienteCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		pacienteId = new IntegerExpression("paciente.id", this);
		paciente = new AssociationExpression("paciente", this);
		codigo = new StringExpression("codigo", this);
	}
	
	public FichapacienteCriteria(PersistentSession session) {
		this(session.createCriteria(Fichapaciente.class));
	}
	
	public FichapacienteCriteria() throws PersistentException {
		this(orm.SaludPersistentManager.instance().getSession());
	}
	
	public PacienteCriteria createPacienteCriteria() {
		return new PacienteCriteria(createCriteria("paciente"));
	}
	
	public Fichapaciente uniqueFichapaciente() {
		return (Fichapaciente) super.uniqueResult();
	}
	
	public Fichapaciente[] listFichapaciente() {
		java.util.List list = super.list();
		return (Fichapaciente[]) list.toArray(new Fichapaciente[list.size()]);
	}
}

