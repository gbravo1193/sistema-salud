/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class RecetaDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final StringExpression detalle;
	public final IntegerExpression pacienteId;
	public final AssociationExpression paciente;
	public final IntegerExpression recetarceId;
	public final AssociationExpression recetarce;
	
	public RecetaDetachedCriteria() {
		super(orm.Receta.class, orm.RecetaCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		detalle = new StringExpression("detalle", this.getDetachedCriteria());
		pacienteId = new IntegerExpression("paciente.id", this.getDetachedCriteria());
		paciente = new AssociationExpression("paciente", this.getDetachedCriteria());
		recetarceId = new IntegerExpression("recetarce.id", this.getDetachedCriteria());
		recetarce = new AssociationExpression("recetarce", this.getDetachedCriteria());
	}
	
	public RecetaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.RecetaCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		detalle = new StringExpression("detalle", this.getDetachedCriteria());
		pacienteId = new IntegerExpression("paciente.id", this.getDetachedCriteria());
		paciente = new AssociationExpression("paciente", this.getDetachedCriteria());
		recetarceId = new IntegerExpression("recetarce.id", this.getDetachedCriteria());
		recetarce = new AssociationExpression("recetarce", this.getDetachedCriteria());
	}
	
	public PacienteDetachedCriteria createPacienteCriteria() {
		return new PacienteDetachedCriteria(createCriteria("paciente"));
	}
	
	public RecetarceDetachedCriteria createRecetarceCriteria() {
		return new RecetarceDetachedCriteria(createCriteria("recetarce"));
	}
	
	public Receta uniqueReceta(PersistentSession session) {
		return (Receta) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Receta[] listReceta(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Receta[]) list.toArray(new Receta[list.size()]);
	}
}

