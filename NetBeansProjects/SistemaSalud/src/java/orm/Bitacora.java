/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Bitacora {
	public Bitacora() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_BITACORA_RCE) {
			this.rce = (orm.Rce) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_BITACORA_PERSONA_ID_INGRESA_BITACORA) {
			this.persona_id_ingresa_bitacora = (orm.Persona) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_BITACORA_ESTADO_RCE_EXAMEN) {
			this.estado_rce_examen = (orm.Estado_rce_examen) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private orm.Estado_rce_examen estado_rce_examen;
	
	private orm.Rce rce;
	
	private orm.Persona persona_id_ingresa_bitacora;
	
	private String observacion;
	
	/**
	 * Clave Primaria
	 */
	public void setId(int value) {
		this.id = value;
	}
	
	/**
	 * Clave Primaria
	 */
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	/**
	 * Observacion
	 */
	public void setObservacion(String value) {
		this.observacion = value;
	}
	
	/**
	 * Observacion
	 */
	public String getObservacion() {
		return observacion;
	}
	
	public void setRce(orm.Rce value) {
		if (rce != null) {
			rce.bitacora.remove(this);
		}
		if (value != null) {
			value.bitacora.add(this);
		}
	}
	
	public orm.Rce getRce() {
		return rce;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Rce(orm.Rce value) {
		this.rce = value;
	}
	
	private orm.Rce getORM_Rce() {
		return rce;
	}
	
	public void setPersona_id_ingresa_bitacora(orm.Persona value) {
		if (persona_id_ingresa_bitacora != null) {
			persona_id_ingresa_bitacora.bitacora.remove(this);
		}
		if (value != null) {
			value.bitacora.add(this);
		}
	}
	
	public orm.Persona getPersona_id_ingresa_bitacora() {
		return persona_id_ingresa_bitacora;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Persona_id_ingresa_bitacora(orm.Persona value) {
		this.persona_id_ingresa_bitacora = value;
	}
	
	private orm.Persona getORM_Persona_id_ingresa_bitacora() {
		return persona_id_ingresa_bitacora;
	}
	
	public void setEstado_rce_examen(orm.Estado_rce_examen value) {
		if (estado_rce_examen != null) {
			estado_rce_examen.bitacora.remove(this);
		}
		if (value != null) {
			value.bitacora.add(this);
		}
	}
	
	public orm.Estado_rce_examen getEstado_rce_examen() {
		return estado_rce_examen;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Estado_rce_examen(orm.Estado_rce_examen value) {
		this.estado_rce_examen = value;
	}
	
	private orm.Estado_rce_examen getORM_Estado_rce_examen() {
		return estado_rce_examen;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
