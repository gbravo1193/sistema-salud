/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class RecetaCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression detalle;
	public final IntegerExpression pacienteId;
	public final AssociationExpression paciente;
	public final IntegerExpression recetarceId;
	public final AssociationExpression recetarce;
	
	public RecetaCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		detalle = new StringExpression("detalle", this);
		pacienteId = new IntegerExpression("paciente.id", this);
		paciente = new AssociationExpression("paciente", this);
		recetarceId = new IntegerExpression("recetarce.id", this);
		recetarce = new AssociationExpression("recetarce", this);
	}
	
	public RecetaCriteria(PersistentSession session) {
		this(session.createCriteria(Receta.class));
	}
	
	public RecetaCriteria() throws PersistentException {
		this(orm.SaludPersistentManager.instance().getSession());
	}
	
	public PacienteCriteria createPacienteCriteria() {
		return new PacienteCriteria(createCriteria("paciente"));
	}
	
	public RecetarceCriteria createRecetarceCriteria() {
		return new RecetarceCriteria(createCriteria("recetarce"));
	}
	
	public Receta uniqueReceta() {
		return (Receta) super.uniqueResult();
	}
	
	public Receta[] listReceta() {
		java.util.List list = super.list();
		return (Receta[]) list.toArray(new Receta[list.size()]);
	}
}

