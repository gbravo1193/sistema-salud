/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class Rce_insumoDAO {
	public static Rce_insumo loadRce_insumoByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadRce_insumoByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_insumo getRce_insumoByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return getRce_insumoByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_insumo loadRce_insumoByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadRce_insumoByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_insumo getRce_insumoByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return getRce_insumoByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_insumo loadRce_insumoByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Rce_insumo) session.load(orm.Rce_insumo.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_insumo getRce_insumoByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Rce_insumo) session.get(orm.Rce_insumo.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_insumo loadRce_insumoByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Rce_insumo) session.load(orm.Rce_insumo.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_insumo getRce_insumoByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Rce_insumo) session.get(orm.Rce_insumo.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRce_insumo(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return queryRce_insumo(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRce_insumo(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return queryRce_insumo(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_insumo[] listRce_insumoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return listRce_insumoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_insumo[] listRce_insumoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return listRce_insumoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRce_insumo(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Rce_insumo as Rce_insumo");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRce_insumo(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Rce_insumo as Rce_insumo");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Rce_insumo", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_insumo[] listRce_insumoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryRce_insumo(session, condition, orderBy);
			return (Rce_insumo[]) list.toArray(new Rce_insumo[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_insumo[] listRce_insumoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryRce_insumo(session, condition, orderBy, lockMode);
			return (Rce_insumo[]) list.toArray(new Rce_insumo[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_insumo loadRce_insumoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadRce_insumoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_insumo loadRce_insumoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadRce_insumoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_insumo loadRce_insumoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Rce_insumo[] rce_insumos = listRce_insumoByQuery(session, condition, orderBy);
		if (rce_insumos != null && rce_insumos.length > 0)
			return rce_insumos[0];
		else
			return null;
	}
	
	public static Rce_insumo loadRce_insumoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Rce_insumo[] rce_insumos = listRce_insumoByQuery(session, condition, orderBy, lockMode);
		if (rce_insumos != null && rce_insumos.length > 0)
			return rce_insumos[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateRce_insumoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return iterateRce_insumoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateRce_insumoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return iterateRce_insumoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateRce_insumoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Rce_insumo as Rce_insumo");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateRce_insumoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Rce_insumo as Rce_insumo");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Rce_insumo", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_insumo createRce_insumo() {
		return new orm.Rce_insumo();
	}
	
	public static boolean save(orm.Rce_insumo rce_insumo) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().saveObject(rce_insumo);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(orm.Rce_insumo rce_insumo) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().deleteObject(rce_insumo);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Rce_insumo rce_insumo)throws PersistentException {
		try {
			if (rce_insumo.getRce() != null) {
				rce_insumo.getRce().rce_insumo.remove(rce_insumo);
			}
			
			if (rce_insumo.getInsumo() != null) {
				rce_insumo.getInsumo().rce_insumo.remove(rce_insumo);
			}
			
			return delete(rce_insumo);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Rce_insumo rce_insumo, org.orm.PersistentSession session)throws PersistentException {
		try {
			if (rce_insumo.getRce() != null) {
				rce_insumo.getRce().rce_insumo.remove(rce_insumo);
			}
			
			if (rce_insumo.getInsumo() != null) {
				rce_insumo.getInsumo().rce_insumo.remove(rce_insumo);
			}
			
			try {
				session.delete(rce_insumo);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(orm.Rce_insumo rce_insumo) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().getSession().refresh(rce_insumo);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(orm.Rce_insumo rce_insumo) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().getSession().evict(rce_insumo);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_insumo loadRce_insumoByCriteria(Rce_insumoCriteria rce_insumoCriteria) {
		Rce_insumo[] rce_insumos = listRce_insumoByCriteria(rce_insumoCriteria);
		if(rce_insumos == null || rce_insumos.length == 0) {
			return null;
		}
		return rce_insumos[0];
	}
	
	public static Rce_insumo[] listRce_insumoByCriteria(Rce_insumoCriteria rce_insumoCriteria) {
		return rce_insumoCriteria.listRce_insumo();
	}
}
