/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Insumo {
	public Insumo() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_INSUMO_RCE_INSUMO) {
			return ORM_rce_insumo;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private int id;
	
	private String nombre;
	
	private Float monto_pmp;
	
	private String codigo;
	
	private java.util.Set ORM_rce_insumo = new java.util.HashSet();
	
	/**
	 * Clave Primaria
	 */
	public void setId(int value) {
		this.id = value;
	}
	
	/**
	 * Clave Primaria
	 */
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	/**
	 * Nombre Insumo
	 */
	public void setNombre(String value) {
		this.nombre = value;
	}
	
	/**
	 * Nombre Insumo
	 */
	public String getNombre() {
		return nombre;
	}
	
	/**
	 * Monto Insumo
	 */
	public void setMonto_pmp(float value) {
		setMonto_pmp(new Float(value));
	}
	
	/**
	 * Monto Insumo
	 */
	public void setMonto_pmp(Float value) {
		this.monto_pmp = value;
	}
	
	/**
	 * Monto Insumo
	 */
	public Float getMonto_pmp() {
		return monto_pmp;
	}
	
	/**
	 * Codigo
	 */
	public void setCodigo(String value) {
		this.codigo = value;
	}
	
	/**
	 * Codigo
	 */
	public String getCodigo() {
		return codigo;
	}
	
	private void setORM_Rce_insumo(java.util.Set value) {
		this.ORM_rce_insumo = value;
	}
	
	private java.util.Set getORM_Rce_insumo() {
		return ORM_rce_insumo;
	}
	
	public final orm.Rce_insumoSetCollection rce_insumo = new orm.Rce_insumoSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_INSUMO_RCE_INSUMO, orm.ORMConstants.KEY_RCE_INSUMO_INSUMO, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
