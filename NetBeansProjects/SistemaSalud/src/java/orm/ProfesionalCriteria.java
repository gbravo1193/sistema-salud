/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ProfesionalCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final IntegerExpression personaId;
	public final AssociationExpression persona;
	public final IntegerExpression activo;
	public final CollectionExpression especialidad;
	public final CollectionExpression horamedica;
	
	public ProfesionalCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		personaId = new IntegerExpression("persona.id", this);
		persona = new AssociationExpression("persona", this);
		activo = new IntegerExpression("activo", this);
		especialidad = new CollectionExpression("ORM_Especialidad", this);
		horamedica = new CollectionExpression("ORM_Horamedica", this);
	}
	
	public ProfesionalCriteria(PersistentSession session) {
		this(session.createCriteria(Profesional.class));
	}
	
	public ProfesionalCriteria() throws PersistentException {
		this(orm.SaludPersistentManager.instance().getSession());
	}
	
	public PersonaCriteria createPersonaCriteria() {
		return new PersonaCriteria(createCriteria("persona"));
	}
	
	public EspecialidadCriteria createEspecialidadCriteria() {
		return new EspecialidadCriteria(createCriteria("ORM_Especialidad"));
	}
	
	public HoramedicaCriteria createHoramedicaCriteria() {
		return new HoramedicaCriteria(createCriteria("ORM_Horamedica"));
	}
	
	public Profesional uniqueProfesional() {
		return (Profesional) super.uniqueResult();
	}
	
	public Profesional[] listProfesional() {
		java.util.List list = super.list();
		return (Profesional[]) list.toArray(new Profesional[list.size()]);
	}
}

