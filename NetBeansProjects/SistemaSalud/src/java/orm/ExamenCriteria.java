/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ExamenCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression descripcion;
	public final DoubleExpression monto;
	public final StringExpression observaciones;
	public final CollectionExpression hora_examen_solicitado;
	public final CollectionExpression rce_examen_examen;
	
	public ExamenCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		descripcion = new StringExpression("descripcion", this);
		monto = new DoubleExpression("monto", this);
		observaciones = new StringExpression("observaciones", this);
		hora_examen_solicitado = new CollectionExpression("ORM_Hora_examen_solicitado", this);
		rce_examen_examen = new CollectionExpression("ORM_Rce_examen_examen", this);
	}
	
	public ExamenCriteria(PersistentSession session) {
		this(session.createCriteria(Examen.class));
	}
	
	public ExamenCriteria() throws PersistentException {
		this(orm.SaludPersistentManager.instance().getSession());
	}
	
	public Hora_examen_solicitadoCriteria createHora_examen_solicitadoCriteria() {
		return new Hora_examen_solicitadoCriteria(createCriteria("ORM_Hora_examen_solicitado"));
	}
	
	public Rce_examen_examenCriteria createRce_examen_examenCriteria() {
		return new Rce_examen_examenCriteria(createCriteria("ORM_Rce_examen_examen"));
	}
	
	public Examen uniqueExamen() {
		return (Examen) super.uniqueResult();
	}
	
	public Examen[] listExamen() {
		java.util.List list = super.list();
		return (Examen[]) list.toArray(new Examen[list.size()]);
	}
}

