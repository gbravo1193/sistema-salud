/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class AlergiasCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression detalle;
	public final IntegerExpression rceId;
	public final AssociationExpression rce;
	
	public AlergiasCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		detalle = new StringExpression("detalle", this);
		rceId = new IntegerExpression("rce.id", this);
		rce = new AssociationExpression("rce", this);
	}
	
	public AlergiasCriteria(PersistentSession session) {
		this(session.createCriteria(Alergias.class));
	}
	
	public AlergiasCriteria() throws PersistentException {
		this(orm.SaludPersistentManager.instance().getSession());
	}
	
	public RceCriteria createRceCriteria() {
		return new RceCriteria(createCriteria("rce"));
	}
	
	public Alergias uniqueAlergias() {
		return (Alergias) super.uniqueResult();
	}
	
	public Alergias[] listAlergias() {
		java.util.List list = super.list();
		return (Alergias[]) list.toArray(new Alergias[list.size()]);
	}
}

