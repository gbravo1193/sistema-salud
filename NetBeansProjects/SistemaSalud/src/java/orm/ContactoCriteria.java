/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ContactoCriteria extends AbstractORMCriteria {
	public final IntegerExpression uid;
	public final StringExpression nombre;
	public final StringExpression apellido;
	public final StringExpression mail;
	public final StringExpression telefono;
	public final IntegerExpression empresauId;
	public final AssociationExpression empresau;
	public final IntegerExpression ciudaduId;
	public final AssociationExpression ciudadu;
	
	public ContactoCriteria(Criteria criteria) {
		super(criteria);
		uid = new IntegerExpression("uid", this);
		nombre = new StringExpression("nombre", this);
		apellido = new StringExpression("apellido", this);
		mail = new StringExpression("mail", this);
		telefono = new StringExpression("telefono", this);
		empresauId = new IntegerExpression("empresau.uid", this);
		empresau = new AssociationExpression("empresau", this);
		ciudaduId = new IntegerExpression("ciudadu.uid", this);
		ciudadu = new AssociationExpression("ciudadu", this);
	}
	
	public ContactoCriteria(PersistentSession session) {
		this(session.createCriteria(Contacto.class));
	}
	
	public ContactoCriteria() throws PersistentException {
		this(orm.SwPersistentManager.instance().getSession());
	}
	
	public EmpresaCriteria createEmpresauCriteria() {
		return new EmpresaCriteria(createCriteria("empresau"));
	}
	
	public CiudadCriteria createCiudaduCriteria() {
		return new CiudadCriteria(createCriteria("ciudadu"));
	}
	
	public Contacto uniqueContacto() {
		return (Contacto) super.uniqueResult();
	}
	
	public Contacto[] listContacto() {
		java.util.List list = super.list();
		return (Contacto[]) list.toArray(new Contacto[list.size()]);
	}
}

