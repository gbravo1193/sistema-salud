/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class RecetarceDAO {
	public static Recetarce loadRecetarceByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadRecetarceByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recetarce getRecetarceByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return getRecetarceByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recetarce loadRecetarceByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadRecetarceByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recetarce getRecetarceByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return getRecetarceByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recetarce loadRecetarceByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Recetarce) session.load(orm.Recetarce.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recetarce getRecetarceByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Recetarce) session.get(orm.Recetarce.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recetarce loadRecetarceByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Recetarce) session.load(orm.Recetarce.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recetarce getRecetarceByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Recetarce) session.get(orm.Recetarce.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRecetarce(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return queryRecetarce(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRecetarce(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return queryRecetarce(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recetarce[] listRecetarceByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return listRecetarceByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recetarce[] listRecetarceByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return listRecetarceByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRecetarce(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Recetarce as Recetarce");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRecetarce(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Recetarce as Recetarce");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Recetarce", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recetarce[] listRecetarceByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryRecetarce(session, condition, orderBy);
			return (Recetarce[]) list.toArray(new Recetarce[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recetarce[] listRecetarceByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryRecetarce(session, condition, orderBy, lockMode);
			return (Recetarce[]) list.toArray(new Recetarce[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recetarce loadRecetarceByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadRecetarceByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recetarce loadRecetarceByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadRecetarceByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recetarce loadRecetarceByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Recetarce[] recetarces = listRecetarceByQuery(session, condition, orderBy);
		if (recetarces != null && recetarces.length > 0)
			return recetarces[0];
		else
			return null;
	}
	
	public static Recetarce loadRecetarceByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Recetarce[] recetarces = listRecetarceByQuery(session, condition, orderBy, lockMode);
		if (recetarces != null && recetarces.length > 0)
			return recetarces[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateRecetarceByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return iterateRecetarceByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateRecetarceByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return iterateRecetarceByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateRecetarceByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Recetarce as Recetarce");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateRecetarceByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Recetarce as Recetarce");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Recetarce", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recetarce createRecetarce() {
		return new orm.Recetarce();
	}
	
	public static boolean save(orm.Recetarce recetarce) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().saveObject(recetarce);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(orm.Recetarce recetarce) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().deleteObject(recetarce);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Recetarce recetarce)throws PersistentException {
		try {
			if (recetarce.getRce() != null) {
				recetarce.getRce().recetarce.remove(recetarce);
			}
			
			if (recetarce.getReceta() != null) {
				recetarce.getReceta().setRecetarce(null);
			}
			
			return delete(recetarce);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Recetarce recetarce, org.orm.PersistentSession session)throws PersistentException {
		try {
			if (recetarce.getRce() != null) {
				recetarce.getRce().recetarce.remove(recetarce);
			}
			
			if (recetarce.getReceta() != null) {
				recetarce.getReceta().setRecetarce(null);
			}
			
			try {
				session.delete(recetarce);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(orm.Recetarce recetarce) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().getSession().refresh(recetarce);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(orm.Recetarce recetarce) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().getSession().evict(recetarce);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recetarce loadRecetarceByCriteria(RecetarceCriteria recetarceCriteria) {
		Recetarce[] recetarces = listRecetarceByCriteria(recetarceCriteria);
		if(recetarces == null || recetarces.length == 0) {
			return null;
		}
		return recetarces[0];
	}
	
	public static Recetarce[] listRecetarceByCriteria(RecetarceCriteria recetarceCriteria) {
		return recetarceCriteria.listRecetarce();
	}
}
