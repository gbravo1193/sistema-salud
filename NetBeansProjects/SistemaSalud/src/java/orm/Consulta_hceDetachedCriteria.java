/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Consulta_hceDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final IntegerExpression personaid_profesionalId;
	public final AssociationExpression personaid_profesional;
	public final IntegerExpression personaid_pacienteId;
	public final AssociationExpression personaid_paciente;
	public final DateExpression fecha;
	public final CollectionExpression rce;
	
	public Consulta_hceDetachedCriteria() {
		super(orm.Consulta_hce.class, orm.Consulta_hceCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		personaid_profesionalId = new IntegerExpression("personaid_profesional.id", this.getDetachedCriteria());
		personaid_profesional = new AssociationExpression("personaid_profesional", this.getDetachedCriteria());
		personaid_pacienteId = new IntegerExpression("personaid_paciente.id", this.getDetachedCriteria());
		personaid_paciente = new AssociationExpression("personaid_paciente", this.getDetachedCriteria());
		fecha = new DateExpression("fecha", this.getDetachedCriteria());
		rce = new CollectionExpression("ORM_Rce", this.getDetachedCriteria());
	}
	
	public Consulta_hceDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.Consulta_hceCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		personaid_profesionalId = new IntegerExpression("personaid_profesional.id", this.getDetachedCriteria());
		personaid_profesional = new AssociationExpression("personaid_profesional", this.getDetachedCriteria());
		personaid_pacienteId = new IntegerExpression("personaid_paciente.id", this.getDetachedCriteria());
		personaid_paciente = new AssociationExpression("personaid_paciente", this.getDetachedCriteria());
		fecha = new DateExpression("fecha", this.getDetachedCriteria());
		rce = new CollectionExpression("ORM_Rce", this.getDetachedCriteria());
	}
	
	public PersonaDetachedCriteria createPersonaid_profesionalCriteria() {
		return new PersonaDetachedCriteria(createCriteria("personaid_profesional"));
	}
	
	public PersonaDetachedCriteria createPersonaid_pacienteCriteria() {
		return new PersonaDetachedCriteria(createCriteria("personaid_paciente"));
	}
	
	public RceDetachedCriteria createRceCriteria() {
		return new RceDetachedCriteria(createCriteria("ORM_Rce"));
	}
	
	public Consulta_hce uniqueConsulta_hce(PersistentSession session) {
		return (Consulta_hce) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Consulta_hce[] listConsulta_hce(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Consulta_hce[]) list.toArray(new Consulta_hce[list.size()]);
	}
}

