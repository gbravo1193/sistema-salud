/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class HoramedicaDAO {
	public static Horamedica loadHoramedicaByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadHoramedicaByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Horamedica getHoramedicaByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return getHoramedicaByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Horamedica loadHoramedicaByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadHoramedicaByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Horamedica getHoramedicaByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return getHoramedicaByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Horamedica loadHoramedicaByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Horamedica) session.load(orm.Horamedica.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Horamedica getHoramedicaByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Horamedica) session.get(orm.Horamedica.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Horamedica loadHoramedicaByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Horamedica) session.load(orm.Horamedica.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Horamedica getHoramedicaByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Horamedica) session.get(orm.Horamedica.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryHoramedica(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return queryHoramedica(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryHoramedica(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return queryHoramedica(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Horamedica[] listHoramedicaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return listHoramedicaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Horamedica[] listHoramedicaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return listHoramedicaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryHoramedica(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Horamedica as Horamedica");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryHoramedica(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Horamedica as Horamedica");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Horamedica", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Horamedica[] listHoramedicaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryHoramedica(session, condition, orderBy);
			return (Horamedica[]) list.toArray(new Horamedica[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Horamedica[] listHoramedicaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryHoramedica(session, condition, orderBy, lockMode);
			return (Horamedica[]) list.toArray(new Horamedica[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Horamedica loadHoramedicaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadHoramedicaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Horamedica loadHoramedicaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadHoramedicaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Horamedica loadHoramedicaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Horamedica[] horamedicas = listHoramedicaByQuery(session, condition, orderBy);
		if (horamedicas != null && horamedicas.length > 0)
			return horamedicas[0];
		else
			return null;
	}
	
	public static Horamedica loadHoramedicaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Horamedica[] horamedicas = listHoramedicaByQuery(session, condition, orderBy, lockMode);
		if (horamedicas != null && horamedicas.length > 0)
			return horamedicas[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateHoramedicaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return iterateHoramedicaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateHoramedicaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return iterateHoramedicaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateHoramedicaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Horamedica as Horamedica");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateHoramedicaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Horamedica as Horamedica");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Horamedica", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Horamedica createHoramedica() {
		return new orm.Horamedica();
	}
	
	public static boolean save(orm.Horamedica horamedica) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().saveObject(horamedica);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(orm.Horamedica horamedica) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().deleteObject(horamedica);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Horamedica horamedica)throws PersistentException {
		try {
			if (horamedica.getProfesional() != null) {
				horamedica.getProfesional().horamedica.remove(horamedica);
			}
			
			if (horamedica.getAdministrador() != null) {
				horamedica.getAdministrador().horamedica.remove(horamedica);
			}
			
			if (horamedica.getReserva() != null) {
				horamedica.getReserva().setHora_medica(null);
			}
			
			orm.Hora_examen_solicitado[] lHora_examen_solicitados = horamedica.hora_examen_solicitado.toArray();
			for(int i = 0; i < lHora_examen_solicitados.length; i++) {
				lHora_examen_solicitados[i].setHoramedica(null);
			}
			return delete(horamedica);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Horamedica horamedica, org.orm.PersistentSession session)throws PersistentException {
		try {
			if (horamedica.getProfesional() != null) {
				horamedica.getProfesional().horamedica.remove(horamedica);
			}
			
			if (horamedica.getAdministrador() != null) {
				horamedica.getAdministrador().horamedica.remove(horamedica);
			}
			
			if (horamedica.getReserva() != null) {
				horamedica.getReserva().setHora_medica(null);
			}
			
			orm.Hora_examen_solicitado[] lHora_examen_solicitados = horamedica.hora_examen_solicitado.toArray();
			for(int i = 0; i < lHora_examen_solicitados.length; i++) {
				lHora_examen_solicitados[i].setHoramedica(null);
			}
			try {
				session.delete(horamedica);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(orm.Horamedica horamedica) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().getSession().refresh(horamedica);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(orm.Horamedica horamedica) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().getSession().evict(horamedica);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Horamedica loadHoramedicaByCriteria(HoramedicaCriteria horamedicaCriteria) {
		Horamedica[] horamedicas = listHoramedicaByCriteria(horamedicaCriteria);
		if(horamedicas == null || horamedicas.length == 0) {
			return null;
		}
		return horamedicas[0];
	}
	
	public static Horamedica[] listHoramedicaByCriteria(HoramedicaCriteria horamedicaCriteria) {
		return horamedicaCriteria.listHoramedica();
	}
}
