/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Hora_examen_solicitado {
	public Hora_examen_solicitado() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_HORA_EXAMEN_SOLICITADO_HORAMEDICA) {
			this.horamedica = (orm.Horamedica) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_HORA_EXAMEN_SOLICITADO_EXAMEN) {
			this.examen = (orm.Examen) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private orm.Horamedica horamedica;
	
	private orm.Examen examen;
	
	/**
	 * Clave Primaria
	 */
	public void setId(int value) {
		this.id = value;
	}
	
	/**
	 * Clave Primaria
	 */
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setHoramedica(orm.Horamedica value) {
		if (horamedica != null) {
			horamedica.hora_examen_solicitado.remove(this);
		}
		if (value != null) {
			value.hora_examen_solicitado.add(this);
		}
	}
	
	public orm.Horamedica getHoramedica() {
		return horamedica;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Horamedica(orm.Horamedica value) {
		this.horamedica = value;
	}
	
	private orm.Horamedica getORM_Horamedica() {
		return horamedica;
	}
	
	public void setExamen(orm.Examen value) {
		if (examen != null) {
			examen.hora_examen_solicitado.remove(this);
		}
		if (value != null) {
			value.hora_examen_solicitado.add(this);
		}
	}
	
	public orm.Examen getExamen() {
		return examen;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Examen(orm.Examen value) {
		this.examen = value;
	}
	
	private orm.Examen getORM_Examen() {
		return examen;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
