/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class PagoDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final IntegerExpression medio_pagoId;
	public final AssociationExpression medio_pago;
	public final IntegerExpression rce_examenId;
	public final AssociationExpression rce_examen;
	public final DoubleExpression monto;
	public final DoubleExpression comprobante;
	public final IntegerExpression estadopago;
	public final FloatExpression subsidio;
	public final CollectionExpression bono;
	
	public PagoDetachedCriteria() {
		super(orm.Pago.class, orm.PagoCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		medio_pagoId = new IntegerExpression("medio_pago.id", this.getDetachedCriteria());
		medio_pago = new AssociationExpression("medio_pago", this.getDetachedCriteria());
		rce_examenId = new IntegerExpression("rce_examen.id", this.getDetachedCriteria());
		rce_examen = new AssociationExpression("rce_examen", this.getDetachedCriteria());
		monto = new DoubleExpression("monto", this.getDetachedCriteria());
		comprobante = new DoubleExpression("comprobante", this.getDetachedCriteria());
		estadopago = new IntegerExpression("estadopago", this.getDetachedCriteria());
		subsidio = new FloatExpression("subsidio", this.getDetachedCriteria());
		bono = new CollectionExpression("ORM_Bono", this.getDetachedCriteria());
	}
	
	public PagoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.PagoCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		medio_pagoId = new IntegerExpression("medio_pago.id", this.getDetachedCriteria());
		medio_pago = new AssociationExpression("medio_pago", this.getDetachedCriteria());
		rce_examenId = new IntegerExpression("rce_examen.id", this.getDetachedCriteria());
		rce_examen = new AssociationExpression("rce_examen", this.getDetachedCriteria());
		monto = new DoubleExpression("monto", this.getDetachedCriteria());
		comprobante = new DoubleExpression("comprobante", this.getDetachedCriteria());
		estadopago = new IntegerExpression("estadopago", this.getDetachedCriteria());
		subsidio = new FloatExpression("subsidio", this.getDetachedCriteria());
		bono = new CollectionExpression("ORM_Bono", this.getDetachedCriteria());
	}
	
	public Medio_pagoDetachedCriteria createMedio_pagoCriteria() {
		return new Medio_pagoDetachedCriteria(createCriteria("medio_pago"));
	}
	
	public RceDetachedCriteria createRce_examenCriteria() {
		return new RceDetachedCriteria(createCriteria("rce_examen"));
	}
	
	public BonoDetachedCriteria createBonoCriteria() {
		return new BonoDetachedCriteria(createCriteria("ORM_Bono"));
	}
	
	public Pago uniquePago(PersistentSession session) {
		return (Pago) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Pago[] listPago(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Pago[]) list.toArray(new Pago[list.size()]);
	}
}

