/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class RceDAO {
	public static Rce loadRceByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadRceByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce getRceByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return getRceByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce loadRceByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadRceByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce getRceByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return getRceByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce loadRceByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Rce) session.load(orm.Rce.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce getRceByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Rce) session.get(orm.Rce.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce loadRceByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Rce) session.load(orm.Rce.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce getRceByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Rce) session.get(orm.Rce.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRce(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return queryRce(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRce(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return queryRce(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce[] listRceByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return listRceByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce[] listRceByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return listRceByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRce(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Rce as Rce");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRce(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Rce as Rce");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Rce", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce[] listRceByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryRce(session, condition, orderBy);
			return (Rce[]) list.toArray(new Rce[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce[] listRceByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryRce(session, condition, orderBy, lockMode);
			return (Rce[]) list.toArray(new Rce[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce loadRceByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadRceByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce loadRceByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadRceByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce loadRceByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Rce[] rces = listRceByQuery(session, condition, orderBy);
		if (rces != null && rces.length > 0)
			return rces[0];
		else
			return null;
	}
	
	public static Rce loadRceByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Rce[] rces = listRceByQuery(session, condition, orderBy, lockMode);
		if (rces != null && rces.length > 0)
			return rces[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateRceByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return iterateRceByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateRceByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return iterateRceByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateRceByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Rce as Rce");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateRceByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Rce as Rce");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Rce", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce createRce() {
		return new orm.Rce();
	}
	
	public static boolean save(orm.Rce rce) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().saveObject(rce);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(orm.Rce rce) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().deleteObject(rce);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Rce rce)throws PersistentException {
		try {
			if (rce.getProcedencia() != null) {
				rce.getProcedencia().rce.remove(rce);
			}
			
			if (rce.getReserva() != null) {
				rce.getReserva().setRce(null);
			}
			
			if (rce.getPersona_id_realiza_examen() != null) {
				rce.getPersona_id_realiza_examen().rce.remove(rce);
			}
			
			if (rce.getBox() != null) {
				rce.getBox().rce.remove(rce);
			}
			
			if (rce.getDestino() != null) {
				rce.getDestino().rce.remove(rce);
			}
			
			orm.Consulta_hce[] lConsulta_hces = rce.consulta_hce.toArray();
			for(int i = 0; i < lConsulta_hces.length; i++) {
				lConsulta_hces[i].rce.remove(rce);
			}
			orm.Pago[] lPagos = rce.pago.toArray();
			for(int i = 0; i < lPagos.length; i++) {
				lPagos[i].setRce_examen(null);
			}
			orm.Rce_examen_examen[] lRce_examen_examens = rce.rce_examen_examen.toArray();
			for(int i = 0; i < lRce_examen_examens.length; i++) {
				lRce_examen_examens[i].setRce(null);
			}
			orm.Bitacora[] lBitacoras = rce.bitacora.toArray();
			for(int i = 0; i < lBitacoras.length; i++) {
				lBitacoras[i].setRce(null);
			}
			orm.Rce_diagnostico[] lRce_diagnosticos = rce.rce_diagnostico.toArray();
			for(int i = 0; i < lRce_diagnosticos.length; i++) {
				lRce_diagnosticos[i].setRce(null);
			}
			orm.Alergias[] lAlergiass = rce.alergias.toArray();
			for(int i = 0; i < lAlergiass.length; i++) {
				lAlergiass[i].setRce(null);
			}
			orm.Recetarce[] lRecetarces = rce.recetarce.toArray();
			for(int i = 0; i < lRecetarces.length; i++) {
				lRecetarces[i].setRce(null);
			}
			orm.Rce_insumo[] lRce_insumos = rce.rce_insumo.toArray();
			for(int i = 0; i < lRce_insumos.length; i++) {
				lRce_insumos[i].setRce(null);
			}
			return delete(rce);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Rce rce, org.orm.PersistentSession session)throws PersistentException {
		try {
			if (rce.getProcedencia() != null) {
				rce.getProcedencia().rce.remove(rce);
			}
			
			if (rce.getReserva() != null) {
				rce.getReserva().setRce(null);
			}
			
			if (rce.getPersona_id_realiza_examen() != null) {
				rce.getPersona_id_realiza_examen().rce.remove(rce);
			}
			
			if (rce.getBox() != null) {
				rce.getBox().rce.remove(rce);
			}
			
			if (rce.getDestino() != null) {
				rce.getDestino().rce.remove(rce);
			}
			
			orm.Consulta_hce[] lConsulta_hces = rce.consulta_hce.toArray();
			for(int i = 0; i < lConsulta_hces.length; i++) {
				lConsulta_hces[i].rce.remove(rce);
			}
			orm.Pago[] lPagos = rce.pago.toArray();
			for(int i = 0; i < lPagos.length; i++) {
				lPagos[i].setRce_examen(null);
			}
			orm.Rce_examen_examen[] lRce_examen_examens = rce.rce_examen_examen.toArray();
			for(int i = 0; i < lRce_examen_examens.length; i++) {
				lRce_examen_examens[i].setRce(null);
			}
			orm.Bitacora[] lBitacoras = rce.bitacora.toArray();
			for(int i = 0; i < lBitacoras.length; i++) {
				lBitacoras[i].setRce(null);
			}
			orm.Rce_diagnostico[] lRce_diagnosticos = rce.rce_diagnostico.toArray();
			for(int i = 0; i < lRce_diagnosticos.length; i++) {
				lRce_diagnosticos[i].setRce(null);
			}
			orm.Alergias[] lAlergiass = rce.alergias.toArray();
			for(int i = 0; i < lAlergiass.length; i++) {
				lAlergiass[i].setRce(null);
			}
			orm.Recetarce[] lRecetarces = rce.recetarce.toArray();
			for(int i = 0; i < lRecetarces.length; i++) {
				lRecetarces[i].setRce(null);
			}
			orm.Rce_insumo[] lRce_insumos = rce.rce_insumo.toArray();
			for(int i = 0; i < lRce_insumos.length; i++) {
				lRce_insumos[i].setRce(null);
			}
			try {
				session.delete(rce);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(orm.Rce rce) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().getSession().refresh(rce);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(orm.Rce rce) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().getSession().evict(rce);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce loadRceByCriteria(RceCriteria rceCriteria) {
		Rce[] rces = listRceByCriteria(rceCriteria);
		if(rces == null || rces.length == 0) {
			return null;
		}
		return rces[0];
	}
	
	public static Rce[] listRceByCriteria(RceCriteria rceCriteria) {
		return rceCriteria.listRce();
	}
}
