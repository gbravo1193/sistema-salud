/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Tipo_previsionCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression descripcion;
	public final CollectionExpression paciente;
	public final CollectionExpression bono;
	
	public Tipo_previsionCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		descripcion = new StringExpression("descripcion", this);
		paciente = new CollectionExpression("ORM_Paciente", this);
		bono = new CollectionExpression("ORM_Bono", this);
	}
	
	public Tipo_previsionCriteria(PersistentSession session) {
		this(session.createCriteria(Tipo_prevision.class));
	}
	
	public Tipo_previsionCriteria() throws PersistentException {
		this(orm.SaludPersistentManager.instance().getSession());
	}
	
	public PacienteCriteria createPacienteCriteria() {
		return new PacienteCriteria(createCriteria("ORM_Paciente"));
	}
	
	public BonoCriteria createBonoCriteria() {
		return new BonoCriteria(createCriteria("ORM_Bono"));
	}
	
	public Tipo_prevision uniqueTipo_prevision() {
		return (Tipo_prevision) super.uniqueResult();
	}
	
	public Tipo_prevision[] listTipo_prevision() {
		java.util.List list = super.list();
		return (Tipo_prevision[]) list.toArray(new Tipo_prevision[list.size()]);
	}
}

