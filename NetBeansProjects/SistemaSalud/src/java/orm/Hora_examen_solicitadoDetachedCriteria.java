/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Hora_examen_solicitadoDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final IntegerExpression horamedicaId;
	public final AssociationExpression horamedica;
	public final IntegerExpression examenId;
	public final AssociationExpression examen;
	
	public Hora_examen_solicitadoDetachedCriteria() {
		super(orm.Hora_examen_solicitado.class, orm.Hora_examen_solicitadoCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		horamedicaId = new IntegerExpression("horamedica.id", this.getDetachedCriteria());
		horamedica = new AssociationExpression("horamedica", this.getDetachedCriteria());
		examenId = new IntegerExpression("examen.id", this.getDetachedCriteria());
		examen = new AssociationExpression("examen", this.getDetachedCriteria());
	}
	
	public Hora_examen_solicitadoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.Hora_examen_solicitadoCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		horamedicaId = new IntegerExpression("horamedica.id", this.getDetachedCriteria());
		horamedica = new AssociationExpression("horamedica", this.getDetachedCriteria());
		examenId = new IntegerExpression("examen.id", this.getDetachedCriteria());
		examen = new AssociationExpression("examen", this.getDetachedCriteria());
	}
	
	public HoramedicaDetachedCriteria createHoramedicaCriteria() {
		return new HoramedicaDetachedCriteria(createCriteria("horamedica"));
	}
	
	public ExamenDetachedCriteria createExamenCriteria() {
		return new ExamenDetachedCriteria(createCriteria("examen"));
	}
	
	public Hora_examen_solicitado uniqueHora_examen_solicitado(PersistentSession session) {
		return (Hora_examen_solicitado) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Hora_examen_solicitado[] listHora_examen_solicitado(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Hora_examen_solicitado[]) list.toArray(new Hora_examen_solicitado[list.size()]);
	}
}

