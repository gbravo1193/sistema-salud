/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class RecetarceDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final IntegerExpression rceId;
	public final AssociationExpression rce;
	public final IntegerExpression recetaId;
	public final AssociationExpression receta;
	
	public RecetarceDetachedCriteria() {
		super(orm.Recetarce.class, orm.RecetarceCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		rceId = new IntegerExpression("rce.id", this.getDetachedCriteria());
		rce = new AssociationExpression("rce", this.getDetachedCriteria());
		recetaId = new IntegerExpression("receta.id", this.getDetachedCriteria());
		receta = new AssociationExpression("receta", this.getDetachedCriteria());
	}
	
	public RecetarceDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.RecetarceCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		rceId = new IntegerExpression("rce.id", this.getDetachedCriteria());
		rce = new AssociationExpression("rce", this.getDetachedCriteria());
		recetaId = new IntegerExpression("receta.id", this.getDetachedCriteria());
		receta = new AssociationExpression("receta", this.getDetachedCriteria());
	}
	
	public RceDetachedCriteria createRceCriteria() {
		return new RceDetachedCriteria(createCriteria("rce"));
	}
	
	public RecetaDetachedCriteria createRecetaCriteria() {
		return new RecetaDetachedCriteria(createCriteria("receta"));
	}
	
	public Recetarce uniqueRecetarce(PersistentSession session) {
		return (Recetarce) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Recetarce[] listRecetarce(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Recetarce[]) list.toArray(new Recetarce[list.size()]);
	}
}

