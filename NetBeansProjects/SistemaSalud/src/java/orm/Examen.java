/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Examen {
	public Examen() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_EXAMEN_HORA_EXAMEN_SOLICITADO) {
			return ORM_hora_examen_solicitado;
		}
		else if (key == orm.ORMConstants.KEY_EXAMEN_RCE_EXAMEN_EXAMEN) {
			return ORM_rce_examen_examen;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private int id;
	
	private String descripcion;
	
	private Double monto;
	
	private String observaciones;
	
	private java.util.Set ORM_hora_examen_solicitado = new java.util.HashSet();
	
	private java.util.Set ORM_rce_examen_examen = new java.util.HashSet();
	
	/**
	 * Clave primaria
	 */
	public void setId(int value) {
		this.id = value;
	}
	
	/**
	 * Clave primaria
	 */
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	/**
	 * Descripcion
	 */
	public void setDescripcion(String value) {
		this.descripcion = value;
	}
	
	/**
	 * Descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	
	/**
	 * Monto Examen
	 */
	public void setMonto(double value) {
		setMonto(new Double(value));
	}
	
	/**
	 * Monto Examen
	 */
	public void setMonto(Double value) {
		this.monto = value;
	}
	
	/**
	 * Monto Examen
	 */
	public Double getMonto() {
		return monto;
	}
	
	/**
	 * Observaciones para realizar el examen
	 */
	public void setObservaciones(String value) {
		this.observaciones = value;
	}
	
	/**
	 * Observaciones para realizar el examen
	 */
	public String getObservaciones() {
		return observaciones;
	}
	
	private void setORM_Hora_examen_solicitado(java.util.Set value) {
		this.ORM_hora_examen_solicitado = value;
	}
	
	private java.util.Set getORM_Hora_examen_solicitado() {
		return ORM_hora_examen_solicitado;
	}
	
	public final orm.Hora_examen_solicitadoSetCollection hora_examen_solicitado = new orm.Hora_examen_solicitadoSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_EXAMEN_HORA_EXAMEN_SOLICITADO, orm.ORMConstants.KEY_HORA_EXAMEN_SOLICITADO_EXAMEN, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Rce_examen_examen(java.util.Set value) {
		this.ORM_rce_examen_examen = value;
	}
	
	private java.util.Set getORM_Rce_examen_examen() {
		return ORM_rce_examen_examen;
	}
	
	public final orm.Rce_examen_examenSetCollection rce_examen_examen = new orm.Rce_examen_examenSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_EXAMEN_RCE_EXAMEN_EXAMEN, orm.ORMConstants.KEY_RCE_EXAMEN_EXAMEN_EXAMEN, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
