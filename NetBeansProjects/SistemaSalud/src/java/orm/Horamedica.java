/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Horamedica {
	public Horamedica() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_HORAMEDICA_HORA_EXAMEN_SOLICITADO) {
			return ORM_hora_examen_solicitado;
		}
		
		return null;
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_HORAMEDICA_PROFESIONAL) {
			this.profesional = (orm.Profesional) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_HORAMEDICA_ADMINISTRADOR) {
			this.administrador = (orm.Administrador) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_HORAMEDICA_RESERVA) {
			this.reserva = (orm.Reserva) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private orm.Profesional profesional;
	
	private orm.Administrador administrador;
	
	private java.sql.Timestamp hora_inicio;
	
	private Integer tiempo_periodo = new Integer(15);
	
	private orm.Reserva reserva;
	
	private java.util.Set ORM_hora_examen_solicitado = new java.util.HashSet();
	
	public void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setHora_inicio(java.sql.Timestamp value) {
		this.hora_inicio = value;
	}
	
	public java.sql.Timestamp getHora_inicio() {
		return hora_inicio;
	}
	
	public void setTiempo_periodo(int value) {
		setTiempo_periodo(new Integer(value));
	}
	
	public void setTiempo_periodo(Integer value) {
		this.tiempo_periodo = value;
	}
	
	public Integer getTiempo_periodo() {
		return tiempo_periodo;
	}
	
	public void setProfesional(orm.Profesional value) {
		if (profesional != null) {
			profesional.horamedica.remove(this);
		}
		if (value != null) {
			value.horamedica.add(this);
		}
	}
	
	public orm.Profesional getProfesional() {
		return profesional;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Profesional(orm.Profesional value) {
		this.profesional = value;
	}
	
	private orm.Profesional getORM_Profesional() {
		return profesional;
	}
	
	public void setAdministrador(orm.Administrador value) {
		if (administrador != null) {
			administrador.horamedica.remove(this);
		}
		if (value != null) {
			value.horamedica.add(this);
		}
	}
	
	public orm.Administrador getAdministrador() {
		return administrador;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Administrador(orm.Administrador value) {
		this.administrador = value;
	}
	
	private orm.Administrador getORM_Administrador() {
		return administrador;
	}
	
	public void setReserva(orm.Reserva value) {
		if (this.reserva != value) {
			orm.Reserva lreserva = this.reserva;
			this.reserva = value;
			if (value != null) {
				reserva.setHora_medica(this);
			}
			if (lreserva != null && lreserva.getHora_medica() == this) {
				lreserva.setHora_medica(null);
			}
		}
	}
	
	public orm.Reserva getReserva() {
		return reserva;
	}
	
	private void setORM_Hora_examen_solicitado(java.util.Set value) {
		this.ORM_hora_examen_solicitado = value;
	}
	
	private java.util.Set getORM_Hora_examen_solicitado() {
		return ORM_hora_examen_solicitado;
	}
	
	public final orm.Hora_examen_solicitadoSetCollection hora_examen_solicitado = new orm.Hora_examen_solicitadoSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_HORAMEDICA_HORA_EXAMEN_SOLICITADO, orm.ORMConstants.KEY_HORA_EXAMEN_SOLICITADO_HORAMEDICA, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
