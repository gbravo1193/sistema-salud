/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class DestinoDAO {
	public static Destino loadDestinoByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadDestinoByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Destino getDestinoByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return getDestinoByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Destino loadDestinoByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadDestinoByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Destino getDestinoByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return getDestinoByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Destino loadDestinoByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Destino) session.load(orm.Destino.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Destino getDestinoByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Destino) session.get(orm.Destino.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Destino loadDestinoByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Destino) session.load(orm.Destino.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Destino getDestinoByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Destino) session.get(orm.Destino.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryDestino(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return queryDestino(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryDestino(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return queryDestino(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Destino[] listDestinoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return listDestinoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Destino[] listDestinoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return listDestinoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryDestino(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Destino as Destino");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryDestino(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Destino as Destino");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Destino", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Destino[] listDestinoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryDestino(session, condition, orderBy);
			return (Destino[]) list.toArray(new Destino[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Destino[] listDestinoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryDestino(session, condition, orderBy, lockMode);
			return (Destino[]) list.toArray(new Destino[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Destino loadDestinoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadDestinoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Destino loadDestinoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadDestinoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Destino loadDestinoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Destino[] destinos = listDestinoByQuery(session, condition, orderBy);
		if (destinos != null && destinos.length > 0)
			return destinos[0];
		else
			return null;
	}
	
	public static Destino loadDestinoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Destino[] destinos = listDestinoByQuery(session, condition, orderBy, lockMode);
		if (destinos != null && destinos.length > 0)
			return destinos[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateDestinoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return iterateDestinoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateDestinoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return iterateDestinoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateDestinoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Destino as Destino");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateDestinoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Destino as Destino");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Destino", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Destino createDestino() {
		return new orm.Destino();
	}
	
	public static boolean save(orm.Destino destino) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().saveObject(destino);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(orm.Destino destino) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().deleteObject(destino);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Destino destino)throws PersistentException {
		try {
			orm.Rce[] lRces = destino.rce.toArray();
			for(int i = 0; i < lRces.length; i++) {
				lRces[i].setDestino(null);
			}
			return delete(destino);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Destino destino, org.orm.PersistentSession session)throws PersistentException {
		try {
			orm.Rce[] lRces = destino.rce.toArray();
			for(int i = 0; i < lRces.length; i++) {
				lRces[i].setDestino(null);
			}
			try {
				session.delete(destino);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(orm.Destino destino) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().getSession().refresh(destino);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(orm.Destino destino) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().getSession().evict(destino);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Destino loadDestinoByCriteria(DestinoCriteria destinoCriteria) {
		Destino[] destinos = listDestinoByCriteria(destinoCriteria);
		if(destinos == null || destinos.length == 0) {
			return null;
		}
		return destinos[0];
	}
	
	public static Destino[] listDestinoByCriteria(DestinoCriteria destinoCriteria) {
		return destinoCriteria.listDestino();
	}
}
