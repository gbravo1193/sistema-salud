/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class PersonaDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final StringExpression rut;
	public final StringExpression nombres;
	public final StringExpression apellido_paterno;
	public final StringExpression apellido_materno;
	public final StringExpression email;
	public final DateExpression fecha_nacimiento;
	public final StringExpression identificadorfacebook;
	public final IntegerExpression activo;
	public final StringExpression password;
	public final IntegerExpression profesionalId;
	public final AssociationExpression profesional;
	public final IntegerExpression pacienteId;
	public final AssociationExpression paciente;
	public final IntegerExpression administradorId;
	public final AssociationExpression administrador;
	public final CollectionExpression reserva;
	public final CollectionExpression consulta_hce;
	public final CollectionExpression consulta_hce1;
	public final CollectionExpression rce;
	public final CollectionExpression bitacora;
	
	public PersonaDetachedCriteria() {
		super(orm.Persona.class, orm.PersonaCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		rut = new StringExpression("rut", this.getDetachedCriteria());
		nombres = new StringExpression("nombres", this.getDetachedCriteria());
		apellido_paterno = new StringExpression("apellido_paterno", this.getDetachedCriteria());
		apellido_materno = new StringExpression("apellido_materno", this.getDetachedCriteria());
		email = new StringExpression("email", this.getDetachedCriteria());
		fecha_nacimiento = new DateExpression("fecha_nacimiento", this.getDetachedCriteria());
		identificadorfacebook = new StringExpression("identificadorfacebook", this.getDetachedCriteria());
		activo = new IntegerExpression("activo", this.getDetachedCriteria());
		password = new StringExpression("password", this.getDetachedCriteria());
		profesionalId = new IntegerExpression("profesional.id", this.getDetachedCriteria());
		profesional = new AssociationExpression("profesional", this.getDetachedCriteria());
		pacienteId = new IntegerExpression("paciente.id", this.getDetachedCriteria());
		paciente = new AssociationExpression("paciente", this.getDetachedCriteria());
		administradorId = new IntegerExpression("administrador.id", this.getDetachedCriteria());
		administrador = new AssociationExpression("administrador", this.getDetachedCriteria());
		reserva = new CollectionExpression("ORM_Reserva", this.getDetachedCriteria());
		consulta_hce = new CollectionExpression("ORM_Consulta_hce", this.getDetachedCriteria());
		consulta_hce1 = new CollectionExpression("ORM_Consulta_hce1", this.getDetachedCriteria());
		rce = new CollectionExpression("ORM_Rce", this.getDetachedCriteria());
		bitacora = new CollectionExpression("ORM_Bitacora", this.getDetachedCriteria());
	}
	
	public PersonaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.PersonaCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		rut = new StringExpression("rut", this.getDetachedCriteria());
		nombres = new StringExpression("nombres", this.getDetachedCriteria());
		apellido_paterno = new StringExpression("apellido_paterno", this.getDetachedCriteria());
		apellido_materno = new StringExpression("apellido_materno", this.getDetachedCriteria());
		email = new StringExpression("email", this.getDetachedCriteria());
		fecha_nacimiento = new DateExpression("fecha_nacimiento", this.getDetachedCriteria());
		identificadorfacebook = new StringExpression("identificadorfacebook", this.getDetachedCriteria());
		activo = new IntegerExpression("activo", this.getDetachedCriteria());
		password = new StringExpression("password", this.getDetachedCriteria());
		profesionalId = new IntegerExpression("profesional.id", this.getDetachedCriteria());
		profesional = new AssociationExpression("profesional", this.getDetachedCriteria());
		pacienteId = new IntegerExpression("paciente.id", this.getDetachedCriteria());
		paciente = new AssociationExpression("paciente", this.getDetachedCriteria());
		administradorId = new IntegerExpression("administrador.id", this.getDetachedCriteria());
		administrador = new AssociationExpression("administrador", this.getDetachedCriteria());
		reserva = new CollectionExpression("ORM_Reserva", this.getDetachedCriteria());
		consulta_hce = new CollectionExpression("ORM_Consulta_hce", this.getDetachedCriteria());
		consulta_hce1 = new CollectionExpression("ORM_Consulta_hce1", this.getDetachedCriteria());
		rce = new CollectionExpression("ORM_Rce", this.getDetachedCriteria());
		bitacora = new CollectionExpression("ORM_Bitacora", this.getDetachedCriteria());
	}
	
	public ProfesionalDetachedCriteria createProfesionalCriteria() {
		return new ProfesionalDetachedCriteria(createCriteria("profesional"));
	}
	
	public PacienteDetachedCriteria createPacienteCriteria() {
		return new PacienteDetachedCriteria(createCriteria("paciente"));
	}
	
	public AdministradorDetachedCriteria createAdministradorCriteria() {
		return new AdministradorDetachedCriteria(createCriteria("administrador"));
	}
	
	public ReservaDetachedCriteria createReservaCriteria() {
		return new ReservaDetachedCriteria(createCriteria("ORM_Reserva"));
	}
	
	public Consulta_hceDetachedCriteria createConsulta_hceCriteria() {
		return new Consulta_hceDetachedCriteria(createCriteria("ORM_Consulta_hce"));
	}
	
	public Consulta_hceDetachedCriteria createConsulta_hce1Criteria() {
		return new Consulta_hceDetachedCriteria(createCriteria("ORM_Consulta_hce1"));
	}
	
	public RceDetachedCriteria createRceCriteria() {
		return new RceDetachedCriteria(createCriteria("ORM_Rce"));
	}
	
	public BitacoraDetachedCriteria createBitacoraCriteria() {
		return new BitacoraDetachedCriteria(createCriteria("ORM_Bitacora"));
	}
	
	public Persona uniquePersona(PersistentSession session) {
		return (Persona) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Persona[] listPersona(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Persona[]) list.toArray(new Persona[list.size()]);
	}
}

