/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Tipo_previsionDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final StringExpression descripcion;
	public final CollectionExpression paciente;
	public final CollectionExpression bono;
	
	public Tipo_previsionDetachedCriteria() {
		super(orm.Tipo_prevision.class, orm.Tipo_previsionCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		descripcion = new StringExpression("descripcion", this.getDetachedCriteria());
		paciente = new CollectionExpression("ORM_Paciente", this.getDetachedCriteria());
		bono = new CollectionExpression("ORM_Bono", this.getDetachedCriteria());
	}
	
	public Tipo_previsionDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.Tipo_previsionCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		descripcion = new StringExpression("descripcion", this.getDetachedCriteria());
		paciente = new CollectionExpression("ORM_Paciente", this.getDetachedCriteria());
		bono = new CollectionExpression("ORM_Bono", this.getDetachedCriteria());
	}
	
	public PacienteDetachedCriteria createPacienteCriteria() {
		return new PacienteDetachedCriteria(createCriteria("ORM_Paciente"));
	}
	
	public BonoDetachedCriteria createBonoCriteria() {
		return new BonoDetachedCriteria(createCriteria("ORM_Bono"));
	}
	
	public Tipo_prevision uniqueTipo_prevision(PersistentSession session) {
		return (Tipo_prevision) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Tipo_prevision[] listTipo_prevision(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Tipo_prevision[]) list.toArray(new Tipo_prevision[list.size()]);
	}
}

