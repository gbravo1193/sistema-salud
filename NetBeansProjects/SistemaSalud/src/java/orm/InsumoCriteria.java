/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class InsumoCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression nombre;
	public final FloatExpression monto_pmp;
	public final StringExpression codigo;
	public final CollectionExpression rce_insumo;
	
	public InsumoCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		nombre = new StringExpression("nombre", this);
		monto_pmp = new FloatExpression("monto_pmp", this);
		codigo = new StringExpression("codigo", this);
		rce_insumo = new CollectionExpression("ORM_Rce_insumo", this);
	}
	
	public InsumoCriteria(PersistentSession session) {
		this(session.createCriteria(Insumo.class));
	}
	
	public InsumoCriteria() throws PersistentException {
		this(orm.SaludPersistentManager.instance().getSession());
	}
	
	public Rce_insumoCriteria createRce_insumoCriteria() {
		return new Rce_insumoCriteria(createCriteria("ORM_Rce_insumo"));
	}
	
	public Insumo uniqueInsumo() {
		return (Insumo) super.uniqueResult();
	}
	
	public Insumo[] listInsumo() {
		java.util.List list = super.list();
		return (Insumo[]) list.toArray(new Insumo[list.size()]);
	}
}

