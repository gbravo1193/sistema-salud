/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Rce {
	public Rce() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_RCE_CONSULTA_HCE) {
			return ORM_consulta_hce;
		}
		else if (key == orm.ORMConstants.KEY_RCE_PAGO) {
			return ORM_pago;
		}
		else if (key == orm.ORMConstants.KEY_RCE_RCE_EXAMEN_EXAMEN) {
			return ORM_rce_examen_examen;
		}
		else if (key == orm.ORMConstants.KEY_RCE_BITACORA) {
			return ORM_bitacora;
		}
		else if (key == orm.ORMConstants.KEY_RCE_RCE_DIAGNOSTICO) {
			return ORM_rce_diagnostico;
		}
		else if (key == orm.ORMConstants.KEY_RCE_ALERGIAS) {
			return ORM_alergias;
		}
		else if (key == orm.ORMConstants.KEY_RCE_RECETARCE) {
			return ORM_recetarce;
		}
		else if (key == orm.ORMConstants.KEY_RCE_RCE_INSUMO) {
			return ORM_rce_insumo;
		}
		
		return null;
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_RCE_PROCEDENCIA) {
			this.procedencia = (orm.Procedencia) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_RCE_RESERVA) {
			this.reserva = (orm.Reserva) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_RCE_PERSONA_ID_REALIZA_EXAMEN) {
			this.persona_id_realiza_examen = (orm.Persona) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_RCE_BOX) {
			this.box = (orm.Box) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_RCE_DESTINO) {
			this.destino = (orm.Destino) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private orm.Procedencia procedencia;
	
	private orm.Reserva reserva;
	
	private orm.Persona persona_id_realiza_examen;
	
	private orm.Box box;
	
	private orm.Destino destino;
	
	private int cerrado = 0;
	
	private String indicaciones;
	
	private String observaciones;
	
	private String anamnesis;
	
	private java.util.Date fechacierre;
	
	private Integer tipocierre;
	
	private java.util.Set ORM_consulta_hce = new java.util.HashSet();
	
	private java.util.Set ORM_pago = new java.util.HashSet();
	
	private java.util.Set ORM_rce_examen_examen = new java.util.HashSet();
	
	private java.util.Set ORM_bitacora = new java.util.HashSet();
	
	private java.util.Set ORM_rce_diagnostico = new java.util.HashSet();
	
	private java.util.Set ORM_alergias = new java.util.HashSet();
	
	private java.util.Set ORM_recetarce = new java.util.HashSet();
	
	private java.util.Set ORM_rce_insumo = new java.util.HashSet();
	
	/**
	 * Clave Primaria
	 */
	public void setId(int value) {
		this.id = value;
	}
	
	/**
	 * Clave Primaria
	 */
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	/**
	 * Indica si el RCE esta cerrado para edicion (1:SI 0:No)
	 */
	public void setCerrado(int value) {
		this.cerrado = value;
	}
	
	/**
	 * Indica si el RCE esta cerrado para edicion (1:SI 0:No)
	 */
	public int getCerrado() {
		return cerrado;
	}
	
	/**
	 * Indicaciones Medicas
	 */
	public void setIndicaciones(String value) {
		this.indicaciones = value;
	}
	
	/**
	 * Indicaciones Medicas
	 */
	public String getIndicaciones() {
		return indicaciones;
	}
	
	public void setObservaciones(String value) {
		this.observaciones = value;
	}
	
	public String getObservaciones() {
		return observaciones;
	}
	
	/**
	 * Anamnesis Medica
	 */
	public void setAnamnesis(String value) {
		this.anamnesis = value;
	}
	
	/**
	 * Anamnesis Medica
	 */
	public String getAnamnesis() {
		return anamnesis;
	}
	
	/**
	 * Fecha de Cierre
	 */
	public void setFechacierre(java.util.Date value) {
		this.fechacierre = value;
	}
	
	/**
	 * Fecha de Cierre
	 */
	public java.util.Date getFechacierre() {
		return fechacierre;
	}
	
	/**
	 * 1: Atendido
	 * 2: No se presento
	 */
	public void setTipocierre(int value) {
		setTipocierre(new Integer(value));
	}
	
	/**
	 * 1: Atendido
	 * 2: No se presento
	 */
	public void setTipocierre(Integer value) {
		this.tipocierre = value;
	}
	
	/**
	 * 1: Atendido
	 * 2: No se presento
	 */
	public Integer getTipocierre() {
		return tipocierre;
	}
	
	private void setORM_Consulta_hce(java.util.Set value) {
		this.ORM_consulta_hce = value;
	}
	
	private java.util.Set getORM_Consulta_hce() {
		return ORM_consulta_hce;
	}
	
	public final orm.Consulta_hceSetCollection consulta_hce = new orm.Consulta_hceSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_RCE_CONSULTA_HCE, orm.ORMConstants.KEY_CONSULTA_HCE_RCE, orm.ORMConstants.KEY_MUL_MANY_TO_MANY);
	
	public void setProcedencia(orm.Procedencia value) {
		if (procedencia != null) {
			procedencia.rce.remove(this);
		}
		if (value != null) {
			value.rce.add(this);
		}
	}
	
	public orm.Procedencia getProcedencia() {
		return procedencia;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Procedencia(orm.Procedencia value) {
		this.procedencia = value;
	}
	
	private orm.Procedencia getORM_Procedencia() {
		return procedencia;
	}
	
	public void setReserva(orm.Reserva value) {
		if (this.reserva != value) {
			orm.Reserva lreserva = this.reserva;
			this.reserva = value;
			if (value != null) {
				reserva.setRce(this);
			}
			if (lreserva != null && lreserva.getRce() == this) {
				lreserva.setRce(null);
			}
		}
	}
	
	public orm.Reserva getReserva() {
		return reserva;
	}
	
	public void setPersona_id_realiza_examen(orm.Persona value) {
		if (persona_id_realiza_examen != null) {
			persona_id_realiza_examen.rce.remove(this);
		}
		if (value != null) {
			value.rce.add(this);
		}
	}
	
	public orm.Persona getPersona_id_realiza_examen() {
		return persona_id_realiza_examen;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Persona_id_realiza_examen(orm.Persona value) {
		this.persona_id_realiza_examen = value;
	}
	
	private orm.Persona getORM_Persona_id_realiza_examen() {
		return persona_id_realiza_examen;
	}
	
	public void setBox(orm.Box value) {
		if (box != null) {
			box.rce.remove(this);
		}
		if (value != null) {
			value.rce.add(this);
		}
	}
	
	public orm.Box getBox() {
		return box;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Box(orm.Box value) {
		this.box = value;
	}
	
	private orm.Box getORM_Box() {
		return box;
	}
	
	public void setDestino(orm.Destino value) {
		if (destino != null) {
			destino.rce.remove(this);
		}
		if (value != null) {
			value.rce.add(this);
		}
	}
	
	public orm.Destino getDestino() {
		return destino;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Destino(orm.Destino value) {
		this.destino = value;
	}
	
	private orm.Destino getORM_Destino() {
		return destino;
	}
	
	private void setORM_Pago(java.util.Set value) {
		this.ORM_pago = value;
	}
	
	private java.util.Set getORM_Pago() {
		return ORM_pago;
	}
	
	public final orm.PagoSetCollection pago = new orm.PagoSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_RCE_PAGO, orm.ORMConstants.KEY_PAGO_RCE_EXAMEN, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Rce_examen_examen(java.util.Set value) {
		this.ORM_rce_examen_examen = value;
	}
	
	private java.util.Set getORM_Rce_examen_examen() {
		return ORM_rce_examen_examen;
	}
	
	public final orm.Rce_examen_examenSetCollection rce_examen_examen = new orm.Rce_examen_examenSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_RCE_RCE_EXAMEN_EXAMEN, orm.ORMConstants.KEY_RCE_EXAMEN_EXAMEN_RCE, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Bitacora(java.util.Set value) {
		this.ORM_bitacora = value;
	}
	
	private java.util.Set getORM_Bitacora() {
		return ORM_bitacora;
	}
	
	public final orm.BitacoraSetCollection bitacora = new orm.BitacoraSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_RCE_BITACORA, orm.ORMConstants.KEY_BITACORA_RCE, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Rce_diagnostico(java.util.Set value) {
		this.ORM_rce_diagnostico = value;
	}
	
	private java.util.Set getORM_Rce_diagnostico() {
		return ORM_rce_diagnostico;
	}
	
	public final orm.Rce_diagnosticoSetCollection rce_diagnostico = new orm.Rce_diagnosticoSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_RCE_RCE_DIAGNOSTICO, orm.ORMConstants.KEY_RCE_DIAGNOSTICO_RCE, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Alergias(java.util.Set value) {
		this.ORM_alergias = value;
	}
	
	private java.util.Set getORM_Alergias() {
		return ORM_alergias;
	}
	
	public final orm.AlergiasSetCollection alergias = new orm.AlergiasSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_RCE_ALERGIAS, orm.ORMConstants.KEY_ALERGIAS_RCE, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Recetarce(java.util.Set value) {
		this.ORM_recetarce = value;
	}
	
	private java.util.Set getORM_Recetarce() {
		return ORM_recetarce;
	}
	
	public final orm.RecetarceSetCollection recetarce = new orm.RecetarceSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_RCE_RECETARCE, orm.ORMConstants.KEY_RECETARCE_RCE, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Rce_insumo(java.util.Set value) {
		this.ORM_rce_insumo = value;
	}
	
	private java.util.Set getORM_Rce_insumo() {
		return ORM_rce_insumo;
	}
	
	public final orm.Rce_insumoSetCollection rce_insumo = new orm.Rce_insumoSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_RCE_RCE_INSUMO, orm.ORMConstants.KEY_RCE_INSUMO_RCE, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
