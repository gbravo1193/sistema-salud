/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class RceCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final IntegerExpression procedenciaId;
	public final AssociationExpression procedencia;
	public final IntegerExpression reservaId;
	public final AssociationExpression reserva;
	public final IntegerExpression persona_id_realiza_examenId;
	public final AssociationExpression persona_id_realiza_examen;
	public final IntegerExpression boxId;
	public final AssociationExpression box;
	public final IntegerExpression destinoId;
	public final AssociationExpression destino;
	public final IntegerExpression cerrado;
	public final StringExpression indicaciones;
	public final StringExpression observaciones;
	public final StringExpression anamnesis;
	public final DateExpression fechacierre;
	public final IntegerExpression tipocierre;
	public final CollectionExpression consulta_hce;
	public final CollectionExpression pago;
	public final CollectionExpression rce_examen_examen;
	public final CollectionExpression bitacora;
	public final CollectionExpression rce_diagnostico;
	public final CollectionExpression alergias;
	public final CollectionExpression recetarce;
	public final CollectionExpression rce_insumo;
	
	public RceCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		procedenciaId = new IntegerExpression("procedencia.id", this);
		procedencia = new AssociationExpression("procedencia", this);
		reservaId = new IntegerExpression("reserva.id", this);
		reserva = new AssociationExpression("reserva", this);
		persona_id_realiza_examenId = new IntegerExpression("persona_id_realiza_examen.id", this);
		persona_id_realiza_examen = new AssociationExpression("persona_id_realiza_examen", this);
		boxId = new IntegerExpression("box.id", this);
		box = new AssociationExpression("box", this);
		destinoId = new IntegerExpression("destino.id", this);
		destino = new AssociationExpression("destino", this);
		cerrado = new IntegerExpression("cerrado", this);
		indicaciones = new StringExpression("indicaciones", this);
		observaciones = new StringExpression("observaciones", this);
		anamnesis = new StringExpression("anamnesis", this);
		fechacierre = new DateExpression("fechacierre", this);
		tipocierre = new IntegerExpression("tipocierre", this);
		consulta_hce = new CollectionExpression("ORM_Consulta_hce", this);
		pago = new CollectionExpression("ORM_Pago", this);
		rce_examen_examen = new CollectionExpression("ORM_Rce_examen_examen", this);
		bitacora = new CollectionExpression("ORM_Bitacora", this);
		rce_diagnostico = new CollectionExpression("ORM_Rce_diagnostico", this);
		alergias = new CollectionExpression("ORM_Alergias", this);
		recetarce = new CollectionExpression("ORM_Recetarce", this);
		rce_insumo = new CollectionExpression("ORM_Rce_insumo", this);
	}
	
	public RceCriteria(PersistentSession session) {
		this(session.createCriteria(Rce.class));
	}
	
	public RceCriteria() throws PersistentException {
		this(orm.SaludPersistentManager.instance().getSession());
	}
	
	public ProcedenciaCriteria createProcedenciaCriteria() {
		return new ProcedenciaCriteria(createCriteria("procedencia"));
	}
	
	public ReservaCriteria createReservaCriteria() {
		return new ReservaCriteria(createCriteria("reserva"));
	}
	
	public PersonaCriteria createPersona_id_realiza_examenCriteria() {
		return new PersonaCriteria(createCriteria("persona_id_realiza_examen"));
	}
	
	public BoxCriteria createBoxCriteria() {
		return new BoxCriteria(createCriteria("box"));
	}
	
	public DestinoCriteria createDestinoCriteria() {
		return new DestinoCriteria(createCriteria("destino"));
	}
	
	public Consulta_hceCriteria createConsulta_hceCriteria() {
		return new Consulta_hceCriteria(createCriteria("ORM_Consulta_hce"));
	}
	
	public PagoCriteria createPagoCriteria() {
		return new PagoCriteria(createCriteria("ORM_Pago"));
	}
	
	public Rce_examen_examenCriteria createRce_examen_examenCriteria() {
		return new Rce_examen_examenCriteria(createCriteria("ORM_Rce_examen_examen"));
	}
	
	public BitacoraCriteria createBitacoraCriteria() {
		return new BitacoraCriteria(createCriteria("ORM_Bitacora"));
	}
	
	public Rce_diagnosticoCriteria createRce_diagnosticoCriteria() {
		return new Rce_diagnosticoCriteria(createCriteria("ORM_Rce_diagnostico"));
	}
	
	public AlergiasCriteria createAlergiasCriteria() {
		return new AlergiasCriteria(createCriteria("ORM_Alergias"));
	}
	
	public RecetarceCriteria createRecetarceCriteria() {
		return new RecetarceCriteria(createCriteria("ORM_Recetarce"));
	}
	
	public Rce_insumoCriteria createRce_insumoCriteria() {
		return new Rce_insumoCriteria(createCriteria("ORM_Rce_insumo"));
	}
	
	public Rce uniqueRce() {
		return (Rce) super.uniqueResult();
	}
	
	public Rce[] listRce() {
		java.util.List list = super.list();
		return (Rce[]) list.toArray(new Rce[list.size()]);
	}
}

