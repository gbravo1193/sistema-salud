/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Recetarce {
	public Recetarce() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_RECETARCE_RCE) {
			this.rce = (orm.Rce) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_RECETARCE_RECETA) {
			this.receta = (orm.Receta) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private orm.Rce rce;
	
	private orm.Receta receta;
	
	/**
	 * Clave Primaria
	 */
	public void setId(int value) {
		this.id = value;
	}
	
	/**
	 * Clave Primaria
	 */
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setRce(orm.Rce value) {
		if (rce != null) {
			rce.recetarce.remove(this);
		}
		if (value != null) {
			value.recetarce.add(this);
		}
	}
	
	public orm.Rce getRce() {
		return rce;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Rce(orm.Rce value) {
		this.rce = value;
	}
	
	private orm.Rce getORM_Rce() {
		return rce;
	}
	
	public void setReceta(orm.Receta value) {
		if (this.receta != value) {
			orm.Receta lreceta = this.receta;
			this.receta = value;
			if (value != null) {
				receta.setRecetarce(this);
			}
			if (lreceta != null && lreceta.getRecetarce() == this) {
				lreceta.setRecetarce(null);
			}
		}
	}
	
	public orm.Receta getReceta() {
		return receta;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
