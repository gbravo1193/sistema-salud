/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Rce_insumoCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final IntegerExpression rceId;
	public final AssociationExpression rce;
	public final IntegerExpression insumoId;
	public final AssociationExpression insumo;
	public final FloatExpression cantidad;
	
	public Rce_insumoCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		rceId = new IntegerExpression("rce.id", this);
		rce = new AssociationExpression("rce", this);
		insumoId = new IntegerExpression("insumo.id", this);
		insumo = new AssociationExpression("insumo", this);
		cantidad = new FloatExpression("cantidad", this);
	}
	
	public Rce_insumoCriteria(PersistentSession session) {
		this(session.createCriteria(Rce_insumo.class));
	}
	
	public Rce_insumoCriteria() throws PersistentException {
		this(orm.SaludPersistentManager.instance().getSession());
	}
	
	public RceCriteria createRceCriteria() {
		return new RceCriteria(createCriteria("rce"));
	}
	
	public InsumoCriteria createInsumoCriteria() {
		return new InsumoCriteria(createCriteria("insumo"));
	}
	
	public Rce_insumo uniqueRce_insumo() {
		return (Rce_insumo) super.uniqueResult();
	}
	
	public Rce_insumo[] listRce_insumo() {
		java.util.List list = super.list();
		return (Rce_insumo[]) list.toArray(new Rce_insumo[list.size()]);
	}
}

