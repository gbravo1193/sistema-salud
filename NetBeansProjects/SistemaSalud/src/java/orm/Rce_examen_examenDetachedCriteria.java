/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Rce_examen_examenDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final IntegerExpression rceId;
	public final AssociationExpression rce;
	public final IntegerExpression examenId;
	public final AssociationExpression examen;
	public final DoubleExpression monto_a_pagar;
	
	public Rce_examen_examenDetachedCriteria() {
		super(orm.Rce_examen_examen.class, orm.Rce_examen_examenCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		rceId = new IntegerExpression("rce.id", this.getDetachedCriteria());
		rce = new AssociationExpression("rce", this.getDetachedCriteria());
		examenId = new IntegerExpression("examen.id", this.getDetachedCriteria());
		examen = new AssociationExpression("examen", this.getDetachedCriteria());
		monto_a_pagar = new DoubleExpression("monto_a_pagar", this.getDetachedCriteria());
	}
	
	public Rce_examen_examenDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.Rce_examen_examenCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		rceId = new IntegerExpression("rce.id", this.getDetachedCriteria());
		rce = new AssociationExpression("rce", this.getDetachedCriteria());
		examenId = new IntegerExpression("examen.id", this.getDetachedCriteria());
		examen = new AssociationExpression("examen", this.getDetachedCriteria());
		monto_a_pagar = new DoubleExpression("monto_a_pagar", this.getDetachedCriteria());
	}
	
	public RceDetachedCriteria createRceCriteria() {
		return new RceDetachedCriteria(createCriteria("rce"));
	}
	
	public ExamenDetachedCriteria createExamenCriteria() {
		return new ExamenDetachedCriteria(createCriteria("examen"));
	}
	
	public Rce_examen_examen uniqueRce_examen_examen(PersistentSession session) {
		return (Rce_examen_examen) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Rce_examen_examen[] listRce_examen_examen(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Rce_examen_examen[]) list.toArray(new Rce_examen_examen[list.size()]);
	}
}

