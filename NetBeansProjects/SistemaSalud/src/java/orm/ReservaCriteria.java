/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ReservaCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final IntegerExpression pacienteId;
	public final AssociationExpression paciente;
	public final IntegerExpression persona_id_ingresa_reservaId;
	public final AssociationExpression persona_id_ingresa_reserva;
	public final IntegerExpression hora_medicaId;
	public final AssociationExpression hora_medica;
	public final TimestampExpression fecha;
	public final StringExpression motivoconsulta;
	public final IntegerExpression rceId;
	public final AssociationExpression rce;
	
	public ReservaCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		pacienteId = new IntegerExpression("paciente.id", this);
		paciente = new AssociationExpression("paciente", this);
		persona_id_ingresa_reservaId = new IntegerExpression("persona_id_ingresa_reserva.id", this);
		persona_id_ingresa_reserva = new AssociationExpression("persona_id_ingresa_reserva", this);
		hora_medicaId = new IntegerExpression("hora_medica.id", this);
		hora_medica = new AssociationExpression("hora_medica", this);
		fecha = new TimestampExpression("fecha", this);
		motivoconsulta = new StringExpression("motivoconsulta", this);
		rceId = new IntegerExpression("rce.id", this);
		rce = new AssociationExpression("rce", this);
	}
	
	public ReservaCriteria(PersistentSession session) {
		this(session.createCriteria(Reserva.class));
	}
	
	public ReservaCriteria() throws PersistentException {
		this(orm.SaludPersistentManager.instance().getSession());
	}
	
	public PacienteCriteria createPacienteCriteria() {
		return new PacienteCriteria(createCriteria("paciente"));
	}
	
	public PersonaCriteria createPersona_id_ingresa_reservaCriteria() {
		return new PersonaCriteria(createCriteria("persona_id_ingresa_reserva"));
	}
	
	public HoramedicaCriteria createHora_medicaCriteria() {
		return new HoramedicaCriteria(createCriteria("hora_medica"));
	}
	
	public RceCriteria createRceCriteria() {
		return new RceCriteria(createCriteria("rce"));
	}
	
	public Reserva uniqueReserva() {
		return (Reserva) super.uniqueResult();
	}
	
	public Reserva[] listReserva() {
		java.util.List list = super.list();
		return (Reserva[]) list.toArray(new Reserva[list.size()]);
	}
}

