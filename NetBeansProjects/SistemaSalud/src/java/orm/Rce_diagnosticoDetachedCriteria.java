/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Rce_diagnosticoDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final IntegerExpression rceId;
	public final AssociationExpression rce;
	public final IntegerExpression diagnosticoId;
	public final AssociationExpression diagnostico;
	
	public Rce_diagnosticoDetachedCriteria() {
		super(orm.Rce_diagnostico.class, orm.Rce_diagnosticoCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		rceId = new IntegerExpression("rce.id", this.getDetachedCriteria());
		rce = new AssociationExpression("rce", this.getDetachedCriteria());
		diagnosticoId = new IntegerExpression("diagnostico.id", this.getDetachedCriteria());
		diagnostico = new AssociationExpression("diagnostico", this.getDetachedCriteria());
	}
	
	public Rce_diagnosticoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.Rce_diagnosticoCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		rceId = new IntegerExpression("rce.id", this.getDetachedCriteria());
		rce = new AssociationExpression("rce", this.getDetachedCriteria());
		diagnosticoId = new IntegerExpression("diagnostico.id", this.getDetachedCriteria());
		diagnostico = new AssociationExpression("diagnostico", this.getDetachedCriteria());
	}
	
	public RceDetachedCriteria createRceCriteria() {
		return new RceDetachedCriteria(createCriteria("rce"));
	}
	
	public DiagnosticoDetachedCriteria createDiagnosticoCriteria() {
		return new DiagnosticoDetachedCriteria(createCriteria("diagnostico"));
	}
	
	public Rce_diagnostico uniqueRce_diagnostico(PersistentSession session) {
		return (Rce_diagnostico) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Rce_diagnostico[] listRce_diagnostico(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Rce_diagnostico[]) list.toArray(new Rce_diagnostico[list.size()]);
	}
}

