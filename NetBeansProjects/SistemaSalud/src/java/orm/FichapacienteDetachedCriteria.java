/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class FichapacienteDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final IntegerExpression pacienteId;
	public final AssociationExpression paciente;
	public final StringExpression codigo;
	
	public FichapacienteDetachedCriteria() {
		super(orm.Fichapaciente.class, orm.FichapacienteCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		pacienteId = new IntegerExpression("paciente.id", this.getDetachedCriteria());
		paciente = new AssociationExpression("paciente", this.getDetachedCriteria());
		codigo = new StringExpression("codigo", this.getDetachedCriteria());
	}
	
	public FichapacienteDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.FichapacienteCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		pacienteId = new IntegerExpression("paciente.id", this.getDetachedCriteria());
		paciente = new AssociationExpression("paciente", this.getDetachedCriteria());
		codigo = new StringExpression("codigo", this.getDetachedCriteria());
	}
	
	public PacienteDetachedCriteria createPacienteCriteria() {
		return new PacienteDetachedCriteria(createCriteria("paciente"));
	}
	
	public Fichapaciente uniqueFichapaciente(PersistentSession session) {
		return (Fichapaciente) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Fichapaciente[] listFichapaciente(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Fichapaciente[]) list.toArray(new Fichapaciente[list.size()]);
	}
}

