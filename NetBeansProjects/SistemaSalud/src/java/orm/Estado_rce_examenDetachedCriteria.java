/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Estado_rce_examenDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final StringExpression descripcion;
	public final CollectionExpression bitacora;
	
	public Estado_rce_examenDetachedCriteria() {
		super(orm.Estado_rce_examen.class, orm.Estado_rce_examenCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		descripcion = new StringExpression("descripcion", this.getDetachedCriteria());
		bitacora = new CollectionExpression("ORM_Bitacora", this.getDetachedCriteria());
	}
	
	public Estado_rce_examenDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.Estado_rce_examenCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		descripcion = new StringExpression("descripcion", this.getDetachedCriteria());
		bitacora = new CollectionExpression("ORM_Bitacora", this.getDetachedCriteria());
	}
	
	public BitacoraDetachedCriteria createBitacoraCriteria() {
		return new BitacoraDetachedCriteria(createCriteria("ORM_Bitacora"));
	}
	
	public Estado_rce_examen uniqueEstado_rce_examen(PersistentSession session) {
		return (Estado_rce_examen) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Estado_rce_examen[] listEstado_rce_examen(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Estado_rce_examen[]) list.toArray(new Estado_rce_examen[list.size()]);
	}
}

