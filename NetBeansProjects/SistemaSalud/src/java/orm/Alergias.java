/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Alergias {
	public Alergias() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_ALERGIAS_RCE) {
			this.rce = (orm.Rce) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private String detalle;
	
	private orm.Rce rce;
	
	/**
	 * Clave primaria
	 */
	public void setId(int value) {
		this.id = value;
	}
	
	/**
	 * Clave primaria
	 */
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	/**
	 * Alergia
	 */
	public void setDetalle(String value) {
		this.detalle = value;
	}
	
	/**
	 * Alergia
	 */
	public String getDetalle() {
		return detalle;
	}
	
	public void setRce(orm.Rce value) {
		if (rce != null) {
			rce.alergias.remove(this);
		}
		if (value != null) {
			value.alergias.add(this);
		}
	}
	
	public orm.Rce getRce() {
		return rce;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Rce(orm.Rce value) {
		this.rce = value;
	}
	
	private orm.Rce getORM_Rce() {
		return rce;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
