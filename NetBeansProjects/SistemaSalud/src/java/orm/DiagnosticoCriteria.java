/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class DiagnosticoCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression codigo;
	public final CollectionExpression rce_diagnostico;
	
	public DiagnosticoCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		codigo = new StringExpression("codigo", this);
		rce_diagnostico = new CollectionExpression("ORM_Rce_diagnostico", this);
	}
	
	public DiagnosticoCriteria(PersistentSession session) {
		this(session.createCriteria(Diagnostico.class));
	}
	
	public DiagnosticoCriteria() throws PersistentException {
		this(orm.SaludPersistentManager.instance().getSession());
	}
	
	public Rce_diagnosticoCriteria createRce_diagnosticoCriteria() {
		return new Rce_diagnosticoCriteria(createCriteria("ORM_Rce_diagnostico"));
	}
	
	public Diagnostico uniqueDiagnostico() {
		return (Diagnostico) super.uniqueResult();
	}
	
	public Diagnostico[] listDiagnostico() {
		java.util.List list = super.list();
		return (Diagnostico[]) list.toArray(new Diagnostico[list.size()]);
	}
}

