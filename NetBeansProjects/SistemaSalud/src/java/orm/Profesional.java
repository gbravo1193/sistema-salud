/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Profesional {
	public Profesional() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_PROFESIONAL_ESPECIALIDAD) {
			return ORM_especialidad;
		}
		else if (key == orm.ORMConstants.KEY_PROFESIONAL_HORAMEDICA) {
			return ORM_horamedica;
		}
		
		return null;
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_PROFESIONAL_PERSONA) {
			this.persona = (orm.Persona) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private orm.Persona persona;
	
	private Integer activo;
	
	private java.util.Set ORM_especialidad = new java.util.HashSet();
	
	private java.util.Set ORM_horamedica = new java.util.HashSet();
	
	public void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	/**
	 * Indica esta activo
	 */
	public void setActivo(int value) {
		setActivo(new Integer(value));
	}
	
	/**
	 * Indica esta activo
	 */
	public void setActivo(Integer value) {
		this.activo = value;
	}
	
	/**
	 * Indica esta activo
	 */
	public Integer getActivo() {
		return activo;
	}
	
	private void setORM_Especialidad(java.util.Set value) {
		this.ORM_especialidad = value;
	}
	
	private java.util.Set getORM_Especialidad() {
		return ORM_especialidad;
	}
	
	public final orm.EspecialidadSetCollection especialidad = new orm.EspecialidadSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_PROFESIONAL_ESPECIALIDAD, orm.ORMConstants.KEY_ESPECIALIDAD_PROFESIONAL, orm.ORMConstants.KEY_MUL_MANY_TO_MANY);
	
	public void setPersona(orm.Persona value) {
		if (this.persona != value) {
			orm.Persona lpersona = this.persona;
			this.persona = value;
			if (value != null) {
				persona.setProfesional(this);
			}
			if (lpersona != null && lpersona.getProfesional() == this) {
				lpersona.setProfesional(null);
			}
		}
	}
	
	public orm.Persona getPersona() {
		return persona;
	}
	
	private void setORM_Horamedica(java.util.Set value) {
		this.ORM_horamedica = value;
	}
	
	private java.util.Set getORM_Horamedica() {
		return ORM_horamedica;
	}
	
	public final orm.HoramedicaSetCollection horamedica = new orm.HoramedicaSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_PROFESIONAL_HORAMEDICA, orm.ORMConstants.KEY_HORAMEDICA_PROFESIONAL, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
