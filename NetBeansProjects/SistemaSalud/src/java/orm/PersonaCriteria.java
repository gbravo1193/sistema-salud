/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class PersonaCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression rut;
	public final StringExpression nombres;
	public final StringExpression apellido_paterno;
	public final StringExpression apellido_materno;
	public final StringExpression email;
	public final DateExpression fecha_nacimiento;
	public final StringExpression identificadorfacebook;
	public final IntegerExpression activo;
	public final StringExpression password;
	public final IntegerExpression profesionalId;
	public final AssociationExpression profesional;
	public final IntegerExpression pacienteId;
	public final AssociationExpression paciente;
	public final IntegerExpression administradorId;
	public final AssociationExpression administrador;
	public final CollectionExpression reserva;
	public final CollectionExpression consulta_hce;
	public final CollectionExpression consulta_hce1;
	public final CollectionExpression rce;
	public final CollectionExpression bitacora;
	
	public PersonaCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		rut = new StringExpression("rut", this);
		nombres = new StringExpression("nombres", this);
		apellido_paterno = new StringExpression("apellido_paterno", this);
		apellido_materno = new StringExpression("apellido_materno", this);
		email = new StringExpression("email", this);
		fecha_nacimiento = new DateExpression("fecha_nacimiento", this);
		identificadorfacebook = new StringExpression("identificadorfacebook", this);
		activo = new IntegerExpression("activo", this);
		password = new StringExpression("password", this);
		profesionalId = new IntegerExpression("profesional.id", this);
		profesional = new AssociationExpression("profesional", this);
		pacienteId = new IntegerExpression("paciente.id", this);
		paciente = new AssociationExpression("paciente", this);
		administradorId = new IntegerExpression("administrador.id", this);
		administrador = new AssociationExpression("administrador", this);
		reserva = new CollectionExpression("ORM_Reserva", this);
		consulta_hce = new CollectionExpression("ORM_Consulta_hce", this);
		consulta_hce1 = new CollectionExpression("ORM_Consulta_hce1", this);
		rce = new CollectionExpression("ORM_Rce", this);
		bitacora = new CollectionExpression("ORM_Bitacora", this);
	}
	
	public PersonaCriteria(PersistentSession session) {
		this(session.createCriteria(Persona.class));
	}
	
	public PersonaCriteria() throws PersistentException {
		this(orm.SaludPersistentManager.instance().getSession());
	}
	
	public ProfesionalCriteria createProfesionalCriteria() {
		return new ProfesionalCriteria(createCriteria("profesional"));
	}
	
	public PacienteCriteria createPacienteCriteria() {
		return new PacienteCriteria(createCriteria("paciente"));
	}
	
	public AdministradorCriteria createAdministradorCriteria() {
		return new AdministradorCriteria(createCriteria("administrador"));
	}
	
	public ReservaCriteria createReservaCriteria() {
		return new ReservaCriteria(createCriteria("ORM_Reserva"));
	}
	
	public Consulta_hceCriteria createConsulta_hceCriteria() {
		return new Consulta_hceCriteria(createCriteria("ORM_Consulta_hce"));
	}
	
	public Consulta_hceCriteria createConsulta_hce1Criteria() {
		return new Consulta_hceCriteria(createCriteria("ORM_Consulta_hce1"));
	}
	
	public RceCriteria createRceCriteria() {
		return new RceCriteria(createCriteria("ORM_Rce"));
	}
	
	public BitacoraCriteria createBitacoraCriteria() {
		return new BitacoraCriteria(createCriteria("ORM_Bitacora"));
	}
	
	public Persona uniquePersona() {
		return (Persona) super.uniqueResult();
	}
	
	public Persona[] listPersona() {
		java.util.List list = super.list();
		return (Persona[]) list.toArray(new Persona[list.size()]);
	}
}

