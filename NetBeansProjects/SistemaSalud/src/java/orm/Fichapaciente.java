/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Fichapaciente {
	public Fichapaciente() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_FICHAPACIENTE_PACIENTE) {
			this.paciente = (orm.Paciente) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private orm.Paciente paciente;
	
	private String codigo;
	
	/**
	 * Clave Primaria
	 */
	public void setId(int value) {
		this.id = value;
	}
	
	/**
	 * Clave Primaria
	 */
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	/**
	 * Codigo Ficha
	 */
	public void setCodigo(String value) {
		this.codigo = value;
	}
	
	/**
	 * Codigo Ficha
	 */
	public String getCodigo() {
		return codigo;
	}
	
	public void setPaciente(orm.Paciente value) {
		if (paciente != null) {
			paciente.fichapaciente.remove(this);
		}
		if (value != null) {
			value.fichapaciente.add(this);
		}
	}
	
	public orm.Paciente getPaciente() {
		return paciente;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Paciente(orm.Paciente value) {
		this.paciente = value;
	}
	
	private orm.Paciente getORM_Paciente() {
		return paciente;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
