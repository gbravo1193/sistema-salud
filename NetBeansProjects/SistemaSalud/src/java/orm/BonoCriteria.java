/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class BonoCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final IntegerExpression pagoId;
	public final AssociationExpression pago;
	public final IntegerExpression tipo_previsionId;
	public final AssociationExpression tipo_prevision;
	public final StringExpression bono;
	
	public BonoCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		pagoId = new IntegerExpression("pago.id", this);
		pago = new AssociationExpression("pago", this);
		tipo_previsionId = new IntegerExpression("tipo_prevision.id", this);
		tipo_prevision = new AssociationExpression("tipo_prevision", this);
		bono = new StringExpression("bono", this);
	}
	
	public BonoCriteria(PersistentSession session) {
		this(session.createCriteria(Bono.class));
	}
	
	public BonoCriteria() throws PersistentException {
		this(orm.SaludPersistentManager.instance().getSession());
	}
	
	public PagoCriteria createPagoCriteria() {
		return new PagoCriteria(createCriteria("pago"));
	}
	
	public Tipo_previsionCriteria createTipo_previsionCriteria() {
		return new Tipo_previsionCriteria(createCriteria("tipo_prevision"));
	}
	
	public Bono uniqueBono() {
		return (Bono) super.uniqueResult();
	}
	
	public Bono[] listBono() {
		java.util.List list = super.list();
		return (Bono[]) list.toArray(new Bono[list.size()]);
	}
}

