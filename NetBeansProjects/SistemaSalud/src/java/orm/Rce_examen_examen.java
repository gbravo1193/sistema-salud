/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Rce_examen_examen {
	public Rce_examen_examen() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_RCE_EXAMEN_EXAMEN_RCE) {
			this.rce = (orm.Rce) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_RCE_EXAMEN_EXAMEN_EXAMEN) {
			this.examen = (orm.Examen) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private orm.Rce rce;
	
	private orm.Examen examen;
	
	private Double monto_a_pagar;
	
	/**
	 * Claver primaria
	 */
	public void setId(int value) {
		this.id = value;
	}
	
	/**
	 * Claver primaria
	 */
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	/**
	 * Monto de Examen
	 */
	public void setMonto_a_pagar(double value) {
		setMonto_a_pagar(new Double(value));
	}
	
	/**
	 * Monto de Examen
	 */
	public void setMonto_a_pagar(Double value) {
		this.monto_a_pagar = value;
	}
	
	/**
	 * Monto de Examen
	 */
	public Double getMonto_a_pagar() {
		return monto_a_pagar;
	}
	
	public void setRce(orm.Rce value) {
		if (rce != null) {
			rce.rce_examen_examen.remove(this);
		}
		if (value != null) {
			value.rce_examen_examen.add(this);
		}
	}
	
	public orm.Rce getRce() {
		return rce;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Rce(orm.Rce value) {
		this.rce = value;
	}
	
	private orm.Rce getORM_Rce() {
		return rce;
	}
	
	public void setExamen(orm.Examen value) {
		if (examen != null) {
			examen.rce_examen_examen.remove(this);
		}
		if (value != null) {
			value.rce_examen_examen.add(this);
		}
	}
	
	public orm.Examen getExamen() {
		return examen;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Examen(orm.Examen value) {
		this.examen = value;
	}
	
	private orm.Examen getORM_Examen() {
		return examen;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
