/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class Tipo_previsionDAO {
	public static Tipo_prevision loadTipo_previsionByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadTipo_previsionByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipo_prevision getTipo_previsionByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return getTipo_previsionByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipo_prevision loadTipo_previsionByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadTipo_previsionByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipo_prevision getTipo_previsionByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return getTipo_previsionByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipo_prevision loadTipo_previsionByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Tipo_prevision) session.load(orm.Tipo_prevision.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipo_prevision getTipo_previsionByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Tipo_prevision) session.get(orm.Tipo_prevision.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipo_prevision loadTipo_previsionByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Tipo_prevision) session.load(orm.Tipo_prevision.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipo_prevision getTipo_previsionByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Tipo_prevision) session.get(orm.Tipo_prevision.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryTipo_prevision(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return queryTipo_prevision(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryTipo_prevision(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return queryTipo_prevision(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipo_prevision[] listTipo_previsionByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return listTipo_previsionByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipo_prevision[] listTipo_previsionByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return listTipo_previsionByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryTipo_prevision(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Tipo_prevision as Tipo_prevision");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryTipo_prevision(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Tipo_prevision as Tipo_prevision");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Tipo_prevision", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipo_prevision[] listTipo_previsionByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryTipo_prevision(session, condition, orderBy);
			return (Tipo_prevision[]) list.toArray(new Tipo_prevision[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipo_prevision[] listTipo_previsionByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryTipo_prevision(session, condition, orderBy, lockMode);
			return (Tipo_prevision[]) list.toArray(new Tipo_prevision[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipo_prevision loadTipo_previsionByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadTipo_previsionByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipo_prevision loadTipo_previsionByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return loadTipo_previsionByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipo_prevision loadTipo_previsionByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Tipo_prevision[] tipo_previsions = listTipo_previsionByQuery(session, condition, orderBy);
		if (tipo_previsions != null && tipo_previsions.length > 0)
			return tipo_previsions[0];
		else
			return null;
	}
	
	public static Tipo_prevision loadTipo_previsionByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Tipo_prevision[] tipo_previsions = listTipo_previsionByQuery(session, condition, orderBy, lockMode);
		if (tipo_previsions != null && tipo_previsions.length > 0)
			return tipo_previsions[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateTipo_previsionByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return iterateTipo_previsionByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateTipo_previsionByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SaludPersistentManager.instance().getSession();
			return iterateTipo_previsionByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateTipo_previsionByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Tipo_prevision as Tipo_prevision");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateTipo_previsionByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Tipo_prevision as Tipo_prevision");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Tipo_prevision", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipo_prevision createTipo_prevision() {
		return new orm.Tipo_prevision();
	}
	
	public static boolean save(orm.Tipo_prevision tipo_prevision) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().saveObject(tipo_prevision);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(orm.Tipo_prevision tipo_prevision) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().deleteObject(tipo_prevision);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Tipo_prevision tipo_prevision)throws PersistentException {
		try {
			orm.Paciente[] lPacientes = tipo_prevision.paciente.toArray();
			for(int i = 0; i < lPacientes.length; i++) {
				lPacientes[i].setTipo_prevision(null);
			}
			orm.Bono[] lBonos = tipo_prevision.bono.toArray();
			for(int i = 0; i < lBonos.length; i++) {
				lBonos[i].setTipo_prevision(null);
			}
			return delete(tipo_prevision);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Tipo_prevision tipo_prevision, org.orm.PersistentSession session)throws PersistentException {
		try {
			orm.Paciente[] lPacientes = tipo_prevision.paciente.toArray();
			for(int i = 0; i < lPacientes.length; i++) {
				lPacientes[i].setTipo_prevision(null);
			}
			orm.Bono[] lBonos = tipo_prevision.bono.toArray();
			for(int i = 0; i < lBonos.length; i++) {
				lBonos[i].setTipo_prevision(null);
			}
			try {
				session.delete(tipo_prevision);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(orm.Tipo_prevision tipo_prevision) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().getSession().refresh(tipo_prevision);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(orm.Tipo_prevision tipo_prevision) throws PersistentException {
		try {
			orm.SaludPersistentManager.instance().getSession().evict(tipo_prevision);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tipo_prevision loadTipo_previsionByCriteria(Tipo_previsionCriteria tipo_previsionCriteria) {
		Tipo_prevision[] tipo_previsions = listTipo_previsionByCriteria(tipo_previsionCriteria);
		if(tipo_previsions == null || tipo_previsions.length == 0) {
			return null;
		}
		return tipo_previsions[0];
	}
	
	public static Tipo_prevision[] listTipo_previsionByCriteria(Tipo_previsionCriteria tipo_previsionCriteria) {
		return tipo_previsionCriteria.listTipo_prevision();
	}
}
