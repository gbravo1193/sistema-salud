/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Consulta_hceCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final IntegerExpression personaid_profesionalId;
	public final AssociationExpression personaid_profesional;
	public final IntegerExpression personaid_pacienteId;
	public final AssociationExpression personaid_paciente;
	public final DateExpression fecha;
	public final CollectionExpression rce;
	
	public Consulta_hceCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		personaid_profesionalId = new IntegerExpression("personaid_profesional.id", this);
		personaid_profesional = new AssociationExpression("personaid_profesional", this);
		personaid_pacienteId = new IntegerExpression("personaid_paciente.id", this);
		personaid_paciente = new AssociationExpression("personaid_paciente", this);
		fecha = new DateExpression("fecha", this);
		rce = new CollectionExpression("ORM_Rce", this);
	}
	
	public Consulta_hceCriteria(PersistentSession session) {
		this(session.createCriteria(Consulta_hce.class));
	}
	
	public Consulta_hceCriteria() throws PersistentException {
		this(orm.SaludPersistentManager.instance().getSession());
	}
	
	public PersonaCriteria createPersonaid_profesionalCriteria() {
		return new PersonaCriteria(createCriteria("personaid_profesional"));
	}
	
	public PersonaCriteria createPersonaid_pacienteCriteria() {
		return new PersonaCriteria(createCriteria("personaid_paciente"));
	}
	
	public RceCriteria createRceCriteria() {
		return new RceCriteria(createCriteria("ORM_Rce"));
	}
	
	public Consulta_hce uniqueConsulta_hce() {
		return (Consulta_hce) super.uniqueResult();
	}
	
	public Consulta_hce[] listConsulta_hce() {
		java.util.List list = super.list();
		return (Consulta_hce[]) list.toArray(new Consulta_hce[list.size()]);
	}
}

