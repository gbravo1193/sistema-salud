/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Especialidad {
	public Especialidad() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_ESPECIALIDAD_PROFESIONAL) {
			return ORM_profesional;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private int id;
	
	private String especialidad;
	
	private java.util.Set ORM_profesional = new java.util.HashSet();
	
	/**
	 * Clave Primaria
	 */
	public void setId(int value) {
		this.id = value;
	}
	
	/**
	 * Clave Primaria
	 */
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	/**
	 * Especialidad Medico
	 */
	public void setEspecialidad(String value) {
		this.especialidad = value;
	}
	
	/**
	 * Especialidad Medico
	 */
	public String getEspecialidad() {
		return especialidad;
	}
	
	private void setORM_Profesional(java.util.Set value) {
		this.ORM_profesional = value;
	}
	
	public java.util.Set getORM_Profesional() {
		return ORM_profesional;
	}
	
	public final orm.ProfesionalSetCollection profesional = new orm.ProfesionalSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_ESPECIALIDAD_PROFESIONAL, orm.ORMConstants.KEY_PROFESIONAL_ESPECIALIDAD, orm.ORMConstants.KEY_MUL_MANY_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
