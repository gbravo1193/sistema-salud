/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Bono {
	public Bono() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_BONO_PAGO) {
			this.pago = (orm.Pago) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_BONO_TIPO_PREVISION) {
			this.tipo_prevision = (orm.Tipo_prevision) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private orm.Pago pago;
	
	private orm.Tipo_prevision tipo_prevision;
	
	private String bono;
	
	/**
	 * Clave Primaria
	 */
	public void setId(int value) {
		this.id = value;
	}
	
	/**
	 * Clave Primaria
	 */
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	/**
	 * Numero de Bono
	 */
	public void setBono(String value) {
		this.bono = value;
	}
	
	/**
	 * Numero de Bono
	 */
	public String getBono() {
		return bono;
	}
	
	public void setPago(orm.Pago value) {
		if (pago != null) {
			pago.bono.remove(this);
		}
		if (value != null) {
			value.bono.add(this);
		}
	}
	
	public orm.Pago getPago() {
		return pago;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Pago(orm.Pago value) {
		this.pago = value;
	}
	
	private orm.Pago getORM_Pago() {
		return pago;
	}
	
	public void setTipo_prevision(orm.Tipo_prevision value) {
		if (tipo_prevision != null) {
			tipo_prevision.bono.remove(this);
		}
		if (value != null) {
			value.bono.add(this);
		}
	}
	
	public orm.Tipo_prevision getTipo_prevision() {
		return tipo_prevision;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Tipo_prevision(orm.Tipo_prevision value) {
		this.tipo_prevision = value;
	}
	
	private orm.Tipo_prevision getORM_Tipo_prevision() {
		return tipo_prevision;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
